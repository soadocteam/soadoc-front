<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:us="us"
                xmlns:s='uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882'
                xmlns:dt='uuid:C2F41010-65B3-11d1-A29F-00AA00C14882'
                xmlns:rs='urn:schemas-microsoft-com:rowset'
                xmlns:z='#RowsetSchema'
                xmlns:MSEvent="http://schemas.microsoft.com/win/2004/08/events/event">
  <!-- Version 7.1.04 Updated 03/23/2007 -PHofle
          Alt-Text data in fields displayed.
          More TYPE colors added.
          Fixed issue with empty character values defaulting to '0'

     Version 8.1.0 Updated 08/18/2011 -PHofle/SKozlov
          Enhanced utility getChar() for use with wide characters


      ESTA ES LA TRANFORMACIÓN UTILIZADA PARA TODOS LOS NAVEGADORES A EXCEPCIÓN DE FIREFOX

-->
  <msxsl:script language="VBScript" implements-prefix="us">
    <![CDATA[

Function getChar(sVal)
	nVal=0
  If Instr(sVal,",")>0 then
    sVal = trim(left(sVal,Instr(sVal,",")))
  End if
  If IsNumeric(sVal) Then
    nVal=CLng(sVal)
    getChar=ChrW(nVal)
  Else
    getChar=""
  End if
End Function

Function getConf(sVal)
	nVal=0
  If Instr(sVal,",")>0 then
    sVal = left(sVal,Instr(sVal,","))
  end if
	If IsNumeric(sVal) Then	nVal=CLng(sVal)-1
	getConf=CStr(nVal)
End Function

Function getAllText(sText,sConf)
  Dim aText
  Dim aConf

  getAllText=""

  aText = array(sText&",",",")
  aConf = array(sConf&",",",")

  getAllText=cstr(Ubound(aText))

End Function

]]>    </msxsl:script>


  <xsl:template match="/">
    <HTML>
      <HEAD>
        <STYLE>
          BULLNOSE
          {
          }
          TD
          {
          BACKGROUND-COLOR: white;
          BORDER-BOTTOM: thin;
          BORDER-LEFT: thin;
          BORDER-RIGHT: thin;
          BORDER-TOP: thin
          }
          TH
          {
          BACKGROUND-COLOR: lightblue;
          COLOR: darkblue
          }
          BODY
          {
          BACKGROUND-COLOR: white;
          FONT-FAMILY: Courier New;
          }
          .MainTable
          {
          BACKGROUND-COLOR: white;
          BORDER-BOTTOM: thin;
          BORDER-LEFT: thin;
          BORDER-RIGHT: thin;
          BORDER-TOP: thin;
          WIDTH: 100%
          }
          .NarrowTable
          {
          BACKGROUND-COLOR: lightsteelblue;
          BORDER-BOTTOM: thin;
          BORDER-LEFT: thin;
          BORDER-RIGHT: thin;
          BORDER-TOP: thin;
          WIDTH: 70%
          }
          .SubTable
          {
          WIDTH: 100%
          }
          .TDFiller
          {
          COLOR: lightcyan;
          BACKGROUND-COLOR: lightcyan
          }
          .Remark
          {
          BACKGROUND-COLOR: white;
          COLOR: black
          }
          .Blue
          {
          BACKGROUND-COLOR: blue;
          COLOR: white
          }
          .Orange
          {
          BACKGROUND-COLOR: orange;
          COLOR: black
          }
          .Green
          {
          BACKGROUND-COLOR: lightgreen;
          COLOR: black
          }
          .Value
          {
          BACKGROUND-COLOR: lightyellow;
          COLOR: black
          }
          .Plain
          {
          COLOR: maroon
          }
          .General
          {
          BACKGROUND-COLOR: white;
          FONT-WEIGHT: bolder;
          COLOR: steelblue
          }
          .Black
          {
          BACKGROUND-COLOR: black;
          COLOR: white
          }
          .Number
          {
          TEXT-ALIGN: right
          }
        </STYLE>
      </HEAD>
      <BODY>
        <TABLE>
          <xsl:apply-templates select="*"/>
        </TABLE>
      </BODY>
    </HTML>
  </xsl:template>


  <!-- DE AQUÍ EN ADELANTE SE AGREGAN LOS NODOS HIJOS QUE TENGA EL XML, LA FUNCIÓN ES IMPRIMIR EL NOMBRE DEL TAG Y SU VALOR -->

  <xsl:template match="CABECERA/*">
    <TR>
      <TD class="TDFiller"></TD>
      <TD>
        <FONT class="Remark">
          <xsl:value-of select="name(.)"/>
          <xsl:apply-templates select="@*"/>
          <xsl:text> : </xsl:text>
        </FONT>
        <A>
          <xsl:if test="name(.)='DATAFILE'">
            <xsl:attribute name="href">
              <xsl:value-of select="."/>
            </xsl:attribute>
          </xsl:if>
          <FONT class="General">
            <xsl:value-of select="."/>
          </FONT>
        </A>
      </TD>
    </TR>
  </xsl:template>

  <xsl:template match="DOCUMENTOS/*">
    <TR>
      <TD class="TDFiller"></TD>
      <TD>
        <FONT class="Remark">
          <xsl:value-of select="name(.)"/>
          <xsl:apply-templates select="@*"/>
          <xsl:text> : </xsl:text>
        </FONT>
        <A>
          <xsl:if test="name(.)='DATAFILE'">
            <xsl:attribute name="href">
              <xsl:value-of select="."/>
            </xsl:attribute>
          </xsl:if>
          <FONT class="General">
            <xsl:value-of select="."/>
          </FONT>
        </A>
      </TD>
    </TR>
  </xsl:template>

  <xsl:template match="DocumentoIndezado/*">
    <TR>
      <TD class="TDFiller"></TD>
      <TD>
        <FONT class="Remark">
          <xsl:value-of select="name(.)"/>
          <xsl:apply-templates select="@*"/>
          <xsl:text> : </xsl:text>
        </FONT>
        <A>
          <xsl:if test="name(.)='DATAFILE'">
            <xsl:attribute name="href">
              <xsl:value-of select="."/>
            </xsl:attribute>
          </xsl:if>
          <FONT class="General">
            <xsl:value-of select="."/>
          </FONT>
        </A>
      </TD>
    </TR>
  </xsl:template>

  <xsl:template match="Evento/*">
    <TR>
      <TD class="TDFiller"></TD>
      <TD>
        <FONT class="Remark">
          <xsl:value-of select="name(.)"/>
          <xsl:apply-templates select="@*"/>
          <xsl:text> : </xsl:text>
        </FONT>
        <A>
          <xsl:if test="name(.)='DATAFILE'">
            <xsl:attribute name="href">
              <xsl:value-of select="."/>
            </xsl:attribute>
          </xsl:if>
          <FONT class="General">
            <xsl:value-of select="."/>
          </FONT>
        </A>
      </TD>
    </TR>
  </xsl:template>

  <xsl:template match="expedienteFoliado/*">
    <TR>
      <TD class="TDFiller"></TD>
      <TD>
        <FONT class="Remark">
          <xsl:value-of select="name(.)"/>
          <xsl:apply-templates select="@*"/>
          <xsl:text> : </xsl:text>
        </FONT>
        <A>
          <xsl:if test="name(.)='DATAFILE'">
            <xsl:attribute name="href">
              <xsl:value-of select="."/>
            </xsl:attribute>
          </xsl:if>
          <FONT class="General">
            <xsl:value-of select="."/>
          </FONT>
        </A>
      </TD>
    </TR>
  </xsl:template>

  <xsl:template match="expedienteFoliadoDTO/*">
    <TR>
      <TD class="TDFiller"></TD>
      <TD>
        <FONT class="Remark">
          <xsl:value-of select="name(.)"/>
          <xsl:apply-templates select="@*"/>
          <xsl:text> : </xsl:text>
        </FONT>
        <A>
          <xsl:if test="name(.)='DATAFILE'">
            <xsl:attribute name="href">
              <xsl:value-of select="."/>
            </xsl:attribute>
          </xsl:if>
          <FONT class="General">
            <xsl:value-of select="."/>
          </FONT>
        </A>
      </TD>
    </TR>
  </xsl:template>

  <xsl:template match="Signature/*">
  <TR>
    <TD class="TDFiller"></TD>
    <TD>
      <FONT class="Remark">
        <xsl:value-of select="name(.)"/>
        <xsl:apply-templates select="@*"/>
        <xsl:text> : </xsl:text>
      </FONT>
      <A>
        <xsl:if test="name(.)='DATAFILE'">
          <xsl:attribute name="href">
            <xsl:value-of select="."/>
          </xsl:attribute>
        </xsl:if>
        <FONT class="General">
          <xsl:value-of select="."/>
        </FONT>
      </A>
    </TD>
  </TR>
</xsl:template>


  <!-- SI EL "elementoDTOS" CONTIENE NODOS HIJOS RECOMIENDO QUITAR LA VALIDACIÓN Y AGREGAR LOS CAMPOS DE ÉL UNO POR UNO EN EL SIGUIENTE TEMPLATE -->

  <xsl:template match="elementoDTOS/*">
    <TR>
      <TD class="TDFiller"></TD>
      <TD>
        <FONT class="Remark">
          <xsl:value-of select="name(.)"/>
          <xsl:apply-templates select="@*"/>
          <xsl:text> : </xsl:text>
        </FONT>
        <A>
          <xsl:if test="name(.)='DATAFILE'">
            <xsl:attribute name="href">
              <xsl:value-of select="."/>
            </xsl:attribute>
          </xsl:if>
          <FONT class="General">
            <xsl:value-of select="."/>
          </FONT>
        </A>
      </TD>
    </TR>
  </xsl:template>

  <!-- EN EL SIGUIENTE TEMPLATE AGREGAR LOS CAMPOS DEL "elementoDTOS" EN CASO DE QUE ESTE TENGA NODOS HIJOS.
       TAMBIÉN AGREGAR LOS DEL "indiceElectronicoDTO" PUESTO QUE AL AGREGAR EL NODO ENTERO NO SE MUESTRA LA INFORMACIÓN DE FORMA CORRECTA
  -->

  <xsl:template match="IdentificadorIndiceElectronico | fechaIndiceContenido | NombreUnidadDocumental | IdUnidadDocumental |
    Dependencia | Serie | SubSerie | fechaCreacionCarpeta | fechaIndiceElectronico | identificadorIndiceElectronico | nombreUnidadDocumental |
    serie | subSerie | idUnidadDocumental | cod | dependencia">
    <TR>
      <TD class="TDFiller"></TD>
      <TD>
        <FONT class="Remark">
          <xsl:value-of select="name(.)"/>
          <xsl:apply-templates select="@*"/>
          <xsl:text> : </xsl:text>
        </FONT>
        <A>
          <xsl:if test="name(.)='DATAFILE'">
            <xsl:attribute name="href">
              <xsl:value-of select="."/>
            </xsl:attribute>
          </xsl:if>
          <FONT class="General">
            <xsl:value-of select="."/>
          </FONT>
        </A>
      </TD>
    </TR>
  </xsl:template>

  <!-- ESTOS SON CAMPOS ADICIONALES QUE SIRVEN PARA LOS ÍNDICES ELECTRÓNICOS ANTIGUOS -->

  <xsl:template match="fechaCreacionDocumento | fechaIncorporacionExpediente | formato | funcionResumen | id | nombreDocumento |
     ordenDocumentoExpediente | origen | paginaFin | paginaInicio | tamano | tipologiaDocumental | valorHuella">
    <TR>
      <TD class="TDFiller"></TD>
      <TD>
        <FONT class="Remark">
          <xsl:value-of select="name(.)"/>
          <xsl:apply-templates select="@*"/>
          <xsl:text> : </xsl:text>
        </FONT>
        <A>
          <xsl:if test="name(.)='DATAFILE'">
            <xsl:attribute name="href">
              <xsl:value-of select="."/>
            </xsl:attribute>
          </xsl:if>
          <FONT class="General">
            <xsl:value-of select="."/>
          </FONT>
        </A>
      </TD>
    </TR>
  </xsl:template>

  <xsl:template match="*">
    <!-- EN EL SIGUIENTE IF AGREGAR EL NOMBRE DE LOS NODOS, ALGUNOS REPETIDOS SON PORQUE EL NOMBRE PUEDE VARIAR EN EL XML -->
    <xsl:if
      test="name(.)='elementoDTOS' or name(.)='expedienteFoliado' or name(.)='DocumentoIndezado'  or name(.)='ns3:indiceElectronicoDTO'
      or name(.)='expedienteFoliadoDTO' or name(.)='ns4:indiceElectronicoDTO' or name(.)='Evento' or name(.)='ds:Signature'
      or name(.)='ns2:indiceElectronicoDTO' or name(.)='CABECERA' or name(.)='DOCUMENTOS'">
      <xsl:if test="name(/*)!=name(.)">
                <xsl:text disable-output-escaping="yes"><![CDATA[
			<TR>
				<TD></TD>
				<TD>
		]]>                </xsl:text>

      </xsl:if>
      <TABLE class="MainTable">
        <TR>
          <xsl:choose>
            <xsl:when test="name(.)='elementoDTOS'">
              <TH width="12%" class="Blue">elementoDTOS</TH>
              <TH width="88%" align="left" class="Blue">
              </TH>
            </xsl:when>
            <xsl:when test="name(.)='expedienteFoliado'">
              <TH width="12%" class="Green">expedienteFoliado</TH>
              <TH width="88%" align="left" class="Green">
              </TH>
            </xsl:when>
            <xsl:when test="name(.)='expedienteFoliadoDTO'">
              <TH width="12%" class="Green">expedienteFoliadoDTO</TH>
              <TH width="88%" align="left" class="Green">
              </TH>
            </xsl:when>
            <xsl:when test="name(.)='DocumentoIndizado'">
              <TH width="12%" class="Orange">DocumentoIndezado</TH>
              <TH width="88%" align="left" class="Orange">
              </TH>
            </xsl:when>
            <xsl:when test="name(.)='Evento'">
              <TH width="12%" class="Orange">Evento</TH>
              <TH width="88%" align="left" class="Orange">
              </TH>
            </xsl:when>
            <xsl:when test="name(.)='ns2:indiceElectronicoDTO'">
              <TH width="5%" class="Black">Indice Electrónico</TH>
              <TH width="88%" align="left" class="Black">
                <xsl:value-of select="identificadorIndiceElectronico"/>
              </TH>
            </xsl:when>
            <xsl:when test="name(.)='ns3:indiceElectronicoDTO'">
              <TH width="12%" class="Black">indiceElectronicoDTO</TH>
              <TH width="88%" align="left" class="Black">
                <xsl:value-of select="identificadorIndiceElectronico"/>
              </TH>
            </xsl:when>
            <xsl:when test="name(.)='ns4:indiceElectronicoDTO'">
              <TH width="12%" class="Black">indiceElectronicoDTO</TH>
              <TH width="88%" align="left" class="Black">
                <xsl:value-of select="IdentificadorIndiceElectronico"/>
              </TH>
            </xsl:when>
            <xsl:when test="name(.)='ds:Signature'">
              <TH width="12%" class="Black">SignatureValue</TH>
              <TH width="88%" align="left" class="Black">
              </TH>
            </xsl:when>
            <xsl:when test="name(.)='CABECERA'">
              <TH width="12%" class="Blue">Información</TH>
              <TH width="88%" align="left" class="Blue">
              </TH>
            </xsl:when>
            <xsl:when test="name(.)='DOCUMENTOS'">
              <TH width="12%" class="Orange">Documento</TH>
              <TH width="88%" align="left" class="Orange">
              </TH>
            </xsl:when>
          </xsl:choose>

        </TR>
        <!-- ESTE 'Default' SE PRESENTARÁ EN CASO DE QUE EXISTA UN NODO QUE NO SE ESTÉ VALIDANDO ARRIBA EN EL IF -->
        <xsl:choose>
          <xsl:when test="name(.)='Default'">
            <TR>
              <TD class="TDFiller"></TD>
              <TD>
                <FONT class="Remark">Text value<xsl:text> : </xsl:text>
                </FONT>
                <FONT class="Value">
                  <xsl:apply-templates select="*[name()='C']" mode="text"/>
                </FONT>
              </TD>
            </TR>
            <TR>
              <TD class="TDFiller"></TD>
              <TD>
                <FONT class="Remark">Char confi<xsl:text> : </xsl:text>
                </FONT>
                <FONT class="Value">
                  <xsl:apply-templates select="*[name()='C']" mode="conf"/>
                </FONT>
              </TD>
            </TR>
            <xsl:apply-templates select="*[name()!='C']"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="*"/>
          </xsl:otherwise>
        </xsl:choose>
      </TABLE>
      <xsl:if test="name(/)!=name(.)">
                <xsl:text disable-output-escaping="yes"><![CDATA[
			</TD>
		</TR>
		]]>                </xsl:text>
      </xsl:if>
    </xsl:if>

  </xsl:template>

</xsl:stylesheet>
