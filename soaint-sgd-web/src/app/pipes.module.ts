import {NgModule} from '@angular/core';
import {FuncionarioNombreCompletoPipe} from './shared/pipes/funcionario-nombre-completo.pipe';
import {FuncionarioNombreIdPipe} from './shared/pipes/funcionario-nombre-id.pipe';
import {TipologiaDocumentalPipePipe} from './shared/pipes/tipologia-documental-pipe.pipe';
import {DropdownItemPipe} from './shared/pipes/dropdown-item';
import {PIPES} from './shared/__shared.include';

@NgModule({
  imports: [],
  declarations: [
    ...PIPES,
    FuncionarioNombreCompletoPipe,
    FuncionarioNombreIdPipe,
    TipologiaDocumentalPipePipe,
    DropdownItemPipe,

  ],
  exports: [...PIPES, FuncionarioNombreCompletoPipe,
    FuncionarioNombreIdPipe,
    TipologiaDocumentalPipePipe,
    DropdownItemPipe]
})
export class PipesModule {

}
