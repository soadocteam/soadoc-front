import {ContactDTO} from "../domain/ContactDTO";
import {DATOS_CONTACTO_PRINCIPAL, DATOS_CONTACTO_SECUNDARIO} from "./bussiness-properties/radicacion-properties";
import {isNullOrUndefined, isObject} from "util";

export function transformContactDataArray(contactRequest): Array<ContactDTO> {
  const contactosArray: Array<ContactDTO> = [];
  contactRequest.map((contact) => {
    contactosArray.push({
      contactId: contact.contactId || null,
      cellphone: contact.celular || null,
      city: contact.city ? contact.ciudad : null,
      departmentCode: contact.departamento ? contact.departamento.codigo : null,
      municipalityCode: contact.municipio ? contact.municipio.codigo : null,
      countryCode: contact.pais ? contact.pais.codigo : null,
      zipCode: isObject(contact.zipCode) ? contact.zipCode.value || null : contact.zipCode || null,
      quadrantPrefixCode: contact.quadrantPrefixCode ? contact.quadrantPrefixCode : null,
      roadTypeCode: contact.tipoVia ? contact.tipoVia.codigo : null,
      email: contact.correoEle || null,
      address: contact.address || null,
      extension: contact.extension || null,
      plateNumber: contact.plateNumber || null,
      generatingRoadNum: contact.generatingRoadNum || null,
      principal: contact.principal ? DATOS_CONTACTO_PRINCIPAL : DATOS_CONTACTO_SECUNDARIO,
      provState: contact.provState ? contact.provState : null,
      landline: contact.numeroTel || null,
      contactType: contact.tipoContacto ? contact.tipoContacto.codigo : 'TP-CONOT',
      creationDate: contact.creationDate || null,
      state: contact.state || null,
      personId: contact.personId || null

    })
  });
  return contactosArray;
}

export function convertDateString(inputFormat) {
  function pad(s) {
    return (s < 10) ? '0' + s : s;
  }

  const d = new Date(inputFormat);
  return [pad(d.getFullYear()), pad(d.getMonth() + 1), d.getDate()].join('-');
}

export function convertDateForDelivery(inputFormat) {
  if (isNullOrUndefined(inputFormat)) {
    return null;
  }

  function pad(s) {
    return (s < 10) ? '0' + s : s;
  }

  const d = new Date(inputFormat);
  return [pad(d.getFullYear()), pad(d.getMonth() + 1), d.getDate()].join('/');
}

export function convertDateForDeliverySecundary(inputFormat) {
  function pad(s) {
    return (s < 10) ? '0' + s : s;
  }

  const d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
}

