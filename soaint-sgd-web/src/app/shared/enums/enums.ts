export enum FormContextEnum {
  SAVE,
  CREATE
}
export enum InstrumentoArchivistivoEnum {
  TRD = 0,
  TVD = 1
}
export enum LocationType {
  TOP_LEFT = "TOP_LEFT" ,
  TOP_RIGHT = "TOP_RIGHT",
  BOTTOM_LEFT = "BOTTOM_LEFT" ,
  BOTTOM_RIGHT = "BOTTOM_RIGHT" ,
}
