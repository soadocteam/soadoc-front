import {ROUTES_PATH} from "../../app.route-names";
import {MenuEventDTO} from "../../domain/menu-eventDTO";
import {ClearActiveTaskAction} from "../../infrastructure/state-management/tareasDTO-state/tareasDTO-actions";

export var itemsGestionDocumentos = {};


export const MENU_OPTIONS = [

  {
    id: '1',
    label: 'Consola de Casos',
    visible: true,
    icon: 'dashboard',
    routerLink: ['bussiness/' + ROUTES_PATH.home],
    command: (event: MenuEventDTO) => {
      event.from.store.dispatch(new ClearActiveTaskAction());
    }
  },
  {
    id: '2',
    label: 'Administración',
    visible: true,
    icon: 'settings',
    routerLink: ['bussiness/' + ROUTES_PATH.administracion],
    command: (event: MenuEventDTO) => {
      event.from.store.dispatch(new ClearActiveTaskAction());
    }
  }

];


export let MENU: any = {};


MENU['consulta'] = [
  {
    id: '3',
    label: 'Consulta',
    visible: true,
    icon: 'search',
    routerLink: ['bussiness/' + ROUTES_PATH.consultaDocumentos.url],
    command: (event: MenuEventDTO) => {
      event.from.store.dispatch(new ClearActiveTaskAction());
    }
  },
];


