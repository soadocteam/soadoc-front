import {CorrespondenciaDTO} from "../../domain/correspondenciaDTO";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../infrastructure/redux-store/redux-reducers";
import {
  getAuthenticatedFuncionario,
  getSelectedDependencyGroupFuncionario
} from "../../infrastructure/state-management/funcionarioDTO-state/funcionarioDTO-selectors";
import {DocumentoDTO} from "../../domain/documentoDTO";
import {ReferidoDTO} from "../../domain/referidoDTO";
import {AnexoDTO} from "../../domain/anexoDTO";
import {RadicacionFormInterface} from "../interfaces/data-transformers/radicacionForm.interface";
import {AgenteFactoryDTV, AgentFactoryFixDTV} from "./agentesDTV";
import {DATOS_CONTACTO_PRINCIPAL, DATOS_CONTACTO_SECUNDARIO} from "../bussiness-properties/radicacion-properties";
import {isNullOrUndefined, isObject} from "util";
import {DireccionDTO} from "../../domain/DireccionDTO";
import {AgentPersonDTO} from "../../domain/agentPersonDTO";
import {AgentContactDTO} from "../../domain/AgentContactDTO";

export abstract class RadicacionBase {

  protected date: Date;
  protected d: Date;

  constructor(protected source: RadicacionFormInterface, private _store: Store<RootState>) {

    this.date = new Date();
  }

  getCorrespondencia(): CorrespondenciaDTO {
    const generales = this.source.generales;
    const task = this.source.task;
    this.d = new Date();

    const fechaRadicacion = !isNullOrUndefined(generales.fechaRadicacion) ? new Date(generales.fechaRadicacion) : new Date(this.d.getTime() - (this.d.getTimezoneOffset() * 60000));

    const correspondenciaDto: CorrespondenciaDTO = {
      ideDocumento: generales.ideDocumento,
      descripcion: generales.asunto,
      tiempoRespuesta: generales.tiempoRespuesta,
      codUnidadTiempo: generales.unidadTiempo,
      codMedioRecepcion: generales.medioRecepcion,
      fecRadicado: fechaRadicacion.toISOString(),
      fecDocumento: this.date.toISOString(),
      nroRadicado: generales.nroRadicado,
      codTipoDoc: generales.tipologiaDocumental,
      codTipoCmc: generales.tipoComunicacion,
      ideInstancia: task.idInstanciaProceso,
      reqDistFisica: generales.reqDistFisica ? '1' : '0',
      reqDistElectronica: generales.reqDistElect ? '1' : '0',
      codFuncRadica: null,
      codSede: null,
      codDependencia: null,
      reqDigita: generales.reqDigit,
      codEmpMsj: generales.empresaMensajeria,
      nroGuia: generales.numeroGuia ? generales.numeroGuia : null,
      fecVenGestion: null,
      codEstado: null,
      inicioConteo: generales.inicioConteo || '',
      codClaseEnvio: null,
      codModalidadEnvio: null,
      adjuntarDocumento: generales.adjuntarDocumento ? '1' : '0'
    };

    const radicadoPadre = this.source.radicadoPadre;

    if (!isNullOrUndefined(radicadoPadre))
      correspondenciaDto.radicadoPadre = radicadoPadre;

    this._store.select(getAuthenticatedFuncionario).subscribe(funcionario => {
      correspondenciaDto.codFuncRadica = funcionario.id;
    }).unsubscribe();

    this._store.select(getSelectedDependencyGroupFuncionario).subscribe(dependencia => {
      correspondenciaDto.codSede = dependencia.codSede;
      correspondenciaDto.codDependencia = dependencia.codigo;
    }).unsubscribe();

    return correspondenciaDto;
  }

  getDocumento(): DocumentoDTO {
    const generales = this.source.generales;
    return {
      idePpdDocumento: this.source.generales.idePpdDocumento,
      codTipoDoc: generales.tipologiaDocumental ? generales.tipologiaDocumental : null,
      fecDocumento: this.date.toISOString(),
      asunto: generales.asunto,
      nroFolios: generales.numeroFolio, // 'Numero Folio',
      nroAnexos: this.source.descripcionAnexos.length, // 'Numero anexos',
      codEstDoc: null,
      ideEcm: !isNullOrUndefined(generales.ideEcm) ? generales.ideEcm : null
    };
  }

  getListaReferidos(): Array<ReferidoDTO> {
    let referidosList = [];
    if (!isNullOrUndefined(this.source.task)) {
      if (!isNullOrUndefined(this.source.task.variables)) {
        if (!isNullOrUndefined(this.source.task.variables.numeroRadicado) && this.source.task.variables.numeroRadicado !== '' && this.source.task.nombre !== 'Corregir Radicacion') {
          referidosList.push({
            ideReferido: null,
            nroRadRef: this.source.task.variables.numeroRadicado
          });
        } else if (!isNullOrUndefined(this.source.task.variables.nroRadicado) && this.source.task.variables.nroRadicado !== '' && this.source.task.nombre !== 'Corregir Radicacion') {
          referidosList.push({
            ideReferido: null,
            nroRadRef: this.source.task.variables.nroRadicado
          });
        }
      }
    }
    if (!isNullOrUndefined(this.source.radicadosReferidos))
      this.source.radicadosReferidos.forEach(referido => {
        referidosList.push({
          ideReferido: referido.ideReferido,
          nroRadRef: referido.nombre
        });
      });
    return referidosList;
  }

  getListaAnexos(): AnexoDTO[] {
    let anexoList = [];
    this.source.descripcionAnexos.forEach((anexo: any) => {
      anexoList.push({
        ideAnexo: "1",
        codAnexo: anexo.tipoAnexo ? anexo.tipoAnexo.codigo : (anexo.tipo) ? anexo.tipo.codigo : null,
        descripcion: anexo.descripcion,
        codTipoSoporte: anexo.soporteAnexo ? anexo.soporteAnexo.codigo : anexo.soporte
      });
    });
    return anexoList;
  }

  abstract getAgenteDestinatario(): Array<AgentPersonDTO>;

  abstract getAgenteDestinatarioFix(): Array<AgentPersonDTO>;

  // getRemitente(): AgentDTO {
  getRemitente(): any {

    const tipoComunicacion = this.source.generales.tipoComunicacion;
    return AgenteFactoryDTV.getAgente(tipoComunicacion).getRemitente(this.source.remitente, this.source.contactosSolicitante);
  }

  getRemitenteFix(): any {

    const tipoComunicacion = this.source.generales.tipoComunicacion;
    return AgentFactoryFixDTV.getAgenteFix(tipoComunicacion).getRemitenteFix(this.source.remitente, this.source.contactosSolicitante);
  }

  getAgentId(): any {
    return this.source.remitente.ideAgente;
  }

  getAgentIdRemitente(): any {
    return this.source.remitente.ideAgente;
  }

  getComunicacionOficial(): any {

    return {
      correspondencia: this.getCorrespondencia(),
      agenteList: this.getAgenteDestinatario(),
      ppdDocumentoList: [this.getDocumento()],
      anexoList: this.getListaAnexos(),
      referidoList: this.getListaReferidos(),
    };
  }

  getComunicacionOficialFix(): any {

    return {
      correspondencia: this.getCorrespondencia(),
      agenteList: this.getAgenteDestinatarioFix(),
      ppdDocumentoList: [this.getDocumento()],
      anexoList: this.getListaAnexos(),
      referidoList: this.getListaReferidos(),
    };
  }


  transformContactData(contact): AgentContactDTO {
    const contactos: any = {};
    // contactos.agentContactId = contact.contactId;
    contactos.agentContactId = null;
    contactos.city = contact.ciudad ? contact.ciudad : null;
    contactos.countryCode = contact.pais ? contact.pais.codigo : null;
    contactos.departmentCode = contact.departamento ? contact.departamento.codigo : null;
    contactos.municipalityCode = contact.municipio ? contact.municipio.codigo : null;
    contactos.principal = contact.principal ? DATOS_CONTACTO_PRINCIPAL : DATOS_CONTACTO_SECUNDARIO;
    contactos.provState = contact.provinciaEstado ? contact.provinciaEstado : null;
    contactos.contactType = contact.tipoContacto ? contact.tipoContacto.codigo : null;
    contactos.email = contact.correoEle || null;
    contactos.extension = null;
    contactos.address = contact.direccion || contact.address || null;
    contactos.cellphone = contact.celular || null;
    contactos.landline = contact.numeroTel || null;
    contactos.zipCode = isObject(contact.zipCode) ? contact.zipCode.value || null : contact.zipCode || null;
    contactos.generatingRoadNum = contact.noViaPrincipal || null;
    contactos.plateNumber = contact.placa || null;
    contactos.roadTypeCode = contact.tipoVia ? contact.tipoVia.codigo : null;
    contactos.quadrantPrefixCode = contact.prefijoCuadrante ? contact.prefijoCuadrante.codigo : null;
    return contactos;
  }

  transformContactDataFix(contact): AgentContactDTO {
    const contactos: any = {};
    // contactos.agentContactId = contact.contactId;
    contactos.agentContactId = contact.contactId;
    contactos.city = contact.ciudad ? contact.ciudad : null;
    contactos.countryCode = contact.pais ? contact.pais.codigo : null;
    contactos.departmentCode = contact.departamento ? contact.departamento.codigo : null;
    contactos.municipalityCode = contact.municipio ? contact.municipio.codigo : null;
    contactos.principal = contact.principal ? DATOS_CONTACTO_PRINCIPAL : DATOS_CONTACTO_SECUNDARIO;
    contactos.provState = contact.provinciaEstado ? contact.provinciaEstado : null;
    contactos.contactType = contact.tipoContacto ? contact.tipoContacto.codigo : null;
    contactos.email = contact.correoEle || null;
    contactos.extension = null;
    contactos.address = contact.direccion || contact.address || null;
    contactos.cellphone = contact.celular || null;
    contactos.landline = contact.numeroTel || null;
    contactos.zipCode = isObject(contact.zipCode) ? contact.zipCode.value || null : contact.zipCode || null;
    contactos.generatingRoadNum = contact.noViaPrincipal || null;
    contactos.plateNumber = contact.placa || null;
    contactos.roadTypeCode = contact.tipoVia ? contact.tipoVia.codigo : null;
    contactos.quadrantPrefixCode = contact.prefijoCuadrante ? contact.prefijoCuadrante.codigo : null;
    return contactos;
  }

  /*getDatosContactos(): Array<ContactoDTO> {

    return null;
  }*/

  GetDireccionText(contact): string {
    let direccion: DireccionDTO = {};
    let direccionText = '';
    try {
      direccion = JSON.parse(contact.direccion);
    } catch (e) {
      return direccionText;
    }
    if (direccion) {
      if (direccion.tipoVia) {
        direccionText += direccion.tipoVia.nombre;
      }
      if (direccion.noViaPrincipal) {
        direccionText += ' ' + direccion.noViaPrincipal;
      }
      if (direccion.prefijoCuadrante) {
        direccionText += ' ' + direccion.prefijoCuadrante.nombre;
      }
      if (direccion.bis) {
        direccionText += ' ' + direccion.bis.nombre;
      }
      if (direccion.orientacion) {
        direccionText += ' ' + direccion.orientacion.nombre;
      }
      if (direccion.noVia) {
        direccionText += ' ' + direccion.noVia;
      }
      if (direccion.prefijoCuadrante_se) {
        direccionText += ' ' + direccion.prefijoCuadrante_se.nombre;
      }
      if (direccion.placa) {
        direccionText += ' ' + direccion.placa;
      }
      if (direccion.orientacion_se) {
        direccionText += ' ' + direccion.orientacion_se.nombre;
      }
      if (direccion.complementoTipo) {
        direccionText += ' ' + direccion.complementoTipo.nombre;
      }
      if (direccion.complementoAdicional) {
        direccionText += ' ' + direccion.complementoAdicional;
      }

    }
    return direccionText;
  }

}


