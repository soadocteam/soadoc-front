import {AgentDTO} from '../../domain/agentDTO';
import {
  COMUNICACION_EXTERNA,
  COMUNICACION_EXTERNA_ENVIADA,
  COMUNICACION_INTERNA,
  COMUNICACION_INTERNA_ENVIADA,
  DATOS_CONTACTO_PRINCIPAL, DATOS_CONTACTO_SECUNDARIO,
  DESTINATARIO_EXTERNO,
  DESTINATARIO_PRINCIPAL,
  TIPO_AGENTE_REMITENTE,
  TIPO_REMITENTE_EXTERNO,
  TIPO_REMITENTE_INTERNO
} from '../bussiness-properties/radicacion-properties';
import {PersonDTO} from "../../domain/personDTO";
import {AgentPersonDTO} from "../../domain/agentPersonDTO";
import {ContactDTO} from "../../domain/ContactDTO";
import {AgentContactDTO} from "../../domain/AgentContactDTO";
import {transformContactDataArray} from "../functions";


export interface AgenteDTV {

  getRemitente(remitente: any, contactosRemitente?: any): AgentDTO;
}

export interface AgentFixDTV {

  getRemitenteFix(remitente: any, contactosRemitente?: any): AgentDTO;
}

export class AgenteInternoDTV implements AgenteDTV {
// Salida
  getRemitente(remitente: any, contactosRemitente?: any): AgentPersonDTO {

    return {
      personDTO: null,
      agentContactDTO: null,
      //
      agentId: null,
      remiTypeCode: TIPO_REMITENTE_INTERNO,
      personTypeCode: null,
      name: remitente.dependenciaGrupo ? remitente.dependenciaGrupo.nombre : null,
      businessName: null,
      nit: null,
      courtesyCode: null,
      qualityCode: null,
      idDocTypeCode: null,
      idNumber: remitente.dependenciaGrupo ? remitente.dependenciaGrupo.codigo : null,
      // codSede: remitente.sedeAdministrativa ? remitente.sedeAdministrativa.codigo || remitente.sedeAdministrativa : null,
      // codDependencia: remitente.dependenciaGrupo ? remitente.dependenciaGrupo.codigo || remitente.dependenciaGrupo : null,
      agentTypeCode: TIPO_AGENTE_REMITENTE,
      stateCode: null,
      originalInd: null
      // ideFunci: remitente.funcionarioGrupo.id || remitente.funcionarioGrupo
    };
  }
}

export class AgenteExternoDTV implements AgenteDTV {
//Entrada
  getRemitente(remitente: any, contactosRemitente?: Array<ContactDTO>): PersonDTO {
    return {
      personId: remitente.ideAgente || null,
      // remiTypeCode: TIPO_REMITENTE_EXTERNO,
      name: remitente.nombreApellidos || null,
      idNumber: remitente.nroDocumentoIdentidad,
      idDocTypeCode: remitente.tipoDocumento ? remitente.tipoDocumento.codigo || remitente.tipoDocumento : null,
      personTypeCode: remitente.tipoPersona ? remitente.tipoPersona.codigo || remitente.tipoPersona : null,
      businessName: remitente.businessName || remitente.razonSocial || null,
      nit: remitente.nit || null,
      qualityCode: remitente.actuaCalidad ? remitente.actuaCalidad.codigo || remitente.actuaCalidad : null,
      contactList: transformContactDataArray(contactosRemitente)
    };
  }

}

export class AgenteFactoryDTV {

  static getAgente(tipo: string): AgenteDTV {

    switch (tipo) {

      case COMUNICACION_INTERNA_ENVIADA:
      case COMUNICACION_INTERNA:
      case COMUNICACION_EXTERNA_ENVIADA:
        return new AgenteInternoDTV();
      case COMUNICACION_EXTERNA :
        return new AgenteExternoDTV();
    }

    return null;

  }
}

export class AgenteInternoFixDTV implements AgentFixDTV {
// Salida
  getRemitenteFix(remitente: any, contactosRemitente?: any): AgentPersonDTO {

    return {
      personDTO: null,
      agentContactDTO: null,
      agentId: remitente.ideAgente,
      remiTypeCode: TIPO_REMITENTE_INTERNO,
      personTypeCode: null,
      name: remitente.dependenciaGrupo ? remitente.dependenciaGrupo.nombre : null,
      businessName: null,
      nit: null,
      courtesyCode: null,
      qualityCode: null,
      idDocTypeCode: null,
      idNumber: remitente.dependenciaGrupo ? remitente.dependenciaGrupo.codigo : null,
      agentTypeCode: TIPO_AGENTE_REMITENTE,
      stateCode: null,
      originalInd: null
    };
  }
}


export class AgenteExtFixDTV implements AgentFixDTV {
//Entrada
  getRemitenteFix(remitente: any, contactosRemitente?: Array<ContactDTO>): PersonDTO {
    return {
      personId: remitente.personId || null,
      name: remitente.nombreApellidos || null,
      idNumber: remitente.nroDocumentoIdentidad,
      idDocTypeCode: remitente.tipoDocumento ? remitente.tipoDocumento.codigo || remitente.tipoDocumento : null,
      personTypeCode: remitente.tipoPersona ? remitente.tipoPersona.codigo || remitente.tipoPersona : null,
      businessName: remitente.businessName || remitente.razonSocial || null,
      nit: remitente.nit || null,
      qualityCode: remitente.actuaCalidad ? remitente.actuaCalidad.codigo || remitente.actuaCalidad : null,
      contactList: transformContactDataArray(contactosRemitente)
    };
  }

}

export class AgentFactoryFixDTV {

  static getAgenteFix(tipo: string): AgentFixDTV {

    switch (tipo) {

      case COMUNICACION_INTERNA_ENVIADA:
      case COMUNICACION_INTERNA:
      case COMUNICACION_EXTERNA_ENVIADA:
        return new AgenteInternoFixDTV();
      case COMUNICACION_EXTERNA :
        return new AgenteExtFixDTV();
    }

    return null;

  }
}
