import {ComunicacionOficialDTO} from '../../domain/comunicacionOficialDTO';
import {Observable} from 'rxjs/Observable';
import {AgentDTO} from '../../domain/agentDTO';
import {DocumentoDTO} from '../../domain/documentoDTO';
import {RadicacionEntradaFormInterface} from '../interfaces/data-transformers/radicacionEntradaForm.interface';
import {
  COMUNICACION_INTERNA,
  DATOS_CONTACTO_PRINCIPAL,
  TIPO_AGENTE_DESTINATARIO,
  TIPO_AGENTE_REMITENTE,
  TIPO_REMITENTE_EXTERNO
} from '../bussiness-properties/radicacion-properties';
import {AnexoDTO} from '../../domain/anexoDTO';
import {DireccionDTO} from '../../domain/DireccionDTO';
import {isNullOrUndefined} from "util";
import {of} from 'rxjs/observable/of';
import {AgentContactDTO} from "../../domain/AgentContactDTO";
import {AgentPersonDTO} from "../../domain/agentPersonDTO";

export class RadicacionEntradaDTV {

  constructor(private source: ComunicacionOficialDTO) {

  }

  getDatosRemitente(): Observable<AgentPersonDTO> {
    return of(this.source.agenteList.find(value => value.agentTypeCode === TIPO_AGENTE_REMITENTE));
    // return Observable.of(this.source.agenteList.find(value => value.codTipoRemite === 'EXT'));
  }

  getDatosContactos(): Observable<AgentContactDTO[]> {
    return of(this.getDatosContactoFormList());
  }

  getDatosDocumento(): Observable<DocumentoDTO[]> {
    return of(this.source.ppdDocumentoList);
  }

  getDatosDestinatarios(): Observable<AgentPersonDTO[]> {
    // return Observable.of(this.source.agenteList.filter(value => value.codTipAgent === 'TP-AGEI'));
    return of(this.source.agenteList.filter(value => value.agentTypeCode === TIPO_AGENTE_DESTINATARIO));
  }

  getDatosDestinatarioInmediate(): AgentDTO[] {

    return !isNullOrUndefined(this.source.agenteList) ? this.source.agenteList.filter(value => value.codTipAgent === 'TP-AGEI') : null;
  }

  getRadicacionEntradaFormData(): RadicacionEntradaFormInterface {
    return {
      generales: {
        fechaRadicacion: this.source.correspondencia.fecRadicado,
        nroRadicado: this.source.correspondencia.nroRadicado,
        tipoComunicacion: this.source.correspondencia.codTipoCmc,
        medioRecepcion: this.source.correspondencia.codMedioRecepcion,
        empresaMensajeria: this.source.correspondencia.codEmpMsj,
        numeroGuia: this.source.correspondencia.nroGuia,
        tipologiaDocumental: this.source.correspondencia.codTipoDoc,
        unidadTiempo: this.source.correspondencia.codUnidadTiempo,
        numeroFolio: this.source.ppdDocumentoList.length > 0 ? this.source.ppdDocumentoList[0].nroFolios : null,
        inicioConteo: this.source.correspondencia.inicioConteo,
        reqDistFisica: this.source.correspondencia.reqDistFisica === '1',
        reqDigit: this.source.correspondencia.reqDigita === '1',
        adjuntarDocumento: this.source.correspondencia.adjuntarDocumento === '1',
        reqDigitInmediata: null,
        tiempoRespuesta: this.source.correspondencia.tiempoRespuesta,
        asunto: this.source.ppdDocumentoList[0].asunto,
        radicadoReferido: null,
        tipoAnexos: null,
        tipoAnexosDescripcion: null,
        hasAnexos: null,
        ideDocumento: this.source.correspondencia.ideDocumento,
        idePpdDocumento: this.source.ppdDocumentoList[0].idePpdDocumento,
      },
      datosContactos: this.getDatosContactoFormList(),
      radicadosReferidos: this.getRadicadosReferidosFormList(),
      remitente: this.getRemitenteForm(),
      descripcionAnexos: this.getAnexosFormList(),
      agentesDestinatario: this.getDestinatarioFormList()
    }
  }

  getRadicacionEntradaFixFormData(): RadicacionEntradaFormInterface {
    return {
      generales: {
        fechaRadicacion: this.source.correspondencia.fecRadicado,
        nroRadicado: this.source.correspondencia.nroRadicado,
        tipoComunicacion: this.source.correspondencia.codTipoCmc,
        medioRecepcion: this.source.correspondencia.codMedioRecepcion,
        empresaMensajeria: this.source.correspondencia.codEmpMsj,
        numeroGuia: this.source.correspondencia.nroGuia,
        tipologiaDocumental: this.source.correspondencia.codTipoDoc,
        unidadTiempo: this.source.correspondencia.codUnidadTiempo,
        numeroFolio: this.source.ppdDocumentoList.length > 0 ? this.source.ppdDocumentoList[0].nroFolios : null,
        inicioConteo: this.source.correspondencia.inicioConteo,
        reqDistFisica: this.source.correspondencia.reqDistFisica === '1',
        reqDigit: this.source.correspondencia.reqDigita === '1',
        adjuntarDocumento: this.source.correspondencia.adjuntarDocumento === '1',
        reqDigitInmediata: null,
        tiempoRespuesta: this.source.correspondencia.tiempoRespuesta,
        asunto: this.source.ppdDocumentoList[0].asunto,
        radicadoReferido: null,
        tipoAnexos: null,
        tipoAnexosDescripcion: null,
        hasAnexos: null,
        ideDocumento: this.source.correspondencia.ideDocumento,
        idePpdDocumento: this.source.ppdDocumentoList[0].idePpdDocumento,
      },
      datosContactos: this.getDatosContactoFixFormList(),
      radicadosReferidos: this.getRadicadosReferidosFormList(),
      remitente: this.getRemitenteFixForm(),
      descripcionAnexos: this.getAnexosFormList(),
      agentesDestinatario: this.getDestinatarioFormList()
    }
  }

  getDatosContactoFormList() {
    const contactos = [];
    this.source.agenteList.forEach((value) => {
      if (value.remiTypeCode === TIPO_REMITENTE_EXTERNO) {
        const agentContactDTO = value.agentContactDTO;
        if (!isNullOrUndefined(agentContactDTO)) {
          const direccion: DireccionDTO = this.GetDireccion(agentContactDTO);
          contactos.push({
            tipoVia: (direccion) ? direccion.tipoVia : null,
            noViaPrincipal: agentContactDTO.generatingRoadNum,
            prefijoCuadrante: (direccion) ? direccion.prefijoCuadrante : null,
            bis: (direccion) ? direccion.bis : null,
            orientacion: (direccion) ? direccion.orientacion : null,
            direccion: agentContactDTO.address || null,
            noVia: (direccion) ? direccion.noVia : null,
            prefijoCuadrante_se: (direccion) ? direccion.prefijoCuadrante_se : null,
            placa: agentContactDTO.plateNumber,
            orientacion_se: (direccion) ? direccion.orientacion_se : null,
            complementoTipo: (direccion) ? direccion.complementoTipo : null,
            complementoAdicional: (direccion) ? direccion.complementoAdicional : null,
            celular: agentContactDTO.cellphone,
            numeroTel: agentContactDTO.landline,
            correoEle: agentContactDTO.email,
            pais: {codigo: agentContactDTO.countryCode},
            departamento: {codigo: agentContactDTO.departmentCode},
            municipio: {codigo: agentContactDTO.municipalityCode},
            tipoContacto: {codigo: agentContactDTO.contactType},
            principal: agentContactDTO.principal === DATOS_CONTACTO_PRINCIPAL,
            ciudad: agentContactDTO.city,
            provEstado: agentContactDTO.provState

          });
        }
      }
    });
    return contactos;
  }

  getDatosContactoFixFormList() {
    const contactos = [];
    this.source.agenteList.forEach((value) => {
      if (value.remiTypeCode === TIPO_REMITENTE_EXTERNO) {
        const agentContactDTO = value.agentContactDTO;
        if (!isNullOrUndefined(agentContactDTO)) {
          const direccion: DireccionDTO = this.GetDireccion(agentContactDTO);
          contactos.push({
            contactId: agentContactDTO.agentContactId,
            tipoVia: (direccion) ? direccion.tipoVia : null,
            noViaPrincipal: agentContactDTO.generatingRoadNum,
            prefijoCuadrante: (direccion) ? direccion.prefijoCuadrante : null,
            bis: (direccion) ? direccion.bis : null,
            orientacion: (direccion) ? direccion.orientacion : null,
            direccion: agentContactDTO.address || null,
            noVia: (direccion) ? direccion.noVia : null,
            prefijoCuadrante_se: (direccion) ? direccion.prefijoCuadrante_se : null,
            placa: agentContactDTO.plateNumber,
            orientacion_se: (direccion) ? direccion.orientacion_se : null,
            complementoTipo: (direccion) ? direccion.complementoTipo : null,
            complementoAdicional: (direccion) ? direccion.complementoAdicional : null,
            celular: agentContactDTO.cellphone,
            numeroTel: agentContactDTO.landline,
            correoEle: agentContactDTO.email,
            pais: {codigo: agentContactDTO.countryCode},
            departamento: {codigo: agentContactDTO.departmentCode},
            municipio: {codigo: agentContactDTO.municipalityCode},
            tipoContacto: {codigo: agentContactDTO.contactType},
            principal: agentContactDTO.principal === DATOS_CONTACTO_PRINCIPAL,
            ciudad: agentContactDTO.city,
            provEstado: agentContactDTO.provState

          });
        }
      }
    });
    return contactos;
  }

  getRadicadosReferidosFormList() {
    const referidos = [];
    this.source.referidoList.forEach(referido => {
      referidos.push({ideReferido: referido.ideReferido, nombre: referido.nroRadRef});
    });

    return referidos;
  }

  getAnexosFormList() {
    const anexos = [];
    this.source.anexoList.forEach((anexo: AnexoDTO) => {
      anexos.push({
        ideAnexo: anexo.ideAnexo,
        tipoAnexo: {codigo: anexo.codAnexo},
        soporteAnexo: {codigo: anexo.codTipoSoporte},
        descripcion: anexo.descripcion
      });
    });

    return anexos;
  }

  getRemitenteInternoForm(remitente: AgentDTO) {
    return {
      ideAgente: remitente.ideAgente || remitente.agentId,
      tipoPersona: null,
      nit: null,
      actuaCalidad: null,
      tipoDocumento: null,
      razonSocial: null,
      nombreApellidos: null,
      nroDocumentoIdentidad: null,
      sedeAdministrativa: {codigo: remitente.codSede},
      dependenciaGrupo: {codigo: remitente.codDependencia}
    }
  }

  getRemitenteExternoForm(remitente: AgentDTO) {
    return {
      ideAgente: remitente.ideAgente || remitente.agentId,
      tipoPersona: remitente.codTipoPers || remitente.personTypeCode,
      nit: remitente.nit || remitente.nit,
      actuaCalidad: remitente.codEnCalidad || remitente.qualityCode,
      tipoDocumento: remitente.codTipDocIdent || remitente.idDocTypeCode,
      razonSocial: remitente.razonSocial || remitente.businessName,
      nombreApellidos: remitente.nombre || remitente.name,
      nroDocumentoIdentidad: remitente.nroDocuIdentidad || remitente.idNumber,
      sedeAdministrativa: null,
      dependenciaGrupo: null
    }
  }
  getRemitenteExtFixForm(remitente: AgentDTO) {
    return {
      ideAgente: remitente.ideAgente || remitente.agentId,
      personId: remitente.personId,
      tipoPersona: remitente.codTipoPers || remitente.personTypeCode,
      nit: remitente.nit || remitente.nit,
      actuaCalidad: remitente.codEnCalidad || remitente.qualityCode,
      tipoDocumento: remitente.codTipDocIdent || remitente.idDocTypeCode,
      razonSocial: remitente.razonSocial || remitente.businessName,
      nombreApellidos: remitente.nombre || remitente.name,
      nroDocumentoIdentidad: remitente.nroDocuIdentidad || remitente.idNumber,
      sedeAdministrativa: null,
      dependenciaGrupo: null
    }
  }
  getRemitenteForm() {
    const isRemitenteInterno = this.source.correspondencia.codTipoCmc === COMUNICACION_INTERNA;
    const agente = this.source.agenteList.find((agente: AgentDTO) => (agente.codTipoRemite === TIPO_REMITENTE_EXTERNO || agente.remiTypeCode === TIPO_REMITENTE_EXTERNO));
    if (isRemitenteInterno) {
      return this.getRemitenteInternoForm(agente);
    } else {
      return this.getRemitenteExternoForm(agente);
    }

  }

  getRemitenteFixForm() {
    const isRemitenteInterno = this.source.correspondencia.codTipoCmc === COMUNICACION_INTERNA;
    const agente = this.source.agenteList.find((agente: AgentDTO) => (agente.codTipoRemite === TIPO_REMITENTE_EXTERNO || agente.remiTypeCode === TIPO_REMITENTE_EXTERNO));
    if (isRemitenteInterno) {
      return this.getRemitenteInternoForm(agente);
    } else {
      return this.getRemitenteExtFixForm(agente);
    }

  }

  getDestinatarioFormList() {
    const destinatarios = [];
    this.source.agenteList.filter((agente: AgentDTO) => agente.codTipAgent === TIPO_AGENTE_DESTINATARIO || agente.agentTypeCode === TIPO_AGENTE_DESTINATARIO).forEach((destinatario: AgentDTO) => {
      destinatarios.push({
        ideAgente: destinatario.ideAgente || destinatario.agentId,
        tipoDestinatario: {codigo: destinatario.indOriginal || destinatario.originalInd},
        sedeAdministrativa: {codigo: destinatario.codSede},
        dependenciaGrupo: {codigo: destinatario.codDependencia || destinatario.idNumber}
      });
    });
    return destinatarios;
  }

  GetDireccion(contact: AgentContactDTO): DireccionDTO {
    let direccion: DireccionDTO = {};
    try {
      direccion = JSON.parse(contact.address);
    } catch (e) {
      direccion = null;
    }
    if (direccion) {
      direccion.tipoVia = (direccion.tipoVia) ? direccion.tipoVia : null;
      direccion.noViaPrincipal = (direccion.noViaPrincipal) ? direccion.noViaPrincipal : null;
      direccion.prefijoCuadrante = (direccion.prefijoCuadrante) ? direccion.prefijoCuadrante : null;
      direccion.bis = (direccion.bis) ? direccion.bis : null;
      direccion.orientacion = (direccion.orientacion) ? direccion.orientacion : null;
      direccion.noVia = (direccion.noVia) ? direccion.noVia : null;
      direccion.prefijoCuadrante_se = (direccion.prefijoCuadrante_se) ? direccion.prefijoCuadrante_se : null;
      direccion.placa = (direccion.placa) ? direccion.placa : null;
      direccion.orientacion_se = (direccion.orientacion_se) ? direccion.orientacion_se : null;
      direccion.complementoTipo = (direccion.complementoTipo) ? direccion.complementoTipo : null;
      direccion.complementoAdicional = (direccion.complementoAdicional) ? direccion.complementoAdicional : null;
    }
    return direccion;
  }


}
