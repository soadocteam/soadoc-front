import {ComunicacionOficialDTO} from "../../domain/comunicacionOficialDTO";
import {CorrespondeceRequestDTO} from "../../domain/massive/CorrespondeceRequestDTO";
import {CorrespondenciaDTO} from "../../domain/correspondenciaDTO";
import {AgentPersonDTO} from "../../domain/agentPersonDTO";
import {AgentRequestDTO} from "../../domain/massive/AgentRequestDTO";
import {PersonRequestDTO} from "../../domain/massive/PersonRequestDTO";
import {PersonDTO} from "../../domain/personDTO";
import {ContactRequestDTO} from "../../domain/massive/ContactRequestDTO";
import {ContactDTO} from "../../domain/ContactDTO";
import {PERSONA_JURIDICA} from "../bussiness-properties/radicacion-properties";
import {RequestTrackDTO} from "../../domain/massive/RequestTrackDTO";
import {DocumentRequestDTO} from "../../domain/massive/DocumentRequestDTO";
import {AttachmentsRequestDTO} from "../../domain/massive/AttachmentsRequestDTO";
import {AgentContactDTO} from "../../domain/AgentContactDTO";
import {isNullOrUndefined} from "util";
import {LocationType} from "../enums/enums";

export class RadicacionMasivaDtv {
  constructor(private source: ComunicacionOficialDTO, private formData: FormData, private attributes: { [key: string]: any },
              private locationStamp: LocationType) {
  }

  getRequestTrackDto(templateId): RequestTrackDTO {
    const isSenderReferredRecipient = false;
    return {
      isSenderReferredRecipient,
      correspondence: this.getCorrespondeceRequestDTO(),
      listAgents: this.getlistAgents(),
      documentList: this.getDocumentRequestDTOList(),
      attachmentList: this.getAttachmentsRequestDTOList(),
      signaturesJson: this.formData.get('firmas').toString(),
      templateId: parseInt(templateId),
      userSign: this.formData.get('userSign').toString(),
      passSign: this.formData.get('passSign').toString(),
      commonTagsAttributes: this.getCommonTagsAttributes(),
      locationStamp: this.locationStamp
    }
  }

  getDocumentRequestDTOList(): Array<DocumentRequestDTO> {
    const source = this.source.ppdDocumentoList;
    const result: Array<DocumentRequestDTO> = [];
    source.forEach(pddDoc => {
      result.push({
        documentType: pddDoc.codTipoDoc,
        documentDate: pddDoc.fecDocumento,
        subject: pddDoc.asunto,
        foliosNumber: pddDoc.nroFolios || 0,
        attachmentNumber: pddDoc.nroAnexos || 0,
        ecmId: pddDoc.ideEcm
      })
    })
    return result;
  }

  getCorrespondeceRequestDTO(): CorrespondeceRequestDTO {
    const correspondencia: CorrespondenciaDTO = this.source.correspondencia;
    const fisica = correspondencia.reqDistFisica || "0";
    const electronica = correspondencia.reqDistElectronica || "0";
    const adjuntar = correspondencia.adjuntarDocumento || "0";
    return {
      dateRequest: correspondencia.fecRadicado,
      dateDocument: correspondencia.fecDocumento,
      electronicDistribution: electronica == "1",
      physicDistribution: fisica == "1",
      attachDocument: this.source.ppdDocumentoList.length > 0,
      requestSubsection: correspondencia.codDependencia,
      requestOffice: correspondencia.codSede,
      documentType: correspondencia.codTipoDoc,
      comunicationType: correspondencia.codTipoCmc,
      shippingClass: correspondencia.codClaseEnvio,
      mailType: correspondencia.codModalidadEnvio
    }
  }

  getlistAgents(): Array<AgentRequestDTO> {
    const result: Array<AgentRequestDTO> = [];
    const agenteList: Array<AgentPersonDTO> = this.source.agenteList;
    result.push(this.getRemitente(agenteList[0])); // Remitente
    for (let indexAgent = 1; indexAgent <= (this.source.agenteList.length - 1); indexAgent++) {
      result.push(this.getDestinatario(agenteList[indexAgent])); // Destinatarios
    }
    return result;
  }

  getDestinatario(agent: AgentPersonDTO): AgentRequestDTO {
    let name = agent.name;
    let identificationNumber = agent.idNumber
    if (agent.personTypeCode == PERSONA_JURIDICA) {
      name = agent.businessName;
      identificationNumber = agent.nit;
    }
    return {
      agentType: agent.agentTypeCode,
      identificationNumber: identificationNumber,
      name: name,
      agentId: agent.agentId,
      personType: agent.personTypeCode,
      person: this.getPersonRequestDTO(agent.personDTO, agent.agentContactDTO)
    }
  }

  getPersonRequestDTO(person: PersonDTO, agentContactPrincipal: AgentContactDTO): PersonRequestDTO {
    let name = person.name;
    let identificationNumber = person.idNumber
    if (person.personTypeCode == PERSONA_JURIDICA) {
      name = person.businessName;
      identificationNumber = person.nit;
    }
    return {
      personId: person.personId,
      name: name,
      identificationType: person.idDocTypeCode,
      personType: person.personTypeCode,
      identificationNumber: identificationNumber,
      qualityCode: person.qualityCode,
      contactList: this.getContactRequestDTOList(person.contactList, agentContactPrincipal)
    }
  }

  getContactRequestDTOList(contactList: Array<ContactDTO>, agentContactPrincipal: AgentContactDTO): Array<ContactRequestDTO> {
    const result: Array<ContactRequestDTO> = []
    contactList.forEach(contact => {
      if (!isNullOrUndefined(contact.email)) {
        if (contact.email == agentContactPrincipal.email && contact.contactType == agentContactPrincipal.contactType) {
          result.push({
            contactId: parseInt(contact.contactId),
            city: contact.city,
            country: contact.countryCode,
            department: contact.departmentCode,
            municipality: contact.municipalityCode,
            email: contact.email,
            cellphone: contact.cellphone,
            phone: contact.landline,
            zipcode: contact.zipCode,
            address: contact.address,
            contactType: contact.contactType,
            creationDate: contact.creationDate,
            IsPrincipal: contact.principal == "P"
          });
        }
      } else if (contact.contactType == agentContactPrincipal.contactType) {
        result.push({
          contactId: parseInt(contact.contactId),
          city: contact.city,
          country: contact.countryCode,
          department: contact.departmentCode,
          municipality: contact.municipalityCode,
          email: contact.email,
          cellphone: contact.cellphone,
          phone: contact.landline,
          zipcode: contact.zipCode,
          address: contact.address,
          contactType: contact.contactType,
          creationDate: contact.creationDate,
          IsPrincipal: contact.principal == "P"
        });
      } else {
        result.push({
          contactId: parseInt(contact.contactId),
          city: contact.city,
          country: contact.countryCode,
          department: contact.departmentCode,
          municipality: contact.municipalityCode,
          email: contact.email,
          cellphone: contact.cellphone,
          phone: contact.landline,
          zipcode: contact.zipCode,
          address: contact.address,
          contactType: contact.contactType,
          creationDate: contact.creationDate,
          IsPrincipal: contact.principal == "P"
        });
      }
    });
    if (agentContactPrincipal && result.length) {
      const principal = result.find(contact => contact.contactType == agentContactPrincipal.contactType);
      if (principal) {
        principal.IsPrincipal = true;
      }
    }
    return result;
  }

  getRemitente(remitenteSource: AgentPersonDTO): AgentRequestDTO {
    return {
      agentType: remitenteSource.agentTypeCode,
      identificationNumber: remitenteSource.idNumber,
      name: remitenteSource.name
    };
  }

  private getAttachmentsRequestDTOList(): Array<AttachmentsRequestDTO> {
    let anexoList = this.source.anexoList;
    const result: Array<AttachmentsRequestDTO> = [];
    anexoList.forEach(anexo => {
      result.push({
        attachmentCode: anexo.codAnexo,
        supportTypeCode: anexo.codTipoSoporte,
        description: anexo.descripcion
      })
    })
    return result;
  }

  private getCommonTagsAttributes(): object {
    const attributes = this.attributes || {};
    return {
      senderName: attributes.senderName || "",
      senderEmail: attributes.senderEmail || "",
      senderCharge: attributes.senderCharge || "",
      senderDependence: attributes.senderDependence || "",
      dependencyAddres: attributes.dependencyAddres || "",
      elaboro: attributes.elaboro || "",
      reviso: attributes.reviso || "",
      aprobo: attributes.aprobo || ""
    };
  }
}
