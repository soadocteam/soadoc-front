import {AgentDTO} from '../../domain/agentDTO';
import {ContactoDTO} from '../../domain/contactoDTO';
import {
  COMUNICACION_INTERNA, COMUNICACION_SIN_DISTRIBUIR,
  DATOS_CONTACTO_PRINCIPAL, DATOS_CONTACTO_SECUNDARIO, DESTINATARIO_INTERNO, DESTINATARIO_PRINCIPAL,
  TIPO_AGENTE_DESTINATARIO,
  TIPO_AGENTE_REMITENTE, TIPO_REMITENTE_EXTERNO,
  TIPO_REMITENTE_INTERNO
} from '../bussiness-properties/radicacion-properties';

import {RadicacionBase} from "./radicacionBase";
import {RadicacionEntradaFormInterface} from "../interfaces/data-transformers/radicacionEntradaForm.interface";
import {DireccionDTO} from "../../domain/DireccionDTO";
import {CorrespondenciaDTO} from "../../domain/correspondenciaDTO";
import {RadicacionFormInterface} from "../interfaces/data-transformers/radicacionForm.interface";
import {isNullOrUndefined} from "util";
import {removeDebugNodeFromIndex} from "@angular/core/src/debug/debug_node";
import {AgentPersonDTO} from "../../domain/agentPersonDTO";
import {ComunicacionOficialDTO} from "../../domain/comunicacionOficialDTO";
import {PersonDTO} from "../../domain/personDTO";

export class ComunicacionOficialEntradaDTV extends RadicacionBase {

  getAgenteDestinatario(): Array<AgentPersonDTO> {

    let response: AgentPersonDTO[] = [];
    let requestDestinatario: AgentPersonDTO = {};
    let requestRemitente: AgentPersonDTO = {};
    requestRemitente.personDTO = this.getRemitente();
    requestRemitente.agentContactDTO = this.getDatosContactos();
    //
    requestRemitente.agentId = null;
    requestRemitente.courtesyCode = null;
    requestRemitente.stateCode = null;
    requestRemitente.agentTypeCode = TIPO_AGENTE_REMITENTE;
    requestRemitente.remiTypeCode = TIPO_REMITENTE_EXTERNO;
    requestRemitente.distributionState = null;
    requestRemitente.creationDate = null;
    requestRemitente.originalInd = null;
    requestRemitente.name = requestRemitente.personDTO.name;
    requestRemitente.nit = requestRemitente.personDTO.nit;
    requestRemitente.idNumber = requestRemitente.personDTO.idNumber;
    requestRemitente.idDocTypeCode = requestRemitente.personDTO.idDocTypeCode;
    requestRemitente.personTypeCode = requestRemitente.personDTO.personTypeCode;
    requestRemitente.businessName = requestRemitente.personDTO.businessName;
    requestRemitente.qualityCode = requestRemitente.personDTO.qualityCode;
    requestRemitente.numRedirects = null;
    requestRemitente.numReturns = null;
    requestRemitente.personId = requestRemitente.personDTO.personId;
    requestRemitente.agentContactId = null;
    // const persona: any = this.getRemitente();
    // persona.contactList = this.getDatosContactos();
    response.push(requestRemitente);


    (<RadicacionEntradaFormInterface>this.source).agentesDestinatario.forEach(agenteInt => {

      // const tipoAgente: AgentDTO = {
      requestDestinatario.personDTO = null;
      requestDestinatario.agentContactDTO = null;
      requestDestinatario.courtesyCode = null;
      requestDestinatario.stateCode = null;
      // request.stateCode = agenteInt.sedeAdministrativa ? agenteInt.sedeAdministrativa.codigo : null;
      requestDestinatario.agentTypeCode = TIPO_AGENTE_DESTINATARIO;
      requestDestinatario.remiTypeCode = TIPO_REMITENTE_INTERNO;
      requestDestinatario.distributionState = null;
      requestDestinatario.creationDate = null;
      requestDestinatario.originalInd = agenteInt.tipoDestinatario ? agenteInt.tipoDestinatario.codigo : DESTINATARIO_INTERNO;
      requestDestinatario.nit = null;
      requestDestinatario.idDocTypeCode = null;
      requestDestinatario.personTypeCode = null;
      requestDestinatario.businessName = null;
      requestDestinatario.qualityCode = null;
      requestDestinatario.numRedirects = null;
      requestDestinatario.numReturns = null;
      requestDestinatario.personId = null;
      requestDestinatario.agentContactId = null;
      requestDestinatario.idNumber = agenteInt.dependenciaGrupo ? agenteInt.dependenciaGrupo.codigo : null;
      requestDestinatario.name = agenteInt.dependenciaGrupo ? agenteInt.dependenciaGrupo.nombre : null;
      response.push(requestDestinatario);
      requestDestinatario = {};
    });

    return response;
  }

  getAgenteDestinatarioFix(): Array<AgentPersonDTO> {

    let response: AgentPersonDTO[] = [];
    let requestDestinatario: AgentPersonDTO = {};
    let requestRemitente: AgentPersonDTO = {};

    this.source.eliminarDestinatarios.forEach(agenteInt => {

      requestDestinatario.agentId = agenteInt.ideAgente;
      requestDestinatario.personDTO = null;
      requestDestinatario.agentContactDTO = null;
      requestDestinatario.courtesyCode = null;
      requestDestinatario.stateCode = 'ED';
      requestDestinatario.agentTypeCode = TIPO_AGENTE_DESTINATARIO;
      requestDestinatario.remiTypeCode = null;
      requestDestinatario.distributionState = null;
      requestDestinatario.creationDate = null;
      requestDestinatario.originalInd = null;
      requestDestinatario.nit = null;
      requestDestinatario.idDocTypeCode = null;
      requestDestinatario.personTypeCode = null;
      requestDestinatario.businessName = null;
      requestDestinatario.qualityCode = null;
      requestDestinatario.numRedirects = null;
      requestDestinatario.numReturns = null;
      requestDestinatario.personId = null;
      requestDestinatario.agentContactId = null;
      requestDestinatario.idNumber = null;
      requestDestinatario.name = null;
      response.push(requestDestinatario);
      requestDestinatario = {};
    });

    requestRemitente.personDTO = this.getRemitenteFix();
    requestRemitente.agentContactDTO = this.getDatosContactoFix();
    requestRemitente.courtesyCode = null;
    requestRemitente.stateCode = null;
    requestRemitente.agentId = this.getAgentIdRemitente();
    requestRemitente.agentTypeCode = TIPO_AGENTE_REMITENTE;
    requestRemitente.remiTypeCode = TIPO_REMITENTE_EXTERNO;
    requestRemitente.distributionState = null;
    requestRemitente.creationDate = null;
    requestRemitente.originalInd = null;
    requestRemitente.name = !isNullOrUndefined(requestRemitente.personDTO.name) ? requestRemitente.personDTO.name != '' ? requestRemitente.personDTO.name : 'actualizarAgenteTabla' : 'actualizarAgenteTabla';
    requestRemitente.nit = !isNullOrUndefined(requestRemitente.personDTO.nit) ? requestRemitente.personDTO.nit != '' ? requestRemitente.personDTO.nit : 'actualizarAgenteTabla' : 'actualizarAgenteTabla';
    requestRemitente.idNumber = !isNullOrUndefined(requestRemitente.personDTO.idNumber) ? requestRemitente.personDTO.idNumber != '' ? requestRemitente.personDTO.idNumber : 'actualizarAgenteTabla' : 'actualizarAgenteTabla';
    requestRemitente.idDocTypeCode = !isNullOrUndefined(requestRemitente.personDTO.idDocTypeCode) ? requestRemitente.personDTO.idDocTypeCode: 'actualizarAgenteTabla' ;
    requestRemitente.personTypeCode = requestRemitente.personDTO.personTypeCode;
    requestRemitente.businessName = !isNullOrUndefined(requestRemitente.personDTO.businessName) ? requestRemitente.personDTO.businessName != '' ? requestRemitente.personDTO.businessName : 'actualizarAgenteTabla' : 'actualizarAgenteTabla';
    requestRemitente.qualityCode = !isNullOrUndefined(requestRemitente.personDTO.qualityCode) ? requestRemitente.personDTO.qualityCode != '' ? requestRemitente.personDTO.qualityCode : 'actualizarAgenteTabla' : 'actualizarAgenteTabla';
    requestRemitente.numRedirects = null;
    requestRemitente.numReturns = null;
    requestRemitente.personId = requestRemitente.personDTO.personId;
    requestRemitente.agentContactId = null;
    // const persona: any = this.getRemitente();
    // persona.contactList = this.getDatosContactos();
    response.push(requestRemitente);


    (this.source as RadicacionEntradaFormInterface).agentesDestinatario.forEach(agenteInt => {

      // const tipoAgente: AgentDTO = {
      requestDestinatario.agentId = agenteInt.ideAgente;
      requestDestinatario.personDTO = null;
      requestDestinatario.agentContactDTO = null;
      requestDestinatario.courtesyCode = null;
      requestDestinatario.stateCode = this.source.cambioDestinatarioPrincipal ? 'SA' : null;
      // request.stateCode = agenteInt.sedeAdministrativa ? agenteInt.sedeAdministrativa.codigo : null;
      requestDestinatario.agentTypeCode = TIPO_AGENTE_DESTINATARIO;
      requestDestinatario.remiTypeCode = TIPO_REMITENTE_INTERNO;
      requestDestinatario.distributionState = COMUNICACION_SIN_DISTRIBUIR;
      requestDestinatario.creationDate = null;
      requestDestinatario.originalInd = agenteInt.tipoDestinatario ? agenteInt.tipoDestinatario.codigo : DESTINATARIO_INTERNO;
      requestDestinatario.nit = null;
      requestDestinatario.idDocTypeCode = null;
      requestDestinatario.personTypeCode = null;
      requestDestinatario.businessName = null;
      requestDestinatario.qualityCode = null;
      requestDestinatario.numRedirects = null;
      requestDestinatario.numReturns = null;
      requestDestinatario.personId = null;
      requestDestinatario.agentContactId = null;
      requestDestinatario.idNumber = agenteInt.dependenciaGrupo ? agenteInt.dependenciaGrupo.codigo : null;
      requestDestinatario.name = agenteInt.dependenciaGrupo ? agenteInt.dependenciaGrupo.nombre : null;
      response.push(requestDestinatario);
      requestDestinatario = {};
    });

    return response;
  }


  getDatosContactos(): any {
    return this.transformContactData((<RadicacionEntradaFormInterface>this.source).datosContactos
      ? (<RadicacionEntradaFormInterface>this.source).datosContactos : []);
  }

  getDatosContactoFix(): any {
    return this.transformContactDataFix((<RadicacionEntradaFormInterface>this.source).datosContactos
      ? (<RadicacionEntradaFormInterface>this.source).datosContactos : []);
  }


  isRemitenteInterno() {
    return this.source.generales.tipoComunicacion === COMUNICACION_INTERNA;
  }

}
