import {AgentDTO} from '../../domain/agentDTO';
import {
  DESTINATARIO_COPIA,
  DESTINATARIO_INTERNO,
  TIPO_AGENTE_DESTINATARIO,
  TIPO_REMITENTE_EXTERNO,
  TIPO_REMITENTE_INTERNO
} from '../bussiness-properties/radicacion-properties';
import {RadicacionSalidaFormInterface} from '../interfaces/data-transformers/radicacionSalidaForm.interface';
import {CorrespondenciaDTO} from "../../domain/correspondenciaDTO";
import {isNullOrUndefined, isObject} from "util";
import {ComunicacionOficialDTO} from "../../domain/comunicacionOficialDTO";
import {AgentPersonDTO} from "../../domain/agentPersonDTO";
import {PersonDTO} from "../../domain/personDTO";
import {AgentContactDTO} from "../../domain/AgentContactDTO";
import {transformContactDataArray} from "../functions";
import {RadicacionBase} from "./radicacionBase";

export class RadicacionSalidaDTV extends RadicacionBase {

  hasError: boolean | { error: string } = false;

  getCorrespondencia(): CorrespondenciaDTO {

    const datosEnvio = (<RadicacionSalidaFormInterface>this.source).datosEnvio;

    let correspondencia = super.getCorrespondencia();

    correspondencia.reqDistFisica = this.source.generales.reqDistFisica == 1 ? '1' : '0';

    if (!isNullOrUndefined(datosEnvio)) {
      correspondencia.codClaseEnvio = datosEnvio.clase_envio.codigo;
      correspondencia.codModalidadEnvio = datosEnvio.modalidad_correo.codigo;
    }

    return correspondencia;
  }

  transformarAgentPersonDTO(agent): AgentContactDTO {
    return {
      // agentContactId: agent.contactId || null,
      agentContactId: null,
      cellphone: agent.cellphone || agent.celular || null,
      city: agent.city || null,
      countryCode: isNullOrUndefined(agent.pais) ? null : agent.pais.codigo || null,
      departmentCode: isNullOrUndefined(agent.departamento) ? null : agent.departamento.codigo || null,
      municipalityCode: isNullOrUndefined(agent.municipio) ? null : agent.municipio.codigo || null,
      zipCode: isObject(agent.zipCode) ? agent.zipCode.value || null : agent.zipCode || null,
      quadrantPrefixCode: agent.quadrantPrefixCode || null,
      roadTypeCode: agent.roadTypeCode || null,
      email: agent.correoEle || null,
      address: agent.address || agent.direccion || null,
      extension: agent.extension || null,
      plateNumber: agent.planillas || null,
      generatingRoadNum: agent.generatingRoadNum || null,
      principal: agent.principal || null,
      provState: agent.provState || null,
      landline: agent.numeroTel || null,
      contactType: agent.tipoContacto ? agent.tipoContacto.codigo : 'TP-CONOT'
    };
  }

  validAddress(datosContacto): boolean {
    if (isNullOrUndefined(datosContacto.address))
      return false;
    try {
      const direccion = JSON.parse(datosContacto.address);
      return Object.keys(direccion).length === 0;
    } catch (e) {
      return false;
    }
  }

  getAgenteDestinatario(): Array<AgentPersonDTO> {

    const response: AgentPersonDTO[] = [];
    var agentes: AgentDTO = {};

    agentes = this.getRemitente();
    response.push(agentes);

    (<RadicacionSalidaFormInterface>this.source).destinatarioInterno.forEach(agenteInt => {
      const tipoAgente: AgentDTO = {
        idNumber: agenteInt.dependencia ? agenteInt.dependencia.codigo : null,
        name: agenteInt.dependencia ? agenteInt.dependencia.nombre : null,
        remiTypeCode: TIPO_REMITENTE_INTERNO,
        agentTypeCode: TIPO_AGENTE_DESTINATARIO,
        originalInd: agenteInt.tipoDestinatario ? agenteInt.tipoDestinatario.codigo : DESTINATARIO_INTERNO,
        personId: !isNullOrUndefined(agenteInt.funcionario) ? agenteInt.funcionario.id : null,
        nit: null,
        idDocTypeCode: null,
        personTypeCode: null,
        businessName: null,
        qualityCode: null,
        numRedirects: null,
        numReturns: null,
        agentContactId: null
      };
      response.push(tipoAgente);
    });

    (<RadicacionSalidaFormInterface>this.source).destinatarioExt.forEach((agenteExt: any) => {

      // const datosContactos = this.transformContactData(agenteExt.datosContactoList);
      const datosContactos = agenteExt.direccionSeleccionada;
      const contactoAnonimo = agenteExt.tipoPersona.codigo == 'TP-PERA';

      if (!this.hasError && this.source.generales.reqDistElect && !contactoAnonimo) {

        // if (datosContactos.length === 0 || datosContactos.some(contact => isNullOrUndefined(contact.correoEle)))
        if (Object.keys(datosContactos).length === 0 || isNullOrUndefined(datosContactos.correoEle))
          this.hasError = {error: "Es probable que exista un destinarario externo que no tenga correo."};
      }

      if (!this.hasError && this.source.generales.reqDistFisica && !contactoAnonimo) {

        const hasError = Object.keys(datosContactos).length === 0 || this.validAddress(datosContactos);

        if (hasError)
          this.hasError = {error: "Es probable que exista un destinarario externo sin dirección."}
      }


      const personRemitente: PersonDTO = {
        qualityCode: isNullOrUndefined(agenteExt.actuaCalidad) ? null : agenteExt.actuaCalidad.codigo,
        personTypeCode: agenteExt.tipoPersona.codigo,
        personId: agenteExt.ideAgente,
        nit: agenteExt.nit || null,
        name: agenteExt.nombre,
        idNumber: agenteExt.nroDocumentoIdentidad || null,
        idDocTypeCode: isNullOrUndefined(agenteExt.tipoDocumento) ? null : agenteExt.tipoDocumento.codigo,
        businessName: agenteExt.razonSocial || null,
        contactList: transformContactDataArray(agenteExt.datosContactoList)
      };

      const agentContact = this.transformarAgentPersonDTO(agenteExt.direccionSeleccionada ? agenteExt.direccionSeleccionada : {});

      const agenteRemitente: AgentPersonDTO = {
        personDTO: personRemitente,
        agentContactDTO: agentContact,
        //
        agentId: null,
        personId: agenteExt.ideAgente,
        remiTypeCode: TIPO_REMITENTE_EXTERNO,
        personTypeCode: agenteExt.tipoPersona.codigo,
        name: agenteExt.nombre,
        businessName: agenteExt.razonSocial || null,
        nit: agenteExt.nit || null,
        courtesyCode: null,
        qualityCode: isNullOrUndefined(agenteExt.actuaCalidad) ? null : agenteExt.actuaCalidad.codigo,
        idDocTypeCode: isNullOrUndefined(agenteExt.tipoDocumento) ? null : agenteExt.tipoDocumento.codigo,
        idNumber: agenteExt.nroDocumentoIdentidad || null,
        // codDependencia: remitente.codDependencia,
        creationDate: null,
        agentTypeCode: TIPO_AGENTE_DESTINATARIO,
        stateCode: null,
        originalInd: agenteExt.tipoDestinatario ? agenteExt.tipoDestinatario.codigo : DESTINATARIO_COPIA,
      };
      response.push(agenteRemitente);
    });

    return response;
  }

  getAgenteDestinatarioFix(): Array<AgentPersonDTO> {
    return undefined;
  }

  getComunicacionOficial(): ComunicacionOficialDTO {

    let comunicacion = super.getComunicacionOficial();

    comunicacion.esRemitenteReferidoDestinatario = this.source.generales.responderRemitente;

    return comunicacion;
  }


}
