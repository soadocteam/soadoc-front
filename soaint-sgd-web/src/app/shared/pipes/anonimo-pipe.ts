import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'anonimo'
})
export class AnonimoPipe implements PipeTransform {

  transform(value: any, ...args): any {
    return value.length == 0 ? 'Anonimo' : value;
  }
}
