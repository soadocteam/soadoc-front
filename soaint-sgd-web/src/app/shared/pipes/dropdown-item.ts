import {Pipe, PipeTransform} from '@angular/core';


@Pipe({name: 'dropdownItem'})
export class DropdownItemPipe implements PipeTransform {
    transform(value, args?) {
        // ES6 array destructuring
        if (value) {
            return value.map(item => {
                var words = item.nombre.toLowerCase().split(' ');
                for (var i = 0, len = words.length; i < len; i++)
                    words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
                return {label: words.join(' '), value: item};
            });
        }
    }
}

@Pipe({name: 'dropdownItemFuncFullName'})
export class DropdownItemFuncFullNamePipe implements PipeTransform {
    transform(value, args?) {
        if (value) {
            return value.map(item => {
                let itemsName =  item.nombre + ' ' + (item.valApellido1 ? item.valApellido1 : '') + ' ' + (item.valApellido2 ? item.valApellido2 : '');
                let words = itemsName.toLowerCase().split(' ');
                for (let i = 0, len = words.length; i < len; i++)
                    words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
                return {label: words.join(' '), value: item};
            });
        }
    }
}

@Pipe({name: 'dropdownItemSingle'})
export class DropdownItemPipeSingle implements PipeTransform {
    transform(value, args?) {
        // ES6 array destructuring
        if (value) {
            return value.map(item => {
                return {label: item.nombre, value: item.codigo};
            });
        }
    }
}

@Pipe({name: 'dropdownTemplatesType'})
export class DropdownTemplatesType implements PipeTransform {
    transform(value, args?) {
        // ES6 array destructuring
        if (value) {
            return value.map(item => {
                return {label: item.templateType, value: item.templateId};
            });
        }
    }
}

@Pipe({name: 'dropdownItemFullName'})
export class DropdownItemPipeFullName implements PipeTransform {
    transform(value, args?) {
        // ES6 array destructuring
        if (value) {
            return value.map(item => {
                return {
                    label: item.nombre + ' ' + (item.valApellido1 ? item.valApellido1 : '') + ' ' + (item.valApellido2 ? item.valApellido2 : ''),
                    value: item
                };
            });
        }
    }
}

@Pipe({name: 'dropdownItemSerie'})
export class DropdownItemPipeSerie implements PipeTransform {
    transform(value, args?) {
        // ES6 array destructuring
        if (value) {
            return value.map(item => {
                return {
                    label: item.nombreSerie,
                    value: item.codigoSerie
                };
            });
        }
    }
}

@Pipe({name: 'dropdownItemSerieGenerica'})
export class DropdownItemPipeSerieGenerica implements PipeTransform {
    transform(value, args?) {
        // ES6 array destructuring
        if (value) {
            return value.map(item => {
                return {
                    label: item.nombreSerie,
                    value: item
                };
            });
        }
    }
}

@Pipe({name: 'dropdownItemSubserie'})
export class DropdownItemPipeSubserie implements PipeTransform {
    transform(value, args?) {
        // ES6 array destructuring
        if (value) {
            return value.map(item => {
                return {
                    label: item.nombreSubSerie,
                    value: item.codigoSubSerie
                };
            });
        }
    }
}

@Pipe({name: 'dropdownItemSubserieGenerica'})
export class DropdownItemPipeSubserieGenerica implements PipeTransform {
    transform(value, args?) {
        // ES6 array destructuring
        if (value) {
            return value.map(item => {
                return {
                    label: item.nombreSubSerie,
                    value: item
                };
            });
        }
    }
}

@Pipe({name: 'dropdownItemString'})
export class DropdownItemPipeString implements PipeTransform {
    transform(value, args?) {
        // ES6 array destructuring
        if (value) {
            return value.map(item => {
                return {
                    label: item,
                    value: item
                };
            });
        }
    }
}
