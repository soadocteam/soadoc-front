import {Pipe, PipeTransform} from '@angular/core';
import {isNullOrUndefined} from "util";
import {Utils} from "../helpers";

@Pipe({
  name: 'guidPipe'
})
export class GuidPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    if (isNullOrUndefined(value))
      return value;

    return value.slice(1,-1);
  }

}
