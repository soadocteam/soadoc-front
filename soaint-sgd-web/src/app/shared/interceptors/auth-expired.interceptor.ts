// import {Injector} from '@angular/core';
// import {HttpResponse} from '@angular/common/http';
// import {Observable} from 'rxjs/Observable';
// import {HttpInterceptor} from './http.interceptor';
// import {ErrorHandlerService} from '../../infrastructure/utils/error-handler.service';
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/observable/throw';
// import {catchError} from 'rxjs/operators';
// import {throwError} from 'rxjs';
//
// export class AuthExpiredInterceptor extends HttpInterceptor {
//
//   errorHandlerService: ErrorHandlerService;
//
//   constructor(private injector: Injector) {
//     super();
//     this.errorHandlerService = this.injector.get(ErrorHandlerService);
//   }
//
//   requestIntercept(url, options?: any): any {
//     return options;
//   }
//
//   responseIntercept(url, observable: Observable<HttpResponse<any>>): Observable<HttpResponse<any>> {
//     return observable.pipe(catchError((error, source) => {
//       if (error.status === 401) {
//         return this.errorHandlerService.handleAuthExpiredError(error);
//       }
//       return throwError(error);
//     })
//     ) as Observable<HttpResponse<any>>;
//   }
// }
