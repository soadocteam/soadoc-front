import {Observable} from 'rxjs/Observable';
import {HttpResponse, HttpRequest, HttpHandler, HttpEvent} from '@angular/common/http';
import {finalize, tap} from 'rxjs/operators';

/**
 * A HTTP interceptor responsibility chain member is a class, which may react on request and response of all requests
 * done by HTTP.
 */
export abstract class HttpInterceptor {
  // tslint:disable-next-line:variable-name
  private _successor: HttpInterceptor = null;

  set successor(successor: HttpInterceptor) {
    this._successor = successor;
  }

  processRequestInterception(url, options?: any): any {
    return (!this._successor) ? this.requestIntercept(url, options) :
      this._successor.processRequestInterception(this.requestIntercept(url, options));
  }

  processResponseInterception(url: string | Request, response: Observable<HttpResponse<any>>): Observable<HttpResponse<any>> {
    return (!this._successor) ? this.responseIntercept(url, response) :
      this._successor.processResponseInterception(url, this.responseIntercept(url, response));
  }

  abstract requestIntercept(url, options?: any): any;

  abstract responseIntercept(url, observable: Observable<HttpResponse<any>>): Observable<HttpResponse<any>>;

}
