import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpEvent, HttpHandler, HttpRequest} from '@angular/common/http';
import {finalize, tap} from 'rxjs/operators';
import {LoadingService} from '../../infrastructure/utils/loading.service';
import {LogoutAction} from '../../ui/page-components/login/redux-state/login-actions';
import {PushNotificationAction} from '../../infrastructure/state-management/notifications-state/notifications-actions';
import {BAD_AUTHENTICATION, ERROR_DEFAULT, ERROR_NOT_FOUND} from '../lang/es';
import {Store} from '@ngrx/store';
import {State} from '../../infrastructure/redux-store/redux-reducers';

@Injectable()
export class InterceptableHttp implements IHttpInterceptor {

  private token$: Observable<any>;
  totalRequest = 0;

  constructor(private loadingService: LoadingService, private store: Store<State>) {
    this.token$ = store.select(s => s.auth);
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    this.token$.subscribe(token => {
      if (token.token === null && token.profile !== null) {
        this.store.dispatch(new LogoutAction());
        this.store.dispatch(new PushNotificationAction({
          severity: 'info',
          summary: 'Su sesión ha expirado.'
        }));
      }
    });
    if (req.headers.has('skipLoading')) {
      const newHeaders = req.headers.delete('skipLoading')
      const newRequest = req.clone({headers: newHeaders});
      return next.handle(newRequest)
        .pipe(
          tap(
            event => {
            },
            error => {
              // this.handleAuthExpiredError();
              this.responseIntercept(error);
            },
          ),
          finalize(() => {
          })
        );
    }
    this.totalRequest++;
    return next.handle(req)
      .pipe(
        tap(
          event => {
            this.loadingService.presentLoading();
          },
          error => {
            // this.handleAuthExpiredError();
            this.responseIntercept(error);
          },
        ),
        finalize(() => {
          this.valid();
        })
      );
  }

  valid() {
    this.totalRequest--;
    if (this.totalRequest === 0) {
      this.loadingService.dismissLoading();
    }
  }

  handleAuthExpiredError() {
    this.token$.subscribe(token => {
      if (token !== null) {
        this.store.dispatch(new LogoutAction());
        this.store.dispatch(new PushNotificationAction({
          severity: 'info',
          summary: 'Su sesión ha expirado.'
        }));
      } else {
        this.store.dispatch(new PushNotificationAction({
          severity: 'info',
          summary: BAD_AUTHENTICATION
        }));
      }
    });
  }

  responseIntercept(error: any) {
    if (!(error.status === 401 && (error.text() === '' || (error.path && error.path.indexOf('/api/account') === 0)))) {
      this.handleError(error);
    } else if (error.status === 401) {
      this.store.dispatch(new PushNotificationAction({
        severity: 'info',
        summary: BAD_AUTHENTICATION
      }));
    }
  }

  handleError(error) {
    if (error.status === 0 && error.url.includes('digitalizar-documento-gateway-api')) {
      this.store.dispatch(new PushNotificationAction({
        severity: 'error',
        summary: 'Uno o más documentos han sido eliminados o renombrados.'
      }));
    } else if (error.statusText.includes('Internal Server Error') || error.statusText.includes('Unknown Error')
      || error.statusText.includes('Method Not Found')) {
      this.store.dispatch(new PushNotificationAction({
        severity: 'error',
        summary: ERROR_DEFAULT
      }));
    } else if (error.statusText.includes('Not found')) {
      this.store.dispatch(new PushNotificationAction({
        severity: 'error',
        summary: ERROR_NOT_FOUND
      }));
    } else if (error.status === 400 && error.error.includes('LA UNIDAD DOCUMENTAL YA SE ENCUENTRA CREADA')) {
      this.store.dispatch(new PushNotificationAction({
        severity: 'info',
        summary: 'La unidad documental ya se encuentra creada. Intente de nuevo con un código diferente'
      }));
    } else if (error.status === 403 && error.url.includes('/tareas/reservar')) {
      this.store.dispatch(new PushNotificationAction({
        severity: 'info',
        summary: 'La tarea está siendo gestionada por otro usuario.'
      }));
    } else if (error.url.includes('/adobe-gateway-api/signature')) {
      console.log('Falla firma - signature');
    } else {
      this.store.dispatch(new PushNotificationAction({
        severity: 'error',
        summary: error.statusText
      }));
    }
  }

}

interface IHttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
