import {Observable} from 'rxjs/Observable';
import {HttpResponse} from '@angular/common/http';
import {HttpInterceptor} from './http.interceptor';
import {Injector} from '@angular/core';
import {LoadingService} from '../../infrastructure/utils/loading.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {ObjectHelper} from '../object-extends';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';


export class PendingRequestInterceptor extends HttpInterceptor {

  pendingRequests = 0;
  filteredUrlPatterns: RegExp[] = [];
  loadingService: LoadingService;
  requestQueuee: any[] = [];



  constructor(private injector: Injector) {
    super();
    this.loadingService = this.injector.get(LoadingService);
  }

  private extractFromRequest(request): any {

    return {
      url: request.url,
      method: request.method,
      _body: request._body
    };
  }

  private shouldBypass(request: any): boolean {

    const rq = this.extractFromRequest(request);

    return this.requestQueuee.some(e => {
        return ObjectHelper.similar(rq, e);

    });
  }

  requestIntercept(url, options?: any): any {

    const r = (typeof url === 'object') ? url : Object.assign({url}, options);

    const shouldBypass = this.shouldBypass(r);

    if (!shouldBypass) {

      this.loadingService.getLoaderAsObservable().subscribe( value => {

        if (!value && this.requestQueuee.length > 0) {
           this.requestQueuee = [];
        }
      });

      this.requestQueuee.push(this.extractFromRequest(r));

      if (this.requestQueuee.length == 1) {
       this.loadingService.presentLoading();
     }
    }
    return options;
  }

  responseIntercept(url, observable: Observable<any>): Observable<any> {

    const r = typeof  url == 'object' ? this.extractFromRequest(url) : {url};

    return observable.pipe(map((response: HttpResponse<any>) => {

      const index = this.requestQueuee.findIndex( request => ObjectHelper.similar(r, request));

      this.requestQueuee.splice(index, 1);


      if (this.requestQueuee.length == 0) {
        this.loadingService.dismissLoading();

        localStorage.setItem('lastActivity', new Date().getTime().toString());
      }
      return response;
    }), catchError(error => {

        this.requestQueuee = [];

        this.loadingService.dismissLoading();

        return throwError(error);
      }
    ));
  }
}
