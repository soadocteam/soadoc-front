// import { HttpResponse} from '@angular/common/http';
// import {Observable} from 'rxjs/Observable';
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/observable/throw';
// import {HttpInterceptor} from './http.interceptor';
// import {Injector} from '@angular/core';
// import {ErrorHandlerService} from '../../infrastructure/utils/error-handler.service';
// import {catchError, map} from 'rxjs/operators';
// import {throwError} from "rxjs";
//
// export class ErrorHandlerInterceptor extends HttpInterceptor {
//
//   errorHandlerService: ErrorHandlerService;
//
//   constructor(private injector: Injector) {
//     super();
//     this.errorHandlerService = this.injector.get(ErrorHandlerService)
//   }
//
//   requestIntercept(url, options?: any): any {
//     return options;
//   }
//
//   responseIntercept(url, observable: Observable<HttpResponse<any>>): Observable<HttpResponse<any>> {
//     return <Observable<HttpResponse<any>>> observable.pipe(catchError((error) => {
//       if (!(error.status === 401 && (error.text() === '' ||
//           (error.json().path && error.json().path.indexOf('/api/account') === 0 )))) {
//         this.errorHandlerService.handleError(error);
//       }
//       return throwError(error);
//     })
//     );
//   }
// }
