import {InterceptableHttp} from './interceptale-http';
import {HTTP_INTERCEPTORS, HttpBackend, HttpClient, HttpRequest, HttpXhrBackend} from '@angular/common/http';

export const httpInterceptorProviders = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: InterceptableHttp,
    multi: true
  }
];
