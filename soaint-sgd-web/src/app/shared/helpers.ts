import {isNullOrUndefined} from "util";
import {Observable} from "rxjs";

export class Utils {

  constructor() {
  }

  public static niceRadicado(nroRadicado): string {

    const radicadoParts = nroRadicado.split('--');

    return radicadoParts.length == 2 ? radicadoParts[1] : nroRadicado;
  }

  static funcionarioFunllName(f) {
    return `${f.nombre} ${!isNullOrUndefined(f.valApellido1) ? f.valApellido1 : ''} ${!isNullOrUndefined(f.valApellido2) ? f.valApellido2 : ''}`;
  }

  static sort(list: any[], sortProperty: string): any[] {
    if (!list) return [];
    return list.sort((a, b) => {
      let comparison = 0;
      if (a[sortProperty] > b[sortProperty]) {
        comparison = 1;
      } else if (a[sortProperty] < b[sortProperty]) {
        comparison = -1;
      }
      return comparison;
    });
  }

  static sortObservable(list$: Observable<any[]>, sortProperty: string) {
    return list$.
      map(results => Utils.sort(results, sortProperty));
  }
  static getFileName(name: string): string {
    const lastIndex = name.lastIndexOf('.');
    if (lastIndex > 0) {
      return name.substring(0, lastIndex);
    }
    return '';
  }
  static isValidFileName(name: string) : boolean {
    const regex = new RegExp('^[\\w,\\s-ñÑá-źÁ-Ź]+\\.[A-Za-z0-9]{1,6}$');
    return regex.test(name);
  }

  static isValidFileNameContingencia(name: string) : boolean {
    const regex = new RegExp('^[\\w-ñÑá-źÁ-Ź]+\\.[A-Za-z0-9]{1,6}$');
    return regex.test(name);
  }

  static validateEmail(email: string) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
  }

  static getType(event: string) {
    return event.substr(event.lastIndexOf(".")+1);
  }

  static getNameRDM(){
    let result = '';
    const characters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "u", "v", "w", "x", "y", "z","1","2","3","4","5","6","7","8","9","0"];
    for (let i = 0; i < 5; i++) {
      result += characters[Math.floor(Math.random() * characters.length)];
    }
    return result;
  }

}
