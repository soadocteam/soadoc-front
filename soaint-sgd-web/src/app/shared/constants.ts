export class Constants {
  static readonly DATE_FMT = 'dd/MMM/yyyy';
  static readonly DATE_TIME_FMT = `${Constants.DATE_FMT} hh:mm:ss`;
}

export const COURIER = 'TP-COURIER';
export const ESTADOS_DESCRIPCION = 'DA-DESC';
export const MOTIVO_DEVOLUCION_SALIDA = 'E-ENTS';
export const ICON_CALENDAR = 'fa ui-icon-today';

export const ID_DESPLIEGUE = 'bpm-process-api_1.1-SNAPSHOT';
export const A_TIEMPO = 'A tiempo';
export const PROXIMO_VENCER = 'Proximo a vencer';
export const VENCIDO = 'Vencido';
export const REDIRECCIONADO = 'Redireccionado';
export const REASIGNAR = 'Reasignar';
export const ASIGNAR = 'Asignar';
export const TRAMITAR = 'Tramitar';
export const HIBRIDA = 'Hibrido';
export const ELECTRONICO = 'Electronico';
export const FISICO = 'Físico';
export const ID_DESCRITO = '228';
