import {NgModule, LOCALE_ID, ErrorHandler} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import 'rxjs/add/operator/toPromise';
import {ToastrModule} from 'ngx-toastr';
import {QRCodeModule} from 'angularx-qrcode';
import {RadioButtonModule} from 'primeng/radiobutton';
// APP COMPONENTS
import {AppComponent} from './app.component';
import {
  BUSSINESS_COMPONENTS_PROVIDERS,
  LAYOUT_COMPONENTS,
  LAYOUT_COMPONENTS_PROVIDERS,
  PAGE_COMPONENTS_PROVIDERS
} from './ui/__ui.include';
// APP MODULES
import {LocalStorageModule} from 'angular-2-local-storage';
import {registerLocaleData} from '@angular/common';
import localeEs from '@angular/common/locales/es';
// APP SERVICES
import {API_SERVICES, EFFECTS_MODULES, INFRASTRUCTURE_SERVICES} from './infrastructure/__infrastructure.include';
// Redux Store and Colaterals
import {MetaReducer, StoreModule} from '@ngrx/store';
import {PIPES_AS_PROVIDERS} from './shared/pipes/__pipes.include';
import {UI_STATE_SERVICES} from './infrastructure/_ui-state-service.include';
import {FuncionariosService} from './infrastructure/api/funcionarios.service';
import {reducers, State} from './infrastructure/redux-store/redux-reducers';
import {environment} from './../environments/environment';
import {storeFreeze} from 'ngrx-store-freeze';
import {EffectsModule} from '@ngrx/effects';
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {MatDialogModule} from '@angular/material/dialog';
import {httpInterceptorProviders} from "./shared/interceptors/http.provider";
import {MessageService} from "primeng/api";
import {HOME_ROUTES} from './app.routes';
import {LoginComponent} from './ui/page-components/login/__login.include';
import {
  BlockUIModule,
  BreadcrumbModule,
  CardModule,
  ConfirmationService,
  ConfirmDialogModule,
  DropdownModule,
  InputTextModule,
  ProgressSpinnerModule
} from 'primeng/primeng';
import {Router, RouterModule} from '@angular/router';
import {PipesModule} from './pipes.module';
import {StoreRouterConnectingModule} from "@ngrx/router-store";
import {DialogComponent} from "./shared/dialog/dialog.component";
import {AdDirective} from "./shared/directives/form/ad.directive";
import {NgxExtendedPdfViewerModule} from "ngx-extended-pdf-viewer";
import {DomToImageService} from "./infrastructure/api/dom-to-image";

registerLocaleData(localeEs);
const servicesElastic = (<any>window).servicesElastic;
export const metaReducers: MetaReducer<State>[] = !environment.production ? [storeFreeze] : [];
import {ApmService} from "@elastic/apm-rum-angular";
import { ApmErrorHandler } from '@elastic/apm-rum-angular'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { Angular2CsvModule } from 'angular2-csv';
import {AdministracionComponent} from "./ui/page-components/administracion/administracion.component";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RadioButtonModule,
    ToastrModule.forRoot({
      closeButton: true, // show close button
      timeOut: 4000, // time to live
      enableHtml: true, // allow html in message. (UNSAFE)
      extendedTimeOut: 1000, // time to close after a user hovers over toast
      progressBar: true, // show progress bar
      newestOnTop: true, // new toast placement
      // preventDuplicates: true,
      // toastClass: string = 'toast'; // class on toast
      // positionClass: string = 'toast-top-right'; // class on toast
      // titleClass: string = 'toast-title'; // class inside toast on title
      // messageClass: string = 'toast-message';
    }),
    QRCodeModule,
    // PrimeNG Modules => view components
    // third party libs
    LocalStorageModule.withConfig({
      prefix: 'my-app',
      //  storageType: 'localStorage'
      storageType: 'sessionStorage'
    }),
    StoreModule.forRoot(reducers, {metaReducers}),
    StoreDevtoolsModule.instrument({
      logOnly: environment.production
    }),
    EffectsModule.forRoot(EFFECTS_MODULES),
    MatDialogModule,
    RouterModule.forRoot(HOME_ROUTES),
    StoreRouterConnectingModule.forRoot(),
    ConfirmDialogModule,
    BlockUIModule,
    ProgressSpinnerModule,
    DropdownModule,
    InputTextModule,
    PipesModule,
    BreadcrumbModule,
    CardModule,
    NgxExtendedPdfViewerModule,
    FontAwesomeModule,
    Angular2CsvModule,
  ],
  entryComponents: [],
  declarations: [
    AppComponent,
    LoginComponent,
    DialogComponent,
    AdDirective,
    ...LAYOUT_COMPONENTS,
  ],
  exports: [],
  providers: [
    {provide: ApmService, useClass: ApmService, deps: [Router]},
    {provide: ErrorHandler, useClass: ApmErrorHandler},
    {provide: LOCALE_ID, useValue: 'es'},
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    ...INFRASTRUCTURE_SERVICES,
    ...API_SERVICES,
    ...PAGE_COMPONENTS_PROVIDERS,
    ...LAYOUT_COMPONENTS_PROVIDERS,
    ...BUSSINESS_COMPONENTS_PROVIDERS,
    ...PIPES_AS_PROVIDERS,
    ...UI_STATE_SERVICES,
    ...EFFECTS_MODULES,
    FuncionariosService,
    httpInterceptorProviders,
    DomToImageService,
    MessageService,
    ConfirmationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
