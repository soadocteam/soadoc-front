import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";

@Injectable()
export class ReimpresionEtiquetasApi {

  constructor(private  _api: ApiBase) {
  }


  getNumRadicadosTicket(payload?: any): Observable<any> {
    const endpoint = environment.anularRadicados_endpoint;
    return this._api.list(`${endpoint}?sticker=true`, payload);
  }

}
