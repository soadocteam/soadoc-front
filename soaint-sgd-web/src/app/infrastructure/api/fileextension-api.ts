import {ApiBase} from "./api-base";
import {Observable} from "rxjs";
import {environment} from '../../../environments/environment';
import {Injectable} from "@angular/core";
import {FileValidationDto} from "../../domain/FileValidationDto";
import {MensajeRespuestaDTO} from "../../domain/MensajeRespuestaDTO";
import {getSelectedDependencyGroupFuncionario} from "../state-management/funcionarioDTO-state/funcionarioDTO-selectors";
import {Store} from "@ngrx/store";
import {State as RootState} from "../redux-store/redux-reducers";
import {HttpHeaders} from "@angular/common/http";

@Injectable()
export class FileextensionApi {
  constructor(private _apiBas: ApiBase,
              protected _store: Store<RootState>) {
  }

  recuperarExtension(extension: string, options?): Observable<any> {
      const endpoint  = environment.fileExtensions;
      return this._apiBas.list(`${endpoint}/${extension}`, {}, options);
  }

  static getExtension(fileName: string) : string {
    return fileName.split(".").pop();
  }

  validateDocumentNames(names: string[], longTrackNumber: string, dependencyCode: string): Observable<FileValidationDto[]> {
    const endpoint = `${environment.validate_documents_name}/${longTrackNumber}/${dependencyCode}`;
    return this._apiBas.post(endpoint, names)
      .map((mensajeRespuestaDTO: MensajeRespuestaDTO) => {
        const response = mensajeRespuestaDTO.response || {};
        return response.validations;
      });
  }
  findFolder(communicationType: string): Observable<any> {
    return this._store.select(getSelectedDependencyGroupFuncionario)
      .flatMap(selectedDependency => {
        const endpoint = `${environment.findFolder}/${communicationType}/${selectedDependency.codigo}`;
        let options: any = {
          headers: new HttpHeaders().append('skipLoading', 'true')
        }
        return this._apiBas.list(endpoint, {}, options)
      }).map((mensajeRespuestaDTO: MensajeRespuestaDTO) => {
      return (mensajeRespuestaDTO.response || {}).folderId;
    });
  }
}
