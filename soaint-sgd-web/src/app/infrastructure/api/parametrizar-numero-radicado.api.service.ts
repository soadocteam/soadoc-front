import { Injectable } from '@angular/core';
import {ApiBase} from "./api-base";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ParametrizarNumeroRadicadoApiService {

  constructor(private _api: ApiBase) { }

  getlistaTable(): Observable<any>{
    const endpoint = environment.getParamRadicados;
    return this._api.list(`${endpoint}/trackIds/fields`);
  }

  saveNumRadicado(payload: any): Observable<any>{
    const endpoint = environment.getParamRadicados;
    return this._api.post(`${endpoint}/trackIds`, payload);
  }

  updateNumRadicado(payload: any): Observable<any>{
    const endpoint = environment.getParamRadicados;
    return this._api.put(`${endpoint}/trackIds/${payload.id}`, payload);
  }

  getParamRadicadosbyTipoComunicacion(trackid: string): Observable<any>{
    const endpoint = environment.getParamRadicados;
    return this._api.list(`${endpoint}/trackIds?communication=${trackid}`);
  }



}



