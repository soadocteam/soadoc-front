import {Injectable} from '@angular/core';
import {ApiBase} from './api-base';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {UnidadDocumentalDTO} from '../../domain/unidadDocumentalDTO';
import {DisposicionFinalDTO} from '../../domain/DisposicionFinalDTO';
import {MensajeRespuestaDTO} from '../../domain/MensajeRespuestaDTO';
import {isNullOrUndefined} from "util";
import {of} from 'rxjs/observable/of';
import {map} from 'rxjs/operators';
import {SelectItem} from "primeng/api";
import {State} from "../redux-store/redux-reducers";
import {Store} from "@ngrx/store";
import {HttpParams} from "@angular/common/http";

@Injectable()
export class UnidadDocumentalApiService {

  constructor(private _api: ApiBase, private _store: Store<State>) {

  }


  Listar(payload: UnidadDocumentalDTO): Observable<any> {
    return this._api.post(environment.listar_unidad_documental_endpoint, payload)
      .pipe(map((resp) => {
        if (resp.response) {
          return resp.response
        }
        return [];

      }));
  }

  ListarUnidadesDocumentalesTranferir(payload: any): Observable<any> {
    return this._api.list(`${environment.listar_unidades_documentales_transferir}`, payload)
      .pipe(map((resp) => {
        if (resp.response) {
          return resp.response
        } else {
          return of([]);
        }
      }));
  }

  ListarUnidadesDocumentalesVerificar(payload): Observable<UnidadDocumentalDTO[]> {

    return this._api.list(environment.listar_unidades_documentales_verificar, payload)
      .pipe(map((resp) => {
        if (resp.response) {
          return resp.response.unidadesDocumentales
        } else {
          return of([]);
        }
      }));
  }

  ListarUnidadesDocumentalesUbicar(payload): Observable<UnidadDocumentalDTO[]> {

    return this._api.list(environment.listar_unidades_documentales_confirmadas, payload)
      .pipe(map((resp) => {
        if (resp.response) {
          return resp.response.unidadesDocumentales
        } else {
          return of([]);
        }
      }));
  }

  GetDetalleUnidadDocumental(payload: string): Observable<UnidadDocumentalDTO> {
    return this._api.list(environment.detalle_unidad_documental_endpoint + payload)
      .pipe(map(response => response.response.unidadDocumental));
  }

  crear(unidadDocumental: UnidadDocumentalDTO): Observable<any> {
    return this._api.post(environment.crear_unidad_documental, unidadDocumental);

  }

  gestionarUnidadesDocumentales(payload: any): Observable<MensajeRespuestaDTO> {
    return this._api.post(`${environment.gestionar_unidades_documentales_endpoint}`, payload)
  }

  setStatusTrazability(payload: any[]) {
    return this._api.post(environment.trazabilityPlanilla, payload);
  }

  noTramitarUnidadesDocumentales(payload: any) {

    //return this._api.post("",payload)

    return of(true);

  }

  quickSave(payload: any) {
    return this._api.post(environment.salvarCorrespondenciaEntrada_endpoint, payload);
  }

  listarUnidadesDocumentales(payload: any): Observable<any[]> {
    return this._api.post(environment.listar_unidad_documental_endpoint, payload)
      .pipe(map(response => response.response.unidadDocumental));
  }

  listarUnidadesDocumentalesDisposicion(payload: DisposicionFinalDTO): Observable<UnidadDocumentalDTO[]> {
    const tipificador = payload.tipificador;
    delete payload.tipificador;
    return this._api.list(`${environment.listar_unidades_documentales_disposicion_endpoint}/${payload.unidadDocumentalDTO.codigoDependencia}`, {
      tipificador: tipificador,
      disposicionFinal: JSON.stringify(payload),
      showRechazas: true
    })
      .pipe(map(response => {
        return response.response ? response.response.unidadesDocumentales : of([]);
      }));
  }

  listarUnidDocDescripcionArchivistica(payload: any): Observable<UnidadDocumentalDTO[]> {
    return this._api.list(environment.listar_ud_descripcion_archivistica, payload);
  }

  listarDescripcionesArchivisticas(payload: any): Observable<UnidadDocumentalDTO[]> {
    return this._api.list(environment.getDocumentaryUnitDescription, payload);
  }

  getDocumentaryUnitDescription(idUnidDoc): Observable<UnidadDocumentalDTO[]> {
    return this._api.list(`${environment.getDocumentaryUnitDescription}/${idUnidDoc}`);
  }

  postDocumentaryUnitDescription(payload): Observable<any> {
    return this._api.post(environment.getDocumentaryUnitDescription, payload);
  }

  exportXmlDescription(idUnidDoc): Observable<any> {
    return this._api.list(`${environment.getDocumentaryUnitDescription}/${idUnidDoc}/export`)
  }

  aprobarRechazarUDDisposicion(payload: UnidadDocumentalDTO[], tipificador: number): Observable<MensajeRespuestaDTO> {
    let params: HttpParams = new HttpParams();
    params = params.append('tipificador', tipificador.toString());
    const options = {
      params: params
    };
    return this._api.post(environment.aprobar_rechazar_unidades_documentales_endpoint, payload, options)
      .pipe(map(response => response));
  }

  aprobarRechazarUDAprobarTransferencia(tipoTransferencia: string, payload: UnidadDocumentalDTO[], instrumentoArchivistivo: number): Observable<MensajeRespuestaDTO> {
    return this._api.put(`${environment.aprobar_rechazar_transferencia_documentales}/${tipoTransferencia}?tipificador=${instrumentoArchivistivo}`, payload);

    /* return Observable.of({
       codMensaje: '0000',
       mensaje: 'Operación Completada',
       response: []
     });*/
  }

  aprobarRechazarUDVerificarTransferencia(payload: UnidadDocumentalDTO[]): Observable<MensajeRespuestaDTO> {

    return this._api.put(environment.confirmar_unidaddes_documentales, payload)
  }

  listarDocumentosPorArchivar(codDependencia): Observable<any> {

    return this._api.list(environment.listar_documentos_archivar + codDependencia);
  }

  archivarDocumento(payload: any): Observable<any> {

    return this._api.post(environment.archivar_documento_endpoint, payload);
  }
  tracks(tackDTO: any): Observable<any> {
    return this._api.post(environment.trazabilityPlanilla, tackDTO);
  }

  listarDocumentosArchivadosPorDependencia(codDependencia): Observable<any[]> {

    return this._api.list(environment.listar_documentos_archivados + codDependencia)
      .pipe(map(response => !isNullOrUndefined(response.response) ? response.response.documentos : []));
  }

  listarDocumentosArchivadosPorUd(idEcm): Observable<any[]> {

    return this._api.list(`${environment.listar_documentos_archivados_ud}${idEcm}`)
      .pipe(map(response => !isNullOrUndefined(response.response) ? response.response.documentos : []));
  }

  subirDocumentosParaArchivar(documentos: FormData): Observable<any> {

    return this._api.sendFile(environment.subir_documentos_por_archivar, documentos, []);
  }

  obtenerDocumentoPorNoRadicado(nroRadicado): Observable<any> {
    return this._api.list(`${environment.obtenerDocumento_asociados_radicado}/${nroRadicado}?allAnnexes=true`);
  }

  obtenerDocumentoPlanillado(nroRadicado, payload): Observable<any> {
    return this._api.list(`${environment.obtenerDocumento_asociados_radicado}/${nroRadicado}`, payload);
  }

  obtenerDocumentoPorIdDocumento(nroRadicado, idDoc): Observable<any> {
    return this._api.list(`${environment.obtenerDocumento_asociados_radicado}/${nroRadicado}?allAnnexes=true&idDocumento=${idDoc}`);
  }

  obtenerDocumentosPorIdDocumentoPrincipal(idDoc, nroRadicado, radicadoProduccion: RadicadoProduccion): Observable<any> {
    return this._api.list(`${environment.obtenerDocumentos_radicado}/${idDoc}/${nroRadicado}`, radicadoProduccion);
  }

  eliminarDocumento(idEcm): Observable<any> {

    return this._api.delete(`${environment.pd_gestion_documental.eliminarVersionDocumento}/${idEcm}`);
  }

  buscarUnidadesDocumentales(payload): Observable<any> {

    return this._api.list(environment.buscar_unidad_documental_endpoint, payload)
      .pipe(map(response => response.response.unidadDocumental));
  }

  getListTrd(idFolfer: any): Observable<SelectItem[]> {
    return this._api.list(`${environment.getListTrd}/${idFolfer}`);
  }

  getObservations(payload): Observable<any[]> {
    return this._api.list(environment.endpoint_observations, payload);
  }

  addObservations(payload): Observable<any> {
    return this._api.post(environment.endpoint_observations, payload);
  }
}
export interface RadicadoProduccion {
  radicadoProduccion?: string,
}
