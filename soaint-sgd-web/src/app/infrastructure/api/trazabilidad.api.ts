import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs/Observable";

@Injectable()
export class TrazabilidadApi {


  constructor(private _apiBase: ApiBase) {
  }

  listTrazabilidad(payload: string): Observable<any> {
    return this._apiBase.list(`${environment.trazabilidad}/filter`, payload);
  }
}
