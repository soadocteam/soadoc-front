import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {ApiBase} from "./api-base";
import {Utils} from "../../shared/helpers";
import {map} from "rxjs/operators";
import {TemplatesDTO} from "../../domain/templatesDTO";

@Injectable()
export class CachedData {
  private allDependenciesCached$: Observable<any> = of({});
  private allSedesCached$: Observable<any> = of();
  private anexosCached$: Observable<any> = of();
  private templates$: Observable<TemplatesDTO[]> = of([])

  constructor(private _api: ApiBase) {
    this.loadCachedData();
  }

  private loadCachedData() {
    let options: any = {
      headers: new HttpHeaders().append('skipLoading', 'true')
    };
    this.allDependenciesCached$ = this._api.list(environment.dependencias_endpoint, {}, options)
      .publishReplay(1).refCount();
    this.allDependenciesCached$.subscribe();

    this.allSedesCached$ = Utils
      .sortObservable(this._api.list(environment.sedeAdministrativa_endpoint, {}, options)
        .pipe(map(res => res.organigrama)), 'nombre')
      .publishReplay(1).refCount();
    this.allSedesCached$.subscribe();

    this.anexosCached$ = Utils.sortObservable(this._api.list(environment.tipoAnexos_endpoint, {}, options)
      .pipe(map(res => res.constantes)), 'nombre')
      .publishReplay(1).refCount();
    this.anexosCached$.subscribe();

    this.templates$ = this._api.list(`${environment.urlTemplateType}`, {}, options)
      .publishReplay(1).refCount();
    this.templates$.subscribe();
  }

  getAllDependencies(): Observable<any> {
    return this.allDependenciesCached$;
  }

  getSedes() {
    return this.allSedesCached$;
  }
  getTiposAnexos() {
    return this.anexosCached$;
  }
  getTiposPlantilla() {
    return this.templates$;
  }
}
