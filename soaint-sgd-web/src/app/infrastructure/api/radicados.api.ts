import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {AnnulmentDTO} from "../../domain/AnnulmentDTO";

@Injectable()
export class RadicadosApi {

  constructor(private  _api: ApiBase) {
  }

  getRadicados(payload?: any): Observable<any> {
    return this._api.list(environment.radicados_padre, payload);
  }

  getRadicadosNoAnulados(params?: any): Observable<any> {
    return this._api.list(environment.anularRadicados_endpoint, params);
  }

  anularRadicados(trackId: string, payload: AnnulmentDTO): Observable<any> {
    return this._api.delete(environment.anularRadicados_endpoint.concat("/").concat(trackId), payload);
  }

  getFullNroRadicado(trackId: string): Observable<any> {
    return this._api.list(environment.findFullTrackNumber.concat(trackId));
  }
}
