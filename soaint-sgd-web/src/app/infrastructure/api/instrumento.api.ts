import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import {environment} from "../../../environments/environment";
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {DependenciaDTO} from "../../domain/dependenciaDTO";
import {isNullOrUndefined} from "util";
import {Utils} from "../../shared/helpers";

@Injectable()
export class InstrumentoApi {
  dependenciesList: Observable<DependenciaDTO[]>;
  constructor(private _api: ApiBase) {
  }

  ListDependences(payload: any): Observable<DependenciaDTO[]> {
    const _endpoint = `${environment.dependenciaGrupo_endpoint}/${payload.value}`;

    return this._api.list(_endpoint).pipe(map(response => {
      return response.organigrama
    }));
  }

  ListarDependencia(): Observable<DependenciaDTO[]>{
    return this._api.list(environment.listarDependneciaEndPoint).pipe(map( response => {
      return Utils
        .sort(response.dependencias, 'nombre')
    }))
  }

  AsociarFuncionarioDependencias(payload):Observable<any>{

    return this._api.put(environment.asociarFuncionarioDependneciasEndPoint,payload);
  }

  ObtenerDependenciaRadicadora():Observable<any>{

    return this._api.list(environment.dependenciaRadicadoraEndpoint)
      .pipe(map( dep => isNullOrUndefined(dep.codigo) ? null : dep));
  }

  getTiplogiasDocumentales(payload):Observable<any>{

    return this._api.list(environment.listarTipologiasDocumentalesEndPoint, payload);
  }

  obtenerDependenciaPorCodigo(codDependencia): Observable<any>{

    return this._api.list(`${environment.obtenerDependenciaPorCodigoEndPoint}/${codDependencia}`);
  }

  getSubseries(codDep):Observable<any>{
    return this._api.list(
    `${environment.listarSubseriesEndPoint}/${codDep}`);
  }

  getDependenciesList(): Observable<DependenciaDTO[]> {
    if (!this.dependenciesList) {
      this.dependenciesList = this.ListarDependencia()
        .publishReplay(1).refCount();
    }
    return this.dependenciesList;
  }
}
