import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {VersionDTO} from "../../domain/VersionDTO";

@Injectable()
export class VersionApi {
  constructor(private _apiBase: ApiBase) {
  }

  findVersiones(codigoDepedenciaProductora: string, tipificador: number = 0 ): Observable<VersionDTO[]> {
    return this._apiBase.list(environment.findVersiones, {
      ideOfcProd: codigoDepedenciaProductora,
      tipificador: tipificador
    });
  }
}
