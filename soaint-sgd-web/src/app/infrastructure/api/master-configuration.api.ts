import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import {environment} from "../../../environments/environment";

@Injectable()
export class MasterConfigurationApi {

  constructor(private _apiBase: ApiBase) {
  }

  findOptinalId(number){
      return this._apiBase.list(`${environment.optionFind}/${number}`)
  }
}
