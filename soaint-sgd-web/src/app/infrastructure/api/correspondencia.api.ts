import {Injectable} from '@angular/core';
import {ApiBase} from './api-base';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {UnidadDocumentalDTO} from '../../domain/unidadDocumentalDTO';
import {MensajeRespuestaDTO} from '../../domain/MensajeRespuestaDTO';
import {AnexoFullDTO} from '../../domain/anexoFullDTO';
import {Subscription} from 'rxjs';
import {ComunicacionOficialDTO} from '../../domain/comunicacionOficialDTO';
import {ComunicacionOficialSalidaFullDTO} from "../../domain/comunicacionOficialSalidaFullDTO";
import {HttpHeaders} from "@angular/common/http";
import {ResponseDefaultPlanillas} from "../../domain/ResponseDefaultPlanillas";

@Injectable()
export class CorrespondenciaApiService {

  constructor(private _api: ApiBase) {

  }

  listarComunicacionesEntradaConsultar(payload: any): Observable<any[]> {
    return this._api.list(environment.deliveryPlanilla, payload)
  }

  ListarComunicacionesSalidaDistibucionFisicaExterna(payload: any): Observable<ResponseDefaultPlanillas[]> {
    return this._api.list(environment.Planillas, payload)
  }

  ListarComunicacionesSalidaDistibucionFisica(payload: any): Observable<ResponseDefaultPlanillas[]> {
    return this._api.list(environment.Planillas, payload)
  }


  actualizarComunicacion(payload: any): Observable<any> {
    return this._api.put(environment.actualizarComunicacion_endpoint, payload);
  }

  actualizarInstanciaGestionDevoluciones(payload: any): Observable<any> {

    return this._api.put(environment.actualizarInstanciaDevolucion, payload);
  }

  recibirDocumentosFisicos(payload) {
    return this._api.put(environment.Planillas, payload);
  }

  enviarCorreoReciboDocumentos(payload: FormData) {
    return this._api.sendFile(environment.sendPreDispatch, payload);
  }
}
