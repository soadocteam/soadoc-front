import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import {environment} from "../../../environments/environment.prod";

@Injectable()
export class GestionEntregaApi {
  constructor(private _api: ApiBase) {
  }

  uploadMessive(payload: any): any {
    return this._api.post(environment.masiva_upload_download, payload);
  }

  getListCourier(): any {
    return this._api.list(environment.listas.getListas);
  }

  downloadMessive(massiveId: any, payload: any): any {
    return this._api.list(`${environment.masiva_upload_download}/${massiveId}`, payload);
  }
}
