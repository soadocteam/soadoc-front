import {Injectable} from '@angular/core';
import {ApiBase} from './api-base';
import {environment} from '../../../environments/environment';
import {Observable, of} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';
import {SerieDTO} from '../../domain/serieDTO';
import {SubserieDTO} from '../../domain/subserieDTO';
import {ContenidoDependenciaTrdDTO} from '../../domain/ContenidoDependenciaTrdDTO';
import {isNullOrUndefined} from "util";
import {PushNotificationAction} from "../state-management/notifications-state/notifications-actions";
import {EmptyObservable} from "rxjs-compat/observable/EmptyObservable";
import {Store} from "@ngrx/store";
import {State as RootState} from "../redux-store/redux-reducers";

@Injectable()
export class SerieSubserieApiService {

  constructor(private _api: ApiBase,
              private _store: Store<RootState>) {
  }

  ListarSerieSubserie(payload: any): Observable<ContenidoDependenciaTrdDTO> {
    const resp = this._api.post(environment.listar_serie_subserie, payload)
      .pipe(map(response =>
        (response.contenidoDependenciaTrdDTOS) ? response.contenidoDependenciaTrdDTOS[0] : of({})));
    return resp

  }

  getSeriesTvd(codigoDependenciaProductora: string): Observable<Array<SerieDTO>> {
    const payload = {
      codOrg: codigoDependenciaProductora
    }
    return this._api.list(environment.listarSeriesTVD, payload)
      .map((series): Array<SerieDTO> => (
        series.map(serie => ({
          idSerie: serie.ideSerie,
          codigoSerie: serie.codSerie,
          nombreSerie: serie.nomSerie
        }))
      ));
  }

  getSubseriesTvd(idSerie: number, codigoDependenciaProductora: string): Observable<Array<SubserieDTO>> {
    const payload = {
      codOrg: codigoDependenciaProductora,
      idSerie: idSerie
    }
    return this._api.list(environment.listarSubSeriesTVD, payload)
      .map((series): Array<SubserieDTO> => (
        series.map(serie => ({
          idSubSerie: serie.ideSubserie,
          codigoSubSerie: serie.codSubserie,
          nombreSubSerie: serie.nomSubserie
        }))
      ));
  }

  findVersionedSeries(codigoDepedencia: string, idVersion?): Observable<Array<SerieDTO>> {
    const params: any = {};
    if (idVersion) {
      params.idVersion = idVersion;
    }
    return this._api.list(`${environment.findVersionedSeries}/${codigoDepedencia}`, params)
      .map((series): Array<SerieDTO> => (series.map(this.serieIntrumentosArchivistivosToSerieDTO)))
  }

  findVersionedSubseries(codigoDepedencia: string, idSerie: number, idVersion?): Observable<Array<SubserieDTO>> {
    const params: any = {};
    if (idVersion) {
      params.idVersion = idVersion;
    }
    return this._api.list(`${environment.findVersionedSubseries}/${codigoDepedencia}/${idSerie}`, params)
      .map((subseries): Array<SubserieDTO> => (subseries.map(this.serieInstrumentosArchivisticosToSubserieDto)))
  }

  private serieIntrumentosArchivistivosToSerieDTO(serie) :SerieDTO {
    return {
      idSerie: serie.ideSerie,
      codigoSerie: serie.codSerie,
      nombreSerie: serie.nomSerie
    }
  }
  private serieInstrumentosArchivisticosToSubserieDto(subserie): SubserieDTO {
    return {
      idSubSerie: subserie.ideSubserie,
      codigoSubSerie: subserie.codSubserie,
      nombreSubSerie: subserie.nomSubserie
    }
  }

  getAllConfiguration(): Observable<any> {
    return this._api.list(environment.getAllConfig)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al consultar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

  getConfiguration(config) {
    return this._api.post(environment.getConfig, config)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al consultar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

  addConfiguration(config) {
    return this._api.post(environment.createConfig, config)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al eliminar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

  deleteConfiguration(idConfiguration) {
    return this._api.post(environment.deleteConfig, idConfiguration)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al eliminar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

  updateConfiguration(config) {
    return this._api.post(environment.updateConfig, config)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al eliminar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }
}
