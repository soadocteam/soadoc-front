import {Injectable} from '@angular/core';
import {ApiBase} from "./api-base";
import {environment} from "../../../environments/environment";
import {State as RootState} from '../../infrastructure/redux-store/redux-reducers';
import {isNullOrUndefined} from "util";
import {FuncionarioDTO} from "../../domain/funcionarioDTO";
import {Observable, of, pipe} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';
import {Store} from "@ngrx/store";
import {HttpHeaders} from "@angular/common/http";

@Injectable()
export class FuncionariosService {
  funcionarios: Observable<any> = of([]);
  constructor(private _api:ApiBase,
              private _store: Store<RootState>
              ) {
   this.loadFuncionarios();
  }
  private loadFuncionarios() {
    let options: any = {
      headers: new HttpHeaders().append('skipLoading', 'true')
    };
    const payload = {
      estado: 'A'
    };
    this.funcionarios = this._api.post(environment.buscarFuncionarios_endpoint, payload, options)
      .publishReplay(1).refCount();
    this.funcionarios.subscribe(() => {});
  }
  getFuncionarioById(id):Observable<any>{

    return this.funcionarios.pipe(map(data => data.funcionarios.find( f => f.id == id)));
  }

  getFuncionarioBySpecification(specification?:(funcionario:any)=>boolean){

    return this.funcionarios.pipe(map(data => data.funcionarios.filter( f => isNullOrUndefined(specification) || specification(f))));
  }

  getFuncionarioBySpecificationEstado(payload, specification?:(funcionario:any)=>boolean){

    return this._api.post(environment.buscarFuncionarios_endpoint, payload)
      .pipe(map(data => data.funcionarios.filter( f => isNullOrUndefined(specification) || specification(f))));
  }

  getAllFuncionarios():Observable<any>{

    return this.funcionarios.pipe(map(data => data.funcionarios));
  }

  getAllFuncionariosEstado(payload):Observable<any>{

    return this.funcionarios.pipe(map(data => data.funcionarios));
  }


  updateRoles(funcionario:FuncionarioDTO):Observable<any>{

    return this._api.put(environment.updateFuncionarios_roles_endpoint,funcionario);
  }

  update(funcionario:FuncionarioDTO):Observable<any>{

    return this._api.put(environment.updateFuncionarios_endpoint,funcionario);
  }

  getAllFuncionariosPayload():Observable<any>{
    return this.funcionarios;
  }

  getFuncionarioUsername(username: string){
    return this._api.list(`${environment.listUser}/${username}`);
  }

  findFuncionariosByDependencyCodeAndRol(dependencyCode: string, rolName: string) {
    return this.getAllFuncionariosPayload()
      .map((funcionariosDto) => {
        const funcionarios = funcionariosDto.funcionarios;
        return funcionarios.filter(funcionario => {
          return funcionario.dependencias.some(dependencia => dependencia.codigo == dependencyCode) &&
            funcionario.roles.some(rol => rol.rol == rolName)
        });
      })
  }

}
