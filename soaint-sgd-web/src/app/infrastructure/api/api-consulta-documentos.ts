import {ApiBase} from "./api-base";
import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";
import {isNullOrUndefined} from "util";
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {State as RootState} from "../redux-store/redux-reducers";
import {PushNotificationAction} from "../state-management/notifications-state/notifications-actions";
import {EmptyObservable} from "rxjs-compat/observable/EmptyObservable";
import {of} from 'rxjs/observable/of';
import {switchMap} from "rxjs/operators";

@Injectable()
export class ApiConsultaDocumentos {

  constructor(private _api: ApiBase, private _store: Store<RootState>) {
  }

  getConsultaExpedientes(payload): Observable<any> {
    return this._api.list(environment.consultarExpediente, payload)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.codMensaje != '0000') {
          this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.mensaje}));
          return new EmptyObservable();
        }
        if (isNullOrUndefined(res.response))
          return of([]);
        return of(res.response['consultar-expedientes']);
      }))
  }

  getConsultaIndiceElectronico(ecmObjld): Observable<any> {
    return this._api.list(`${environment.consultarIndiceElectronico}/${ecmObjld}`);
  }

  getDocumentoPorExpediente(ecmObjld): Observable<any> {
    return this._api.list(`${environment.consultarDocumentosPorExpediente}/${ecmObjld}`,)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.codMensaje != '0000') {
          this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.mensaje}));
          return new EmptyObservable();
        }
        if (isNullOrUndefined(res.response))
          return of([]);
        return of(res.response['consultar-documentos']);
      }))
  }

  getConsultaDocumentos(payload): Observable<any> {
    return this._api.list(environment.consultarDocumentos, payload)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.codMensaje != '0000') {
          this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.mensaje}));
          return new EmptyObservable();
        }
        if (isNullOrUndefined(res.response))
          return of([]);
        return of(res.response['consultar-documentos']);
      }))
  }

  getDetalleDocumento(idDocumento, payload?): Observable<any> {
    return this._api.list(`${environment.detalleDcoumento}/${idDocumento}`, payload)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.codMensaje != '0000') {
          this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.mensaje}));
          return new EmptyObservable();
        }
        if (isNullOrUndefined(res.response))
          return of([]);
        return of(res.response['consultar-documento']);
      }))
  }

  getDocumentoEcm(ecmId: string): Observable<any> {
    return this._api.list(`${environment.descargarDocumentoEcm}/${ecmId}`);
  }

  getAnexosList(idDocumento: string, payload: any): Observable<any> {
    return this._api.list(`${environment.obtenerDocumento_asociados_endpoint}/${idDocumento}`, payload);
  }

  getExcel(tipComunicacion: string, dto): Observable<any> {
    return this._api.post(`${environment.getExcel}/${tipComunicacion}`, dto);
  }

  getDocumentHtml(endpoint) {
    return this._api.list(endpoint, {}, { responseType: 'text' });
  }
}
