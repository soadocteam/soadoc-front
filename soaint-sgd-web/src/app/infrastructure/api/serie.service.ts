import {Injectable} from '@angular/core';
import {SerieSubserieApiService} from "./serie-subserie.api";
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {SerieDTO} from "../../domain/serieDTO";
import {SubserieDTO} from "../../domain/subserieDTO";
import {Utils} from "../../shared/helpers";

@Injectable()
export class SerieService {

  constructor(private serieSubserieService: SerieSubserieApiService) {
  }

  getSeriePorDependencia(codDependencia) {

    return Utils.sortObservable(this.serieSubserieService
      .ListarSerieSubserie({idOrgOfc: codDependencia})
      .pipe(map(response => response.listaSerie)), 'nombreSerie');
  }

  getSubSeriePorDependencia(codDependencia) {

    return Utils.sortObservable(this.serieSubserieService
      .ListarSerieSubserie({idOrgOfc: codDependencia})
      .pipe(map(response => response.listaSubSerie)), 'nombreSubSerie');
  }

  getSubseriePorDependenciaSerie(codDependencia, codSerie, nomSerie) {

    return this
      .serieSubserieService
      .ListarSerieSubserie({idOrgOfc: codDependencia, codSerie: codSerie, nomSerie: nomSerie})
      .pipe(map(response => response.listaSubSerie));
  }

  findVersionedSeries(codigoDependencia: string): Observable<SerieDTO[]> {
    return this.serieSubserieService.findVersionedSeries(codigoDependencia);
  }

  findVersionedSubseries(codigoDependencia: string, idSerie: number): Observable<SubserieDTO[]> {
    return this.serieSubserieService.findVersionedSubseries(codigoDependencia, idSerie);
  }
}

