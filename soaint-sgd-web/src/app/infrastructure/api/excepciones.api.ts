import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import {environment} from "../../../environments/environment";
import {switchMap} from "rxjs/operators";
import {isNullOrUndefined} from "util";
import {of} from "rxjs/observable/of";
import {PushNotificationAction} from "../state-management/notifications-state/notifications-actions";
import {EmptyObservable} from "rxjs-compat/observable/EmptyObservable";
import {Store} from "@ngrx/store";
import {State as RootState} from "../redux-store/redux-reducers";

@Injectable({
  providedIn: "root"
})
export class ExcepcionesApiService {
  constructor(private http: HttpClient,
              private _api: ApiBase,
              private _store: Store<RootState>) {}

  getExcepctions(): Observable<any> {
    return this._api.list(environment.findException)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al consultar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

  addException(payload): Observable<any> {
    return this._api.post(environment.addException, payload)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al agregar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

  deleteException(payload): Observable<any> {
    return this._api.post(environment.deleteException, payload)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al eliminar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

  updateException(payload): Observable<any> {
    return this._api.post(environment.updateException, payload)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al actualizar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

}
