import * as domtoimage from 'dom-to-image';
import * as html2canvas from 'html2canvas';
import * as detectBrowser from 'detect-browser';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DomToImageService {
  constructor() {
  }

  convertToBlob(node: HTMLElement): Promise<Blob> {
    return this.convertToBlobTwo(node);
  }

  readonly browser = detectBrowser.detect();

  convertToBlobTwo(node: HTMLElement): Promise<Blob> {
    switch (this.browser.name) {
      case 'edge' || 'safari' : {
        return this.convertToBlobNavigation(node);
        break;

      }
      default: {
        return domtoimage.toBlob(node);
      }
    }
  }

  convertToBlobNavigation(node): Promise<Blob> {
    return html2canvas.default(node).then(canvas => {

      const dataUrl = canvas.toDataURL();

      const binStr = atob(dataUrl.split(',')[1]);

      const len = binStr.length;
      const arr = new Uint8Array(len);

      for (let i = 0; i < len; i++) {
        arr[i] = binStr.charCodeAt(i);
      }

      return new Blob([arr], {type: 'image/png'});
    });
  }
}

interface IDomToImage {
  convertToBlob(node: HTMLElement): Promise<Blob>;
}
