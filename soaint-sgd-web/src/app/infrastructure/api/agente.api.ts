import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {PersonDTO} from "../../domain/personDTO";

@Injectable()
export class AgenteApi {


  constructor(private _apiBase: ApiBase) {
  }

  getAgenteByNroIdentificacion(nroIdentificacion: string, tipoPersona: string) {

    return this._apiBase.list(`${environment.agenteByNroIdentificacionEndPoint}${nroIdentificacion}/${tipoPersona}`);

  }

  getPersonasByFilters(personFilter: PersonFilters): Observable<PersonDTO[]> {
    return this._apiBase.list(`${environment.personasByFilters}`, personFilter)
  }
}
export interface PersonFilters {
  personTypeCode?: string,
  idDocTypeCode?: string,
  name?: string,
  idNumber?: string,
  businessName?: string
}
