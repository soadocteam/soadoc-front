import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import { Observable } from 'rxjs';
import {environment} from "../../../environments/environment";
import {EmptyObservable} from "rxjs-compat/observable/EmptyObservable";

@Injectable()
export class MegafServiceApi {


  constructor(private _apiService:ApiBase){

  }

  createUnidadDocumental():Observable<any>{

    return new EmptyObservable();
  }

  getDirectChilds(codDependencia,tipoArchivo,idPadre:any = 0): Observable<any>{
    return this._apiService.list(`${environment.megaf.hijosDirectos}/${codDependencia}/${tipoArchivo}/${idPadre}`);
  }

  getUnidadesFisicas(payload?):Observable<any>{
    return  this._apiService.list(environment.megaf.unidadesConservacion,payload);
  }

}
