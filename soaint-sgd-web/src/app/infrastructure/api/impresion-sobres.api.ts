import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";

@Injectable()
export class ImpresionSobresApi {

  constructor(private  _api: ApiBase) {
  }

  getNumRadicadosforSobres(payload?: any): Observable<any> {
    const endpoint = environment.anularRadicados_endpoint;
    return this._api.list(`${endpoint}?distFisica=true`, payload);
  }

  downloadSobres(payload: any): Observable<any> {
    const endpoint = environment.sobresDescargar;
    return this._api.post(`${endpoint}`, payload);
  }
}
