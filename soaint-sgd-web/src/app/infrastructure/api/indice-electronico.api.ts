import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {environment} from "../../../environments/environment";
import {IndiceElectronicoDTO} from "../../domain/indiceElectronicoDTO";
import {ApiBase} from "./api-base";
import {isNullOrUndefined} from "util";
import {switchMap} from "rxjs/operators";
import {of} from 'rxjs/observable/of';
import {PushNotificationAction} from "../state-management/notifications-state/notifications-actions";
import {EmptyObservable} from "rxjs-compat/observable/EmptyObservable";
import {Store} from "@ngrx/store";
import {State as RootState} from "../redux-store/redux-reducers";



@Injectable({
  providedIn: "root"
})
export class IndiceElectronicoApi {
  constructor(private  _api: ApiBase,
              private _store: Store<RootState>) {}

  findIndices(indice: IndiceElectronicoDTO): Observable<any>{
    return this._api.post(environment.findIndice, indice)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al consultar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

  findIndiceData(idExpediente): Observable<any> {
    return this._api.post(`${environment.findIndiceData}/${idExpediente}`)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al consultar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

  createIndice(user, payload): Observable<any> {
    return this._api.post(`${environment.createIndice}/${user}`, payload)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al reintentar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

  getExecutionTime(type): Observable<any> {
    return this._api.list(`${environment.getExecutionTime}/${type}`)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al consultar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

  updateTimeExecute(payload): Observable<any> {
    return this._api.post(environment.updateTimeExecute, payload)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al actualizar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

  getDocuments(payload, fileType): Observable<any> {
    return this._api.post(`${environment.findDocuments}/${fileType}`, payload)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al actualizar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

  generarIndiceDemanda(payload): Observable<any> {
    return this._api.post(environment.generarIndiceDemanda, payload)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al actualizar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

  getIPM(idOption): Observable<any> {
    return this._api.list(`${environment.getIPM}/${idOption}`)
      .pipe(switchMap(res => {
        if (isNullOrUndefined(res))
          return of([]);
        if (res.statusCode != 200) {
          if (res.statusCode == 500) {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: 'Se ha producido un error al actualizar. Por favor, intente de nuevo.'}));
            return new EmptyObservable();
          } else {
            this._store.dispatch(new PushNotificationAction({severity: 'error', summary: res.message}));
            return new EmptyObservable();
          }
        }
        if (isNullOrUndefined(res.objectResponse))
          return of([]);
        return of(res.objectResponse);
      }))
  }

}
