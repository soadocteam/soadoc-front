import {Store} from "@ngrx/store";
import {State} from "../redux-store/redux-reducers";
import {ApiBase} from "./api-base";
import {environment} from "../../../environments/environment.prod";
import {Observable} from "rxjs";
import {ResponseDefaultPlanillas} from "../../domain/ResponseDefaultPlanillas";
import {Injectable} from "@angular/core";
import {DeliveryFormDTO} from "../../domain/deliveryFormDTO";

@Injectable()
export class EntregaTercerosApiServices {
  constructor(private _store: Store<State>,
              private _api: ApiBase) {
  }

  searchTrack(payload?: any): Observable<ResponseDefaultPlanillas[]> {
    return this._api.list(environment.Planillas, payload);
  }

  saveTrack(payload?: any): Observable<any> {
    return this._api.put(environment.Planillas, payload);
  }

  uploadFile(trackId: string, payload: FormData): Observable<any> {
    return this._api.sendFile(`${environment.Planillas}/${trackId}/proofs`, payload);
  }

  setStatusTrazability(payload: any[]) {
    return this._api.post(environment.trazabilityPlanilla, payload);
  }
}
