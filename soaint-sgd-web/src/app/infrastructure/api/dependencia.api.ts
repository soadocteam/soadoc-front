import { Injectable } from '@angular/core';
import {ApiBase} from './api-base';
import {ComunicacionOficialDTO} from '../../domain/comunicacionOficialDTO';
import {environment} from '../../../environments/environment';
import { CacheResponse } from '../../shared/cache-response';
import {Observable} from 'rxjs/Observable';
import { DependenciaDTO } from '../../domain/dependenciaDTO';
import {map} from 'rxjs/operators';

@Injectable()
export class DependenciaApiService extends CacheResponse {

  constructor(private _api: ApiBase) {
    super();
   }

  Listar(payload?: any): Observable<DependenciaDTO[]> {

    const endpoint = environment.dependencias_endpoint;

    return this.getResponse(payload, this._api.list(endpoint, payload)
        .pipe(map(response => {
               return response.dependencias;
        })),endpoint );
  }

  getAllDependencies(payload?: any): Observable<DependenciaDTO[]> {
    return this._api.list(environment.all_dependencias_endpoint, payload);
  }


  ListarPorSede(payload: string): Observable<any> {
    const endpoint = environment.dependenciaGrupo_endpoint + '/' + payload;
    return this.getResponse(payload, this._api.list(endpoint), endpoint);
  }

  GetDependenciaPorCodigo(codigo:string):Observable<any>{

    const endpoint = environment.obtenerDependenciasPorCodigo_endpoint;

    return this._api.list(endpoint,{codigos:[codigo]});
  }

}
