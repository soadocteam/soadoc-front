import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs/Observable";

@Injectable()
export class SmsApi {

  constructor(private _apiBase:ApiBase){}

  sendSms(addressee: string): Observable<any> {
    const enpoint = environment.sendSms;
    return this._apiBase.list(`${enpoint}/${addressee}`);
  }
}
