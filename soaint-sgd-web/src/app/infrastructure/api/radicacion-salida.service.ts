import {Injectable} from '@angular/core';
import {ApiBase} from "./api-base";
import {ComunicacionOficialDTO} from "../../domain/comunicacionOficialDTO";
import {environment} from "../../../environments/environment";
import {RequestTrackDTO} from "../../domain/massive/RequestTrackDTO";
import {HttpHeaders} from "@angular/common/http";
import {LocationType} from "../../shared/enums/enums";

@Injectable()
export class RadicacionSalidaService {

  constructor(private _api:ApiBase) { }

  radicar(comunicacion:ComunicacionOficialDTO){

    return this._api.post(environment.radicarSalida_endpoint,comunicacion);
  }

  verificacionUser(userlogin: string) {
    const endpoint = environment.digitalizar_doc_upload_endpoint;
    return this._api.list(`${endpoint}/digital-signature-user/${userlogin}`);
  }

  validacionUser(body: any){
    const endpoint = environment.digitalizar_doc_upload_endpoint;
    return this._api.post(`${endpoint}/validate-digital-signature-credentials`, body);
  }

  radicarDocProducido(comunicacion:ComunicacionOficialDTO){

    return this._api.post(environment.radicarDocProducido_endpoint,comunicacion);
  }

  radicarMasivo(comunicacion: RequestTrackDTO, user: string, pass: string) {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append("BasicAuth", 'Basic ' + btoa(`${user}:${pass}`));
    headers = headers.append('skipLoading', 'true');
    return this._api.post(environment.massive_information, comunicacion,{
      headers
    });
  }

  quickSave(payload: any) {
    return this._api.post(environment.salvarCorrespondenciaEntrada_endpoint, payload);
  }

  quickRestore(idproceso: string, idtarea: string) {
    const endpoint = environment.restablecerCorrespondenciaEntrada_endpoint;
    return this._api.list(`${endpoint}/${idproceso}/${idtarea}`);
  }

  uploadTemplate(payload:FormData, locationType?: LocationType){
    let endpoint = environment.upload_template;
    if (locationType) {
      endpoint = `${endpoint}?locationStamp=${locationType}`
    }
    return this._api.sendFile(endpoint,payload,[]);
  }

  uploadTemplateMassive(payload:FormData, locationType: LocationType, user: string, pass: string){
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append("BasicAuth", 'Basic ' + btoa(`${user}:${pass}`));
    headers = headers.append('skipLoading', 'true');
    let endpoint = environment.upload_template_massive;
    if (locationType) {
      endpoint = `${endpoint}?locationStamp=${locationType}`
    }
    return this._api.sendFile(endpoint,payload,[], {headers});
  }


}
