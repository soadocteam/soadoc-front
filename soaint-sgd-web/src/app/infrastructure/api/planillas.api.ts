import {Injectable} from '@angular/core';
import {ApiBase} from './api-base';
import {environment} from '../../../environments/environment';
import {PlanillaDTO} from '../../domain/PlanillaDTO';
import {Observable} from "rxjs";

@Injectable()
export class PlanillasApiService {

  constructor(private _api: ApiBase) {
  }

  generarPlanillas(payload: PlanillaDTO[]) {
    return this._api.post(environment.deliveriesPlanilla, payload);
  }

  generarPlanillasSalida(payload: PlanillaDTO[]) {
    return this._api.post(environment.deliveriesPlanilla, payload);
  }

  generarPlanillaInterna(payload: any[]) {
    return this._api.post(environment.deliveriesPlanilla, payload)
  }

  exportarPlanillaConsulta(formId: any, payload: any): Observable<any> {
    return this._api.list(environment.deliveryPlanilla + '/' + formId, payload);
  }

  cargarPlanillas(payload: PlanillaDTO) {
    return this._api.post(environment.cargarPlanilla_endpoint, payload);
  }

  setStatusTrazability(payload: any[]) {
    return this._api.post(environment.trazabilityPlanilla, payload);
  }

}
