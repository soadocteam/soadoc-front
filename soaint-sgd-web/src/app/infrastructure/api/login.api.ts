import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class LoginApiService {
  constructor(private http: HttpClient,
              private _api: ApiBase) {}

  login(user: any): Observable<any> {
    return this._api.post(environment.login, user);
  }
}
