import {TemplatesDTO} from './../../domain/templatesDTO';
import {Injectable} from '@angular/core';
import {ApiBase} from './api-base';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {RolDTO} from '../../domain/rolesDTO';
import {TransformVersionDocumentoToDocumentoEcm} from "../../shared/data-transformers/transform-version-documento-to-documento-ecm";
import {of} from 'rxjs/observable/of';
import {map} from 'rxjs/operators';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {isNullOrUndefined} from 'util';
import {Utils} from "../../shared/helpers";
import {LocationType} from "../../shared/enums/enums";

@Injectable()
export class ProduccionDocumentalApiService {

  roles: RolDTO[] = [
    {'id': 1, 'rol': 'administrador', 'nombre': 'Administrador'},
    {'id': 2, 'rol': 'proyector', 'nombre': 'Proyector'},
    {'id': 3, 'rol': 'revisor', 'nombre': 'Revisor'},
    {'id': 4, 'rol': 'aprobador', 'nombre': 'Aprobador'}
  ];

  constructor(private _api: ApiBase, private _adapter: TransformVersionDocumentoToDocumentoEcm, private http: HttpClient) {
  }

  guardarEstadoTarea(payload: any, options = {}) {
    return this._api.post(environment.taskStatus_endpoint, payload, options).pipe(map(response => response));
  }

  obtenerEstadoTarea(payload: { idInstanciaProceso: string, idTareaProceso: string }) {
    return this._api.list(`${environment.taskStatus_endpoint}/${payload.idInstanciaProceso}/${payload.idTareaProceso}`, {}).pipe(map(response => response.payload));
  }

  obtenerContactosDestinatarioExterno(payload: { nroRadicado: string }) {
    return this._api.list(`${environment.obtenerContactoDestinatarioExterno_endpoint}/${payload.nroRadicado}`, {});
  }

  obtenerDatosDocXnroRadicado(payload: { id: string }) {
    return this._api.list(`${environment.pd_obtenerDatosDocXnroRadicado}/${payload.id}`, {});
  }

  subirVersionDocumento(formData: FormData, folderId?: string, showLoading = true) {
    let url = environment.pd_gestion_documental.subirDocumentoVersionado;
    if (folderId) {
      url = `${url}/?folderId=${folderId}`
    }
    let options = {};
    if (!showLoading) {
      options = {
        headers: new HttpHeaders().append('skipLoading', 'true')
      };
    }
    return this._api.sendFile(url, formData, [], options);
  }

  obtenerVersionDocumento(payload: { id: string, version: string }) {
    return this._api.list(environment.pd_gestion_documental.obtenerVersionDocumento, {
      identificadorDoc: payload.id,
      version: payload.version
    }, {responseType: 'text'});
  }

  obtenerVersionDocumentoUrl(payload: { id: string, version: string }) {
    return `${environment.pd_gestion_documental.obtenerVersionDocumento}?identificadorDoc=${payload.id}&version=${payload.version}`;
  }

  obtenerVersionDocumentoUrlGet(payload: { id: string, version: string }): Observable<any> {
    return this._api.list(this.obtenerVersionDocumentoUrl(payload), {}, {responseType: 'blob'});
  }

  transformarAPdf(htmlContent: any, tipoPlantilla, templateID, massive): Observable<any> {

    return this._api.put(`${environment.pdVersionPdfEndPoint}/${tipoPlantilla}/${templateID}/${massive}`, htmlContent)
      .pipe(map(res => res.response.documento));
  }


  eliminarVersionDocumento(payload: { id: string }) {
    return this._api.delete(`${environment.pd_gestion_documental.eliminarVersionDocumento}/${payload.id}`, {});
  }

  obtenerDocumentoUrl(payload: { id: string }) {
    return this._api.list(environment.pd_gestion_documental.obtenerDocumentoPorId, {identificadorDoc: payload.id});
  }

  subirAnexo(formData: FormData) {
    return this._api.sendFile(environment.pd_gestion_documental.subirAnexo, formData, []);
  }

  eliminarAnexo(payload: { id: string }) {
    return this._api.delete(`${environment.pd_gestion_documental.eliminarAnexo}/${payload.id}`, {});
  }


  getFuncionariosByLoginnames(loginnames: string) {
    return this._api.list(`${environment.obtenerFuncionario_endpoint}/funcionarios/listar-by-loginnames/`, {loginNames: loginnames}).pipe(map(res => res.funcionarios));
  }


  ejecutarProyeccionMultiple(payload: {}) {
    return this._api.post(environment.pd_ejecutar_proyeccion_multiple, payload);
  }

  getTiposComunicacion(payload: {}) {
    return this._api.list(environment.tipoComunicacion_endpoint, payload).pipe(map(res => res.constantes));
  }

  getTiposComunicacionSalida(payload: {}) {
    return this._api.list(environment.tipoComunicacionSalida_endpoint, payload).pipe(map(res => res.constantes));
  }

  getFuncionariosPorDependenciaRol(codDependencia) {
    return this._api.list(environment.listarFuncionarios_endpoint + '/' + codDependencia).pipe(map(res => res.funcionarios));
  }

  getTiposAnexo(payload: {}) {
    return Utils.sortObservable(this._api.list(environment.tipoAnexos_endpoint, payload).pipe(map(res => res.constantes)), 'nombre');
  }

  getTiposDestinatario(payload: {}) {
    return this._api.list(environment.tipoDestinatario_endpoint, payload).pipe(map(res => res.constantes));
  }

  getTiposDocumento(payload: {}) {
    return this._api.list(environment.tipoDocumento_endpoint, payload).pipe(map(res => res.constantes));
  }

  getTiposPersona(payload: {}) {
    return this._api.list(environment.tipoPersona_endpoint, payload).pipe(map(res => res.constantes));
  }

  getActuaEnCalidad(payload: {}) {
    return this._api.list(environment.actuaCalidad_endpoint, payload).pipe(map(res => res.constantes));
  }

  getSedes(payload: {}) {
    return Utils
      .sortObservable(this._api.list(environment.sedeAdministrativa_endpoint, payload).pipe(map(res => res.organigrama)), 'nombre');
  }

  getDependencias(payload: {}) {
    return this._api.list(environment.dependencias_endpoint, payload).pipe(map(res => res.dependencias));
  }

  getTipoPlantilla(payload): Observable<string> {
    return this._api.list(`${environment.tipoPlantilla_endpoint}/obtener/${payload.codigo}`, payload).pipe(map(res => res.text));
  }

  generarPdf(payload): Observable<{ success: boolean, text: string }> {
    return this._api.post(`${environment.tipoPlantilla_endpoint}/generar-pdf`, payload).pipe(map(res => res));
  }

  getTiposPlantilla(com?: string): Observable<TemplatesDTO[]> {
    return this._api.list(`${environment.urlTemplateType}`, com).publishReplay(1).refCount();
  }

  getRoles(payload: {}): Observable<RolDTO[]> {
    return of(this.roles);
  }

  getRoleByRolename(rolname: string): RolDTO {
    return this.roles.find((el: RolDTO) => el.rol === rolname);
  }

  getTemplateType(idTemplate?) {
    if (isNullOrUndefined(idTemplate)) {
      return this._api.list(environment.urlTemplateType);
    } else {
      return this._api.list(`${environment.urlTemplateType}/${idTemplate}`);
    }
  }

  obtenerVersionDocumentoObservable(payload: { id: string, version: string }): Observable<any> {
    return this._api.list(`${environment.pd_gestion_documental.obtenerVersionDocumento}?identificadorDoc=${payload.id}&version=${payload.version}`);
  }

  getDocumentoEcm(ecmId: string, options?): Observable<any> {
    return this._api.list(`${environment.descargarDocumentoEcm}/${ecmId}`, {}, options);
  }

  guardarAsuntoTarea(payload: any) {
    return this._api.put(`${environment.proceso_endpoint}/proceso/actualizar/variable`, payload)
  }

  validarRadicadoCompletado(idInstancia: any) {
    return this._api.list(`${environment.comprobar_radicado_completado}/${idInstancia}`);
  }

  processStatusSummary(ProcessInstanceId: any, detail?: boolean, step?: string): Observable<any> {
    let options: any = {
      headers: new HttpHeaders().append('skipLoading', 'true')
    };
    let endpoint = `${environment.estado_proceso_masivo}/${ProcessInstanceId}`;
    if (detail === true && isNullOrUndefined(step)) {
      endpoint = `${endpoint}?detail=${detail}`
      return this._api.list(endpoint);
    } else {
      return this._api.list(endpoint, {}, options);
    }
  }

  statusmassive(ProcessInstanceId: any, detail?: boolean, step?: string) {
    let options: any = {
      headers: new HttpHeaders().append('skipLoading', 'true')
    };
    let endpoint = `${environment.estado_masivo}/${ProcessInstanceId}`;
    if ((step === 'fallido' && detail === false) || (step === 'fallido_adobe' && detail === false)) {
      endpoint = `${endpoint}?step=${step}`
      return this._api.put(endpoint, {}, options);
    } else if (step === 'progreso_adobe' && detail === false) {
      endpoint = `${endpoint}?step=${step}`
      return this._api.put(endpoint, {}, options);
    } else if (step === 'progreso_adobe' && detail === true) {
      endpoint = `${endpoint}?step=${step}&detail=${detail}`
      return this._api.list(endpoint);
    } else {
      return this._api.list(endpoint, {}, options);
    }
  }

  finalizarProcesoMasivo(ProcessInstanceId: number, user: string, pass: string) {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append("BasicAuth", 'Basic ' + btoa(`${user}:${pass}`));
    return this._api.post(`${environment.finalizar_proceso_massivo}?id_proceso=${ProcessInstanceId}`, {}, {headers});
  }

  createTrackBatchMassive(instanceProcess: string) {
    return this._api.post(`${environment.trazability_massive}/${instanceProcess}`);
  }
}
