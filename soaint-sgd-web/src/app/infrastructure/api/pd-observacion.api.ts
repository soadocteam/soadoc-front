import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {PdObservacionDTO} from "../../domain/pdObservacionDTO";
import {HttpHeaders} from "@angular/common/http";

@Injectable()
export class PdObservacionApi {

  constructor(private  _api:ApiBase){}

  listar(idInstancia, options?: any):Observable<any>{

    return this._api.list(`${environment.pdObservacionesListarEndPoint}${idInstancia}`, {}, options);
  }

  actualizar(observaciones: PdObservacionDTO[]): Observable<any> {
    let options: any = {
      headers: new HttpHeaders().append('skipLoading', 'true')
    };
    return this._api.put(environment.pdObservacionesActualizarEndPoint, observaciones, options);
  }
}
