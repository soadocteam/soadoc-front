import {Injectable} from "@angular/core";
import {ApiBase} from "./api-base";
import {AdobeDocumentDTO, AdobeDocumentIdDTO, AdobeDocumentMassiveDTO} from "../../domain/adobe/AdobeDocumentDTO";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {HttpHeaders} from "@angular/common/http";

@Injectable()
export class AdobeApi {

  constructor(private _apiBase: ApiBase) {
  }

  uploadDocument(adobeDocument: AdobeDocumentDTO, ecmId: string, option): Observable<any> {
    return this._apiBase.post(`${environment.adobeSign}/sign?documentId=${ecmId}`, adobeDocument, option);
  }

  signMassive(adobeDocument: any, option) {
    return this._apiBase.post(`${environment.adobeSign}/signMassive`, adobeDocument, option);
  }

  getUrlAdobe(idDocument, options?) {
    return this._apiBase.list(`${environment.adobeSign}/${idDocument}`, {}, options);
  }

  getStatusDocument(idDocument, options?) {
    return this._apiBase.list(`${environment.adobeSign}/findDocument/${idDocument}`, {}, options);
  }

  getDocumentDownload(idDocument) {
    return this._apiBase.list(`${environment.adobeSign}/document/${idDocument}`);
  }

  getDocument(idDocument) {
    return this._apiBase.list(`${environment.adobeSign}/document/${idDocument}`);
  }

  getAdobeConfiguration(idDocument, option): Observable<any> {
    return this._apiBase.list(`${environment.adobeSign}/signature/${idDocument}`, {}, option);
  }

  getListDocumentsAdobe(idDocument, options?): Observable<AdobeDocumentMassiveDTO> {
    return this._apiBase.list(`${environment.adobeSign}/documentMassiveId/${idDocument}`, {}, options);
  }

  uploadDocumentMassiveApi(idDocument, adobeDocumentMassive: AdobeDocumentIdDTO) {
    let options: any = {
      headers: new HttpHeaders().append('skipLoading', 'true')
    };
    return this._apiBase.post(`${environment.adobeSign}/documentMassive/${idDocument}`, adobeDocumentMassive, options)
  }

  saveIdDocumentAdobeMassive(instanceProduction: string, idDocumentAdobe: string) {
    return this._apiBase.put(`${environment.save_id_document_adobe_massive}?instanceProduction=${instanceProduction}
    &idDocumentAdobe=${idDocumentAdobe}`, {} ,{observe: 'response', responseType: 'text', skipLoading: 'true'});
  }

}
