import {Observable} from 'rxjs/Observable';
import {HttpHandler} from '../utils/http-handler';
import {Injectable} from '@angular/core';
import {_ParseAST} from '@angular/compiler';
import {isNullOrUndefined} from "util";

@Injectable()
export class ApiBase {

  constructor(protected _http: HttpHandler) {
  }

  public list(endpoint: string, payload = {}, options = {}): Observable<any> {
    return this._http.get(endpoint, payload, options);
  }

  public post(endpoint: string, payload = {}, options = {}): Observable<any> {
    return this._http.post(endpoint, payload, options);
  }

  public put(endpoint: string, payload = {}, options = {}): Observable<any> {
    return this._http.put(endpoint, payload, options);
  }

  public delete(endpoint: string, payload = {}, options = {}): Observable<any> {
    return this._http.delete(endpoint, payload, options);
  }

  public sendFile(endpoint: string, formData: FormData, pathParams?: Array<string>, options = {}): Observable<any> {

    let fullEndpoint = `${endpoint}`;
    if (!isNullOrUndefined(pathParams)) {
      pathParams.forEach((value) => {
        fullEndpoint += `/${value}`;
      });
    }
    return this._http.putFile(fullEndpoint, formData, options);
  }

}
