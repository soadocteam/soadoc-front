import {Injectable} from '@angular/core';
import {ApiBase} from './api-base';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {ComunicacionOficialSalidaFullDTO} from "../../domain/comunicacionOficialSalidaFullDTO";
import {ResponseDefaultPlanillas} from "../../domain/ResponseDefaultPlanillas";

@Injectable()
export class trasladoInternoApiService {

  constructor(private _api: ApiBase) {
  }

  listarComunicaciones(payload: any): Observable<ResponseDefaultPlanillas[]> {
    return this._api.list(environment.Planillas, payload)
  }

  actualizarEnvioInterno(payload: any) {
    return this._api.put(environment.actualizarDatosEnvioInterno, payload)
  }

}
