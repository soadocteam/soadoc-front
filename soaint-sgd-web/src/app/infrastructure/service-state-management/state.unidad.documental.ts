import {UnidadDocumentalApiService} from '../../infrastructure/api/unidad-documental.api';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {map} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {State} from '../../infrastructure/redux-store/redux-reducers';
import {SerieSubserieApiService} from '../../infrastructure/api/serie-subserie.api';
import {SerieDTO} from '../../domain/serieDTO';
import {SubserieDTO} from '../../domain/subserieDTO';
import {
  Sandbox,
  Sandbox as DependenciaGrupoSandbox
} from '../../infrastructure/state-management/dependenciaGrupoDTO-state/dependenciaGrupoDTO-sandbox';
import {Sandbox as TaskSandBox} from '../../infrastructure/state-management/tareasDTO-state/tareasDTO-sandbox';
import {UnidadDocumentalDTO} from '../../domain/unidadDocumentalDTO';
import {DisposicionFinalDTO} from '../../domain/DisposicionFinalDTO';
import {ConfirmationService} from 'primeng/primeng';
import {PushNotificationAction} from '../../infrastructure/state-management/notifications-state/notifications-actions';
import {MensajeRespuestaDTO} from '../../domain/MensajeRespuestaDTO';
import {ApplicationRef, Injectable} from '@angular/core';
import {isNullOrUndefined} from 'util';
import {ConstanteDTO} from '../../domain/constanteDTO';
import {sedeDestinatarioEntradaSelector} from '../../infrastructure/state-management/radicarComunicaciones-state/radicarComunicaciones-selectors';
import {LoadAction as SedeAdministrativaLoadAction} from '../../infrastructure/state-management/sedeAdministrativaDTO-state/sedeAdministrativaDTO-actions';
import {Subscription} from 'rxjs/Subscription';
import {DependenciaApiService} from '../../infrastructure/api/dependencia.api';
import {DependenciaDTO} from '../../domain/dependenciaDTO';
import {Subject} from 'rxjs/Subject';
import {ROUTES_PATH} from '../../app.route-names';
import {LoadingService} from './../../infrastructure/utils/loading.service';
import {Sandbox as ProccesSandbox} from "../state-management/procesoDTO-state/procesoDTO-sandbox";
import {ProcesoDTO} from "../../domain/procesoDTO";
import {
  getAuthenticatedFuncionario,
  getSelectedDependencyGroupFuncionario
} from "../state-management/funcionarioDTO-state/funcionarioDTO-selectors";
import * as actions from "../state-management/procesoDTO-state/procesoDTO-actions";
import {Router} from '@angular/router';
import {EmptyObservable} from "rxjs-compat/observable/EmptyObservable";
import {ERROR_TRD} from "../../shared/lang/es";
import {FISICO, HIBRIDA, ID_DESPLIEGUE} from "../../shared/constants";
import {FuncionarioDTO} from "../../domain/funcionarioDTO";
import {MessageService} from 'primeng/api';
import {InstrumentoArchivistivoEnum} from "../../shared/enums/enums";
import * as moment from "moment";
import {VersionApi} from "../api/version.api";
import {VersionDTO} from "../../domain/VersionDTO";
import {SOPORTE_FISICO, SOPORTE_HIBRIDO} from "../../shared/bussiness-properties/radicacion-properties";
import {Utils} from "../../shared/helpers";


@Injectable()
export class StateUnidadDocumentalService {

  ListadoUnidadDocumental: UnidadDocumentalDTO[] = [];
  ListadoUnidadDocumental$: Observable<UnidadDocumentalDTO[]> = of([]);
  unidadesSeleccionadas: UnidadDocumentalDTO[] = [];
  selectedIndex;
  funcionarioLog: FuncionarioDTO;
  subscriptions: Subscription[] = [];
  //
  private ListadoActualizadoSubject = new Subject<any>();
  public ListadoActualizado$ = this.ListadoActualizadoSubject.asObservable();

  // dropdowns
  instrumentosArchivistivos$: Observable<ConstanteDTO[]> = of([]);
  tiposDisposicionFinal$: Observable<ConstanteDTO[]> = of([]);
  sedes$: Observable<ConstanteDTO[]> = of([]);
  dependencias: DependenciaDTO[] = [];
  ListadoSeries: SerieDTO[];
  ListadoSubseries: SubserieDTO[];
  versiones: VersionDTO[] = [];

  // generales
  UnidadDocumentalSeleccionada: UnidadDocumentalDTO;
  EsSubserieRequerido: boolean;
  NoUnidadesSeleccionadas = 'No hay unidades documentales seleccionadas';
  validations: any = {};
  subscribers: Array<Subscription> = [];

  ultimolistarPayload: any;
  ultimoList2Payload: any;
  ultimolistarDisposicionPayload: DisposicionFinalDTO = {};

  // gestionar unidad documental
  OpcionSeleccionada: number;
  AbrirDetalle: boolean = false;
  FechaExtremaFinal: Date;
  MensajeIngreseFechaExtremaFinal = 'Ingrese la fecha extrema final para proceder al cierre.';

  constructor(
    private unidadDocumentalApiService: UnidadDocumentalApiService,
    private serieSubserieApiService: SerieSubserieApiService,
    private _store: Store<State>,
    private _dependenciaSandbox: Sandbox,
    private _taskSandBox: TaskSandBox,
    private _processSandBox: ProccesSandbox,
    private confirmationService: ConfirmationService,
    private _appRef: ApplicationRef,
    private _dependenciaGrupoSandbox: DependenciaGrupoSandbox,
    private _dependenciaApiService: DependenciaApiService,
    private loading: LoadingService,
    private _router: Router,
    private messageService: MessageService,
    private versionApi: VersionApi
  ) {
    this.subscriptions.push(this._store.select(getAuthenticatedFuncionario).subscribe((funcionario) => {
      this.funcionarioLog = funcionario;
    }));
    this.loadInstrumentosArchivisticos();
  }

  loadInstrumentosArchivisticos() {
    const instrumentosArchivisticos: ConstanteDTO[] = [
      {
        id: InstrumentoArchivistivoEnum.TRD,
        codigo: InstrumentoArchivistivoEnum.TRD.toString(),
        nombre: 'TRD'
      },
      {
        id: InstrumentoArchivistivoEnum.TVD,
        codigo: InstrumentoArchivistivoEnum.TRD.toString(),
        nombre: 'TVD'
      }
    ];
    this.instrumentosArchivistivos$ = of(instrumentosArchivisticos);
  }

  GetDetalleUnidadUnidadDocumental(index: string) {
    const unidadDocumentalIndex = this.ListadoUnidadDocumental[index];
    this.unidadDocumentalApiService.GetDetalleUnidadDocumental(unidadDocumentalIndex.id)
      .subscribe(response => {
        this.UnidadDocumentalSeleccionada = response;
        this.AbrirDetalle = true;
        this.ListadoActualizadoSubject.next();
      });
  }

  GetListadoTiposDisposicion() {
    this.tiposDisposicionFinal$ = of([]);
  }

  GetListadoSedes() {
    this._store.dispatch(new SedeAdministrativaLoadAction());
    this.sedes$ = Utils.sortObservable(this._store.select(sedeDestinatarioEntradaSelector), 'nombre');
  }

  GetListadoDependencias(codSede: any) {
    this._dependenciaGrupoSandbox.loadData({codigo: codSede})
      .pipe(map(deps => deps.organigrama))
      .subscribe(resp => {
        this.dependencias = Utils.sort(resp, 'nombre');
      });
  }

  GetListadosSeries(codigodependencia): void {
    this.serieSubserieApiService.ListarSerieSubserie({
      idOrgOfc: codigodependencia,
    })
      .pipe(map(map => map.listaSerie))
      .subscribe(response => {
        this.ListadoSeries = Utils.sort(response, 'nombreSerie');
      });
  }

  GetSubSeries(serie, codigodependencia: string): Observable<SubserieDTO[]> {
    this.ListadoSubseries = [];
    return this.serieSubserieApiService.ListarSerieSubserie({
      idOrgOfc: codigodependencia,
      codSerie: serie.codigoSerie,
      nomSerie: serie.nombreSerie
    })
      .pipe(map(map => map.listaSubSerie))
  }


  CerrarDetalle() {
    this.AbrirDetalle = false;
    this.selectedIndex = null;
  }

  Listar(payload?: any) {
    this.ultimolistarPayload = payload;
    this.unidadesSeleccionadas = [];
    this.unidadDocumentalApiService.ListarUnidadesDocumentalesTranferir(payload)
      .subscribe(response => {

        if (!isNullOrUndefined(response)) {
          const unidadesDocumentales = response.unidadesDocumentales || [];
          let ListadoMapeado = unidadesDocumentales.length ? unidadesDocumentales : [];
          this.ListadoUnidadDocumental = [...ListadoMapeado];
          this.ListadoActualizadoSubject.next(response);
        }


      });
  }

  ListarUnidadesDocumentalesVerificar(payload) {

    this.unidadesSeleccionadas = [];
    this.ultimolistarPayload = payload;

    this.unidadDocumentalApiService.ListarUnidadesDocumentalesVerificar(payload)
      .subscribe(response => {
        let ListadoMapeado = response.length ? response : [];
        //this.ListadoUnidadDocumental = [...ListadoMapeado];
        this.ListadoUnidadDocumental = ListadoMapeado.map(unidad => {

          if (unidad.estado == 'Rechazado')
            unidad.blocked = true;
          return unidad;
        });
        this.ListadoActualizadoSubject.next();
      });

  }

  ListarUnidadesDocumentalesUbicar(payload) {

    this.unidadesSeleccionadas = [];
    this.ultimolistarPayload = payload;
    this.unidadDocumentalApiService.ListarUnidadesDocumentalesUbicar(payload)
      .subscribe(response => {
        let ListadoMapeado = response.length ? response : [];
        this.ListadoUnidadDocumental = [...ListadoMapeado];
        this.ListadoActualizadoSubject.next();
      });

  }

  ListarForGestionarUnidadDocumental(payload?: any, ignoreHint?: boolean) {
    const noShowNoFoundNotification = payload.noShowNoFoundNotification;
    delete payload.noShowNoFoundNotification;
    this.ultimoList2Payload = payload;
    this.unidadesSeleccionadas = [];
    this.unidadDocumentalApiService.buscarUnidadesDocumentales(payload)
      .subscribe(response => {
        let ListadoMapeado = response.length ? response : [];
        ListadoMapeado.forEach(unidad => {
          unidad.canSelectTrd = !unidad.versionTrdCierre
        });
        this.ListadoUnidadDocumental = [...ListadoMapeado];

        if (isNullOrUndefined(ignoreHint)) {
          if (this.ListadoUnidadDocumental.length == 0 && !noShowNoFoundNotification)
            this._store.dispatch(new PushNotificationAction({
              severity: 'info',
              summary: 'El sistema no encuentra la unidad documental que está buscando. Verifique los criterios de búsqueda'
            }));
        }
        ;

        this.ListadoActualizadoSubject.next();
      });
  }

  ListarDisposicionFinal(payload?: DisposicionFinalDTO, value?: any) {
    let ListadoMapeado: any = [];
    this.ultimolistarDisposicionPayload = payload;
    this.unidadesSeleccionadas = [];
    this.unidadDocumentalApiService.listarUnidadesDocumentalesDisposicion({...this.ultimolistarDisposicionPayload})
      .subscribe(response => {
        if (response.length) {
        response.forEach(val => {
          if(val.estado == "Rechazado" ){
            val.estadoDispo = val.estado;
          }
          ListadoMapeado.push(val)
        });
        }
         ListadoMapeado = ListadoMapeado.length ? ListadoMapeado : [];
        this.ListadoUnidadDocumental = [...ListadoMapeado];
        this.ListadoActualizadoSubject.next();
      });
  }

  listarDescripcionArchivistica(payload: any) {
    this.ultimolistarDisposicionPayload = payload;
    this.UnidadDocumentalSeleccionada = undefined;
    this.ListadoUnidadDocumental$ = this.unidadDocumentalApiService.listarUnidDocDescripcionArchivistica(this.ultimolistarDisposicionPayload).pipe(map((response: any) => {
      let listadoMapeado = response.response ? response.response.unidadesDocumentales : [];
      this.ListadoUnidadDocumental = listadoMapeado;
      this.ListadoActualizadoSubject.next();
      return this.ListadoUnidadDocumental;
    }));
  }

  listarDescripcionesArchivisticasNoUD(payload: any) {
    this.ultimolistarDisposicionPayload = payload;
    this.UnidadDocumentalSeleccionada = undefined;
    this.ListadoUnidadDocumental$ = this.unidadDocumentalApiService.listarDescripcionesArchivisticas(this.ultimolistarDisposicionPayload).pipe(map((response: any) => {
      this.ListadoUnidadDocumental = response;
      this.ListadoActualizadoSubject.next();
      return this.ListadoUnidadDocumental;
    }));
  }

  getDocumentaryUnitDescription(idUnidDoc): Observable<any> {
    return this.unidadDocumentalApiService.getDocumentaryUnitDescription(idUnidDoc);
  }

  Agregar() {
    const unidadesSeleccionadas = this.GetUnidadesSeleccionadas();
    if (unidadesSeleccionadas.length) {
      const item = this.unidadesSeleccionadas.filter(_item => _item.fechaCierre === null);
      const listadoMapeado = this.ListadoUnidadDocumental
        .reduce((listado, currenvalue: any) => {
          item.forEach(value => {
            if (value === currenvalue) {
              currenvalue.fechaExtremaFinal = this.FechaExtremaFinal.getTime();
              this._store.dispatch(new PushNotificationAction({
                severity: 'success',
                summary: 'La Fecha extrema final se actualizo correctamente. '
              }));
            }
          });
          listado.push(currenvalue);
          return listado;
        }, []);
      this.ListadoUnidadDocumental = [...listadoMapeado];
    }
  }

  GetUnidadesSeleccionadas(): UnidadDocumentalDTO[] {
    const ListadoFiltrado = this.unidadesSeleccionadas;
    if (!ListadoFiltrado.length) {
      this._store.dispatch(new PushNotificationAction({severity: 'info', summary: this.NoUnidadesSeleccionadas}));
    }
    return ListadoFiltrado;
  }

  Abrir() {
    const unidadesSeleccionadas = this.GetUnidadesSeleccionadas();
    if (unidadesSeleccionadas.length) {
      const payload: any = unidadesSeleccionadas.map(_map => {
        return {id: _map.id, accion: 'abrir', soporte: _map.soporte}
      });
      this.ultimoList2Payload.noShowNoFoundNotification = true;
      this.GestionarUnidadesDocumentales(payload, "43", unidadesSeleccionadas);
    }
  }

  Cerrar() {
    const unidadesSeleccionadas: UnidadDocumentalDTO[] = [...this.GetUnidadesSeleccionadas()];
    let payload = {};
    if (unidadesSeleccionadas.length) {
      if (unidadesSeleccionadas.find(unidadRes => !unidadRes.versionTrdCierre)) {
        this._store.dispatch(new PushNotificationAction({
          severity: 'info',
          summary: ERROR_TRD
        }));
        return;
      }
      const pendienteFechaYSoporte = unidadesSeleccionadas.find(item => (item.fechaExtremaFinal === null || item.soporte !== 'Físico'));
      this.ultimoList2Payload.noShowNoFoundNotification = true;
      if (pendienteFechaYSoporte) {
        this.confirmationService.confirm({
          message: `<p style="text-align: center">¿Desea que la fecha extrema final sea la misma fecha del cierre del trámite Aceptar / Cancelar. ?</p>`,
          accept: () => {
            payload = unidadesSeleccionadas.reduce((listado, current) => {
              const item = {
                id: current.id,
                fechaExtremaFinal: (current.fechaExtremaFinal === null && current.soporte === 'Físico') ? current.fechaCierre : current.fechaExtremaFinal,
                accion: 'cerrar',
                soporte: current.soporte,
                trdCierre: current.trdCierre,
                versionTrdCierre: current.versionTrdCierre,
                dataTrdCierre: current.versionesTrd.find(version => version.title == current.versionTrdCierre).id,
                fechaFinal: new Date(current.fechaExtremaFinal).toLocaleString().split(' ')[0]
              };

              listado.push(item);
              return listado;
            }, []);
            this.SubmitCerrar(payload, unidadesSeleccionadas)
          },
          reject: () => {
            this._store.dispatch(new PushNotificationAction({
              severity: 'info',
              summary: this.MensajeIngreseFechaExtremaFinal
            }));
          }
        });
      } else {
        this.confirmationService.confirm({
          message: `<p style="text-align: center">¿Desea que la fecha extrema final sea la misma fecha del cierre del trámite Aceptar / Cancelar. ?</p>`,
          accept: () => {
            payload = unidadesSeleccionadas.map(_map => {
              return {
                id: _map.id,
                accion: 'cerrar',
                soporte: _map.soporte,
                trdCierre: _map.trdCierre,
                versionTrdCierre: _map.versionTrdCierre,
                dataTrdCierre: _map.versionesTrd.find(version => version.title == _map.versionTrdCierre).id,
                fechaFinal: new Date(_map.fechaExtremaFinal).toLocaleString().split(' ')[0]
              }
            });

            this.SubmitCerrar(payload, unidadesSeleccionadas);
          }, reject: () => {
            this._store.dispatch(new PushNotificationAction({
              severity: 'info',
              summary: this.MensajeIngreseFechaExtremaFinal
            }));
          }
        });
      }
    }
  }

  SubmitCerrar(payload: any, unidadesDocumentales: UnidadDocumentalDTO[]) {
    this.GestionarUnidadesDocumentales(payload, "42", unidadesDocumentales, 'Cerrar');
  }

  Reactivar() {
    const unidadesSeleccionadas = this.GetUnidadesSeleccionadas();
    if (unidadesSeleccionadas.length) {
      const payload: any = unidadesSeleccionadas.map(_map => {
        return {id: _map.id, accion: 'reactivar', soporte: _map.soporte}
      });
      payload.noShowNoFoundNotification = true;
      this.GestionarUnidadesDocumentales(payload, "44", unidadesSeleccionadas);
    }
  }

  GestionarUnidadesDocumentales(payload: any, trackStateId: string, unidadesDocumentales: UnidadDocumentalDTO[], action?: string) {
    this.unidadDocumentalApiService.gestionarUnidadesDocumentales(payload)
      .subscribe(response => {
        if (action === 'Cerrar') {
          this.addObservations(unidadesDocumentales);
        }
        this.ManageActionResponse2(response);
        this.setStatusTrack(this.getPayloadStatus(trackStateId, unidadesDocumentales));
      });
  }

  addObservations(unidadesDocumentales: UnidadDocumentalDTO[]) {
    let payload = [];
    this.unidadesSeleccionadas.forEach(unidad => {
      if (!isNullOrUndefined(unidad.observaciones)) {
        payload.push({
          date: moment().format('DD/MM/YYYY'),
          nameUser: this.funcionarioLog.loginName,
          observation: unidad.observaciones,
          objectId: unidad.id,
          type: 'unidad documental',
          process: 3
        });
      }
    });
    this.unidadDocumentalApiService.addObservations(payload).subscribe();
  }

  getPayloadStatus(trackStateId: string, unidadesSeleccionadas: UnidadDocumentalDTO[]) {
    let payload: any[] = [];
    unidadesSeleccionadas.forEach(track => {
      payload.push({
        objectId: track.id,
        objectType: 'Unidad documental',
        user: this.funcionarioLog.loginName,
        trackStateId: trackStateId,
        subsection: track.codigoDependencia
      });
    });
    return payload;
  }

  setStatusTrack(payload) {
    this.unidadDocumentalApiService.setStatusTrazability(payload).subscribe();
  }

  AplicarDisposicion(tipodisposicion: string) {

    const unidadesSeleccionadas = this.GetUnidadesSeleccionadas();
    const existeSeleccionar = this.unidadesSeleccionadas.find(_item => _item.disposicion === 'S');
    if (unidadesSeleccionadas.length) {
      if (existeSeleccionar) {
        this.ListadoUnidadDocumental = this.ListadoUnidadDocumental.reduce((_listado, _current) => {
          const item_seleccionado = this.unidadesSeleccionadas.find(_item => _item.id === _current.id && _item.disposicion === 'S')
          _current.disposicion = item_seleccionado ? tipodisposicion : _current.disposicion;
          _listado.push(_current);
          return _listado;
        }, []);
        this.ListadoActualizadoSubject.next();
        this._store.dispatch(new PushNotificationAction({
          severity: 'success',
          summary: 'La disposición final  ha sido aplicada exitosamente.'
        }));
      } else {
        this._store.dispatch(new PushNotificationAction({
          severity: 'info',
          summary: 'No hay unidades documentales seleccionadas con disposición final.  Seleccione alguna.'
        }));

      }
    }
  }

  ActualizarEstadoUD(estado: string, isDisposicion: boolean = false, disposicion?: string) {
    const unidadesSeleccionadas = this.GetUnidadesSeleccionadas();
    const requiereObservaciones = unidadesSeleccionadas.some(_item => (_item.observaciones === '' || _item.observaciones === null) && estado === 'Rechazado');

    if (!isNullOrUndefined(disposicion) && unidadesSeleccionadas.length) {
      let alerta: string, component: string;
      switch (disposicion) {
        case  'E':
          component = 'eliminar';
          break;
        case 'CT':
          component = 'conservar totalmente';
          break;
      }
      if (!isNullOrUndefined(component)) {
        alerta = '¿Está seguro que desea ' + component + ' la(s) unidad(es) documental(es) seleccionada(s)?';
        this.confirmationService.confirm({
          message: alerta,
          header: 'Confirmación',
          icon: 'fa fa-question-circle',
          accept: () => {
            this.ActualizarEstadoUD(estado, isDisposicion);
          },
          reject: () => {
          }
        });
        return;
      }
    }

    if (unidadesSeleccionadas.length) {
      if (requiereObservaciones) {
        this._store.dispatch(new PushNotificationAction({
          severity: 'info',
          summary: 'Hay unidades documentales pendiente de notas.'
        }));
      } else if (isDisposicion && unidadesSeleccionadas.some(_item => _item.disposicion === 'S')) {
        this._store.dispatch(new PushNotificationAction({
          severity: 'info',
          summary: 'Hay unidades documentales con disposición final "Seleccionar". Recuerde actualizarlas.'
        }));
      } else {
        this.ListadoUnidadDocumental = this.ListadoUnidadDocumental.reduce((_listado, _current) => {
          const item_seleccionado = this.unidadesSeleccionadas.find(_item => _item === _current);
          _current.estado = item_seleccionado ? estado : _current.estado;
          _current.estadoDispo = item_seleccionado ? estado : _current.estadoDispo;
          _listado.push(_current);
          return _listado;
        }, []);


        const data = {datetime: new Date()};
        this.ListadoActualizadoSubject.next(data);

        this._store.dispatch(new PushNotificationAction({
          severity: 'success',
          summary: 'El estado de la unidad documental ha sido actualizado exitosamente.'
        }));

      }
    }
  }


  ActualizarDisposicionFinal(): void {

    const listado = this.ListadoUnidadDocumental
      .map(unidad => {
        delete unidad['_$visited'];
        return unidad
      })
      .filter(unidad => unidad.estado === 'Aprobado' || unidad.estado === 'Rechazado');


    if (listado.length == 0) {

      this._store.dispatch(new PushNotificationAction({
        severity: 'info',
        summary: 'Debe aprobar o rechazar alguna unidad documental.'
      }));
      return;
    }

    if (listado.some(unidad => unidad.estado == 'Rechazado' && !unidad.observaciones)) {
      this._store.dispatch(new PushNotificationAction({
        severity: 'info',
        summary: 'Verifique que las unidades rechazadas tengan su observación.'
      }));
      return;
    }


    this.unidadDocumentalApiService.aprobarRechazarUDDisposicion(listado, this.ultimolistarDisposicionPayload.tipificador)
      .subscribe(response => {

        if (isNullOrUndefined(this.ultimolistarDisposicionPayload))
          this.ultimolistarDisposicionPayload = {};

        this.ultimolistarDisposicionPayload.noShowNoFoundNotification = true;
        this.ManageActionResponseForDisposicionFinal(response);
        if (response.codMensaje === '0000') {

          this.subscribers.push(this._store.select(getSelectedDependencyGroupFuncionario)
            .subscribe(dependency => {

              if (listado.some(u => u.disposicion == "CT" && u.estado == 'Aprobado')) {

                const first = listado[0];
                const proceso: ProcesoDTO = {
                  codigoProceso: 'proceso.transferencia-documentales',
                  nombreProceso: 'transferencia-documentales',
                  idDespliegue: ID_DESPLIEGUE,
                  estado: 'LISTO',
                  customParams: {
                    soporte: listado.find(unid => unid.soporte == FISICO || unid.soporte == HIBRIDA) ? true : false,
                    megaf: 1,
                    nombreDependencia: dependency.nombre,
                    dependencia: first.codigoDependencia,
                    tipificador: this.ultimolistarDisposicionPayload.tipificador
                  }
                };

                this._processSandBox.startProcess(proceso, dependency)
                  .subscribe(_ => {
                    this._router.navigate(['bussiness/' + ROUTES_PATH.workspace]);
                  }, error => {
                    this._store.dispatch(new actions.LoadFailAction({error}))
                  });

                return;
              }

              this._router.navigate(['bussiness/' + ROUTES_PATH.workspace]);

            }))
        }

      });
  }

  ActualizarAprobarTransferencia(tipoTransferencia: string, instrumentoArchivistivo: number): Observable<any> {

    const estado = tipoTransferencia == 'primaria' ? 'Aprobado' : 'Ubicada';

    const listado = this.ListadoUnidadDocumental
      .map(unidad => {
        delete unidad['_$visited'];
        return unidad
      })
      .filter(unidad => unidad.estado == estado || unidad.estado == 'Rechazado');

    if (listado.length === 0) {

      this._store.dispatch(new PushNotificationAction({
        severity: "info",
        summary: "Debe aprobar o rechazar todas las unidades seleccionadas"
      }))
      return new EmptyObservable();
    }
    return this.unidadDocumentalApiService.aprobarRechazarUDAprobarTransferencia(tipoTransferencia, listado, instrumentoArchivistivo)
    //.do(response =>  this.ManageActionResponse(response));

  }

  ActualizarVerificarTransferencia(): Observable<any> {

    const listadoVerificar = this.ListadoUnidadDocumental.filter(unidad => !unidad.blocked)
      .map(unidad => {
        delete unidad['_$visited'];
        return unidad
      })
      .filter(unidad => (unidad.estado == 'Confirmada' || unidad.estado == 'Rechazado') && !unidad.blocked);

    if (listadoVerificar.length === 0) {

      this._store.dispatch(new PushNotificationAction({
        severity: "info",
        summary: "Debe aprobar o rechazar todas las unidades seleccionadas"
      }))
      return new EmptyObservable();
    }

    return this.unidadDocumentalApiService.aprobarRechazarUDVerificarTransferencia(listadoVerificar)
    // .do(response => this.ManageActionResponseVerificar(response))

  }

  ActualizarUbicarTransferencia(): Observable<any> {

    const listadoVerificar = this.ListadoUnidadDocumental.filter(unidad => unidad.estado == 'Ubicada')
      .map(unidad => {
        delete unidad['_$visited'];

        if (!isNullOrUndefined(unidad.unidadesConservacion)) {
          unidad.unidadesConservacion = unidad.unidadesConservacion.map(uc => {
            delete (<any>uc)._$visited;
            uc.ideBodega = uc.ideBodega.toString().split('-')[0];
            uc.ideUbicFisiBod = uc.ideUbicFisiBod.toString().split('-')[0];
            return uc;
          });
        }
        return unidad;
      });

    if (listadoVerificar.length === 0) {

      this._store.dispatch(new PushNotificationAction({
        severity: "info",
        summary: "Debe aprobar o rechazar todas las unidades seleccionadas"
      }));
      return new EmptyObservable();
    }

    return this.unidadDocumentalApiService.aprobarRechazarUDVerificarTransferencia(listadoVerificar)
    // .do(response => this.ManageActionResponseUbicar(response))

  }

  GuardarObservacion(unidadDocumental: UnidadDocumentalDTO, index: number) {
    this.ListadoUnidadDocumental[index] = unidadDocumental;
  }

  ManageActionResponse(response: MensajeRespuestaDTO) {
    const mensajeRespuesta: MensajeRespuestaDTO = response;
    const mensajeSeverity = (mensajeRespuesta.codMensaje === '0000') ? 'success' : 'error';
    this._store.dispatch(new PushNotificationAction({
      severity: mensajeSeverity,
      summary: mensajeRespuesta.mensaje
    }));
    this.Listar(this.ultimolistarPayload);
  }


  ManageActionResponseVerificar(response: MensajeRespuestaDTO) {
    const mensajeRespuesta: MensajeRespuestaDTO = response;
    const mensajeSeverity = (mensajeRespuesta.codMensaje === '0000') ? 'success' : 'error';
    this._store.dispatch(new PushNotificationAction({
      severity: mensajeSeverity,
      summary: mensajeRespuesta.mensaje
    }));
    this.ListarUnidadesDocumentalesVerificar(this.ultimolistarPayload);
  }

  ManageActionResponseUbicar(response: MensajeRespuestaDTO) {
    const mensajeRespuesta: MensajeRespuestaDTO = response;
    const mensajeSeverity = (mensajeRespuesta.codMensaje === '0000') ? 'success' : 'error';
    this._store.dispatch(new PushNotificationAction({
      severity: mensajeSeverity,
      summary: mensajeRespuesta.mensaje
    }));
    this.ListarUnidadesDocumentalesUbicar(this.ultimolistarPayload);
  }


  ManageActionResponse2(response: MensajeRespuestaDTO) {
    const mensajeRespuesta: MensajeRespuestaDTO = response;
    const mensajeSeverity = (mensajeRespuesta.codMensaje === '0000') ? 'success' : 'error';
    this._store.dispatch(new PushNotificationAction({
      severity: mensajeSeverity,
      summary: mensajeRespuesta.mensaje
    }));
    this.ListarForGestionarUnidadDocumental(this.ultimoList2Payload, true);
  }

  ManageActionResponseForDisposicionFinal(response: MensajeRespuestaDTO) {
    const mensajeRespuesta: MensajeRespuestaDTO = response;
    const mensajeSeverity = (mensajeRespuesta.codMensaje === '0000') ? 'success' : 'error';
    if (!this.ultimolistarDisposicionPayload.noShowNoFoundNotification)
      this._store.dispatch(new PushNotificationAction({
        severity: mensajeSeverity,
        summary: mensajeRespuesta.mensaje
      }));

    delete this.ultimolistarDisposicionPayload.noShowNoFoundNotification;
    this.ListarDisposicionFinal(this.ultimolistarDisposicionPayload);
  }


  exportXmlDescription(idUnidDoc) {
    return this.unidadDocumentalApiService.exportXmlDescription(idUnidDoc);
  }

  loadSubseries(codigoSerie: string, codigodependencia: string) {
    this.GetSubSeries({codigoSerie: codigoSerie}, codigodependencia)
      .subscribe(result => {
        this.ListadoSubseries =  Utils.sort(result, 'nombreSubSerie');
      });
  }

  loadSubseriesTVD(codigoSerie: string, codigoDependenciaProductora: string) {
    const serie = this.findSerieByCodigo(codigoSerie);
    return this.serieSubserieApiService.getSubseriesTvd(serie.idSerie, codigoDependenciaProductora)
      .subscribe((subseries) => (this.ListadoSubseries =  Utils.sort(subseries, 'nombreSubSerie')));
  }

  findSerieByCodigo(codigoSerie: string): SerieDTO {
    return this.ListadoSeries.find(serie => serie.codigoSerie === codigoSerie);
  }

  loadSeriesTvd(codigoDependenciaProductora: string) {
    this.serieSubserieApiService.getSeriesTvd(codigoDependenciaProductora)
      .subscribe(series => (this.ListadoSeries = Utils.sort(series, 'nombreSerie')));
  }

  loadDependenciasProductorasTvd(codigoDepedenciaAdministrativa: string) {
    this._dependenciaGrupoSandbox.getDependenciasProductorasTVD(codigoDepedenciaAdministrativa)
      .subscribe(dependencias => {
        this.dependencias = Utils.sort(dependencias, 'nombre')
      });
  }

  loadDependenciasAdministrativasTvd() {
    this.sedes$ = Utils.sortObservable(this._dependenciaGrupoSandbox.getDependenciasAdministrativasTVD(), 'nombre');
  }

  loadVersionedSeries(codigoDepedencia: string, idVersion?): void {
    this.findVersionedSeries(codigoDepedencia, idVersion)
      .subscribe(series => this.ListadoSeries = Utils.sort(series, 'nombreSerie'));
  }

  loadVersionedSubseries(codigoDepedencia: string, codigoSerie: string, idVersion?): void {
    const serie = this.findSerieByCodigo(codigoSerie);
    this.findVersionedSubseries(codigoDepedencia, serie.idSerie, idVersion).subscribe(subseries => {
      this.ListadoSubseries = Utils.sort(subseries, 'nombreSubSerie');
    });
  }

  private findVersionedSeries(codigoDepedencia: string, idVersion?): Observable<Array<SerieDTO>> {
    return this.serieSubserieApiService.findVersionedSeries(codigoDepedencia, idVersion);
  }

  findVersionedSubseries(codigoDepedencia: string, idSerie: number, idVersion?): Observable<Array<SubserieDTO>> {
    return this.serieSubserieApiService.findVersionedSubseries(codigoDepedencia, idSerie, idVersion);
  }

  loadVersiones(codigoDependeciaProductora: string): void {
    this.versionApi.findVersiones(codigoDependeciaProductora)
      .subscribe(versiones => {
        this.versiones = Utils.sort(versiones, 'valVersion');
      })
  }
}

