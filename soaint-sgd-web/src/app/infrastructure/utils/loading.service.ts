import {EventEmitter, Injectable} from '@angular/core';
import {Sandbox as NotificationsService} from "../state-management/notifications-state/notifications-sandbox";
import {PushNotificationAction} from "../state-management/notifications-state/notifications-actions";
import {Store} from "@ngrx/store";
import {State} from "../redux-store/redux-reducers";

@Injectable()
export class LoadingService {

  private loading: EventEmitter<boolean> = new EventEmitter(true);

  constructor(private _notify: NotificationsService, private _store: Store<State>) {
  }

  presentLoading() {
    this.loading.next(true);
  }

  dismissLoading() {
    this.loading.next(false);
  }

  getLoaderAsObservable() {
    return this.loading.asObservable();
  }
}
