import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {State} from './../redux-store/redux-reducers';
import {HttpResponse, HttpHeaders, HttpClient, HttpRequest} from '@angular/common/http';
import {isNullOrUndefined} from 'util';
import {map, take, switchMap} from 'rxjs/operators';


@Injectable()
export class HttpHandler {

  private token$: Observable<string>;

  constructor(private _http: HttpClient, private _store: Store<State>) {
    this.token$ = _store.select(s => s.auth.token);
  }

  requestHelper(url: any, method: string, options?: any): Observable<HttpResponse<any>> {


    return this.token$.pipe(take(1), switchMap(token => {
      // console.log('Calling protected URL ...', token);

      options = options || {responseType: 'json', observe: 'events'};
      options.headers = options.headers || new HttpHeaders();
      if (!options.headers.has('Content-Type'))
        options.headers = options.headers.append('Content-Type', 'application/json');
      if (isNullOrUndefined(options.headers))
        options.headers = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token})
      else if (token !== null && !options.headers.has('Authorization')) {
        options.headers = options.headers.append('Authorization', 'Bearer ' + token);
      }

      if (options.body && typeof options.body !== 'string') {
        options.body = JSON.stringify(options.body);
      }


      const request$ = this._http.request(method, url, options);

      return this.handleResponse(request$);

    }));


  }

  uploadHelper(url: string, options?: any): Observable<HttpResponse<any>> {
    return this.token$.pipe(take(1), switchMap(token => {
      options = options || {responseType: 'blob', observe: 'response'};
      options.headers = options.headers || new HttpHeaders();
      options.headers = options.headers.append('Accept', 'application/json');

      if (token !== null) {
        options.headers = options.headers.append('Authorization', 'Bearer ' + token);
      }

      const request$ = this._http.request('post', url, options);
      return this.handleResponse(request$);
    }));
  }

  handleResponse(request$: Observable<HttpResponse<any> | ArrayBuffer>): any {


    return request$.pipe(map((res?: any) => {
      if (isNullOrUndefined(res)) {
        return
      }
      if (isNullOrUndefined(res.headers)) {
        return res;
      }

      const type = res.headers.get('content-type').split(';')[0];

      if ('application/json' === type) {
        return res.body;
      }
      return res;
    }));
  }


  public get(url: string, params: any, options?: any): Observable<HttpResponse<any>> {

    options = options || {observe: 'response', responseType: 'json'};

    options.params = params;

    return this.requestHelper(url, 'get', options);
  }

  public post(url: string, body: any, options?: any): Observable<HttpResponse<any>> {

    options = options || {observe: 'response', responseType: 'json'};

    options.body = body;

    return this.requestHelper(url, 'post', options);
  }

  public put(url: string, body: any, options ?: any): Observable<HttpResponse<any>> {

    options = options || {observe: 'response', responseType: 'json'};

    options.body = body;

    return this.requestHelper(url, 'put', options);
  }

  public delete(url: string, body: any, options ?: any): Observable<HttpResponse<any>> {
    options = options || {observe: 'response', responseType: 'json'};

    options.body = body;
    return this.requestHelper(url, 'delete', options);
  }

  public patch(url: string, body: any, options?: any): Observable<HttpResponse<any>> {

    options = options || {observe: 'response', responseType: 'json'};

    options.body = body;

    return this.requestHelper(url, 'patch', options);
  }

  public putFile(url: string, body: any, options ?: any): Observable<HttpResponse<any>> {

    options = options || {observe: 'response', responseType: 'json'};

    options.body = body;

    return this.uploadHelper(url, options);
  }


}
