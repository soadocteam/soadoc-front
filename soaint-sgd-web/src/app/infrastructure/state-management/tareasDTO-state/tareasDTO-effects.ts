import {EventEmitter, Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import * as actions from './tareasDTO-actions';
import {Sandbox} from './tareasDTO-sandbox';
import {State as RootState} from '../../../infrastructure/redux-store/redux-reducers';
import {LoadTasksInsideProcessAction} from '../procesoDTO-state/procesoDTO-actions';
import {tassign} from 'tassign';
import {ROUTES_PATH} from '../../../app.route-names';
import {getSelectedDependencyGroupFuncionario} from '../funcionarioDTO-state/funcionarioDTO-selectors';
import {catchError, distinctUntilChanged, flatMap, map, mergeMap, switchMap, tap, withLatestFrom} from 'rxjs/operators';
import {Router} from '@angular/router';
import {of, timer} from 'rxjs';
import {TareaDTO} from '../../../domain/tareaDTO';
import {PushNotificationAction} from "../notifications-state/notifications-actions";
import {RETRY} from "../../../shared/bussiness-properties/error-messages.properties";
import {LoadingService} from "../../utils/loading.service";
import {HttpHeaders} from "@angular/common/http";

export const startTaskSuccesEvent: EventEmitter<any> = new EventEmitter;

@Injectable()
export class Effects {

  contador = 0;

  constructor(private actions$: Actions, private loadingService: LoadingService,
              private _store$: Store<RootState>,
              private _sandbox: Sandbox,
              private _router: Router) {

  }

  @Effect()
  load: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.LOAD)
    , distinctUntilChanged()
    , map(action => action['payload'])
    , withLatestFrom(this._store$.select(getSelectedDependencyGroupFuncionario))
    , switchMap(
      ([payload, dependency]) => this._sandbox.loadData(payload, dependency)
        .pipe(map((response) => new actions.LoadSuccessAction(response)),
          catchError((error) => of(new actions.LoadFailAction({error})))
        )));

  @Effect()
  getStats: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.GET_TASK_STATS)
    , map(action => action['payload'])
    , switchMap(
      () => this._sandbox.getTaskStats()
        .pipe(map((response) => new actions.GetTaskStatsSuccessAction(response)),
          catchError((error) => of(new actions.LoadFailAction({error})))
        )));

  @Effect()
  startTask: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.START_TASK)
    , map(action => action['payload'])
    , switchMap(
      (payload) => this._sandbox.getTaskVariables(payload)
        .pipe(mergeMap((taskVariables) => {
            return this._sandbox.startTask(payload)
              .pipe(map(
                (res) => Object.assign({}, res, {variables: taskVariables})
              ))
          })
          , map((response: any) => {
            this.contador = 0;
            return new actions.StartTaskSuccessAction(response)
          })
          , tap(_ => startTaskSuccesEvent.emit())
          , catchError((error) => {
            this.contador++;
            this.retryStartTask(payload);
            return of(new actions.StartTaskFailAction({error}));
          }))
    ));

  retryStartTask(payload) {
    if (this.contador <= 3) {
      this.loadingService.presentLoading();
      timer(3000).subscribe(time => {
        this._store$.dispatch(new actions.StartTaskAction(payload));
      });
    } else {
      this.contador = 0;
      this.loadingService.dismissLoading();
      this._store$.dispatch(new PushNotificationAction({
        severity: 'info',
        summary: RETRY
      }))
    }
  }

  @Effect()
  reserveTask: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.RESERVE_TASK)
    , map(action => action['payload'])
    , switchMap(
      (payload) => this._sandbox.getTaskVariables(payload)
        .pipe(mergeMap((taskVariables) => this._sandbox.reserveTask(payload)
            .pipe(map((res) => Object.assign({}, res, {variables: taskVariables})
            )))
          , map((response: any) => new actions.StartTaskSuccessAction(tassign(payload, {
            estado: response.estado,
            variables: response.variables
          })))
          , catchError((error) => of(new actions.StartTaskFailAction({error})))
        )));

  @Effect()
  takeInProgressTask: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.START_INPROGRESS_TASK)
    , map(action => action['payload'])
    , switchMap(
      (payload) => this._sandbox.getTaskVariables(payload)
        .pipe(mergeMap((taskVariables: any) => [
            new actions.LockActiveTaskAction(Object.assign({}, payload, {variables: taskVariables})),
            new actions.StartTaskSuccessAction(Object.assign({}, payload, {variables: taskVariables}))
          ])
          , catchError((error) => of(new actions.StartTaskFailAction({error})))
        )));

  @Effect({dispatch: false})
  startSuccesstask: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.START_TASK_SUCCESS)
    , map(action => action['payload'])
    , tap(payload => {

      localStorage.setItem('activeTask', JSON.stringify(payload));
    }));


  @Effect({dispatch: false})
  upViewRelatedToTask: Observable<TareaDTO> = this.actions$.pipe(
    ofType(actions.ActionTypes.START_TASK_SUCCESS)
    , map(action => action['payload'])
    , tap((payload: TareaDTO) => {
      -
        this._sandbox.taskRoutingStart();
      this._sandbox.initTaskDispatch(payload)
    }));


  @Effect()
  goToNextTask: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.CONTINUE_WITH_NEXT_TASK)
    , withLatestFrom(this._store$.select(s => s.tareas.nextTask))
    , flatMap(([payload, nextTask]) => [new actions.UnlockActiveTaskAction(), new LoadTasksInsideProcessAction(nextTask)]));


  @Effect()
  completeTask: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.COMPLETE_TASK)
    , map(action => action['payload'])
    , switchMap(
      (payload) => {
        let aprobar = payload.aprobar ? payload.aprobar : false;
        return this._sandbox.completeTask(payload)
          .pipe(map((response: any) => {
              this.contador = 0;
              return new actions.CompleteTaskSuccessAction(aprobar)
            })
            , catchError((error) => {
              this.contador++;
              this.retryCompleteTask(payload);
              return of(new actions.CompleteTaskFailAction({error}))
            })
          )
      }));

  retryCompleteTask(payload) {
    if (this.contador <= 3) {
      this.loadingService.presentLoading();
      timer(3000).subscribe(time => {
        let options: any = {
          headers: new HttpHeaders().append('skipLoading', 'true')
        };
        payload.optionsHeader = options;
        this._store$.dispatch(new actions.CompleteTaskAction(payload));
      });
    } else {
      this.contador = 0;
      this.loadingService.dismissLoading();
      this._store$.dispatch(new PushNotificationAction({
        severity: 'info',
        summary: RETRY
      }))
    }
  }

  @Effect({dispatch: false})
  completeSuccessTask: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.COMPLETE_TASK_SUCCESS)
    , map(action => action['payload'])
    , tap(_ => {
      localStorage.removeItem('activeTask');
    }));


  @Effect()
  completeBackTask: Observable<Action> = <Observable<Action>>this.actions$.pipe(
    ofType(actions.ActionTypes.COMPLETE_BACK_TASK)
    , map(action => action['payload'])
    , switchMap(
      (payload) => this._sandbox.completeTask(payload)
        .pipe(mergeMap((response: any) => [new actions.CompleteBackTaskSuccessAction(response[0])]),
          tap(() => {
            return this._router.navigate(['bussiness/' + ROUTES_PATH.workspace])
          })
          , catchError((error) => of(new actions.CompleteBackTaskFailAction({error})))
        )));


  @Effect()
  abortTask: Observable<Action> = <Observable<Action>>this.actions$.pipe(
    ofType(actions.ActionTypes.ABORT_TASK)
    , map(action => action['payload'])
    , switchMap(
      (payload) => this._sandbox.abortTask(payload)
        .pipe(mergeMap((response: any) => [new actions.AbortTaskSuccessAction(response)]),
          tap(() => {
            return this._router.navigate(['bussiness/' + ROUTES_PATH.workspace])
          })
          , catchError((error) => of(new actions.AbortTaskFailAction({error})))
        )));


}
