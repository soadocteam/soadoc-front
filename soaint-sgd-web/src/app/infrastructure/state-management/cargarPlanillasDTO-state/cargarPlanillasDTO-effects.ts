import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/pairwise';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/let';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/distinctUntilChanged';
import * as actions from './cargarPlanillasDTO-actions';
import {Sandbox} from './cargarPlanillasDTO-sandbox';
import {State as RootState} from '../../../infrastructure/redux-store/redux-reducers';
import {getSelectedDependencyGroupFuncionario} from '../../../infrastructure/state-management/funcionarioDTO-state/funcionarioDTO-selectors';
import {map, distinctUntilChanged, withLatestFrom, switchMap, catchError} from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

@Injectable()
export class Effects {

  constructor(private actions$: Actions,
              private _store$: Store<RootState>,
              private _sandbox: Sandbox) {
  }

  @Effect()
  load: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.LOAD)
    ,map( action => action['payload'] )
    ,distinctUntilChanged()
    ,withLatestFrom(this._store$.select(getSelectedDependencyGroupFuncionario))
    ,distinctUntilChanged()
    ,switchMap(
      ([payload, state]) => {
        return this._sandbox.loadData(payload)
          .pipe(map((response) => new actions.LoadSuccessAction(response))
          ,catchError((error) => of(new actions.LoadFailAction({error}))
          ))
      }
    ));

  @Effect()
  reload: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.RELOAD)
    ,distinctUntilChanged()
    ,withLatestFrom(this._store$.select((state) => state.distribucionFisica.filters), this._store$.select(getSelectedDependencyGroupFuncionario))
    ,switchMap(
      ([payload, filters, authFuncionarioDependencySelected]) => {
        const request = Object.assign({}, filters, {
          cod_dependencia: authFuncionarioDependencySelected.codigo
        });
        return this._sandbox.loadData(request)
          .pipe(map((response) => new actions.LoadSuccessAction(response))
          ,catchError((error) => of(new actions.LoadFailAction({error}))
          ))
      }
    ));
}
