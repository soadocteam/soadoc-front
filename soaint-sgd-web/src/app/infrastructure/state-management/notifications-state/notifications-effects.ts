import {Injectable} from '@angular/core';
import {Effect, Actions, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import * as actions from './notifications-actions';
import {Sandbox} from './notifications-sandbox';
import { tap, map } from 'rxjs/operators';

@Injectable()
export class Effects {

  constructor(private actions$: Actions,
              private _sandbox: Sandbox) {
  }

  @Effect({dispatch: false})
  notify: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.PUSH_NOTIFICATION),
    map(action => action['payload']),
    tap(payload => this._sandbox.showNotification(payload)));

}
