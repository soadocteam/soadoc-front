import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/pairwise';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/let';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/distinctUntilChanged';
import * as actions from './procesoDTO-actions';
import {LoadTasksInsideProcessAction, StartProcessAction} from './procesoDTO-actions';
import {StartTaskAction} from '../tareasDTO-state/tareasDTO-actions';
import {Sandbox} from './procesoDTO-sandbox';
import {State as RootState} from '../../redux-store/redux-reducers';
import {getSelectedDependencyGroupFuncionario} from '../funcionarioDTO-state/funcionarioDTO-selectors';
import {catchError, map, mergeMap, switchMap, withLatestFrom} from 'rxjs/operators';
import {of, timer} from 'rxjs';
import {LoadingService} from "../../utils/loading.service";
import {PushNotificationAction} from "../notifications-state/notifications-actions";
import {RETRY} from "../../../shared/bussiness-properties/error-messages.properties";

@Injectable()
export class Effects {

  contador = 0;

  constructor(private actions$: Actions, private loadingService: LoadingService,
              private _store$: Store<RootState>,
              private _sandbox: Sandbox) {
  }

  @Effect()
  load: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.LOAD)
    , map(action => action['payload'])
    , switchMap(
      (payload) => this._sandbox.loadData(payload)
        .pipe(map((response) => new actions.LoadSuccessAction({data: response}))
          , catchError((error) => of(new actions.LoadFailAction({error}))
          ))
    ));

  @Effect()
  startProcess: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.START_PROCESS)
    , map(action => action['payload'])
    , withLatestFrom(this._store$.select(getSelectedDependencyGroupFuncionario))
    , switchMap(
      ([payload, dependency]) => this._sandbox.startProcess(payload, dependency)
        .pipe(map((response) => {
            this.contador = 0;
            return new actions.LoadTasksInsideProcessAction({data: response})
          })
          , catchError((error) => {
              this.contador++;
              this.retryStartProcess(payload);
              return of(new actions.LoadFailAction({error}));
            }
          ))
    ));

  @Effect()
  LoadTasksInside: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.LOAD_TASKS_INSIDE_PROCESS)
    , map(action => action['payload'])
    , switchMap(
      (payload) => this._sandbox.loadTasksInsideProcess(payload)
        .pipe(mergeMap((response) => {
            this.contador = 0;
            return [
              new actions.LoadTaskSuccessAction({data: response}),
              new StartTaskAction(response)
            ];
          })
          , catchError((error) => {
              this.contador++;
              this.retryLoadTasksInsideProcess(payload);
              return of(new actions.LoadFailAction({error}));
            }
          ))
    ));

  retryStartProcess(payload) {
    if (this.contador <= 3) {
      this.loadingService.presentLoading();
      timer(3000).subscribe(time => {
        this._store$.dispatch(new StartProcessAction(payload));
      });
    } else {
      this.contador = 0;
      this.loadingService.dismissLoading();
      this._store$.dispatch(new PushNotificationAction({
        severity: 'info',
        summary: RETRY
      }))
    }
  }

  retryLoadTasksInsideProcess(payload): any {
    if (this.contador <= 3) {
      this.loadingService.presentLoading();
      timer(3000).subscribe(time => {
        this._store$.dispatch(new LoadTasksInsideProcessAction(payload));
      });
    } else {
      this.contador = 0;
      this.loadingService.dismissLoading();
      this._store$.dispatch(new PushNotificationAction({
        severity: 'info',
        summary: RETRY
      }))
    }
  }

}
