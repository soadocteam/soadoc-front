import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {Store} from '@ngrx/store';
import {State} from '../../../infrastructure/redux-store/redux-reducers';
import {createSelector} from 'reselect';
import * as selectors from './procesoDTO-selectors';
import * as actions from './procesoDTO-actions';
import {ApiBase} from '../../api/api-base';
import {Subscription} from 'rxjs/Subscription';
import {isNullOrUndefined, log} from 'util';
import {HttpHeaders} from '@angular/common/http';
import {getAuthenticatedFuncionario} from '../funcionarioDTO-state/funcionarioDTO-selectors';
import {profileStore} from '../../../ui/page-components/login/redux-state/login-selectors';
import {Observable} from 'rxjs/Observable';
import {switchMap, map} from "rxjs/operators";
import {FuncionarioDTO} from "../../../domain/funcionarioDTO";

@Injectable()
export class Sandbox implements OnDestroy {

  authPayload: { usuario: string, pass: string } |  {};
  authPayloadUnsubscriber: Subscription;
  funcionario: any;

  constructor(private _store: Store<State>,
              private _api: ApiBase) {
    this.authPayloadUnsubscriber = this._store.select(createSelector((s: State) => s.auth.profile, (profile) => {
      return profile ? {usuario: profile.username, pass: profile.password} : {};
    })).subscribe((value: any) => {
      this.funcionario = value.usuario;
      this.authPayload = value;
    });
  }

  getHeaders():Observable<any>{
    const options:any = {};
    options.headers = new HttpHeaders();
    return this._store.select(profileStore)
      .pipe(map(p => {
        const user = {
          user: p.username,
          password: p.password
        };

        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Authorization', 'Basic ' + window.btoa(unescape(encodeURIComponent(user.user + ':' + user.password))));
        options.headers = headers;
        return options;
      }));
  }

  loadData(payload: any) {

   const  loginName = payload.loginName;
    return this.getHeaders().pipe(switchMap( options =>
      this._api.list(`${environment.proceso_endpoint}/${loginName}`, payload,options)
    ))
  }

  startProcess(payload: any, dependency?: any) {

    let  params: any=  Object.assign({},payload.customParams || {}, {
      codDependencia: dependency.codigo,
      depAsignador: dependency.nombre,
      userAsignador: this.funcionario
    }) ;

    return this.getHeaders().pipe(switchMap (options => this._api.post(environment.startProcess_endpoint,
      Object.assign({}, {
        idProceso: payload.codigoProceso,
        idDespliegue: payload.idDespliegue,
        estados: [
          'LISTO'
        ],
        parametros: params
      }, this.authPayload), options)))
  }

  IniciarProcesso(payload: any) {
    return this._api.post(environment.startProcess_endpoint,
      Object.assign({}, payload, this.authPayload));
  }

  loadTasksInsideProcess(payload: any) {

    const params = payload.data || payload;
    return this.getHeaders().pipe(switchMap( options => this._api.post(environment.tasksInsideProcess_endpoint,
      Object.assign({}, {
        idProceso: params.nombreProceso || params.idProceso,
        instanciaProceso: isNullOrUndefined(params.instance) ? params.idInstanciaProceso : params.instance.instanceId,
       // idDespliegue: params.idDespliegue,
        estados: [
          'RESERVADO',
          'COMPLETADO',
          'ENPROGRESO',
          'LISTO'
        ]
      }, this.authPayload), options)))
  }

  filterDispatch(target, query) {
    this._store.dispatch(new actions.FilterAction({key: target, data: query}));
  }

  loadDispatch(payload?) {
    this._store.dispatch(new actions.LoadAction(payload));
  }

  initProcessDispatch(entity) {
    this._store.dispatch(new actions.StartProcessAction(entity));
  }

  selectorMenuOptions() {
    return createSelector(selectors.getEntities, selectors.getGrupoIds, (entities, ids) => {
      return ids.map(id => {
        return {
          label: entities[id].nombreProceso, icon: 'assignment',
          command: () => this._store.dispatch(new actions.StartProcessAction(entities[id]))
        }
      })
    });
  }

  ngOnDestroy() {
    this.authPayloadUnsubscriber.unsubscribe();
  }
}

