import {EffectsModule} from '@ngrx/effects';
import {LoginEffects} from '../../ui/page-components/login/redux-state/login-effects';
import {Effects as ConstanteDtoEffects} from '../../infrastructure/state-management/constanteDTO-state/constanteDTO-effects';
import {Effects as ProcesoDtoEffects} from '../../infrastructure/state-management/procesoDTO-state/procesoDTO-effects';
import {Effects as PaisDtoEffects} from '../../infrastructure/state-management/paisDTO-state/paisDTO-effects';
import {Effects as DepartamentoDtoEffects} from '../../infrastructure/state-management/departamentoDTO-state/departamentoDTO-effects';
import {Effects as MunicipioDtoEffects} from '../../infrastructure/state-management/municipioDTO-state/municipioDTO-effects';
import {Effects as DependenciaGrupoDtoEffects} from '../../infrastructure/state-management/dependenciaGrupoDTO-state/dependenciaGrupoDTO-effects';
import {Effects as TareasDtoEffects} from '../../infrastructure/state-management/tareasDTO-state/tareasDTO-effects';
import {Effects as RadicarComunicacionesEffects} from '../../infrastructure/state-management/radicarComunicaciones-state/radicarComunicaciones-effects';
import {Effects as ComunicacionOficialDtoEffects} from '../../infrastructure/state-management/comunicacionOficial-state/comunicacionOficialDTO-effects';
import {Effects as FuncionarioDtoEffects} from '../../infrastructure/state-management/funcionarioDTO-state/funcionarioDTO-effects';
import {Effects as sedeAdministrativaDtoEffects} from '../../infrastructure/state-management/sedeAdministrativaDTO-state/sedeAdministrativaDTO-effects';
import {Effects as asignacionDtoEffects} from '../../infrastructure/state-management/asignacionDTO-state/asignacionDTO-effects';
import {Effects as notificationEffects} from '../../infrastructure/state-management/notifications-state/notifications-effects';
import {Effects as DistribucionFisicaEffects} from '../../infrastructure/state-management/distrubucionFisicaDTO-state/distrubucionFisicaDTO-effects';
import {Effects as CargarPlanillasEffects} from '../../infrastructure/state-management/cargarPlanillasDTO-state/cargarPlanillasDTO-effects';

/**
 * Effects offer a way to isolate and easily test side-effects within your
 * application.
 * The `toPayload` helper function returns just
 * the payload of the currently dispatched action, useful in
 * instances where the current state is not necessary.
 *
 * Documentation on `toPayload` can be found here:
 * https://github.com/ngrx/effects/blob/master/docs/api.md#topayload
 *
 * If you are unfamiliar with the operators being used in these examples, please
 * check out the sources below:
 *
 * Official Docs: http://reactivex.io/rxjs/manual/overview.html#categories-of-operators
 * RxJS 5 Operators By Example: https://gist.github.com/btroncone/d6cf141d6f2c00dc6b35
 */


export const EFFECTS_MODULES = [
  LoginEffects,
  ConstanteDtoEffects,
  ProcesoDtoEffects,
  PaisDtoEffects,
  DepartamentoDtoEffects,
  MunicipioDtoEffects,
  DependenciaGrupoDtoEffects,
  TareasDtoEffects,
  RadicarComunicacionesEffects,
  ComunicacionOficialDtoEffects,
  FuncionarioDtoEffects,
  sedeAdministrativaDtoEffects,
  asignacionDtoEffects,
  notificationEffects,
  DistribucionFisicaEffects,
  CargarPlanillasEffects,
];
