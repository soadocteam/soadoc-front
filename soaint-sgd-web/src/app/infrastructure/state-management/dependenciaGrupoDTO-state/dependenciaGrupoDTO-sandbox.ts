import {Injectable} from '@angular/core';
import {environment} from 'environments/environment';
import {Store} from '@ngrx/store';
import {State} from 'app/infrastructure/redux-store/redux-reducers';
import * as actions from './dependenciaGrupoDTO-actions';
import {ApiBase} from '../../api/api-base';
import {Observable} from "rxjs";
import {DependenciaDTO} from "../../../domain/dependenciaDTO";
import {Utils} from "../../../shared/helpers";


@Injectable()
export class Sandbox {

  constructor(private _store: Store<State>,
              private _api: ApiBase) {
  }

  loadData(payload: any) {
    const _endpoint = `${environment.dependenciaGrupo_endpoint}/${payload.codigo}`;
    return this._api.list(_endpoint);
  }

  loadDependencies(payload?: any) {
    return this._api.list(environment.dependencias_endpoint, payload);
  }

  loadDispatch(payload) {
    this._store.dispatch(new actions.LoadAction(payload));
  }

  getMock(): any {
    return {
      constantes: [
        {ideConst: 1, nombre: 'Dependencia #1', codigo: 1},
        {ideConst: 2, nombre: 'Dependencia #2', codigo: 2},
        {ideConst: 3, nombre: 'Dependencia #3', codigo: 3},
        {ideConst: 4, nombre: 'Dependencia #4', codigo: 4},
      ]
    }
  }

  getDependenciasAdministrativasTVD(): Observable<Array<DependenciaDTO>> {
    return Utils.sortObservable(this._api.list(environment.listarDependeciasAdministrativasTvd)
      .map((dependencias): Array<DependenciaDTO> => (
        dependencias.map((dependencia) => ({
            id: dependencia.ideOrgaAdmin,
            codigo: dependencia.codOrg,
            nombre: dependencia.nomOrg
          }
        ))
      )), 'nombre');
  }

  getDependenciasProductorasTVD(codigoDependenciaAdministrativa: string): Observable<Array<DependenciaDTO>> {
    const payload = {
      codOrg: codigoDependenciaAdministrativa
    }
    return Utils.sortObservable(this._api.list(environment.listarDependenciasProductorasTVD, payload)
      .map((dependencias): Array<DependenciaDTO> => (
        dependencias.map((dependencia) => ({
            id: dependencia.ideOrgaAdmin,
            codigo: dependencia.codOrg,
            nombre: dependencia.nomOrg
          }
        ))
      )), 'nombre')
  }
}

