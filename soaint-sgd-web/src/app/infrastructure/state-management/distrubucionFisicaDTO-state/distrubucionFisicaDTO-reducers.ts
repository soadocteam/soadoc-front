import {Actions, ActionTypes as Autocomplete} from './distrubucionFisicaDTO-actions';
import {tassign} from 'tassign';
import {CorrespondenciaDTO} from '../../../domain/correspondenciaDTO';
import {ComunicacionOficialDTO} from '../../../domain/comunicacionOficialDTO';


export interface State {
  ids: number[];
  entities: { [ideDocumento: number]: ComunicacionOficialDTO };
  selectedId: number;
  filters: {
    cmcType: string;
    selectType: string;
    receiverSubsection: string;
    dateRange: string;
    trackId: string;
    documentType: string;
  }
}

const initialState: State = {
  ids: [],
  entities: {},
  selectedId: null,
  filters: {
    cmcType: null,
    selectType: null,
    receiverSubsection: null,
    dateRange: null,
    trackId: null,
    documentType: null
  }
};

/**
 * The reducer function.
 * @function reducer
 * @param {State} state Current state
 * @param {Actions} action Incoming action
 */
export function reducer(state = initialState, action: Actions): State {
  switch (action.type) {

    case Autocomplete.FILTER_COMPLETE:
    case Autocomplete.LOAD_SUCCESS: {
      const values = action.payload.comunicacionesOficiales;

      if (!values) {
        return tassign(state, {
          ids: [],
          entities: {},
          selectedId: null,
        });
      }
      // const newValues = values.filter(data => !state.entities[data.ideDocumento]);
      const ids = values.map(data => data.correspondencia.ideDocumento);
      const entities = values.reduce((entities: { [ideDocumento: number]: ComunicacionOficialDTO }, value: ComunicacionOficialDTO) => {
        return Object.assign(entities, {
          [value.correspondencia.ideDocumento]: value
        });
      }, {});
      return tassign(state, {
        ids: [...ids],
        entities: entities,
        selectedId: state.selectedId
      });
    }
    case Autocomplete.LOAD: {
      const filters = action.payload;
      return tassign(state, {
        filters: {
          cmcType: filters.cmcType || null,
          selectType: filters.selectType || null,
          receiverSubsection: filters.receiverSubsection || null,
          dateRange: filters.dateRange || null,
          trackId: filters.trackId || null,
          documentType: filters.documentType || null,
        }
      });
    }

    case Autocomplete.CLEAR :
      return {
        ids: [], entities: [], selectedId: null, filters: {
          cmcType: null,
          selectType: null,
          receiverSubsection: null,
          dateRange: null,
          trackId: null,
          documentType: null
        }
      };

    default:
      return state;
  }
}


