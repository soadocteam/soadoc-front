import {Actions, ActionTypes as Autocomplete} from './comunicacionOficialDTO-actions';
import {tassign} from 'tassign';
import {ComunicacionOficialDTO} from '../../../domain/comunicacionOficialDTO';
import {isNullOrUndefined} from "util";


export interface State {
  ids: number[];
  entities: { [ideDocumento: number]: ComunicacionOficialDTO };
  selectedId: number;
  filters: {
    fecha_ini: string;
    fecha_fin: string;
    cod_estado: string;
    nro_radicado: string;
    pages: number;
    pagesSize: number;
  }
  totalRecords: number
}

const initialState: State = {
  ids: [],
  entities: {},
  selectedId: null,
  filters: {
    fecha_ini: null,
    fecha_fin: null,
    cod_estado: null,
    nro_radicado: null,
    pages: 1,
    pagesSize: 10
  },
  totalRecords: 0
};

/**
 * The reducer function.
 * @function reducer
 * @param {State} state Current state
 * @param {Actions} action Incoming action
 */
export function reducer(state = initialState, action: Actions) {
  switch (action.type) {

    case Autocomplete.FILTER_COMPLETE:
    case Autocomplete.LOAD_SUCCESS: {
      let values = undefined;
      if (action.payload.comunicacionesOficialesDTO) {
        values = action.payload.comunicacionesOficialesDTO.comunicacionesOficiales;
      }
      const totalRecords = isNullOrUndefined(action.payload.pagesResult) ? 0 : action.payload.pagesResult;
      if (!values) {
        return tassign(state, {
          ids: [],
          entities: {},
          selectedId: null,
          totalRecords: 0
        });
      }
      // const newValues = values.filter(data => !state.entities[data.ideDocumento]);
      const ids = values.map(data => data.correspondencia.ideDocumento);
      const entities = values.reduce((entities: { [ideDocumento: number]: ComunicacionOficialDTO }, value: ComunicacionOficialDTO) => {
        return Object.assign(entities, {
          [value.correspondencia.ideDocumento]: value
        });
      }, {});
      return tassign(state, {
        ids: [...ids],
        entities: entities,
        selectedId: state.selectedId,
        totalRecords: totalRecords * 10
      });
    }
    case Autocomplete.LOAD: {
      const filters = action.payload;
      return tassign(state, {
        filters: {
          fecha_ini: filters.fecha_ini || null,
          fecha_fin: filters.fecha_fin || null,
          cod_estado: filters.cod_estado || null,
          pages: filters.pages || null,
          pagesSize: filters.pagesSize || null
        }
      });
    }

    default:
      return state;
  }
}


