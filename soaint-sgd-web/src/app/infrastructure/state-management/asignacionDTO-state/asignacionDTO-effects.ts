import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/pairwise';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/let';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/distinctUntilChanged';
import * as actions from './asignacionDTO-actions';
import {ChangeStatusAssigns, InitAssigns, SetJustificationDialogVisibleAction} from './asignacionDTO-actions';
import {Sandbox} from './asignacionDTO-sandbox';
import {State as RootState} from '../../../infrastructure/redux-store/redux-reducers';
import {ReloadAction as ReloadComunicacionesAction} from '../comunicacionOficial-state/comunicacionOficialDTO-actions';
import {catchError, flatMap, map, mergeMap, switchMap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {ASIGNAR, REASIGNAR, REDIRECCIONADO, TRAMITAR} from "../../../shared/constants";
import {REDIRECCION_FAIL, REDIRECTION_SUCCESS} from "../../../shared/lang/es";
import {PushNotificationAction} from "../notifications-state/notifications-actions";

function isLoaded() {
  return (source) =>
    source.filter(values => {
      return true
    })
}

@Injectable()
export class Effects {

  @Effect()
  assign: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.ASSIGN)
    , map(action => action['payload'])
    , switchMap(
      (payload: any) => {
        let option = payload.option == 7 ? ASIGNAR : TRAMITAR;
        return this._sandbox.assignComunications(payload)
          .pipe(mergeMap((response) => [new actions.AssignSuccessAction(response)
              , new ReloadComunicacionesAction()
              , new InitAssigns(option)])
            , catchError((error) => of(new actions.AssignFailAction({error}))
            ))
      }
    ));
  @Effect()
  initAssigns: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.INIT_ASSIGNS)
    , map(action => action['payload'])
    , switchMap(
      (payload) => {
        return this._sandbox.initAssigns()
          .pipe(mergeMap(() => [new actions.CompleteAssigns(payload)])
            , catchError((error) => of(new actions.AssignFailAction({error})))
          )
      }
    )
  );
  @Effect()
  completeAssigns: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.COMPLETE_ASSIGNS)
    , map(action => action['payload'])
    , switchMap(
      (payload) => {
        return this._sandbox.completeTask()
          .pipe(mergeMap((response) => [new actions.ChangeStatusAssigns(payload)])
            , catchError((error) => of(new actions.AssignFailAction({error}))
            ))
      }
    )
  );
  @Effect()
  changeStatusAssigns: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.CHANGE_STATUS_ASSIGNS)
    , map(action => action['payload'])
    , switchMap(
      (payload) => {
        return this._sandbox.changeStatusTrazability(payload)
          .pipe(mergeMap(() => [])
            , catchError((error) => of(new actions.AssignFailAction({error}))))
      }
    )
  );
  @Effect()
  reassign: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.REASSIGN)
    , map(action => action['payload'])
    , flatMap(
      (payload) => {
        return this._sandbox.reassignComunications(payload)
          .pipe(mergeMap((response: any) => {
              if (payload.estado === 'SA') {
                return [new actions.ReassignSuccessAction(response),
                  new ReloadComunicacionesAction(),
                  new InitAssigns(REASIGNAR)]
              } else {
                return [new actions.ReassignSuccessAction(response),
                  new ReloadComunicacionesAction(),
                  new ChangeStatusAssigns(REASIGNAR)
                ]
              }
            })
            , catchError((error) => of(new actions.ReassignFailAction({error}))))
      }
    ));
  @Effect()
  redirect: Observable<Action> = this.actions$.pipe(
    ofType(actions.ActionTypes.REDIRECT)
    , map(action => action['payload'])
    , flatMap(
      (payload) => {
        return this._sandbox.redirectMultipleComunications(payload)
          .pipe(mergeMap((response) => {
              if (response.message) {
                if (response.message === REDIRECCION_FAIL) {
                  this._store$.dispatch(new PushNotificationAction({
                    severity: 'info',
                    summary: REDIRECCION_FAIL
                  }));

                  return [new ReloadComunicacionesAction()
                    , new SetJustificationDialogVisibleAction(false)]
                } else {
                  this._store$.dispatch(new PushNotificationAction({
                    severity: 'success',
                    summary: REDIRECTION_SUCCESS
                  }));
                }
              }
              return [new actions.RedirectSuccessAction(response)
                , new ReloadComunicacionesAction()
                , new SetJustificationDialogVisibleAction(false)
                , new ChangeStatusAssigns(REDIRECCIONADO)]
            })
            , catchError((error) => of(new actions.RedirectFailAction({error}))
            ))
      }
    ));

  constructor(private actions$: Actions,
              private _store$: Store<RootState>,
              private _sandbox: Sandbox) {
  }


}
