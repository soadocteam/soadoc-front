import {Injectable} from '@angular/core';
import {environment} from 'environments/environment';
import {Store} from '@ngrx/store';
import {State} from 'app/infrastructure/redux-store/redux-reducers';
import * as actions from './asignacionDTO-actions';
import {ApiBase} from '../../api/api-base';
import {ObservacionDTO} from '../../../domain/observacionDTO';
import {isNullOrUndefined} from 'util';
import {createSelector} from "reselect";
import {ComunicacionOficialDTO} from "../../../domain/comunicacionOficialDTO";
import {getAssings} from "./asignacionDTO-selectors";
import {
  getAuthenticatedFuncionario,
  getSelectedDependencyGroupFuncionario
} from "../funcionarioDTO-state/funcionarioDTO-selectors";
import {FuncionarioDTO} from "../../../domain/funcionarioDTO";
import {ASIGNAR, ID_DESPLIEGUE, REASIGNAR, REDIRECCIONADO, TRAMITAR} from "../../../shared/constants";
import {of, throwError} from "rxjs";
import {HttpHeaders} from "@angular/common/http";

@Injectable()
export class Sandbox {

  authPayload: { usuario: string, pass: string } | any;
  selectedComunications: ComunicacionOficialDTO[] = [];
  funcionarioLog: FuncionarioDTO;
  dependency: any;

  constructor(private _store: Store<State>,
              private _api: ApiBase) {
    this._store.select(createSelector((s: State) => s.auth.profile, (profile) => {
      return profile ? {usuario: profile.username, pass: profile.password} : {};
    })).subscribe((value) => {
      this.authPayload = value;
    });
    this._store.select(getAssings).subscribe(assings => this.selectedComunications = assings);
    this._store.select(getSelectedDependencyGroupFuncionario).subscribe(dependency => this.dependency = dependency);
    this._store.select(getAuthenticatedFuncionario).subscribe((funcionario) => {
      this.funcionarioLog = funcionario;
    })
  }

  assignComunications(payload: any) {
    if (payload.option) {
      delete payload.option;
    }
    return this._api.post(environment.asignarComunicaciones_endpoint, payload);
  }

  completeTrack(ideAgente, nroRadicado) {
    return this._api.post(`${environment.completarComunicaciones_endpoint}?ideAgente=${ideAgente}&nroRadicado=${nroRadicado}`);
  }

  reassignComunications(payload: any) {
    delete payload.estadoAsignacion;
    return this._api.post(environment.reasignarComunicaciones_endpoint, payload);
  }

  obtenerObservaciones(documentId: any) {
    return this._api.list(`${environment.obtenerObservaciones_endpoint}${documentId}`);
  }

  obtenerDocumento(documentId: any) {
    return this._api.list(`${environment.obtenerDocumento_endpoint}${documentId}`);
  }

  obtnerConstantesPorCodigos(codigos: string) {
    return this._api.list(`${environment.obtenerConstantesPorCodigo_endpoint}`, {
      codigos: codigos
    });
  }

  obtnerDependenciasPorCodigos(codigos: string) {
    return this._api.list(`${environment.obtenerDependenciasPorCodigo_endpoint}`, {
      codigos: codigos
    });
  }

  obtenerMunicipiosPorCodigos(codigos: string) {
    return this._api.list(`${environment.obtenerMunicipiosPorCodigo_endpoint}`, {
      codigos: codigos
    });
  }

  obtenerComunicacionPorNroRadicado(nroRadicado: string) {
    return this._api.list(`${environment.obtenerComunicacion_endpoint}${nroRadicado}`);
  }

  registrarObservacion(payload: ObservacionDTO) {
    return this._api.post(environment.registrarObservaciones_endpoint, payload);
  }

  redirectComunications(payload: any) {
    return this._api.post(environment.redireccionarComunicaciones_endpoint, payload);
  }

  redirectMultipleComunications(payload: any) {
    return this._api.post(environment.redireccionarMultipleComunicaciones_endpoint, payload);
  }

  rejectComunications(payload: any) {
    return this._api.post(environment.devolverComunicaciones_endpoint, payload);
  }

  rejectComunicationsAsignacion(payload: any) {
    return this._api.post(environment.devolverComunicacionesAsigancion_endpoint, payload);
  }

  assignDispatch(payload) {
    this._store.dispatch(new actions.AssignAction(payload));
  }

  reassignDispatch(payload) {
    this._store.dispatch(new actions.ReassignAction(payload));
  }

  redirectDispatch(payload) {
    this._store.dispatch(new actions.RedirectAction(payload));
  }

  setVisibleJustificationDialogDispatch(payload: boolean) {
    this._store.dispatch(new actions.SetJustificationDialogVisibleAction(payload));
  }

  setVisibleDetailsDialogDispatch(payload: boolean) {
    this._store.dispatch(new actions.SetDetailsDialogVisibleAction(payload));
  }

  setVisibleAddObservationsDialogDispatch(payload: boolean) {
    this._store.dispatch(new actions.SetAddObservationsDialogVisibleAction(payload));
  }

  setVisibleRejectDialogDispatch(payload: boolean) {
    this._store.dispatch(new actions.SetRejectDialogVisibleAction(payload));
  }

  setAssigns(payload: any) {
    this._store.dispatch(new actions.SetAssignsAction(payload))
  }

  initAssigns() {
    let payload = this.getPayloadInitAssigns();
    if (isNullOrUndefined(payload)) {
      return throwError(null);
    } else {
      let options: any = {
        headers: new HttpHeaders().append('skipLoading', 'true')
      };
      return this._api.post(environment.tasksStartProcess, Object.assign({}, payload, this.authPayload), options)
    }
  }

  changeStatusTrazability(statusActivity) {
    var payload = this.getPayloadChangeStatus(statusActivity);
    if (isNullOrUndefined(payload)) {
      return of([]);
    } else {
      let options: any = {
        headers: new HttpHeaders().append('skipLoading', 'true')
      };
      return this._api.post(environment.trazabilityPlanilla, payload, options);
    }
  }

  completeTask() {
    var payload = this.getPayloadCompleteTask();
    if (!isNullOrUndefined(payload)) {
      let options: any = {
        headers: new HttpHeaders().append('skipLoading', 'true')
      };
      return this._api.post(environment.tasksCompleteProcess, Object.assign({}, payload, this.authPayload), options);
    }
  }

  getPayloadCompleteTask(): any {
    var payload: any;
    var listData: any[] = [];
    if (this.selectedComunications.length == 0) {
      return null;
    }
    this.selectedComunications.forEach(comunication => {
      let agente = comunication.agenteList.find(agente => agente.idNumber == this.dependency.codigo);
      listData.push({
        idTarea: agente.taskId,
        parametros: {
          codDependencia: comunication.correspondencia.codDependencia,
          nomuser: this.authPayload.usuario
        }
      })
    });
    payload = {
      idProceso: 'proceso.asignar-comunicaciones',
      idDespliegue: ID_DESPLIEGUE,
      dataProcess: listData
    };
    return payload;
  }

  setStatusTrazability(payload: any[]) {
    return this._api.post(environment.trazabilityPlanilla, payload);
  }

  getPayloadInitAssigns(): any {
    var payload: any;
    var listIdTask: any[] = [];
    if (this.selectedComunications.length == 0) {
      return null;
    }
    this.selectedComunications.forEach(task => {
      let agente = task.agenteList.find(agente => agente.idNumber == this.dependency.codigo);
      listIdTask.push(+agente.taskId)
    });
    payload = {
      idProceso: 'proceso.asignar-comunicaciones',
      idDespliegue: ID_DESPLIEGUE,
      idTareas: listIdTask
    };
    return payload;
  }

  getPayloadChangeStatus(statusActivity) {
    if (isNullOrUndefined(statusActivity)) {
      return null;
    }
    if (this.selectedComunications.length == 0) {
      return null;
    }
    let payload: any[] = [];
    this.selectedComunications.forEach(track => {
      var response: any = {
        objectId: track.correspondencia.nroRadicado,
        objectType: 'Documento',
        user: this.funcionarioLog.loginName,
        subsection: this.dependency.codigo,
        instanceId: Number(track.correspondencia.ideInstancia)
      };
      switch (statusActivity) {
        case REDIRECCIONADO: {
          response.trackStateId = 40;
          break;
        }
        case REASIGNAR: {
          response.trackStateId = 41;
          break
        }
        case TRAMITAR: {
          response.trackStateId = 6;
          break
        }
        case ASIGNAR: {
          response.trackStateId = 7;
          break
        }

      }
      payload.push(response);
    });
    return payload;
  }

}
