import {ApiBase} from './api/api-base';
import {DatosGeneralesApiService} from './api/datos-generales.api';
import {PlanillasApiService} from './api/planillas.api';
import {DroolsRedireccionarCorrespondenciaApi} from './api/drools-redireccionar-correspondencia.api';
import {ProduccionDocumentalApiService} from './api/produccion-documental.api';
import {MessagingService} from '../shared/providers/MessagingService';
import {UnidadDocumentalApiService} from 'app/infrastructure/api/unidad-documental.api';
import {SerieSubserieApiService} from 'app/infrastructure/api/serie-subserie.api';
import {SerieService} from './api/serie.service';
import {ConstanteApiService} from './api/constantes.api';
import {CorrespondenciaApiService} from './api/correspondencia.api';
import {DependenciaApiService} from './api/dependencia.api';
import {LocalizacionApiService} from './api/localizacion.api';
import {TipologiaDocumentaService} from "./api/tipologia-documenta.service";
import {TaskManagerService} from "./api/task-manager.service";
import {RadicadosApi} from "./api/radicados.api";
import {DatosContactoApi} from "./api/datos-contacto.api";
import {AnexoApi} from "./api/anexo.api";
import {InstrumentoApi} from "./api/instrumento.api";
import {PdObservacionApi} from "./api/pd-observacion.api";
import {TransformVersionDocumentoToDocumentoEcm} from "../shared/data-transformers/transform-version-documento-to-documento-ecm";
import {AgenteApi} from "./api/agente.api";
import {DigitalizarEpehsoftApi} from './api/digitalizar-epehsoft.api';
import {MegafServiceApi} from "./api/megaf-service.api";
import {GestionEntregaApi} from "./api/gestionEntrega.api";
import {ParametrizarNumeroRadicadoApiService} from "./api/parametrizar-numero-radicado.api.service";
import {TrazabilidadApi} from "./api/trazabilidad.api";
import {ImpresionSobresApi} from "./api/impresion-sobres.api";
import {FileextensionApi} from "./api/fileextension-api";
import {VersionApi} from "./api/version.api";
import {SmsApi} from "./api/sms.api";
import {AdobeApi} from "./api/Adobe.api";
import {MasterConfigurationApi} from "./api/master-configuration.api";
import {ReimpresionEtiquetasApi} from "./api/reimpresion-etiquetas.api";
import {CachedData} from "./api/cached-data";
import {LoginApiService} from "./api/login.api";
import {IndiceElectronicoApi} from "./api/indice-electronico.api";
import {ExcepcionesApiService} from "./api/excepciones.api";


export const API_SERVICES = [
  ApiBase,
  DatosGeneralesApiService,
  PlanillasApiService,
  DroolsRedireccionarCorrespondenciaApi,
  ProduccionDocumentalApiService,
  MessagingService,
  UnidadDocumentalApiService,
  SerieSubserieApiService,
  SerieService,
  ConstanteApiService,
  GestionEntregaApi,
  CorrespondenciaApiService,
  GestionEntregaApi,
  DependenciaApiService,
  LocalizacionApiService,
  TipologiaDocumentaService,
  TaskManagerService,
  RadicadosApi,
  DatosContactoApi,
  AnexoApi,
  InstrumentoApi,
  PdObservacionApi,
  TransformVersionDocumentoToDocumentoEcm,
  AgenteApi,
  DigitalizarEpehsoftApi,
  MegafServiceApi,
  ParametrizarNumeroRadicadoApiService,
  TrazabilidadApi,
  ImpresionSobresApi,
  FileextensionApi,
  VersionApi,
  SmsApi,
  AdobeApi,
  MasterConfigurationApi,
  ReimpresionEtiquetasApi,
  CachedData,
  LoginApiService,
  IndiceElectronicoApi,
  ExcepcionesApiService
];

export {
  ApiBase,
  DatosGeneralesApiService,
  PlanillasApiService,
  DroolsRedireccionarCorrespondenciaApi,
  ProduccionDocumentalApiService,
  MessagingService,
  ParametrizarNumeroRadicadoApiService
};
