import * as fromRouter from '@ngrx/router-store';
import * as loginStore from '../../ui/page-components/login/redux-state/login-reducers';
import * as adminLayoutStore from '../../ui/layout-components/container/admin-layout/redux-state/admin-layout-reducers';
import * as constantesStore from '../state-management/constanteDTO-state/constanteDTO-reducers';
import * as procesoStore from '../state-management/procesoDTO-state/procesoDTO-reducers';
import * as paisStore from '../state-management/paisDTO-state/paisDTO-reducers';
import * as municipioStore from '../state-management/municipioDTO-state/municipioDTO-reducers';
import * as departamentoStore from '../state-management/departamentoDTO-state/departamentoDTO-reducers';
import * as correspondenciaStore from '../state-management/comunicacionOficial-state/comunicacionOficialDTO-reducers';
import * as distribucionStore from '../state-management/distrubucionFisicaDTO-state/distrubucionFisicaDTO-reducers';
import * as dependenciaGrupoStore from '../state-management/dependenciaGrupoDTO-state/dependenciaGrupoDTO-reducers';
import * as sedeAdministrativaStore from '../state-management/sedeAdministrativaDTO-state/sedeAdministrativaDTO-reducers';
import * as tareasStore from '../state-management/tareasDTO-state/tareasDTO-reducers';
import * as comunicacionOficialStore from '../state-management/radicarComunicaciones-state/radicarComunicaciones-reducers';
import * as funcionarioStore from '../state-management/funcionarioDTO-state/funcionarioDTO-reducers';
import * as asignacionStore from '../state-management/asignacionDTO-state/asignacionDTO-reducers';
import * as cargarPlanillasStore from '../state-management/cargarPlanillasDTO-state/cargarPlanillasDTO-reducers';
import * as notificationStore from '../state-management/notifications-state/notifications-reducers';

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface State {
  auth: loginStore.State
  adminLayout: adminLayoutStore.State,
  constantes: constantesStore.State,
  paises: paisStore.State,
  municipios: municipioStore.State,
  departamentos: departamentoStore.State,
  comunicacionesOficiales: correspondenciaStore.State,  
  cargarPlanillas: cargarPlanillasStore.State,
  asignaciones: asignacionStore.State,
  dependenciaGrupo: dependenciaGrupoStore.State,
  sedeAdministrativa: sedeAdministrativaStore.State,
  tareas: tareasStore.State,
  proceso: procesoStore.State,
  funcionario: funcionarioStore.State,
  radicarComunicacion: comunicacionOficialStore.State,
  notifications: notificationStore.State,
  distribucionFisica: distribucionStore.State,
}


/**
 * Because metareducers take a reducer function and return a new reducer,
 * we can use our compose helper to chain them together. Here we are
 * using combineReducers to make our top level reducer, and then
 * wrapping that in storeLogger. Remember that compose applies
 * the result from right to left.
 */
export const reducers = {
  auth: loginStore.reducer,
  adminLayout: adminLayoutStore.reducer,
  constantes: constantesStore.reducer,
  proceso: procesoStore.reducer,
  paises: paisStore.reducer,
  municipios: municipioStore.reducer,
  departamentos: departamentoStore.reducer,
  comunicacionesOficiales: correspondenciaStore.reducer, 
  cargarPlanillas: cargarPlanillasStore.reducer,
  asignaciones: asignacionStore.reducer,
  dependenciaGrupo: dependenciaGrupoStore.reducer,
  sedeAdministrativa: sedeAdministrativaStore.reducer,
  tareas: tareasStore.reducer,
  funcionario: funcionarioStore.reducer,
  radicarComunicacion: comunicacionOficialStore.reducer,
  notifications: notificationStore.reducer,
   distribucionFisica: distribucionStore.reducer,
};


