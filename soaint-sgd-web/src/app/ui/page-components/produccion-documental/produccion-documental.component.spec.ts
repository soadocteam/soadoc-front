import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProduccionDocumentalComponent } from './produccion-documental.component';

describe('ProduccionDocumentalComponent', () => {
  let component: ProduccionDocumentalComponent;
  let fixture: ComponentFixture<ProduccionDocumentalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProduccionDocumentalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProduccionDocumentalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
