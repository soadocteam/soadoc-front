import {ConstanteDTO} from '../../../../domain/constanteDTO';


export interface AnexoDTO {
    id: string,
    soporte: string,
    tipo: ConstanteDTO,
    descripcion?: string,
    file?: any,
    blocked?:boolean
}

export interface VersionDocumentoDTO {
    id: string;
    tipo: string;
    nombre: string;
    size: number;
    version?: string;
    contenido?: string;
    footer?: string;
    file?: Blob;
    taskId?:any;
    disabled?:boolean;
    editable?:boolean;
    documento?: any;
   nombreDocumento?: string;
  idDocumento?: string;
}


