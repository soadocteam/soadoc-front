import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsolaSeguimientoComponent } from './consola-seguimiento.component';

describe('ConsolaSeguimientoComponent', () => {
  let component: ConsolaSeguimientoComponent;
  let fixture: ComponentFixture<ConsolaSeguimientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsolaSeguimientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsolaSeguimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
