import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { faDownload, faRedo } from '@fortawesome/free-solid-svg-icons';
import {LocaleSettings} from "primeng/calendar";
import {ICON_CALENDAR} from "../../../shared/constants";
import {localeCalendarEs} from "../../../shared/localeCalendarEs";
import {AngularCsv} from "angular-csv-ext";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {IndiceElectronicoApi} from "../../../infrastructure/api/indice-electronico.api";
import {Observable} from "rxjs/Observable";
import {isNullOrUndefined} from "util";
import {map, switchMap} from "rxjs/operators";
import {Subscription} from "rxjs";
import {IndiceElectronicoDTO} from "../../../domain/indiceElectronicoDTO";
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../infrastructure/redux-store/redux-reducers";

@Component({
  selector: 'app-consola-seguimiento',
  templateUrl: './consola-seguimiento.component.html',
  styleUrls: ['./consola-seguimiento.component.scss']
})
export class ConsolaSeguimientoComponent implements OnInit,OnDestroy {

  faDownload = faDownload;
  faRedo = faRedo;
  executionDetails: boolean = false;
  es: LocaleSettings;
  iconCalendar = ICON_CALENDAR;
  formIndice: FormGroup;
  indices: any[];
  start_date: Date = new Date();
  end_date: Date = new Date();
  folderSelected: any[] = [];
  reintentarVisible: boolean = false;
  listadoIndices$: Observable<IndiceElectronicoDTO>;
  estados: Array<{ label: string, value: string }>;
  subscriptions: Subscription[] = [];
  dataCSV: any;
  date = new Date();
  @ViewChild('details') details;
  user: any;
  fechaInicial: boolean = false;
  fechaFinal: boolean = false;



  constructor(private fe: FormBuilder,
              private _indiceElectronicoApi: IndiceElectronicoApi,
              private _store: Store<RootState>,) { }

  ngOnInit() {

    this.estados = [
      {label: "Todos", value: ""},
      {label: "Exitoso", value: "Exitoso"},
      {label: "Fallido", value: "Fallido"},
      {label: "Pendiente", value: "Pendiente"},
      {label: "Reintento", value: "Reintento"},
    ]

    this.es = localeCalendarEs;
    this.initFormIndice();
    this.start_date = new Date(this.date.getFullYear(), this.date.getMonth(), 1);

  }

  initFormIndice() {
    this.formIndice = this.fe.group({
      fechaInicial: [null, Validators.required],
      fechaFinal: [null, Validators.required],
      codigoSerie: [null],
      codSubSerie: [null],
      codigoDependencia: [null],
      estado: [null],
      objectStore: [null],
      idExpediente: [null],
    });

  }

  findIndices(){
    if (this.formIndice.invalid){
      this.validateFields();
      this._store.dispatch(new PushNotificationAction({
        summary: 'Faltan datos obligatorios',
        severity: 'info'
      }))
    } else {
      const payload: any = {
        fechaInicial: this.convertDate(this.start_date.setHours(0, 0, 0)),
        fechaFinal: this.convertDate(this.end_date.setHours(23, 59, 59)),
        codigoSerie: !isNullOrUndefined(this.formIndice.get('codigoSerie').value) ? this.formIndice.get('codigoSerie').value : "",
        codSubSerie: !isNullOrUndefined(this.formIndice.get('codSubSerie').value) ? this.formIndice.get('codSubSerie').value : "",
        codigoDependencia: !isNullOrUndefined(this.formIndice.get('codigoDependencia').value) ? this.formIndice.get('codigoDependencia').value : "",
        estado: !isNullOrUndefined(this.formIndice.get('estado').value) ? this.formIndice.get('estado').value : "",
        objectStore: "COLPE",
        idExpediente: !isNullOrUndefined(this.formIndice.get('idExpediente').value) ? this.formIndice.get('idExpediente').value : "",
      };
      this.listadoIndices$ = this._indiceElectronicoApi.findIndices(payload)
      this.fechaInicial = false;
      this.fechaFinal = false;
    }
  }


  convertDate(inputFormat) {
    function pad(s) {
      return (s < 10) ? '0' + s : s;
    }

    const d = new Date(inputFormat);
    return [pad(d.getFullYear()), pad(d.getMonth() + 1), d.getDate()].join('/') + " " + [d.getHours(),d.getMinutes()].join(':');
  }

  cleanForm() {
    this.formIndice.reset();
    this.listadoIndices$ = null;
    this.end_date = new Date();
    this.start_date = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    this.fechaInicial = false;
    this.fechaFinal = false;
  }

  showDetailsDialog(data: any) {
    this.executionDetails = true
    this.details.dataDetails(data);
  }

  hideExecutionDetailsPopup() {
    this.executionDetails = false;
  }

  foldersSelected(){
    if (this.folderSelected){
      this.reintentarVisible = true;
    }
  }

  reintentar(){
    let data = [];
    this.folderSelected.forEach(item => {
      if (!isNullOrUndefined(item.estado) && item.estado == 'Fallido'){
        data.push(item.idExpediente);
      }
    })
    if (!isNullOrUndefined(data) && data.length > 0) {
      this.user = JSON.parse(localStorage.getItem('user'));
      this._indiceElectronicoApi.createIndice(this.user.loginName, data).subscribe(() => {
        this._store.dispatch(new PushNotificationAction({
          summary: 'Se ha registrado el intento de los índices fallidos',
          severity: 'success'
        }))
      });
    } else {
      this._store.dispatch(new PushNotificationAction({
        summary: 'No hay índices electrónicos fallidos por tramitar',
        severity: 'info'
      }))
    }

  }

  downloadCSV(){
    if (!isNullOrUndefined(this.listadoIndices$)) {
      var options = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true,
        showTitle: true,
        title: 'Consola de Seguimiento ejecucción índice electrónico',
        useBom: true,
        noDownload: false,
        headers: ["ID Indice", "Versión", "GUID de Expediente", "Fecha de Registro", "Fecha de Ejecución", "Object Store", "Código Serie", "Código Subserie", "Cód. depen. productora", "Origen Ejecución", "Estado", "Usuario"],
        useHeader: false,
        nullToEmptyString: true,
      };

      this.subscriptions.push(this.listadoIndices$.subscribe((response) => {
        this.dataCSV = response;
        let data = [];
        this.dataCSV.forEach(item =>{
          data.push({
            idIndice: item.idIndice,
            version: item.version,
            idExpediente: item.idExpediente,
            creationDate: item.creationDate,
            fechaEjecucion: item.fechaEjecucion,
            objectStore: item.objectStore,
            codSerie: item.codSerie,
            codSubSerie: item.codSubSerie,
            codDependencia: item.codDependencia,
            origenDeEjecucion: item.origenDeEjecucion,
            estado: item.estado,
            usuarioExecute: item.usuarioExecute
          })
        })
        // This data will be generated in the CSV file

        new AngularCsv(data, "Seguimiento-Ejecucion", options);
      }))



    } else {
      this._store.dispatch(new PushNotificationAction({
        summary: 'No hay datos para generar el CSV',
        severity: 'info'
      }))
    }
  }

  validateFields() {
    if (isNullOrUndefined(this.start_date) && isNullOrUndefined(this.end_date)) {
      this.fechaInicial = true;
      this.fechaFinal = true;
    } else if (isNullOrUndefined(this.start_date)) {
      this.fechaInicial = true;
    } else if (isNullOrUndefined(this.end_date)) {
      this.fechaFinal = true;
    }

  }

  ngOnDestroy(): void {
  }

}
