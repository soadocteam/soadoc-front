import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {PRIMENG_MODULES} from '../../../shared/primeng/__primeng';
import {BUSSINESS_COMPONENTS, PAGE_COMPONENTS} from '../../__ui.include';
import {AppRoutes} from './pages-routing.module';
import {DIRECTIVES} from '../../../shared/__shared.include';
import {PipesModule} from '../../../../app/pipes.module';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {NgxBarcodeModule} from 'ngx-barcode';
import {OrderModule} from 'ngx-order-pipe';
import {CKEditorModule} from 'ng2-ckeditor';
import {NgxExtendedPdfViewerModule} from 'ngx-extended-pdf-viewer';
import {PdfViewerComponent} from 'ng2-pdf-viewer';
import {NgxUsefulSwiperModule} from 'ngx-useful-swiper';
import {QRCodeModule} from "angularx-qrcode";
import {CardModule} from "primeng/card";
import {MatTooltipModule} from "@angular/material/tooltip";
import {XmlViewerComponent} from "../../bussiness-components/xml-viewer/xml-viewer.component";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {Angular2CsvModule} from "angular2-csv";

const COMPONENTS = [

  ...BUSSINESS_COMPONENTS,
  ...DIRECTIVES,
  PdfViewerComponent,
  ...PAGE_COMPONENTS,

];

@NgModule({
    imports: [
        NgxUsefulSwiperModule,
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        ...PRIMENG_MODULES,
        AppRoutes,
        NgxExtendedPdfViewerModule,
        NgxChartsModule,
        NgxBarcodeModule,
        OrderModule,
        CKEditorModule,

        PipesModule,
        QRCodeModule,
        CardModule,
        MatTooltipModule,
        FontAwesomeModule,
        Angular2CsvModule,
    ],
    declarations: [COMPONENTS,  XmlViewerComponent],
  exports: [COMPONENTS],
  providers: [

  ],
})
export class HomeModule {
}
