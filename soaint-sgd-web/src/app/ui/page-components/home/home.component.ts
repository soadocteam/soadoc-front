import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {faDownload, faRedo} from '@fortawesome/free-solid-svg-icons';
import {LocaleSettings} from "primeng/calendar";
import {ICON_CALENDAR} from "../../../shared/constants";
import {localeCalendarEs} from "../../../shared/localeCalendarEs";
import {AngularCsv} from "angular-csv-ext";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {IndiceElectronicoApi} from "../../../infrastructure/api/indice-electronico.api";
import {Observable} from "rxjs/Observable";
import {isNullOrUndefined} from "util";
import {map, switchMap} from "rxjs/operators";
import {Subscription} from "rxjs";
import {IndiceElectronicoDTO} from "../../../domain/indiceElectronicoDTO";
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../infrastructure/redux-store/redux-reducers";
import {MenuItem} from "primeng/primeng";
import {saveAs} from 'file-saver';
import * as JSZip from 'jszip';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit, OnDestroy {

  faDownload = faDownload;
  faRedo = faRedo;
  executionDetails: boolean = false;
  es: LocaleSettings;
  iconCalendar = ICON_CALENDAR;
  formIndice: FormGroup;
  indices: any;
  start_date: Date = new Date();
  end_date: Date = new Date();
  folderSelected: any[] = [];
  listadoIndices$: Observable<IndiceElectronicoDTO>;
  estados: Array<{ label: string, value: string }>;
  subscriptions: Subscription[] = [];
  dataCSV: any;
  date = new Date();
  @ViewChild('details') details;
  user: any;
  fechaInicial: boolean = false;
  fechaFinal: boolean = false;
  objectStore: Array<{ label: string, value: string }>;
  menuIndices: MenuItem[];
  isMasivoVisible: boolean = false;
  isIndividualVisible: boolean = false;
  @ViewChild('ipmValueMassive') ipmValueMassive;
  @ViewChild('ipmValueIndividual') ipmValueIndividual;
  showXML: Boolean = false;
  document: any;
  buttonDetails: boolean = false;
  @ViewChild('viewXml') viewXML;
  xmlFileName: string;
  title: string;
  documentView: any;

  constructor(private fe: FormBuilder,
              private _indiceElectronicoApi: IndiceElectronicoApi,
              private _store: Store<RootState>,
              private changeDetector: ChangeDetectorRef,) {
  }

  ngOnInit() {

    this.objectStore = [
      {label: "Todos", value: "Todos"},
      {label: "COLPE", value: "COLPE"},
    ]

    this.estados = [
      {label: "Todos", value: ""},
      {label: "Exitoso", value: "Exitoso"},
      {label: "Fallido", value: "Fallido"},
      {label: "Pendiente", value: "Pendiente"},
      {label: "Reintento", value: "Reintento"},
    ]

    this.es = localeCalendarEs;
    this.initFormIndice();
    this.start_date = new Date(this.date.getFullYear(), this.date.getMonth(), 1);

    this.menuIndices = [
      {
        label: 'Individual',
        command: () => {
          this.isIndividualVisible = true;
          this.ipmValueIndividual.getIPMValueIndividual();
        }
      },
      {
        label: 'Masivo',
        command: () => {
          this.isMasivoVisible = true;
          this.ipmValueMassive.getIPMValueMassive();
        }
      },

    ];

  }

  initFormIndice() {
    this.formIndice = this.fe.group({
      fechaInicial: [null, Validators.required],
      fechaFinal: [null, Validators.required],
      codigoSerie: [null],
      codSubSerie: [null],
      codigoDependencia: [null],
      estado: [null],
      objectStore: [this.objectStore[0].value],
      idExpediente: [null],
    });

  }

  findIndices() {
    if (this.formIndice.invalid) {
      this.validateFields();
      this._store.dispatch(new PushNotificationAction({
        summary: 'Faltan datos obligatorios',
        severity: 'info'
      }))
    } else {
      const payload: any = {
        fechaInicial: this.convertDate(this.start_date.setHours(0, 0, 0)),
        fechaFinal: this.convertDate(this.end_date.setHours(23, 59, 59)),
        codigoSerie: !isNullOrUndefined(this.formIndice.get('codigoSerie').value) ? this.formIndice.get('codigoSerie').value : "",
        codSubSerie: !isNullOrUndefined(this.formIndice.get('codSubSerie').value) ? this.formIndice.get('codSubSerie').value : "",
        codigoDependencia: !isNullOrUndefined(this.formIndice.get('codigoDependencia').value) ? this.formIndice.get('codigoDependencia').value : "",
        estado: !isNullOrUndefined(this.formIndice.get('estado').value) ? this.formIndice.get('estado').value : "",
        objectStore: !isNullOrUndefined(this.formIndice.get('objectStore').value) ? this.formIndice.get('objectStore').value : "",
        idExpediente: !isNullOrUndefined(this.formIndice.get('idExpediente').value) ? this.formIndice.get('idExpediente').value : "",
      };
      this.listadoIndices$ = this._indiceElectronicoApi.findIndices(payload)
      this.subscriptions.push(this.listadoIndices$.subscribe((data) => {
        this.indices = data;
      }));
      this.fechaInicial = false;
      this.fechaFinal = false;
    }
  }


  convertDate(inputFormat) {
    function pad(s) {
      return (s < 10) ? '0' + s : s;
    }

    const d = new Date(inputFormat);
    return [pad(d.getFullYear()), pad(d.getMonth() + 1), d.getDate()].join('/') + " " + [d.getHours(), d.getMinutes()].join(':');
  }

  cleanForm() {
    this.formIndice.reset();
    this.listadoIndices$ = null;
    this.end_date = new Date();
    this.start_date = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    this.fechaInicial = false;
    this.fechaFinal = false;
    this.indices = [];
    this.folderSelected = [];
  }

  showDetailsDialog(data: any) {
    this.executionDetails = true
    this.details.dataDetails(data);
  }

  hideExecutionDetailsPopup() {
    this.executionDetails = false;
  }

  hideGenerateMassiveIndexPopup() {
    this.isMasivoVisible = false;
  }

  hideGanerateIndividualIndexPopup() {
    this.isIndividualVisible = false;
  }

  retry() {
    let data = [];
    this.folderSelected.forEach(item => {
      if (!isNullOrUndefined(item.estado) && item.estado == 'Fallido') {
        data.push(item.idExpediente);
      }
    })
    if (!isNullOrUndefined(data) && data.length > 0) {
      this.user = JSON.parse(localStorage.getItem('user'));
      this._indiceElectronicoApi.createIndice(this.user.loginName, data).subscribe(() => {
        this._store.dispatch(new PushNotificationAction({
          summary: 'Se ha registrado el intento de los índices fallidos',
          severity: 'success'
        }))
      });
    } else {
      this._store.dispatch(new PushNotificationAction({
        summary: 'No hay índices electrónicos fallidos por procesar',
        severity: 'info'
      }))
    }

  }

  downloadCSV() {

    if (!isNullOrUndefined(this.indices) && this.indices.length > 0) {
      var options = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true,
        showTitle: true,
        title: 'Consola de Seguimiento ejecucción índice electrónico',
        useBom: true,
        noDownload: false,
        headers: ["ID Indice", "Versión", "GUID de Expediente", "Fecha de Registro", "Fecha de Ejecución", "Object Store", "Código Serie", "Código Subserie", "Cód. depen. productora", "Origen Ejecución", "Estado", "Usuario"],
        useHeader: false,
        nullToEmptyString: true,
      };


      this.dataCSV = this.indices;
      let data = [];
      this.dataCSV.forEach(item => {
        data.push({
          idIndice: item.idIndice,
          version: item.version,
          idExpediente: item.idExpediente,
          creationDate: item.creationDate,
          fechaEjecucion: item.fechaEjecucion,
          objectStore: item.objectStore,
          codSerie: item.codSerie,
          codSubSerie: item.codSubSerie,
          codDependencia: item.codDependencia,
          origenDeEjecucion: item.origenDeEjecucion,
          estado: item.estado,
          usuarioExecute: item.usuarioExecute
        })
      })
      // This data will be generated in the CSV file
      new AngularCsv(data, "Seguimiento-Ejecucion", options);
    } else {
      this._store.dispatch(new PushNotificationAction({
        summary: 'No hay datos para generar el CSV',
        severity: 'info'
      }))
    }
  }

  validateFields() {
    if (isNullOrUndefined(this.start_date) && isNullOrUndefined(this.end_date)) {
      this.fechaInicial = true;
      this.fechaFinal = true;
    } else if (isNullOrUndefined(this.start_date)) {
      this.fechaInicial = true;
    } else if (isNullOrUndefined(this.end_date)) {
      this.fechaFinal = true;
    }

  }

  loadXMLActionsList(): MenuItem[] {
    const items: MenuItem[] = [];
    items.push(
      {
        label: 'XML',
        disabled: false,
        visible: true,
        command: () => {
          var fileType = 'xml';
          this.downloadFile(this.documentView, fileType)
        }
      });
    items.push(
      {
        label: 'PDF/A',
        disabled: false,
        visible: true,
        command: () => {
          var fileType = 'pdf';
          this.downloadFile(this.documentView, fileType);
        }
      });
    return items;
  }

  loadActionsList(): MenuItem[] {
    const items: MenuItem[] = [];
    items.push({
      label: 'Reintentar',
      icon: 'fa ui-icon-refresh menuIcons',
      visible: true,
      disabled: !!isNullOrUndefined(this.folderSelected) || this.folderSelected.length == 0,
      command: () => {
        this.retry();
      }
    });
    items.push(
      {
        label: 'Exp. consulta CSV',
        icon: 'pi pi-download menuIcons',
        disabled: !!isNullOrUndefined(this.indices) || this.indices.length == 0,
        visible: true,
        command: () => {
          this.downloadCSV();
        }
      });
    items.push(
      {
        label: 'Exp. índice XML',
        icon: 'pi pi-download menuIcons',
        disabled: !!isNullOrUndefined(this.folderSelected) || this.folderSelected.length == 0,
        visible: true,
        command: () => {
          var fileType = 'xml';
          this.downloadFile(this.folderSelected, fileType)
        }
      });
    items.push(
      {
        label: 'Exp. índice PDF/A',
        icon: 'pi pi-download menuIcons',
        disabled: !!isNullOrUndefined(this.folderSelected) || this.folderSelected.length == 0,
        visible: true,
        command: () => {
          var fileType = 'pdf';
          this.downloadFile(this.folderSelected, fileType);
        }
      });
    return items;
  }

  downloadFile(folderSelected, fileType) {

    let data = [];
    if (!isNullOrUndefined(folderSelected.length)){
      folderSelected.forEach(item => {
        data.push({
          expedienteClass: item.expedienteClass,
          idExpediente: item.idExpediente,
          objectStore: item.objectStore,
        })
      });
    } else {
      data.push({
        expedienteClass: folderSelected.expedienteClass,
        idExpediente: folderSelected.idExpediente,
        objectStore: folderSelected.objectStore,
      })
    }
    this._indiceElectronicoApi.getDocuments(data, fileType).subscribe(result => {
      if (!isNullOrUndefined(result)) {
      if (folderSelected.length > 1 && !isNullOrUndefined(result)) {
        const jszip = new JSZip();
        for (let index of result) {
          if (!isNullOrUndefined(index)) {
            jszip.file(index.title, index.file, {base64: true});
          }
        }
        if (Object.keys(jszip.files).length !== 0) {
          jszip.generateAsync({type: 'blob'}).then(function (content) {
            saveAs(content, "Indices Electronicos.zip");
          });
        }
      } else {
        result.forEach(item => {
          const dlnk: any = document.getElementById('dwnldLnk');
          dlnk.href = `data:${item.type};base64,${item.file}`;
          dlnk.download = `${item.title}`;
          dlnk.click();
        });
      }
    } else {
        this._store.dispatch(new PushNotificationAction({
          summary: 'El expediente no cuenta con índice electrónico',
          severity: 'info'
        }))
      }
    })


  }

  hideDocumentXML() {
    //this.viewerManager = null;
    //this.showPDF = false;
    this.showXML = false;
    this.buttonDetails = false;
    this.viewXML.hideXML();
    this.changeDetector.detectChanges();
    this.documentView = null;
  }

  showDocumentXml(folderSelected) {
    let data = [];
    var fileType = 'xml';
    this.documentView = folderSelected;
    this.title = folderSelected.idExpediente;
    data.push({
      expedienteClass: folderSelected.expedienteClass,
      idExpediente: folderSelected.idExpediente,
      objectStore: folderSelected.objectStore,
    })
    this.buttonDetails = true;
    this.showXML = true;
    this._indiceElectronicoApi.getDocuments(data, fileType).subscribe(result => {
      if (!isNullOrUndefined(result[0])) {
      this.document = result;
      if (this.document) {
        this.xmlFileName = this.document[0].title;
        const xmlDocument: any = document.getElementById('dwnldLnk');
        xmlDocument.href = `data:${this.document[0].type};base64,${this.document[0].file}`;
        xmlDocument.download = `${this.document[0].title}`;
        // this.displayResult(xmlDocument);
        this.viewXML.getParameters(xmlDocument);
        //this.viewXML.loadXMLDoc(this.xmlFileName);
        //this.viewXML.displayResult();
        this.changeDetector.detectChanges();
      }
      } else {
        this.hideDocumentXML()
        this._store.dispatch(new PushNotificationAction({
          summary: 'El expediente no cuenta con índice electrónico',
          severity: 'info'
        }))
      }
    });

  }

  ngOnDestroy(): void {
  }

}
