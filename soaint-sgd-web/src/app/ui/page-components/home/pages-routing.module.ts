import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home.component';
import {ROUTES_PATH} from '../../../app.route-names';
import {AuthenticatedGuard} from '../login/__login.include';
import {BpmmanagerGuard} from '../../../shared/guards/bpmmanager.guard';
import {WorkspaceComponent} from '../workspace/workspace.component';
//import {SecurityRoleComponent} from '../security-role/security-role.component';
import {WorkspaceUserComponent} from '../workspace-user/workspace-user.component';
import {ModuleWithProviders} from '@angular/core';
import {ConsolaSeguimientoComponent} from "../consola-seguimiento/consola-seguimiento.component";
import {AdministracionComponent} from "../administracion/administracion.component";


export const routes: Routes = [
  {path: 'bussiness/dashboard', component: HomeComponent},
  {path: '', redirectTo: ROUTES_PATH.home, pathMatch: 'full'},
  {path: ROUTES_PATH.home, component: HomeComponent, canActivate: [AuthenticatedGuard]},
  {
    path: ROUTES_PATH.task,
    canActivate: [AuthenticatedGuard],
    children: [
      ]
  },
    {path: ROUTES_PATH.workspace, component: WorkspaceComponent, canActivate: [AuthenticatedGuard]},
    /*{
    path: ROUTES_PATH.securityRole.url,
    component: SecurityRoleComponent,
    canActivate: [BpmmanagerGuard]
  },*/
  {
    path: ROUTES_PATH.taskManager.url,
    component: WorkspaceUserComponent,
    canActivate: [BpmmanagerGuard]
  },
  {
    path: ROUTES_PATH.consolaSeguimiento,
    component: ConsolaSeguimientoComponent,
  },
  {
    path: ROUTES_PATH.administracion,
    component: AdministracionComponent,
  }
];


export const AppRoutes: ModuleWithProviders = RouterModule.forChild(routes);
