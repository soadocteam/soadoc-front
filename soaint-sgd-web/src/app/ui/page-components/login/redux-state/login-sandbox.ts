import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../../../environments/environment';
import {HttpHandler} from '../../../../infrastructure/utils/http-handler';
import {Usuario} from '../../../../domain/usuario';
import {Router} from '@angular/router';
import {Store, Action} from '@ngrx/store';
import {State} from '../../../../infrastructure/redux-store/redux-reducers';
import * as selectors from './login-selectors';
import * as actions from './login-actions';
import {UserCredentials} from '../models/user-credentials.model';
import { of } from 'rxjs/observable/of';
import {HttpClient} from "@angular/common/http";
import {LoginApiService} from "../../../../infrastructure/api/login.api";


@Injectable()
export class LoginSandbox {

  constructor(private _router: Router,
              private _http: HttpHandler,
              private _store: Store<State>,
              private http: HttpClient,
              private _loginApi: LoginApiService) {
  }


  login(user: Usuario){
    //return this._http.post(environment.security_endpoint + '/login', user);
   // return this.http.post("https://reqres.in/api/login", user);
    return this._loginApi.login(user);
  }

  routeToHome(): void {
    this._router.navigate(['bussiness/home'])
  }

  routeToLogin(): void {
    this._router.navigate(['/login'])
  }

  selectorLoading(): Observable<boolean> {
    return this._store.select(selectors.isLoading);
  }

  selectorError(): Observable<string> {
    return this._store.select(selectors.getError)
  }

  selectorToken(): Observable<string> {
    return this._store.select(selectors.getToken);
  }

  selectorAuthenticated(): any {
    return this._store.select(selectors.isAuthenticated);
  }

  loginDispatch(payload: UserCredentials) {
    this._store.dispatch(new actions.LoginAction(payload));
  }

  logoutDispatch() {
    this._store.dispatch(new actions.LogoutAction());
  }


}
