import {Injectable} from '@angular/core';
import {Effect, Actions, ofType} from '@ngrx/effects';
import {Action, Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import * as login from './login-actions';
import {LoginSandbox} from './login-sandbox';
import {tassign} from 'tassign';
import {State as RootState} from '../../../../infrastructure/redux-store/redux-reducers';
import { Router } from '@angular/router';
import { map, switchMap, tap, catchError } from 'rxjs/operators';
import { of, from } from 'rxjs';
import { ROUTES_PATH } from '../../../../app.route-names';
import { LoginAction } from './login-actions';
import {LoadSuccessAction as FuncionarioAutenticatedAction} from "../../../../infrastructure/state-management/funcionarioDTO-state/funcionarioDTO-actions";


@Injectable()
export class LoginEffects {

  constructor(private actions$: Actions,
              private loginSandbox: LoginSandbox,
              private _store:Store<RootState>,
              private _router: Router
              ) {
  }

  @Effect()
  login: Observable<Action> = <Observable<Action>>this.actions$.pipe(
    ofType(login.ActionTypes.LOGIN),
    map(action => (<LoginAction>action).payload),
    switchMap(payload => {
        return this.loginSandbox.login({estado: payload['estado'], loginName: payload['loginName'], password: payload['password']})
          .pipe(
            catchError(error => of( new login.LoginFailAction({error: error}) )),
            switchMap((response: any) => {
                return response instanceof login.LoginFailAction ? of(response) : from([
                  new login.LoginSuccessAction(tassign(response, {credentials: payload})),
                  new FuncionarioAutenticatedAction(response),
                ]);
              }
            ));
      }));

  // login: Observable<Action> = <Observable<Action>>this.actions$.pipe(
  //   ofType(login.ActionTypes.LOGIN)
  //   ,map(action=> (<LoginAction>action)['payload'])
  //   ,switchMap((payload) => {
  //         return this.loginSandbox.login({login: payload['username'], password: payload['password']})
  //         .pipe(
  //           catchError(error => of( new login.LoginFailAction({error: error}) )),
  //           switchMap((response: any) => {
  //               return response instanceof login.LoginFailAction ? of(response) : from([
  //                 new login.LoginSuccessAction(tassign(response, { credentials: payload})),
  //                 new FuncionarioAutenticatedAction(response.profile),
  //                 this._router.navigate(['/home'])
  //               ]);
  //             }
  //           ))
  //         }));



  @Effect({dispatch: false})
  loginSuccess: Observable<Action> = this.actions$.pipe(
    ofType(login.ActionTypes.LOGIN_SUCCESS)
    ,map(action => action['payload'])
    ,tap( r => {

      if(r['noSaveSession'])
        return;
        let user = r['credentials']['username'];
        let pssw =  btoa(r['credentials']['password'])
       const sessionData = {
         credentials:{username: user, password: pssw},
         token:r['token'],
         profile:r['profile']
       };
       localStorage.setItem("session",JSON.stringify(sessionData));


    }
  ));

  @Effect()
  logout: Observable<Action> = this.actions$.pipe(
    ofType(login.ActionTypes.LOGOUT)
    ,map(action => action['payload'])
    ,tap( _ => {
      localStorage.removeItem("session");
      localStorage.removeItem("lastActivity");
      this._router.navigate(['/login'])
    }))

}
