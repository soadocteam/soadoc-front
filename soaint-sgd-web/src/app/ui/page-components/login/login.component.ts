import {ChangeDetectionStrategy, Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {LoginSandbox} from './redux-state/login-sandbox';
import {Observable} from 'rxjs/Observable';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {environment} from "../../../../environments/environment";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../infrastructure/redux-store/redux-reducers";
import {getAuthenticatedFuncionario} from "../../../infrastructure/state-management/funcionarioDTO-state/funcionarioDTO-selectors";
import {isNullOrUndefined} from "util";
import {Subscription} from "rxjs/Subscription";
import {LoginAction} from './redux-state/login-actions';
import {AdminLayoutSandbox} from '../../layout-components/container/admin-layout/redux-state/admin-layout-sandbox';


@Component({
  selector: 'app-login',
  providers: [LoginSandbox],
  templateUrl: './login.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit, OnDestroy {

  public loading$: Observable<boolean>;
  public error$: Observable<string>;
  public form: FormGroup;
  isAuthenticated$: Observable<boolean>;
  isLoadingToolbox = true;
  subscriptions: Subscription[] = [];
  user: any;

  public readonly baseUrl = environment.base_url;

  constructor(private _sandbox: LoginSandbox,
              private _formBuilder: FormBuilder,
              private _store: Store<RootState>,
              private _sandboxA: AdminLayoutSandbox
  ) {
    this.initForm();
    this.isAuthenticated$ = this._sandboxA.selectorIsAutenticated();
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('toolboxSession'));
    if (!isNullOrUndefined(this.user)){
      this.signIn();
    }
    this.subscriptions.push(
      this._store.select(getAuthenticatedFuncionario).subscribe(funcionario => {

        if (!isNullOrUndefined(funcionario)) {
          window.history.forward();
          return
        } else {
          this._sandbox.routeToHome();
        }
        this.loading$ = this._sandbox.selectorLoading();
        this.error$ = this._sandbox.selectorError();
      })
    );
  }

  @HostListener('window:message', ['$event'])
  login(evt) {
    if (evt.origin === (<any>window).toolboxOrigin) {
      const data = evt.data ? evt.data : (!isNullOrUndefined(evt.originalEvent) ? evt.originalEvent.data : null);
      if (!isNullOrUndefined(data) && !isNullOrUndefined(data.user) && !isNullOrUndefined(data.password)) {
        this._store.dispatch(new LoginAction({username: data.user, password: data.password}));
        this.isLoadingToolbox = false;
      }
    }
  }

  initForm() {
    this.form = this._formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  /**
   * Submit the authentication form.
   * @method submit
   */
  public signIn() {
    // get email and password values
    const username: string = this.form.get('username').value;
    const password: string = this.form.get('password').value;


    // trim values
    username.trim();
    password.trim();

    // set payload
    var payload;
    if (!isNullOrUndefined(this.user)){
      payload = {
        estado: "Activo",
        loginName: this.user.credentials.loginName,
        password: atob(this.user.credentials.password)
      };
    } else {
      payload = {
        estado: "Activo",
        loginName: username,
        password: password
      };
    }

    localStorage.setItem('user',JSON.stringify(payload));
    this._sandbox.loginDispatch(payload);
  }

  ngOnDestroy(): void {
    this.isLoadingToolbox = true;
    this.subscriptions.forEach(s => s.unsubscribe());
  }

}
