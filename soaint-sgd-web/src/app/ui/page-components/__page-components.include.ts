import {HomeComponent} from './home/home.component';
import {WorkspaceComponent} from './workspace/workspace.component';
import {DatosDireccionComponent} from '../bussiness-components/datos-direccion/datos-direccion.component';
import {PopupJustificacionComponent} from '../bussiness-components/popup-justificacion/popup-justificacion.component';
import {PopupAgregarObservacionesComponent} from '../bussiness-components/popup-agregar-observaciones/popup-agregar-observaciones.component';
import {PopupRejectComponent} from '../bussiness-components/popup-reject/popup-reject.component';
import {GestionarComunicacionComponent} from '../bussiness-components/gestionar-comunicacion/gestionar-comunicacion.component';
import {EditarPlanillaComponent} from '../bussiness-components/editar-planilla/editar-planilla.component';
import {PlanillaGeneradaComponent} from '../bussiness-components/planilla-generada/planilla-generada.component';
import {PdfViewerComponent} from '../bussiness-components/pdf-viewer/pdf-viewer.component';
import {DireccionToTextPipe} from '../bussiness-components/datos-direccion/direccion-to-text.pipe';
import {SingleUploadComponent} from '../layout-components/presentation/single-upload/single-upload.component';
//import {SecurityRoleComponent} from './security-role/security-role.component';
//import {FuncionarioListComponent} from './security-role/components/funcionario-list/funcionario-list.component';
//import {ControlMessagesComponent} from './security-role/components/control-messages/control-messages.component';
import {DocumentoEcmComponent} from '../bussiness-components/documento-ecm/documento-ecm.component';
import {DatosRemitentesComponent} from '../bussiness-components/datos-remitentes/datos-remitentes.component';
import {TicketRadicadoComponent} from '../bussiness-components/ticket-radicado/ticket-radicado.component';
import {DatosGeneralesEditComponent} from '../bussiness-components/datos-generales-edit/datos-generales-edit.component';
import {DatosRemitenteEditComponent} from '../bussiness-components/datos-remitente-edit/datos-remitente-edit.component';
import {DatosDestinatarioEditComponent} from '../bussiness-components/datos-destinatario-edit/datos-destinatario-edit.component';
import {AlertComponent} from '../bussiness-components/notifications/alert/alert.component';
import {PopupUnidadDocumentalNotasComponent} from '../bussiness-components/popup-unidad-documental-notas/popup-unidad-documental-notas.component';
import {InformacionEnvioComponent} from '../bussiness-components/informacion-envio/informacion-envio.component';
import {PopupUbicarTransferenciaComponent} from '../bussiness-components/popup-ubicar-transferencia/popup-ubicar-transferencia.component';
import {WorkspaceUserComponent} from './workspace-user/workspace-user.component';
import {AsociarRadicadosComponent} from '../bussiness-components/asociar-radicados/asociar-radicados.component';
//import {ProfileEditComponent} from './security-role/components/profile-edit/profile-edit.component';
import {DatosUnidadConservacionComponent} from '../bussiness-components/datos-unidad-conservacion/datos-unidad-conservacion.component';
import {PieChartComponent} from '../layout-components/presentation/charts/pie-chart';
import {PieGridChartComponent} from '../layout-components/presentation/charts/pie-grid';
import {GaugeChartComponent} from '../layout-components/presentation/charts/gauche-chart';
import {InformacionAnulacionComponent} from "../bussiness-components/informacion-anulacion/informacion-anulacion.component";
import {ConsolaSeguimientoComponent} from "./consola-seguimiento/consola-seguimiento.component";
import {PopupDetalleEjecucionComponent} from "../bussiness-components/popup-detalle-ejecucion/popup-detalle-ejecucion.component";
import {AdministracionComponent} from "./administracion/administracion.component";
import {ProduccionDocumentalComponent} from "./produccion-documental/produccion-documental.component";
import {PopupGenerarIndiceIndividualComponent} from "../bussiness-components/popup-generar-indice-individual/popup-generar-indice-individual.component";
import {PopupGenerarIndiceMasivoComponent} from "../bussiness-components/popup-generar-indice-masivo/popup-generar-indice-masivo.component";
import {PopupParametrizarHorarioDemandaComponent} from "../bussiness-components/popup-parametrizar-horario-demanda/popup-parametrizar-horario-demanda.component";


/**
 * All state updates are handled through dispatched actions in 'container'
 * components. This provides a clear, reproducible history of state
 * updates and user interaction through the life of our
 * application.
 *
 * Note: Container components are also reusable. Whether or not
 * a component is a presentation component or a container
 * component is an implementation detail.
 */
export const PAGE_COMPONENTS = [
    PdfViewerComponent,
  PieChartComponent,
  PieGridChartComponent,
  GaugeChartComponent,
  HomeComponent,
  WorkspaceComponent,
  DatosDireccionComponent,
  PopupJustificacionComponent,
  PopupAgregarObservacionesComponent,
  PopupRejectComponent,
  GestionarComunicacionComponent,
  EditarPlanillaComponent,
  PlanillaGeneradaComponent,
  DireccionToTextPipe,
  SingleUploadComponent,
 // SecurityRoleComponent,
  //FuncionarioListComponent,
  //ControlMessagesComponent,
  DocumentoEcmComponent,
  DatosRemitentesComponent,
  TicketRadicadoComponent,
  DatosGeneralesEditComponent,
  DatosRemitenteEditComponent,
  DatosDestinatarioEditComponent,
  AlertComponent,
  PopupUnidadDocumentalNotasComponent,
  InformacionEnvioComponent,
  PopupUbicarTransferenciaComponent,
  WorkspaceUserComponent,
  AsociarRadicadosComponent,
 // ProfileEditComponent,
  DatosUnidadConservacionComponent,
  InformacionAnulacionComponent,
  ConsolaSeguimientoComponent,
  PopupDetalleEjecucionComponent,
  AdministracionComponent,
  ProduccionDocumentalComponent,
  PopupGenerarIndiceIndividualComponent,
  PopupGenerarIndiceMasivoComponent,
  PopupParametrizarHorarioDemandaComponent
];

export * from './__page-providers.include';
