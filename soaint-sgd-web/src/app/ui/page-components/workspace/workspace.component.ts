import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {TareaDTO} from '../../../domain/tareaDTO';
import {State as RootState} from '../../../infrastructure/redux-store/redux-reducers';
import {Store} from '@ngrx/store';
import {getArrayData, getTaskCount} from '../../../infrastructure/state-management/tareasDTO-state/tareasDTO-selectors';
import {Sandbox as TaskDtoSandbox} from '../../../infrastructure/state-management/tareasDTO-state/tareasDTO-sandbox';
import {getSelectedDependencyGroupFuncionario} from '../../../infrastructure/state-management/funcionarioDTO-state/funcionarioDTO-selectors';
import {Subscription} from 'rxjs/Subscription';
import {DependenciaDTO} from '../../../domain/dependenciaDTO';
import {isNullOrUndefined} from "util";
import {DatePipe} from "@angular/common";
import {A_TIEMPO, PROXIMO_VENCER, VENCIDO} from "../../../shared/constants";
import {RadicadosApi} from "../../../infrastructure/api/radicados.api";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html'
})
export class WorkspaceComponent implements OnInit, OnDestroy {

  tasks$: Observable<TareaDTO[]>;

  totalRecords$: Observable<number>;

  currentPage: number;

  readonly pageSize = 10;

  private init = false;

  selectedTask: any;

  globalDependencySubcription: Subscription;

  dependenciaSelected$: Observable<DependenciaDTO>;

  form: FormGroup;

  search: boolean = false;

  constructor(private _store: Store<RootState>,
              private _taskSandbox: TaskDtoSandbox,
              private dataPipe: DatePipe,
              private cdRef: ChangeDetectorRef,
              private _router: Router,
              private radicadosService: RadicadosApi,
              private formBuilder: FormBuilder) {
    this.tasks$ = this._store.select(getArrayData);
    this.totalRecords$ = this._store.select(getTaskCount);
    this.dependenciaSelected$ = this._store.select(getSelectedDependencyGroupFuncionario);
    this.initForm();
  }

  initForm() {
    this.form = this.formBuilder.group({
      'filter': [null, Validators.required]
    });
  }

  ngOnInit() {
    this.globalDependencySubcription = this.dependenciaSelected$.subscribe((result) => {
      this._taskSandbox.loadDispatch({page: 0, pageSize: this.pageSize});
    });

    // this.totalRecords$.subscribe( e => console.log(e));
  }

  ngOnDestroy() {
    this.globalDependencySubcription.unsubscribe();
  }

  iniciarTarea(task) {
    if (task.estado === 'LISTO') {
      this._taskSandbox.reserveTaskDispatch(task);
    } else {
      this._taskSandbox.startTaskDispatch(task);
    }
  }

  loadTaskPage(evt) {
    let sortOrder = "";
    let payload: any = {
      page: Math.round(evt.first / evt.rows),
      pageSize: this.pageSize
    }
    if (!this.init) {
      this.init = true;
      return;
    }
    if (evt.sortOrder) {
      if (evt.sortOrder == -1) {
        sortOrder = "desc"
      } else {
        sortOrder = "asc"
      }
      payload.sortOrder = sortOrder;
    }
    if (evt.sortField) {
      payload.sortField = evt.sortField;
    }
    const filter: any = this.form.get("filter").value;
    if (!isNullOrUndefined(filter) && filter.trim() != "") {
      payload.filter = filter.trim();
    }
    this._taskSandbox.loadDispatch(payload);
  }

  getFechaVencimiento(row: any): any {
    return this.getStatusVencimiento(row) + " : " + this.dataPipe.transform(row.variables.fechaVencimiento, 'dd/MM/yyyy hh:mm a');
  }

  getStatusTrackIcon(track) {
    const status = this.getStatusVencimiento(track);
    let iconClass;
    switch (status) {
      case A_TIEMPO: {
        iconClass = 'icons-primary';
        break;
      }
      case PROXIMO_VENCER  : {
        iconClass = 'icons-secundary';
        break;
      }
      case VENCIDO: {
        iconClass = 'icons-tertiary';
        break;
      }
    }
    return iconClass;
  }

  getStatusVencimiento(track): string {
    let status;
    const today = new Date();
    const preFecha = new Date(track.variables.preAlerta);
    const fechaVen = new Date(track.variables.fechaVencimiento);
    if (today >= fechaVen) {
      status = VENCIDO
    } else if (today >= preFecha) {
      status = PROXIMO_VENCER
    } else {
      status = A_TIEMPO
    }
    return status;
  }

  isAnnulled(nroRadicado: string) {
    const params: any = {};
    params.annulmentInd = true;
    params.trackId = nroRadicado;
    let response;
    response = this.radicadosService.getRadicadosNoAnulados(params).subscribe(res => {

    });
    return isNullOrUndefined(response);
  }

  loadTasks() {
    if (this.form.valid) {
      this.search = true;
      let payload: any = {
        page: 0,
        pageSize: this.pageSize
      }
      const filter: any = this.form.get("filter").value;
      if (!isNullOrUndefined(filter) && filter.trim() != "") {
        payload.filter = filter.trim();
      }
      this._taskSandbox.loadDispatch(payload);
    } else {
      this._store.dispatch(new PushNotificationAction({
        summary: 'Debe ingresar un criterio de búsqueda',
        severity: 'info'
      }));
    }
  }

  clear() {
    this.form.get("filter").reset();
    if (this.search) {
      this._taskSandbox.loadDispatch({page: 0, pageSize: this.pageSize});
    }
  }
}

