import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import 'rxjs/add/operator/withLatestFrom';

@Component({
  selector: 'app-administracion',
  templateUrl: './administracion.component.html',
  styleUrls: ['./administracion.component.scss']
})
export class AdministracionComponent implements OnInit, OnDestroy {

  isParametrizarHorario: boolean = false;
  parametrizarExcepcion: boolean = false;
  visibleDialogCierreAuto = false;
  isParametrizarHorarioDemanda: boolean = false;
  @ViewChild('exceptions') exceptions;
  @ViewChild('dailyExecution') dailyExecution;
  @ViewChild('demandExecution') demandExecution;
  @ViewChild('configurations') configurations;

  constructor() {

  }

  ngOnInit() {

  }

  showParametrizarHorario() {
    this.isParametrizarHorario = true;
    this.dailyExecution.getExecuteTime();
  }

  showParametrizarHorarioDemanda() {
    this.isParametrizarHorarioDemanda = true;
    this.demandExecution.getExecuteTime();
  }

  hideParametrizarHorario() {
    this.isParametrizarHorario = false;
  }

  hideParametrizarHorarioDemanda() {
    this.isParametrizarHorarioDemanda = false;
  }

  showParametrizarExcepcion() {
    this.parametrizarExcepcion = true;
    this.exceptions.getExceptions();
  }

  showDialogCierreAuto() {
    this.visibleDialogCierreAuto = true;
    this.configurations.getAllConfigurations();
  }

  hideParametrizarExcepcion() {
    this.parametrizarExcepcion = false;
    this.configurations.clearForm();
  }


  ngOnDestroy(): void {
  }


}
