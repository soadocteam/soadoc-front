import {Component, EventEmitter, forwardRef, Inject, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {AdminLayoutComponent} from '../../container/admin-layout/admin-layout.component';
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../../infrastructure/redux-store/redux-reducers";
import {Observable} from 'rxjs';
import {getAuthenticatedFuncionario} from "../../../../infrastructure/state-management/funcionarioDTO-state/funcionarioDTO-selectors";
import {isNullOrUndefined} from "util";
import {MenuItem} from "primeng/primeng";
import {AdminLayoutSandbox} from "../../container/admin-layout/redux-state/admin-layout-sandbox";
import {ROUTES_PATH} from "../../../../app.route-names";
import {BPMMANAGER_ROL} from "../../../../app.roles";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Subscription} from "rxjs/Subscription";
import {isOpenTask} from "../../../../infrastructure/state-management/tareasDTO-state/tareasDTO-selectors";
import {TareaDTO} from "../../../../domain/tareaDTO";
import {Router} from "@angular/router";

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopBarComponent implements OnInit {

  @Input() isAuthenticated: boolean;
  form: FormGroup;
  formSubscription: Subscription;
  @Input() dependencias: Array<any> = [];
  @Input() dependenciaSelected: any;
  @Output() onSelectDependencia: EventEmitter<any> = new EventEmitter();
  activeTaskUnsubscriber: Subscription;
  logo = true;
  isOpenTask = false;
  task: TareaDTO;
  routerLinks: MenuItem[];
  urlBreadCrumb: string;
  gestionExpediente = {label: 'Gestión de Expediente'};
  gestionDocumentos = {label: 'Gestion de Documentos'};
  planillas = {label: 'Planillas'};
  menuIcon = 'menu';
  private items: MenuItem[];

  constructor(@Inject(forwardRef(() => AdminLayoutComponent)) public app: AdminLayoutComponent
    , private _store: Store<RootState>, private _sanbox: AdminLayoutSandbox, private formBuilder: FormBuilder
    , public router: Router) {

    this.routerLinks = [
      {label: 'Administración de tareas', icon: 'settings', routerLink: ['bussiness/' + ROUTES_PATH.taskManager.url]},
      {label: 'Administración de listas', icon: 'settings', routerLink: ['bussiness/' + ROUTES_PATH.listaManager.url]},
    ];

  }

  ngOnInit() {
    this.router.events.subscribe(data => {
      this.breadCrumb(this.router.url);
    });

    this.form = this.formBuilder.group({
      'dependencia': [null]
    });

    this.formSubscription = this.form.get('dependencia').valueChanges.distinctUntilChanged().subscribe(value => {
      this.onSelectDependencia.emit(value);
    });

    this.activeTaskUnsubscriber = this._store.select(isOpenTask)
      .subscribe(inTask => {
        this.isOpenTask = inTask;
      });

  }

  iconMenu() {
    if (this.menuIcon === 'menu') {
      this.menuIcon = 'menu_open';
    } else if (this.menuIcon === 'menu_open') {
      this.menuIcon = 'menu';
    }
  }

  breadCrumb(url: any): any {
    if (url.includes('/bussiness/home')) {
      return this.items = [
        {label: 'Administración', url: ''}];
    } else if (url.includes('/bussiness/workspace')) {
      this.items = [
        {label: 'Mis Asignaciones', url: ''}];
    } else if (url.includes('/bussiness/consulta-documentos')) {
      this.items = [
        {label: 'Consulta', url: ''}];
    } else if (url.includes('/bussiness/disposicion-final')) {
      this.items = [
        this.gestionExpediente,
        {label: 'Disposición Final', url: ''}];
    } else if (url.includes('/bussiness/descripcion-archivistica')) {
      this.items = [
        this.gestionExpediente,
        {label: 'Descripción Archivistica', url: ''}];
    } else if (url.includes('/bussiness/task/gestion-unidad-documental')) {
      this.items = [
        this.gestionExpediente,
        {label: 'Gestión de Unidades Documentales', url: ''}];
    }  else if (url.includes('/bussiness/task/transferencia-documentales')) {
      this.items = [
        this.gestionExpediente,
        {label: 'Transferencia', url: ''}];
    } else if (url.includes('/bussiness/task/archivar-documento') || url.includes('/bussiness/task/crear-unidad-documental')) {
      this.items = [
        this.gestionExpediente,
        {label: 'Organizacion y Archivo', url: ''}];
    } else if (url.includes('/bussiness/asignacion-comunicaciones')) {
      this.items = [
        this.gestionDocumentos,
        {label: 'Asignar Comunicaciones', url: ''}];
    } else if (url.includes('/bussiness/task/documentos-tramite')) {
      this.items = [
        this.gestionDocumentos,
        {label: 'Recibir y Gestionar Documentos', url: ''}];
    } else if (url.includes('/bussiness/carga-masiva')) {
      this.items = [
        this.gestionDocumentos,
        {label: 'Contingencia de Entrada', url: ''}];
    } else if (url.includes('/bussiness/task/produccion-documental-multiple')) {
      this.items = [
        this.gestionDocumentos,
        {label: 'Producción Multiples Documentos', url: ''}];
    } else if (url.includes('/bussiness/task/producir-documento')) {
      this.items = [
        this.gestionDocumentos,
        {label: 'Producción Documental', url: ''}];
    } else if (url.includes('/bussiness/task/firmar-documento')) {
      this.items = [
        this.gestionDocumentos,
        {label: 'Firmar documento', url: ''}];
    } else if (url.includes('/bussiness/task/radicar-comunicaciones')) {
      this.items = [
        this.gestionDocumentos,
        {label: 'Correspondencia de Entrada', url: ''}];
    } else if (url.includes('/bussiness/task/radicacion-salida') || url.includes('/bussiness/task/radicacion-documento_salida')) {
      this.items = [
        this.gestionDocumentos,
        {label: 'Correspondencia de Salida', url: ''}];
    } else if (url.includes('/bussiness/task/adjuntar-documentos')) {
      this.items = [
        this.gestionDocumentos,
        {label: 'Adjuntar Documentos', url: ''}];
    } else if (url.includes('/bussiness/task/corregir-radicacion') || url.includes('/bussiness/task/gestionar-devoluciones')) {
      this.items = [
        this.gestionDocumentos,
        {label: 'Gestionar Devoluciones', url: ''}];
    } else if (url.includes('/bussiness/distribucion-fisica')) {
      this.items = [
        this.planillas,
        {label: 'Distribución Física Planillas', url: ''}];
    } else if (url.includes('/bussiness/gestion-entrega-terceros')) {
      this.items = [
        this.planillas,
        {label: 'Gestión de Entregas'}];
    } else if (url.includes('/bussiness/recibo-documentos-fisicos')) {
      this.items = [
        this.planillas,
        {label: 'Recibo Documentos Fisicos', url: ''}];
    } else if (url.includes('/bussiness/anular-radicados')) {
      this.items = [
        this.gestionDocumentos,
        {label: 'Anular radicados'}]
    }else if (url.includes('/bussiness/sobres-enviar')) {
      this.items = [
        this.gestionDocumentos,
        {label: 'Visualización de sobres'}]
    }else if(url.includes('/revisar-progreso-comunicacion-masiva')){
      this.items = [
        {label: "Revisar progreso comunicaciones masivas"}
      ]
    }else if(url.includes('/validar-comunicacion-masiva')){
      this.items = [
        {label: "Validar comunicaciones masivas"}
      ]
    } else if(url.includes('/administracion')){
      this.items = [
        {label: "Administración"}
      ]
    } else if(url.includes('/consola-seguimiento')){
      this.items = [
        {label: "Consola de Casos"}
      ]
    }
  }

  clickLink(item: MenuItem) {

    if (!isNullOrUndefined(item.command))
      item.command.call(this);

    this.app.activeTopbarItem = null;

  }

  showAdministration(): Observable<boolean> {

    return this._store.select(getAuthenticatedFuncionario)
      .map(funcionario => !isNullOrUndefined(funcionario) && !isNullOrUndefined(funcionario.roles) && funcionario.roles.some(rol => rol.rol == BPMMANAGER_ROL));
  }

}
