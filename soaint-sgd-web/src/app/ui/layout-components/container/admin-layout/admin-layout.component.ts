import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  Renderer,
  ViewChild
} from '@angular/core';

import {MenuOrientation} from './models/admin-layout.model';
import {Observable} from 'rxjs/Observable';
import {AdminLayoutSandbox} from './redux-state/admin-layout-sandbox';
import {
  MENU,
  MENU_OPTIONS,
} from '../../../../shared/menu-options/menu-options';
import {ConstanteDTO} from '../../../../domain/constanteDTO';
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../../infrastructure/redux-store/redux-reducers";
import {
  getAuthenticatedFuncionario,
  getSelectedDependencyGroupFuncionario
} from "../../../../infrastructure/state-management/funcionarioDTO-state/funcionarioDTO-selectors";
import {isNullOrUndefined} from "util";
import {Subscription} from "rxjs/Subscription";
import {combineLatest} from "rxjs/observable/combineLatest";
import {LoginSandbox} from '../../../../ui/page-components/login/__login.include';
import {GESTOR_DOCUMENTAL, GESTOR_EXPEDIENTE} from "../../../../app.roles";
import {Utils} from "../../../../shared/helpers";


enum LayoutResponsive {
  MOBILE,
  TABLET,
  DESKTOP
}

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminLayoutComponent implements AfterViewInit, OnInit, OnDestroy {

  processOptions: Observable<any[]>;

  layoutWidth$: Observable<number>;

  menuOptions: Array<any> = [];


  layoutMode: MenuOrientation = MenuOrientation.STATIC;

  darkMenu = false;

  profileMode = 'inline';

  rotateMenuButton: boolean;

  topbarMenuActive: boolean;

  overlayMenuActive: boolean;

  staticMenuDesktopInactive: boolean;

  staticMenuMobileActive: boolean;

  layoutContainer: HTMLDivElement;

  layoutMenuScroller: HTMLDivElement;

  menuClick: boolean;

  topbarItemClick: boolean;

  activeTopbarItem: any;

  documentClickListener: Function;

  resetMenu: boolean;

  menuIsVisible: boolean = true;

  funcionarioLog;

  @ViewChild('layoutContainer') layourContainerViewChild: ElementRef;

  @ViewChild('layoutMenuScroller') layoutMenuScrollerViewChild: ElementRef;

  isAuthenticated$: Observable<boolean>;

  layoutResponsive: LayoutResponsive;

  funcionarioDependenciaSuggestions$: Observable<ConstanteDTO[]>;
  funcionarioDependenciaSelected$: Observable<ConstanteDTO>;
  usernameAuth$: Observable<string>;
  subscriptions: Subscription[] = [];
  dependencia;

  constructor(private _sandboxL: LoginSandbox, private _sandbox: AdminLayoutSandbox, public renderer: Renderer,
              private _store: Store<RootState>
  ) {
  }

  async ngOnInit() {
    await this._store.select(getSelectedDependencyGroupFuncionario).subscribe(res => this.dependencia = res);

    combineLatest(this._store.select(getAuthenticatedFuncionario), this._store.select(getSelectedDependencyGroupFuncionario))
      .subscribe(([funcionario, dependencia]) => {

        if (isNullOrUndefined(funcionario))
          return;

        this.funcionarioLog = funcionario.loginName;

        this.menuIsVisible = true;

        this.menuOptions = MENU_OPTIONS;



        this.menuOptions.sort( (a, b) => (a.id - b.id));
      });

    //this.processOptions = this._sandbox.selectorDeployedProcess();
    this.isAuthenticated$ = this._sandbox.selectorIsAutenticated();
    this.usernameAuth$ = this._sandbox.selectorUsername();
    this.layoutWidth$ = this._sandbox.selectorWindowWidth();
    this.funcionarioDependenciaSuggestions$ = Utils.sortObservable(this._sandbox.selectorFuncionarioAuthDependenciasSuggestions(), 'nombre');
    this.funcionarioDependenciaSelected$ = this._sandbox.selectorFuncionarioAuthDependenciaSelected();

    this.layoutWidth$.subscribe(width => {
      if (width <= 640) {
        return this.layoutResponsive = LayoutResponsive.MOBILE
      }

      if (width <= 1024 && width > 640) {
        return this.layoutResponsive = LayoutResponsive.TABLET
      }

      if (width >= 1024) {
        return this.layoutResponsive = LayoutResponsive.DESKTOP
      }
    });

    this.hideMenu();

    this.isAuthenticated$.subscribe(isAuthenticated => {
      // console.info(isAuthenticated);
      if (isAuthenticated) {
       // this._sandbox.dispatchMenuOptionsLoad();
        this.displayMenu();
        this._sandboxL.routeToHome();
      } else {
        this.hideMenu();
      }
    });
  }

  ngAfterViewInit() {
    this.layoutContainer = <HTMLDivElement>this.layourContainerViewChild.nativeElement;
    this.layoutMenuScroller = <HTMLDivElement>this.layoutMenuScrollerViewChild.nativeElement;

    // hides the horizontal submenus or top menu if outside is clicked
    this.documentClickListener = this.renderer.listenGlobal('body', 'click', (event) => {
      if (!this.topbarItemClick) {
        this.activeTopbarItem = null;
        this.topbarMenuActive = false;
      }

      if (!this.menuClick && this.isHorizontal()) {
        this.resetMenu = true;
      }

      this.topbarItemClick = false;
      this.menuClick = false;
    });


  }

  onMenuButtonClick(event) {
    this.rotateMenuButton = !this.rotateMenuButton;
    this.topbarMenuActive = false;

    if (this.layoutMode === MenuOrientation.OVERLAY) {
      this.overlayMenuActive = !this.overlayMenuActive;
    } else {
      if (this.isDesktop()) {

        this.staticMenuDesktopInactive = !this.staticMenuDesktopInactive;
      } else {
        this.staticMenuMobileActive = !this.staticMenuMobileActive;
      }
    }

    event.preventDefault();
  }

  onMenuClick($event) {
    this.menuClick = true;
    this.resetMenu = false;


  }

  onTopbarMenuButtonClick(event) {
    this.topbarItemClick = true;
    this.topbarMenuActive = !this.topbarMenuActive;

    if (this.overlayMenuActive || this.staticMenuMobileActive) {
      this.rotateMenuButton = false;
      this.overlayMenuActive = false;
      this.staticMenuMobileActive = false;
    }

    event.preventDefault();
  }

  onTopbarItemClick(event, item) {
    this.topbarItemClick = true;

    if (this.activeTopbarItem === item) {
      this.activeTopbarItem = null;
    } else {
      this.activeTopbarItem = item;
    }
    event.preventDefault();
  }

  onFuncionarioDependenciaChange(dependenciaGrupo) {
    this._sandbox.dispatchFuncionarioAuthDependenciaSelected(dependenciaGrupo);
  }

  signOff(): void {
    this._sandbox.dispatchLogoutUser();
  }

  public hideMenu(): void {
    this.staticMenuDesktopInactive = true;
    this.staticMenuMobileActive = false;
  }

  public displayMenu(): void {
    this.staticMenuDesktopInactive = false;
    this.staticMenuMobileActive = true;
  }


  isTablet() {
    return this.layoutResponsive === LayoutResponsive.TABLET;
  }

  isDesktop() {
    return this.layoutResponsive === LayoutResponsive.DESKTOP;
  }

  isMobile() {
    return this.layoutResponsive === LayoutResponsive.MOBILE;
  }

  isOverlay() {
    return this.layoutMode === MenuOrientation.OVERLAY;
  }

  isHorizontal() {
    return this.layoutMode === MenuOrientation.HORIZONTAL;
  }

  changeToStaticMenu() {
    this.layoutMode = MenuOrientation.STATIC;
  }

  changeToOverlayMenu() {
    this.layoutMode = MenuOrientation.OVERLAY;
  }

  changeToHorizontalMenu() {
    this.layoutMode = MenuOrientation.HORIZONTAL;
  }

  triggerProccess(param1, param2) {
  }

  ngOnDestroy() {
    if (this.documentClickListener) {
      this.documentClickListener();
    }


    this.subscriptions.forEach(s => s.unsubscribe());
  }

  @HostListener('window:resize', ['$event'])
  onResize($event) {
    this._sandbox.dispatchWindowResize({width: $event.target.innerWidth, height: $event.target.innerHeight});
  }

  routeToSecurityRole(): void {
    this._sandbox.dispatchRoutingSecurityRole();
  }

}
