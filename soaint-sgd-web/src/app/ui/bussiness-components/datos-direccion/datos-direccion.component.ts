import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ConstanteDTO} from 'app/domain/constanteDTO';
import {Store} from '@ngrx/store';
import {State} from 'app/infrastructure/redux-store/redux-reducers';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {getPrefijoCuadranteArrayData} from 'app/infrastructure/state-management/constanteDTO-state/selectors/prefijo-cuadrante-selectors';
import {getTipoViaArrayData} from 'app/infrastructure/state-management/constanteDTO-state/selectors/tipo-via-selectors';
import {getOrientacionArrayData} from 'app/infrastructure/state-management/constanteDTO-state/selectors/orientacion-selectors';
import {getBisArrayData} from 'app/infrastructure/state-management/constanteDTO-state/selectors/bis-selectors';
import {getTipoComplementoArrayData} from '../../../infrastructure/state-management/constanteDTO-state/selectors/tipo-complemento-selectors';
import {tassign} from 'tassign';
import {VALIDATION_MESSAGES} from '../../../shared/validation-messages';
import {Sandbox as PaisSandbox} from '../../../infrastructure/state-management/paisDTO-state/paisDTO-sandbox';
import {Sandbox as DepartamentoSandbox} from '../../../infrastructure/state-management/departamentoDTO-state/departamentoDTO-sandbox';
import {Sandbox as MunicipioSandbox} from '../../../infrastructure/state-management/municipioDTO-state/municipioDTO-sandbox';
import {PaisDTO} from '../../../domain/paisDTO';
import {DepartamentoDTO} from '../../../domain/departamentoDTO';
import {MunicipioDTO} from '../../../domain/municipioDTO';
import {getArrayData as municipioArrayData} from 'app/infrastructure/state-management/municipioDTO-state/municipioDTO-selectors';
import {getArrayData as paisArrayData} from 'app/infrastructure/state-management/paisDTO-state/paisDTO-selectors';
import {getArrayData as departamentoArrayData} from 'app/infrastructure/state-management/departamentoDTO-state/departamentoDTO-selectors';
import {Subscription} from 'rxjs/Subscription';
import "rxjs/add/operator/filter";
import {AutoComplete} from "primeng/components/autocomplete/autocomplete";
import {isNullOrUndefined} from 'util';
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";
import {combineLatest} from "rxjs";
import {filter} from "rxjs/operators";
import {
  CODIGO_COLOMBIA,
  COMUNICACION_INTERNA_ENVIADA
} from "../../../shared/bussiness-properties/radicacion-properties";
import {LocalizacionApiService} from '../../../infrastructure/api/localizacion.api';
import {getTipoContactoArrayData} from "../../../infrastructure/state-management/constanteDTO-state/selectors/tipo-contacto-selectors";
import {FormContextEnum} from "../../../shared/enums/enums";
import {INVALID_TYPE_AGENT} from "../../../shared/lang/es";
import {Utils} from "../../../shared/helpers";


@Component({
  selector: 'app-datos-direccion',
  templateUrl: './datos-direccion.component.html'
})
export class DatosDireccionComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {

  form: FormGroup;
  display = false;

  @Input() editable = true;
  @Input() anonimo: boolean = false;
  @Input() contactsDefault: Array<any> = [];
  @Input() distribucion: boolean;
  @Output()
  nuevosContactos = new EventEmitter();

  @Output() loadComponent: EventEmitter<DatosDireccionComponent> = new EventEmitter;

  @ViewChild('paisAutoComplete') paisAutoComplete: AutoComplete;
  @ViewChild('departamentoAutoComplete') departamentoAutoComplete: AutoComplete;
  @ViewChild('municipioAutoComplete') municipioAutoComplete: AutoComplete;


  validations: any = {};
  visibility: any = {};

  paisSuggestions$: Observable<PaisDTO[]>;
  departamentoSuggestions$: Observable<DepartamentoDTO[]>;
  municipioSuggestions$: Observable<MunicipioDTO[]>;
  prefijoCuadranteSuggestions$: Observable<ConstanteDTO[]>;
  tipoViaSuggestions$: Observable<ConstanteDTO[]>;
  orientacionSuggestions$: Observable<ConstanteDTO[]>;
  bisSuggestons$: Observable<ConstanteDTO[]>;
  tipoComplementoSuggestions$: Observable<ConstanteDTO[]>;
  paises$: Observable<PaisDTO[]>;
  departamentos$: Observable<DepartamentoDTO[]>;
  municipios$: Observable<MunicipioDTO[]>;
  tipoContacto$: Observable<ConstanteDTO[]>;

  contacts: Array<any> = [];
  direccionSeleccionada: any = null;
  paises: Array<any> = [];
  departamentos: Array<any> = [];
  municipios: Array<any> = [];
  contactPrincial = false;
  showDireccionForm = false;
  showCheckDireccionForm = false;
  showContactForm = false;
  formContext: FormContextEnum;
  editIndexContext: number;

  subscribers: Array<Subscription> = [];
  contactEditing: any = {};
  isInvalidTypeAgent = false;
  contactAdd: any = {};

  constructor(private _store: Store<State>,
              private _paisSandbox: PaisSandbox,
              private _departamentoSandbox: DepartamentoSandbox,
              private _municipioSandbox: MunicipioSandbox,
              private formBuilder: FormBuilder,
              private _changeDetectorRef: ChangeDetectorRef,
              private _localizacionService: LocalizacionApiService
  ) {

    this.initForm();
    this.listenForChanges();
    this.listenForErrors();
  }


  ngOnInit(): void {
    this._paisSandbox.loadDispatch();
    this.prefijoCuadranteSuggestions$ = Utils.sortObservable(this._store.select(getPrefijoCuadranteArrayData), 'nombre');
    this.tipoViaSuggestions$ = Utils.sortObservable(this._store.select(getTipoViaArrayData), 'nombre');
    this.orientacionSuggestions$ = Utils.sortObservable(this._store.select(getOrientacionArrayData), 'nombre');
    this.bisSuggestons$ = Utils.sortObservable(this._store.select(getBisArrayData), 'nombre');
    this.tipoComplementoSuggestions$ = Utils.sortObservable(this._store.select(getTipoComplementoArrayData), 'nombre');
    this.municipios$ = Utils.sortObservable(this._store.select(municipioArrayData), 'nombre');
    this.departamentos$ = Utils.sortObservable(this._store.select(departamentoArrayData), 'nombre');
    this.paises$ = Utils.sortObservable(this._store.select(paisArrayData), 'nombre');
    this.tipoContacto$ = Utils.sortObservable(this._store.select(getTipoContactoArrayData), 'nombre');

    this.contacts = this.contactsDefault;

    this.addColombiaByDefault();

    this.paisSuggestions$ = this.paisAutoComplete.completeMethod
      .combineLatest(this.paises$, (event: any, paises) => paises.filter(pais => pais.nombre.toLowerCase().indexOf(event.query.toLowerCase()) >= 0));

    this.departamentoSuggestions$ = this.departamentoAutoComplete.completeMethod
      .combineLatest(this.departamentos$, (event: any, departamentos) => departamentos.filter(departamento => departamento.nombre.toLowerCase().indexOf(event.query.toLowerCase()) >= 0));

    this.municipioSuggestions$ = this.municipioAutoComplete.completeMethod
      .combineLatest(this.municipios$, (event: any, municipios) => municipios.filter(municipio => municipio.nombre.toLowerCase().indexOf(event.query.toLowerCase()) >= 0));

    this.loadComponent.emit(this);

    // para el caso de lista con datos incompletos
    if (this.contactsDefault && this.contactsDefault.length > 0) {
      this.CompletarDatosContacto();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!isNullOrUndefined(changes.anonimo)) {
      this.initForm();
    }
  }

  initForm() {
    this.form = this.formBuilder.group({
      'tipoVia': [null],
      'noViaPrincipal': [null],
      'prefijoCuadrante': [null],
      'bis': [null],
      'orientacion': [null],
      'noVia': [null],
      'prefijoCuadrante_se': [null],
      'placa': [null],
      'orientacion_se': [null],
      'complementoTipo': [null],
      'complementoAdicional': [null],
      'celular': [null],
      'numeroTel': [null],
      'correoEle': [null],
      'pais': [null],
      'departamento': [null],
      'municipio': [null],
      'principal': [null],
      'provinciaEstado': [null],
      'direccionText': [null],
      'ciudad': [null],
      'tipoContacto': [null, this.anonimo == true ? !Validators.required : Validators.required],
      'zipCode': [{value: null}, Validators.compose([
        Validators.min(0),
        Validators.max(1000000)
      ])],

    });
  }

  listenForErrors() {
    this.bindToValidationErrorsOf('tipoVia');
    this.bindToValidationErrorsOf('noViaPrincipal');
    this.bindToValidationErrorsOf('noVia');
    this.bindToValidationErrorsOf('placa');
    this.bindToValidationErrorsOf('correoEle');
  }


  listenForChanges() {
    const paisControl = this.form.get('pais');
    const departamentoControl = this.form.get('departamento');
    const municipioControl = this.form.get('municipio');

    this.subscribers.push(paisControl.valueChanges.subscribe(value => {
      if (this.editable && value) {
        departamentoControl.enable();
      } else {
        departamentoControl.reset();
        departamentoControl.disable();
      }
    }));

    paisControl.valueChanges.subscribe(value => {
      this.visibility.selectedColombia = true;
      if (value && value.codigo && (value.codigo.toUpperCase() !== 'CO')) {
        this.visibility.selectedColombia = false;
        this.showCheckDireccionForm = false;
        this.showCheckDireccionForm = false;
        departamentoControl.reset();
        departamentoControl.disable();
        municipioControl.reset();
        municipioControl.disable();
      } else {
        this.showCheckDireccionForm = true;
      }

    });

    this.subscribers.push(departamentoControl.valueChanges.subscribe(value => {
      if (this.editable && value) {
        municipioControl.enable();
      } else {
        municipioControl.reset();
        municipioControl.disable();
      }
    }));

  }


  onDropdownClickPais(event?) {
    this._paisSandbox.loadDispatch();
  }

  onDropdownClickDepartamento($event) {
    const pais = this.form.get('pais').value;
    if (pais) {
      this._departamentoSandbox.loadDispatch({codPais: pais.codigo});
      this.form.get('municipio').reset();
    }
  }

  onDropdownClickMunicipio($event) {
    const departamento = this.form.get('departamento').value;
    if (departamento) {
      this._municipioSandbox.loadDispatch({codDepar: departamento.codigo});
    }
  }

  listenForBlurEvents(control: string) {
    const ac = this.form.get(control);

    const keyFields = ['noViaPrincipal', 'noVia', 'placa', 'complementoAdicional'];

    if (keyFields.some(k => k == control))
      if (!isNullOrUndefined(ac) && !isNullOrUndefined(ac.value))
        ac.setValue(ac.value.toString().trim());

    // if (ac.touched && ac.invalid) {
    //   const error_keys = Object.keys(ac.errors);
    //   const last_error_key = error_keys[error_keys.length - 1];
    //   this.validations[control] = VALIDATION_MESSAGES[last_error_key];
    // }
  }

  bindToValidationErrorsOf(control: string) {
    const ac = this.form.get(control);
    ac.valueChanges.subscribe(value => {
      if ((ac.touched || ac.dirty) && ac.errors) {
        const error_keys = Object.keys(ac.errors);
        const last_error_key = error_keys[error_keys.length - 1];
        this.validations[control] = VALIDATION_MESSAGES[last_error_key];
      } else {
        delete this.validations[control];
      }
    });
  }

  saveAndRetriveContact(): any {

    const pais = this.form.get('pais');
    const departamento = this.form.get('departamento');
    const municipio = this.form.get('municipio');
    const numeroTel = this.form.get('numeroTel');
    const celular = this.form.get('celular');
    const email = this.form.get('correoEle');
    const provinciaEstado = this.form.get('provinciaEstado');
    const ciudad = this.form.get('ciudad');
    const direccionText = this.form.get('direccionText');
    const principal = this.form.get('principal');
    const tipoContacto = this.form.get('tipoContacto');
    const codigoPostal = this.form.get('zipCode');
    const toSave = tassign({
      zipCode: codigoPostal.value,
      tipoContacto: tipoContacto.value,
      pais: pais.value,
      departamento: departamento.value,
      municipio: municipio.value,
      numeroTel: numeroTel.value,
      celular: celular.value,
      correoEle: email.value,
      provState: provinciaEstado.value,
      city: ciudad.value,
      direccionText: direccionText.value,
      principal: (principal.value === true ? true : false)
    }, this.saveDireccionData());

    pais.reset();
    provinciaEstado.reset();
    ciudad.reset();
    direccionText.reset();
    departamento.reset();
    municipio.reset();
    numeroTel.reset();
    celular.reset();
    email.reset();
    principal.reset();
    codigoPostal.reset();

    this.showContactForm = false;
    this.showDireccionForm = false;

    return toSave;

  }

  contactEdit(): any {

    const pais = this.form.get('pais');
    const departamento = this.form.get('departamento');
    const municipio = this.form.get('municipio');
    const numeroTel = this.form.get('numeroTel');
    const celular = this.form.get('celular');
    const email = this.form.get('correoEle');
    const provinciaEstado = this.form.get('provinciaEstado');
    const ciudad = this.form.get('ciudad');
    const direccionText = this.form.get('direccionText');
    const principal = this.form.get('principal');
    const tipoContacto = this.form.get('tipoContacto');
    const codigoPostal = this.form.get('zipCode');

    this.contactEditing.tipoContacto = tipoContacto.value;
    this.contactEditing.pais = pais.value;
    this.contactEditing.departamento = departamento.value;
    this.contactEditing.municipio = municipio.value;
    this.contactEditing.numeroTel = numeroTel.value;
    this.contactEditing.celular = celular.value;
    this.contactEditing.correoEle = email.value;
    this.contactEditing.provState = provinciaEstado.value;
    this.contactEditing.city = ciudad.value;
    this.contactEditing.direccionText = direccionText.value;
    this.contactEditing.principal = (principal.value === true ? true : false);
    this.contactEditing.zipCode = codigoPostal.value;
    this.saveDireccionData();

    pais.reset();
    provinciaEstado.reset();
    ciudad.reset();
    direccionText.reset();
    departamento.reset();
    municipio.reset();
    numeroTel.reset();
    celular.reset();
    email.reset();
    principal.reset();
    codigoPostal.reset();

    this.showContactForm = false;
    this.showDireccionForm = false;

    return this.contactEditing;

  }

  saveDireccionData() {
    const tipoVia = this.form.get('tipoVia');
    const noViaPrincipal = this.form.get('noViaPrincipal');
    const prefijoCuadrante = this.form.get('prefijoCuadrante');
    const bis = this.form.get('bis');
    const orientacion = this.form.get('orientacion');
    const noVia = this.form.get('noVia');
    const prefijoCuadrante_se = this.form.get('prefijoCuadrante_se');
    const placa = this.form.get('placa');
    const orientacion_se = this.form.get('orientacion_se');
    const tipoComplemento = this.form.get('complementoTipo');
    const complementoAdicional = this.form.get('complementoAdicional');
    const codigoPostal = this.form.get('zipCode');


    const pais = this.form.get('pais');
    const direccionText = this.form.get('direccionText');

    const value = {};

    if (pais.value && pais.value.codigo.toUpperCase() === 'CO') {

      if (tipoVia.value) {
        value['tipoVia'] = tipoVia.value;
        this.contactEditing['tipoVia'] = tipoVia.value;
        tipoVia.reset();
      }
      if (noViaPrincipal.value) {
        value['noViaPrincipal'] = noViaPrincipal.value;
        this.contactEditing['noViaPrincipal'] = noViaPrincipal.value;
        noViaPrincipal.reset();
      }
      if (prefijoCuadrante.value) {
        value['prefijoCuadrante'] = prefijoCuadrante.value;
        this.contactEditing['prefijoCuadrante'] = prefijoCuadrante.value;
        prefijoCuadrante.reset();
      }
      if (bis.value) {
        value['bis'] = bis.value;
        this.contactEditing['bis'] = bis.value;
        bis.reset();
      }
      if (orientacion.value) {
        value['orientacion'] = orientacion.value;
        this.contactEditing['orientacion'] = orientacion.value;
        orientacion.reset();
      }
      if (noVia.value) {
        value['noVia'] = noVia.value;
        this.contactEditing['noVia'] = noVia.value;
        noVia.reset();
      }
      if (prefijoCuadrante_se.value) {
        value['prefijoCuadrante_se'] = prefijoCuadrante_se.value;
        this.contactEditing['prefijoCuadrante_se'] = prefijoCuadrante_se.value;
        prefijoCuadrante_se.reset();
      }
      if (placa.value) {
        value['placa'] = placa.value;
        this.contactEditing['placa'] = placa.value;
        placa.reset();
      }
      if (orientacion_se.value) {
        value['orientacion_se'] = orientacion_se.value;
        this.contactEditing['orientacion_se'] = orientacion_se.value;
        orientacion_se.reset();
      }
      if (tipoComplemento.value) {
        value['complementoTipo'] = tipoComplemento.value;
        this.contactEditing['complementoTipo'] = tipoComplemento.value;
        tipoComplemento.reset();
      }
      if (complementoAdicional.value) {
        value['complementoAdicional'] = complementoAdicional.value;
        this.contactEditing['complementoAdicional'] = complementoAdicional.value;
        complementoAdicional.reset();
      }

      if (codigoPostal.value) {
        value['codigoPostal'] = codigoPostal.value;
        this.contactEditing['codigoPostal'] = codigoPostal.value;
        codigoPostal.reset();
      }

      value['address'] = JSON.stringify(value);
      this.contactEditing['address'] = value['address'];

    } else {
      value['address'] = direccionText.value;
      this.contactEditing['address'] = value['address'];
      this.showContactForm = false;
    }

    return value;
  }

  deleteContact(index) {
    const radref = [...this.contacts];
    radref.splice(index, 1);
    if (this.contacts[index] === this.direccionSeleccionada) {
      this.direccionSeleccionada = null;
    }
    this.contacts = radref;
    this.nuevosContactos.emit(this.contacts);
  }

  editContact(index) {
    const addressKeys = ['tipoVia', 'noViaPrincipal', 'prefijoCuadrante', 'bis', 'orientacion', 'noVia',
      'prefijoCuadrante_se', 'placa', 'orientacion_se', 'complementoTipo', 'complementoAdicional', 'pais', 'direccion', 'codigoPostal'];

    const values = this.contacts[index];
    this.form.patchValue(values);
    this.contactEditing = values;
    this.showContactForm = true;
    this.showCheckDireccionForm = (isNullOrUndefined(values.pais) || values.pais.codigo.toString().toUpperCase() == 'CO');
    this.showDireccionForm = this.showCheckDireccionForm && addressKeys.some(key => values[key]);
    this.formContext = FormContextEnum.SAVE;
    this.editIndexContext = index;
    if (this.showDireccionForm)
      this.toggleDireccionForm(true);
  }

  isActive;

  addContact() {
    this.showDireccionForm = true;
    this.showCheckDireccionForm = true;
    this.isActive = true;
    this.showContactForm = true;
    this.formContext = FormContextEnum.CREATE;
    this.addColombiaByDefault();
    if (this.anonimo == true && this.distribucion == false) {
      this.save();
    }
  }

  hasDireccionPrincipal() {

    return this.contacts.some((contact, index) => contact.principal === true && index !== this.editIndexContext)

  }

  onFilterPais(event) {

    //this.paisSuggestions$ = this.paisAutoComplete.completeMethod
    //  .combineLatest(this.paises$, (event: any, paises) => paises.filter(pais => pais.nombre.toLowerCase().indexOf(event.query.toLowerCase()) >= 0 ));


    //this.paisSuggestions$.map(paises => {
    //  paises.filter(pais => pais.nombre.toLowerCase().indexOf(event.query.toLowerCase()) >= 0 );
    //})
    /*this.paisSuggestions$.take(2).subscribe((values) => {
      console.log(values);
    })
    console.log("Lista ", this.paisSuggestions$);
    console.log(event);*/
  }

  onFilterDepartamento(event) {

  }

  onFilterMinicipio(event) {

  }

  save() {
    if (this.form.valid) {
      this.isInvalidTypeAgent = false;

      const principal = this.form.get('principal');
      if (principal.value === true && this.hasDireccionPrincipal() === true) {

        this._store.dispatch(new PushNotificationAction({
          severity: 'warn',
          summary: 'Recuerde que únicamente puede existir una dirección principal. Por lo cual se tomará esta dirección como la principal'
        }));

        this.contacts.forEach(contact => {

          contact.principal = false;
        });

      } else {
        if (this.formContext === FormContextEnum.CREATE) {
          this.contacts = [this.saveAndRetriveContact(), ...this.contacts];
        } else {
          // const temp = [...this.contacts];
          // temp[this.editIndexContext] = this.contactEdit();
          // this.contactEditing = this.contactEdit();
          this.contacts[this.editIndexContext] = this.contactEdit();
        }
        this.formContext = null;
        this.editIndexContext = null;

        this.nuevosContactos.emit(this.contacts);
      }
      // this._store.dispatch(new PushNotificationAction({
      //   severity: 'success',
      //   summary: 'Se ha guardado exitosamente'
      // }));
    }
    this._changeDetectorRef.detectChanges();
  }

  toggleDireccionVisibility() {
    this.showDireccionForm = !this.showDireccionForm;
  }

  ngOnDestroy() {
    this.subscribers.forEach(subscriber => {
      subscriber.unsubscribe();
    });
  }

  ngAfterViewInit() {
    this.refreshView();
  }

  addColombiaByDefault() {
    this.form.reset();
    const subscription = this.paises$
      .pipe(filter(values => values.length > 0))
      .subscribe((values) => {
        this.paises = values;
        this.form.get('pais').setValue(values.find(value => value.codigo === 'CO'));
        setTimeout(() => subscription.unsubscribe());
      });

    this._paisSandbox.loadDispatch();

    this.visibility.selectedColombia = true;
    this.addDepartamentoBogotaByDefault();
  }

  addDepartamentoBogotaByDefault() {
    const subscription = this.departamentos$
      .pipe(filter(values => values.length > 0))
      .subscribe((values) => {
        this.departamentos = values;
        this.form.get('departamento').setValue(values.find(value => value.codigo === "11"));
        setTimeout(() => subscription.unsubscribe());
      });

    const pais = this.form.get('pais').value;
    if (pais) {
      this._departamentoSandbox.loadDispatch({codPais: pais.codigo});
    }
    this.addMunicipioBogotaByDefault();
  }

  addMunicipioBogotaByDefault() {
    //this.form.reset();
    const subscription = this.municipios$
      .pipe(filter(values => values.length > 0))
      .subscribe((values) => {
        this.municipios = values;
        this.form.get('municipio').setValue(values.find(value => value.codigo === "11001"));
        setTimeout(() => subscription.unsubscribe());
      });

    const departamento = this.form.get('departamento').value;
    if (departamento) {
      this._municipioSandbox.loadDispatch({codDepar: departamento.codigo});
    }
    //this.visibility.sele = true;
  }


  refreshView() {
    this._changeDetectorRef.detectChanges();
  }

  toggleDireccionForm(checked: boolean) {

    if (!checked || this.formContext != FormContextEnum.SAVE)
      return;


    this.form.get("principal").setValue(this.contacts[this.editIndexContext].principal);

    try {
      let direccionData = JSON.parse(this.contacts[this.editIndexContext].address);

      if (isNullOrUndefined(direccionData))
        return;

      Object.keys(direccionData).forEach(key => {

        if (isNullOrUndefined(direccionData[key]))
          return;

        let control = this.form.get(key);

        if (control) {

          control.setValue(direccionData[key]);
        }

      });
    } catch (e) {
    }

  }

  CompletarDatosContacto() {

    this.subscribers.push(combineLatest(this._store.select(getTipoContactoArrayData), this._localizacionService.ListarMunicipiosActivos({})).subscribe(([tiposContacto, result]) => {

      this.contacts = this.contacts
        .reduce((_listado, _contact) => {
          if (result) {
            if (!isNullOrUndefined(_contact.municipio)) {
              const municipio = result.find(_item => _item.codigo === _contact.municipio.codigo);
              if (municipio) {
                const departamento = (<any>municipio).departamento;
                const pais = departamento.pais;
                if (!isNullOrUndefined(_contact.municipio))
                  _contact.municipio.nombre = _contact.municipio.nombre ? _contact.municipio.nombre : (municipio) ? municipio.nombre : '';
                if (!isNullOrUndefined(_contact.departamento))
                  _contact.departamento.nombre = _contact.departamento.nombre ? _contact.departamento.nombre : (departamento) ? departamento.nombre : '';
                if (!isNullOrUndefined(_contact.pais))
                  _contact.pais.nombre = _contact.pais.nombre ? _contact.pais.nombre : (pais) ? pais.nombre : '';
              }
            }

            if (!isNullOrUndefined(_contact.tipoContacto)) {
              _contact.tipoContacto = tiposContacto.find(tc => tc.codigo == _contact.tipoContacto.codigo);
            }
            _listado.push(_contact);
            return _listado;
          } else {
            return this.contacts;
          }
        }, []);

      this._changeDetectorRef.detectChanges();

    }))
  }

  validTypeAgent() {
    if (this.contacts.some(res => res.tipoContacto ? res.tipoContacto.nombre === this.form.get('tipoContacto').value.nombre : true)) {
      if (Object.keys(this.contactEditing).length !== 0 && this.formContext === 0) {
        if (this.contactEditing.tipoContacto ? this.contactEditing.tipoContacto.nombre !== this.form.get('tipoContacto').value.nombre : false) {
          this._store.dispatch(new PushNotificationAction({
            severity: 'info',
            summary: INVALID_TYPE_AGENT
          }));
          this.isInvalidTypeAgent = true;
        } else {
          this.isInvalidTypeAgent = false;
        }
      } else {
        this._store.dispatch(new PushNotificationAction({
          severity: 'info',
          summary: INVALID_TYPE_AGENT
        }));
        this.isInvalidTypeAgent = true;
      }
    } else {
      this.isInvalidTypeAgent = false;
    }
  }

  isColombiaCodigo(codigo) {
    return codigo == CODIGO_COLOMBIA;
  }


}
