import {Component, Input, OnInit} from '@angular/core';
import {LocationType} from "../../../shared/enums/enums";

@Component({
  selector: 'app-ticket-location',
  templateUrl: './ticket-location.component.html',
  styleUrls: ['./ticket-location.component.scss']
})
export class TicketLocationComponent implements OnInit {
  @Input() printelement: string = "";
  @Input() inlineStyles: string = "";
  @Input() showOpenDialogButton: boolean = false;
  @Input() showPrintButton: boolean = false;
  @Input() showSaveButton: boolean = false;

  locationDialogVisibility: boolean;

  ticketLocations: Array<TicketLocation> = ticketLocations;

  initialTicketLocationIndex: number;
  selectedTicketLocation: TicketLocation;


  selectedLocations: number[];

  constructor() {
  }

  ngOnInit() {
    this.locationDialogVisibility = false;
    this.initialTicketLocationIndex = 1;
    this.selectedTicketLocation = this.ticketLocations[this.initialTicketLocationIndex];
    this.selectedLocations = [this.initialTicketLocationIndex];
  }

  showLocationDialog() {
    this.locationDialogVisibility = true;
  }

  hideLocationDialog() {
    this.locationDialogVisibility = false;
  }

  handleChangeSelectedLocations() {
    this.selectedLocations = this.selectedLocations.slice(this.selectedLocations.length - 1);
    if (!this.selectedLocations || !this.selectedLocations.length) {
      this.selectedLocations = [this.initialTicketLocationIndex]
    }
    this.selectedTicketLocation = this.ticketLocations[this.selectedLocations[0]];
  }

  getSelectedLocation(): TicketLocation {
    return this.selectedTicketLocation;
  }
}

export interface TicketLocation {
  locationType: LocationType,
  location: string
}
const scaleTransform = "transform: scale(0.6,0.6); ";
export const ticketLocations: Array<TicketLocation> = [
  {
    locationType: LocationType.TOP_LEFT,
    location: "float: left;" +
      "    padding: 8px;" +
      scaleTransform +
      "transform-origin: left top;"
  },
  {
    locationType: LocationType.TOP_RIGHT,
    location: "float: right;\n" +
      "    padding: 8px;\n" +
      scaleTransform +
      "transform-origin: right top;"
  },
  {
    locationType: LocationType.BOTTOM_LEFT,
    location: "bottom: 0; " +
      "float: left; " +
      "position: absolute; " +
      "padding: 8px;" +
      scaleTransform +
      "transform-origin: left bottom;"
  },
  {
    locationType: LocationType.BOTTOM_RIGHT,
    location: "bottom: 0; " +
      "right: 0; " +
      "float: right; " +
      "position: absolute; " +
      "padding: 8px;" +
      scaleTransform +
      "transform-origin: right bottom;"
  }
]
