import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {IndiceElectronicoApi} from "../../../infrastructure/api/indice-electronico.api";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../infrastructure/redux-store/redux-reducers";
import {isNullOrUndefined} from "util";
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";
import {ICON_CALENDAR} from "../../../shared/constants";
import {LocaleSettings} from "primeng/calendar";
import {localeCalendarEs} from "../../../shared/localeCalendarEs";

@Component({
  selector: 'app-popup-parametrizar-horario-demanda',
  templateUrl: './popup-parametrizar-horario-demanda.component.html',
  styleUrls: ['./popup-parametrizar-horario-demanda.component.scss']
})
export class PopupParametrizarHorarioDemandaComponent implements OnInit {

  executeData: any[];
  start: any[] = [];
  end: any[] = [];
  formHorario: FormGroup;
  idTable: any;
  strartExecute = '00:00';
  endExecute = '00:00';
  invalidDate: boolean = false;
  iconCalendar = ICON_CALENDAR;
  es: LocaleSettings;
  end_date: Date = new Date();
  start_date: Date = new Date();
  date = new Date();


  constructor(private _indiceElectronicoApi: IndiceElectronicoApi,
              private fe: FormBuilder,
              private _store: Store<RootState>,
              protected _detectChanges: ChangeDetectorRef,) {
  }

  ngOnInit() {
    this.es = localeCalendarEs;
    this.initFormHorario();
    this.start_date = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
  }


  initFormHorario() {
    this.formHorario = this.fe.group({
      strartExecute: [this.strartExecute, Validators.required],
      endExecute: [this.endExecute, Validators.required],
      fechaInicial: [null, Validators.required],
      fechaFinal: [null, Validators.required],
    });

  }

  getExecuteTime() {
    this.cargarHorario();
    var type = 1;
    this._indiceElectronicoApi.getExecutionTime(type).subscribe(data => {
      this.idTable = data.idTable;
      this.strartExecute = data.strartExecute;
      this.endExecute = data.endExecute;
      this.start_date = new Date(data.startDate);
      this.end_date = new Date(data.endDate);
      this.refreshView();
    });
  }

  refreshView() {
    this._detectChanges.detectChanges();
  }

  updateExecuteData() {
    const payload: any = {
      idTable: this.idTable,
      startDate: this.convertDate(this.start_date),
      endDate: this.convertDate(this.end_date),
      strartExecute: !isNullOrUndefined(this.formHorario.get('strartExecute').value) ? this.formHorario.get('strartExecute').value : "",
      endExecute: !isNullOrUndefined(this.formHorario.get('endExecute').value) ? this.formHorario.get('endExecute').value : "",
    };
    this._indiceElectronicoApi.updateTimeExecute(payload).subscribe(() => {
      this._store.dispatch(new PushNotificationAction({
        summary: 'Se ha registrado el nuevo horario de generación del índice electrónico',
        severity: 'success'
      }))
    });
  }


  cargarHorario() {
    let getHorarios = this.getHorario('00:00-23:30');
    this.start = getHorarios;
    this.end = getHorarios;
  }


  getHorario(horario): any[] {
    let payload: { value: any, label: any }[] = [];
    let jornada = horario.split('-');
    if (jornada[1] === '00:00') {
      return [];
    }
    let manada = jornada[0].split(':');
    let tarde = jornada[1].split(':');
    let pref = 'am';
    let hora1;
    let hora2;
    let maxManada = manada[1];
    let maxTarde = tarde[1];
    let index1;
    let data: { value: any, label: any }[] = [];
    if (manada[0] === tarde[0] && manada[1] < tarde[1]) {
      if (+manada[0] >= 13) {
        pref = 'pm';
      }
      payload.push({
        value: `${jornada[0]} ${pref}`,
        label: jornada[0]
      }, {
        value: `${jornada[1]} ${pref}`,
        label: jornada[1]
      });
      return payload;
    }
    for (let i = +manada[0]; i <= +tarde[0]; i++) {
      index1 = i;
      if (i === +tarde[0]) {
        hora1 = ((index1 <= 9) ? '0' + index1 : index1) + ':00';
        data = this.setPayloadHorario(payload, hora1);
        if (data.length > 0) {
          payload = data;
        }
        if (maxTarde === '30') {
          hora1 = ((index1 <= 9) ? '0' + index1 : index1) + ':30';
        }
        payload = this.setPayloadHorario(payload, hora1);
      } else {
        if (maxManada === '30') {
          hora1 = ((index1 <= 9) ? '0' + index1 : index1) + ':30';
          index1++;
          if (index1 <= +tarde[0] && hora1 !== jornada[1]) {
            hora2 = ((index1 <= 9) ? '0' + index1 : index1) + ':00';

            maxManada = '30'
          } else {
            hora2 = null;
          }
        } else {
          hora1 = ((index1 <= 9) ? '0' + index1 : index1) + ':00';
          hora2 = ((index1 <= 9) ? '0' + index1 : index1) + ':30';
          maxManada = '00';
        }
        payload = this.setPayloadHorario(payload, hora1);
        if (!isNullOrUndefined(hora2)) {
          payload = this.setPayloadHorario(payload, hora2);
        }
      }
    }
    return payload;
  }

  setPayloadHorario(payload: { value: any, label: any }[], hora: string): { value: any, label: any }[] {
    let dataP: any = {
      value: hora,
      label: hora
    };
    if (payload.length > 0) {
      if (!payload.find(date => date.value === dataP.value)) {
        payload.push(dataP);
      }
    } else {
      payload.push(dataP);
    }
    return payload;
  }

  changeStart(date) {

  }

  changeEnd(date) {

  }

  changeDateAndHourRack(date, type, typeRank) {
    if (typeRank == 'date') {
      let isEqualHourRank = this.endExecute === this.strartExecute;
      if (type == 'start') {
        if (this.convertDate(date) === this.convertDate(this.end_date) && isEqualHourRank) {
          this.invalidDate = true;
          this.showMessageInvalidateDate();
        } else if (this.strartExecute > this.endExecute && this.convertDate(date) === this.convertDate(this.end_date)) {
          this.invalidDate = true;
          this.showMessageInvalidateHour();
        } else {
          this.invalidDate = false;
        }
      } else if (type == 'end') {
        if (this.convertDate(date) === this.convertDate(this.start_date) && isEqualHourRank) {
          this.invalidDate = true;
          this.showMessageInvalidateDate();
        } else if (this.strartExecute > this.endExecute && this.convertDate(date) === this.convertDate(this.start_date)) {
          this.invalidDate = true;
          this.showMessageInvalidateHour();
        } else {
          this.invalidDate = false;
        }
      }
    } else if (typeRank == 'hour') {
      let isEqualsDateRank = this.convertDate(this.end_date) == this.convertDate(this.start_date);
      if (type == 'start') {
        this.strartExecute = date.value;
        if (this.endExecute === this.strartExecute && isEqualsDateRank) {
          this.invalidDate = true;
          this.showMessageInvalidateDate();
        } else if (this.strartExecute > this.endExecute && isEqualsDateRank) {
          this.invalidDate = true;
          this.showMessageInvalidateHour();
        } else {
          this.invalidDate = false;
        }
      } else if (type == 'end') {
        this.endExecute = date.value;
        if (this.strartExecute === this.endExecute && isEqualsDateRank) {
          this.invalidDate = true;
          this.showMessageInvalidateDate();
        } else if (this.strartExecute > this.endExecute && isEqualsDateRank) {
          this.invalidDate = true;
          this.showMessageInvalidateHour();
        } else {
          this.invalidDate = false;
        }
      }
    }
  }

  showMessageInvalidateDate() {
    this._store.dispatch(new PushNotificationAction({
      summary: 'El rango de hora inicial y final no pueden ser iguales para un mismo día',
      severity: 'info'
    }));
  }

  showMessageInvalidateHour() {
    this._store.dispatch(new PushNotificationAction({
      summary: 'La hora inicial no puede ser mayor a la hora final para un mismo día',
      severity: 'info'
    }));
  }

  convertDate(inputFormat, dateFin?) {
    function pad(s) {
      return (s < 10) ? '0' + s : s;
    }

    const d = new Date(inputFormat);
    if (dateFin) {
      d.setDate(d.getDate() + 1);
    }
    return [pad(d.getFullYear()), pad(d.getMonth() + 1), d.getDate()].join('/');
  }


}
