import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupParametrizarHorarioDemandaComponent } from './popup-parametrizar-horario-demanda.component';

describe('PopupParametrizarHorarioDemandaComponent', () => {
  let component: PopupParametrizarHorarioDemandaComponent;
  let fixture: ComponentFixture<PopupParametrizarHorarioDemandaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupParametrizarHorarioDemandaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupParametrizarHorarioDemandaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
