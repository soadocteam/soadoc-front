import {AfterViewInit, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../../infrastructure/redux-store/redux-reducers';
import 'rxjs/add/operator/single';
import {ApiBase} from '../../../infrastructure/api/api-base';
import {environment} from '../../../../environments/environment';
import {LoadingService} from '../../../infrastructure/utils/loading.service';
import {FileUpload} from 'primeng/primeng';
import {VersionDocumentoDTO} from '../../page-components/produccion-documental/models/DocumentoDTO';
import {Sandbox as TaskSandBox} from '../../../infrastructure/state-management/tareasDTO-state/tareasDTO-sandbox';
import {MessagingService, ProduccionDocumentalApiService} from '../../../infrastructure/__api.include';
import {TareaDTO} from '../../../domain/tareaDTO';
import {Subscription} from 'rxjs/Subscription';
import {getActiveTask} from '../../../infrastructure/state-management/tareasDTO-state/tareasDTO-selectors';
import {PushNotificationAction} from '../../../infrastructure/state-management/notifications-state/notifications-actions';
import {Observable} from 'rxjs/Observable';
import {map} from 'rxjs/operators';
import {PDFJSStatic} from 'pdfjs-dist';
import {isNullOrUndefined} from "util";
import {FileextensionApi} from "../../../infrastructure/api/fileextension-api";
import {FileExtensionDTO} from "../../../domain/FileExtensionDTO";
import {Utf8Helper} from "../../../shared/utf8";
import {HttpHeaders} from "@angular/common/http";
import {Utils} from "../../../shared/helpers";

export enum ViewerType {
  PDF = 1,
  HTML = 2
}

declare var PDFObject: any;
declare var PDFJS: PDFJSStatic;

@Component({
  selector: 'app-documentos-ecm-list',
  templateUrl: './documentos-ecm-list.component.html',
  styleUrls: ['./documentos-ecm-list.css']
})
export class DocumentosECMListComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {


  @Input()
  versionar = false;

  @Input()
  idDocumentECM: string;

  @Input()
  nroRadicadoPrincipal = null;

  @Input('showLoading') showLoading: boolean = true;
  @Input()
  causaDI;

  docSrc = '';
  isLoading = false;
  viewDocument: boolean;


  readonly $viewer = ViewerType;

  documentsList: Array<any>;
  uploadUrl: String;
  editable = true;
  loading = false;
  viewerManager: ViewerType;

  contentHtml: string = "";

  selectedFile = '';
  fileExtension: FileExtensionDTO;

  task: TareaDTO;
  activeTaskUnsubscriber: Subscription;
  pdfName: string;
  subscriptions: Subscription[] = [];
  showVersionado: Boolean = false;
  documentPreviewUrl: string | Uint8Array = '';

  constructor(
    private _store: Store<State>,
    private _api: ApiBase,
    public loadingService: LoadingService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _taskSandBox: TaskSandBox,
    private _produccionDocumentalApi: ProduccionDocumentalApiService,
    private messagingService: MessagingService,
    private fileextensionapi: FileextensionApi
  ) {
  }

  ngOnInit() {
    if (isNullOrUndefined(this.causaDI)) {
      this.causaDI = this.versionar;
    }
    this.subscriptions.push(this._store.select(getActiveTask).subscribe(activeTask => {
      this.task = activeTask;
    }));
    //this.loadDocumentos();
  }

  ngAfterViewInit() {
    //this.loadDocumentos();
  }

  ngOnChanges(): void {
    this.loadDocumentos();

    this._changeDetectorRef.detectChanges();
  }

  loadDocumentos() {
    let options: any;
    if (!this.showLoading) {
      options = {
        headers: new HttpHeaders().append('skipLoading', 'true')
      }
    } else {
      this.loading = true;
    }
    if (this.idDocumentECM !== undefined) {
      let endpoint = '';
      if (isNullOrUndefined(this.nroRadicadoPrincipal)) {
        endpoint = `${environment.obtenerDocumento_asociados_endpoint}/${this.idDocumentECM}?allDocuments=false`;
      } else {
        endpoint = `${environment.obtenerDocumentos_radicado}/${this.idDocumentECM}/${Utils.niceRadicado(this.nroRadicadoPrincipal)}`;
      }
      this.subscriptions.push(
        this._api.list(endpoint, {}, options).subscribe(response => {
          this.documentsList = [];
          if (response.codMensaje === '0000') {
            this.documentsList = response.documentoDTOList;
          }
          this.loading = false;
          this._changeDetectorRef.detectChanges();
        })
      );

    }
  }

  setDataDocument(data: any) {
    this.idDocumentECM = data.comunicacion;
    this.versionar = data.versionar;
  }
  /**
   * @description
   * logica del visor pantalla de asignar comunicaciones selector de tipos de documento
   *
   * */
  showDocument(documento: any) {
    let fileExt = documento.nombreDocumento.split(".");
    let ext = fileExt[1];
    let options: any = {
      headers: new HttpHeaders().append('skipLoading', 'true')
    };
    this.fileextensionapi.recuperarExtension(ext.toLowerCase(), options).subscribe(response => {
        if (response.transformation) {
          this.loadingService.presentLoading();
          let docSrc = environment.obtenerDocumento + documento.idDocumento;
          PDFJS.getDocument(docSrc).then(doc => {
            this.loadingService.presentLoading();
            doc.getData().then(array => {
              const blob = new Blob([array], {type: 'application/pdf'});
              const reader = new FileReader();
              reader.readAsDataURL(blob);
              reader.onloadend = () => {
                let base64 = reader.result.toString().split('base64,');
                const url = `${base64[1]}`;
                this.showPdfViewer(url, documento.nombreDocumento);
                this.refreshView();
                this.loadingService.dismissLoading();
              }
            })
          }).catch(error => {
            console.log('Error', error);
            if(error.name === 'PasswordException'){
              this._produccionDocumentalApi.getDocumentoEcm(documento.idDocumento).subscribe(val => {
                const dlnk: any = document.getElementById('dwnldLnk');
                dlnk.href = `data:${val.type};base64,${val.base64File}`;
                dlnk.download = `${val.name}`;
                dlnk.click();
                return this._store.dispatch(new PushNotificationAction({
                  severity: 'info',
                  summary: 'Este es un documento cifrado, debe tener la clave correspondiente para que pueda ser visualizado'
                }));
              });
              this.loadingService.dismissLoading();
              this._changeDetectorRef.detectChanges();
            }else{
              this.loadingService.dismissLoading();
              return this._store.dispatch(new PushNotificationAction({
                severity: 'info',
                summary: 'El documento no se cargo correctamente'
              }));
              this._changeDetectorRef.detectChanges();
            }
          });
        } else if (response.mimetype === 'text/html') {
          this.loadingService.presentLoading();
          this._produccionDocumentalApi.getDocumentoEcm(documento.idDocumento).subscribe(val => {
            this.viewDocument = true;
            this.contentHtml = Utf8Helper.decode(atob(val.base64File));
            this.viewerManager = ViewerType.HTML;
            this.loadingService.dismissLoading();
            this._changeDetectorRef.detectChanges();
          });
        } else {
          this._produccionDocumentalApi.getDocumentoEcm(documento.idDocumento).subscribe(val => {
            const dlnk: any = document.getElementById('dwnldLnk');
            dlnk.href = `data:${val.type};base64,${val.base64File}`;
            dlnk.download = `${val.name}`;
            dlnk.click();
            this._changeDetectorRef.detectChanges();
          });
        }
      }
    );
  }

  showPdfViewer(pdfUrl: any, nombre: string) {
    this.showVersionado = true;
    this.documentPreviewUrl = null;
    this.pdfName = nombre;
    this.documentPreviewUrl = pdfUrl;
  }

  refreshView() {
    this._changeDetectorRef.detectChanges();
  }

  hideDocument() {
    this.viewDocument = false;
    this.viewerManager = null;
    this.showVersionado = false;
  }


  docLoaded() {
    this.loadingService.dismissLoading();
  }

  selectFile(uploader: FileUpload) {
    uploader.showUploadButton = true;
    uploader.styleClass = 'doc-selected';
  }

  clearFileList(uploader: FileUpload) {
    uploader.showUploadButton = false;
    uploader.styleClass = '';
  }

  onUpload() {

  }

  uploadHandler(event: any, uploader: FileUpload) {
    if (event.files.length === 0) {
      return false;
    }
    const nv: VersionDocumentoDTO = {
      id: this.idDocumentECM,
      nombre: event.files[0].name,
      tipo: 'pdf',
      version: '',
      contenido: '',
      file: event.files[0],
      size: event.files[0].size
    };
    this.uploadVersionDocumento(nv)
      .subscribe(doc => {
        const indexDocumentoAnterior = this.documentsList.findIndex(item => item.idDocumento === this.idDocumentECM);
        this.documentsList[indexDocumentoAnterior] = doc;
        this.documentsList = [...this.documentsList];
        uploader.clear();
        // uploader.basicFileInput.nativeElement.value = '';
        uploader.showUploadButton = true;
        uploader.styleClass = 'doc-selected';
        uploader.disabled = false;
        this._changeDetectorRef.detectChanges();
      });
  }

  uploadVersionDocumento(doc: VersionDocumentoDTO): Observable<VersionDocumentoDTO> {
    const formData = new FormData();
    formData.append('documento', doc.file, doc.nombre);
    if (doc.id) {
      formData.append('idDocumento', doc.id);
    }
    formData.append('selector', 'PD');
    formData.append('nombreDocumento', doc.nombre);
    formData.append('tipoDocumento', doc.tipo);
    formData.append('sede', this.task.variables.nombreSede);
    formData.append('dependencia', this.task.variables.nombreDependencia);
    formData.append('nroRadicado', this.task.variables && this.task.variables.numeroRadicado || null);
    const documentoVersionado = this._produccionDocumentalApi.subirVersionDocumento(formData).pipe(map(
      resp => {
        if ('0000' === resp.codMensaje) {
          this._store.dispatch(new PushNotificationAction({severity: 'success', summary: resp.mensaje}))
          return resp.documentoDTOList[0];
        } else {
          this._store.dispatch(new PushNotificationAction({severity: 'error', summary: resp.mensaje}));
          return doc;
        }
      },
      error => {
        this._store.dispatch(new PushNotificationAction({severity: 'error', summary: error}))
        return doc;
      }
    ));
    return documentoVersionado;
  }

  ngOnDestroy(): void {

    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
