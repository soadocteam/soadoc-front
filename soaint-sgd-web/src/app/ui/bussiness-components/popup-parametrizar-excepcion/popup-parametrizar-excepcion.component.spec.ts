import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupParametrizarExcepcionComponent } from './popup-parametrizar-excepcion.component';

describe('PopupParametrizarExcepcionComponent', () => {
  let component: PopupParametrizarExcepcionComponent;
  let fixture: ComponentFixture<PopupParametrizarExcepcionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupParametrizarExcepcionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupParametrizarExcepcionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
