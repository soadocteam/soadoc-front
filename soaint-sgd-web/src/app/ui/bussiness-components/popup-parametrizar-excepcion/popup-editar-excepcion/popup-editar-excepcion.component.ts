import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ExcepcionesApiService} from "../../../../infrastructure/api/excepciones.api";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PushNotificationAction} from "../../../../infrastructure/state-management/notifications-state/notifications-actions";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../../infrastructure/redux-store/redux-reducers";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-popup-editar-excepcion',
  templateUrl: './popup-editar-excepcion.component.html',
  styleUrls: ['./popup-editar-excepcion.component.scss']
})
export class EditarExcepcionComponent implements OnInit {

  formUpdate: FormGroup;
  exception: any;
  @Output() opt: EventEmitter<any> = new EventEmitter<any>();
  objectStore: boolean = false;
  codSerie: boolean = false;
  objectStoreList: Array<{ label: string, value: string }>;



  constructor(private fe: FormBuilder,
              private _excepcionesApi: ExcepcionesApiService,
              private _store: Store<RootState>) {
  }


  ngOnInit() {

    this.objectStoreList = [
      {label: "COLPE", value:"COLPE"},
    ]

    this.initFormUpdate();
  }

  initFormUpdate() {
    this.formUpdate = this.fe.group({
      objectStore: [this.objectStoreList[0].value, Validators.required],
      codDependencia: [null],
      codSerie: [null, Validators.required],
      codSubSerie: [null],
    });
  }

  editException(exception) {
    this.exception = exception;
    this.formUpdate = this.fe.group({
      objectStore: [this.exception.objectStore, Validators.required],
      codDependencia: [this.exception.codDependencia],
      codSerie: [this.exception.codSerie, Validators.required],
      codSubSerie: [this.exception.codSubSerie],
    });
  }

  updateException() {
    if (this.formUpdate.invalid) {
      this.validateFields();
      this._store.dispatch(new PushNotificationAction({
        summary: 'Faltan datos obligatorios',
        severity: 'info'
      }))
    } else {
      const payload: any = {
        idTable: this.exception.idTable,
        objectStore: !isNullOrUndefined(this.formUpdate.get('objectStore').value) ? this.formUpdate.get('objectStore').value : "",
        codDependencia: !isNullOrUndefined(this.formUpdate.get('codDependencia').value) ? this.formUpdate.get('codDependencia').value : "",
        codSerie: !isNullOrUndefined(this.formUpdate.get('codSerie').value) ? this.formUpdate.get('codSerie').value : "",
        codSubSerie: !isNullOrUndefined(this.formUpdate.get('codSubSerie').value) ? this.formUpdate.get('codSubSerie').value : "",
      };
      this._excepcionesApi.updateException(payload).subscribe(() => {
        this._store.dispatch(new PushNotificationAction({
          summary: 'Se han actualizado los datos  exitosamente',
          severity: 'success'
        }))
        this.opt.emit();
      })
    }
  }

  validateFields() {
    if (isNullOrUndefined(this.objectStore) && isNullOrUndefined(this.codSerie)) {
      this.objectStore = true;
      this.codSerie = true;
    } else if (isNullOrUndefined(this.objectStore)) {
      this.objectStore = true;
    } else if (isNullOrUndefined(this.codSerie)) {
      this.codSerie = true;
    }
  }

}
