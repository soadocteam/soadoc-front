import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupEditarExcepcionComponent } from './popup-editar-excepcion.component';

describe('PopupEditarExcepcionComponent', () => {
  let component: PopupEditarExcepcionComponent;
  let fixture: ComponentFixture<PopupEditarExcepcionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupEditarExcepcionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupEditarExcepcionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
