import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {isNullOrUndefined} from "util";
import {ExcepcionesApiService} from "../../../infrastructure/api/excepciones.api";
import {Observable} from "rxjs/Observable";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../infrastructure/redux-store/redux-reducers";
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";
import {ConfirmationService} from "primeng/api";


@Component({
  selector: 'app-popup-parametrizar-excepcion',
  templateUrl: './popup-parametrizar-excepcion.component.html',
  styleUrls: ['./popup-parametrizar-excepcion.component.scss']
})
export class ParametrizarExcepcionComponent implements OnInit {

  editarExcepcion: boolean = false;
  formException: FormGroup;
  listadoExepciones$: Observable<any[]>;
  @ViewChild('editException') editException;
  exceptions: any;
  objectStore: boolean = false;
  codSerie: boolean = false;
  objectStoreList: Array<{ label: string, value: string }>;


  constructor(private fe: FormBuilder,
              private _excepcionesApi: ExcepcionesApiService,
              private _store: Store<RootState>,
              private changeDetectorRef: ChangeDetectorRef,
              private _confirmDialog: ConfirmationService,) { }

  ngOnInit() {

    this.objectStoreList = [
      {label: "COLPE", value:"COLPE"},
    ]

    this.initFormException();
  }

  initFormException() {
    this.formException = this.fe.group({
      objectStore: [this.objectStoreList[0].value, Validators.required],
      codDependencia: [null, Validators.required],
      codSerie: [null, Validators.required],
      codSubSerie: [null, Validators.required],
    });
  }


  addException(){
    if (this.formException.invalid) {
      this.validateFields();
      this._store.dispatch(new PushNotificationAction({
        summary: 'Faltan datos obligatorios',
        severity: 'info'
      }))
    } else {
      const payload: any = {
        objectStore: !isNullOrUndefined(this.formException.get('objectStore').value) ? this.formException.get('objectStore').value : "",
        codDependencia: !isNullOrUndefined(this.formException.get('codDependencia').value) ? this.formException.get('codDependencia').value : "",
        codSerie: !isNullOrUndefined(this.formException.get('codSerie').value) ? this.formException.get('codSerie').value : "",
        codSubSerie: !isNullOrUndefined(this.formException.get('codSubSerie').value) ? this.formException.get('codSubSerie').value : "",
      };
      this._excepcionesApi.addException(payload).subscribe(data => {
        this._store.dispatch(new PushNotificationAction({
          summary: 'Se han guardado los datos  exitosamente',
          severity: 'success'
        }))
        this.changeDetectorRef.detectChanges();
        this.getExceptions();
      })
    }
  }

  getExceptions(){
    this.listadoExepciones$ = this._excepcionesApi.getExcepctions()
    this.changeDetectorRef.detectChanges();
  }

  editExcepcion(excepcion: any){
    this.editarExcepcion = true;
    this.editException.editException(excepcion);
    this.changeDetectorRef.detectChanges();
    this.getExceptions();
  }

  hideEditarExcepcion(){
    this.editarExcepcion = false;
    this.getExceptions();
  }

  deleteExeption(excepcion: any){
    this._confirmDialog.confirm({
      message: `<p style="text-align: center">¿Está seguro que desea eliminar?</p>`,
      header: 'Atención',
      accept: () => {
        this._excepcionesApi.deleteException(excepcion).subscribe(() => {
          this._store.dispatch(new PushNotificationAction({
            summary: 'Se han eliminado los datos  exitosamente',
            severity: 'success'
          }))
          this.changeDetectorRef.detectChanges();
          this.getExceptions();
        })

      },
      reject: () => {

      }
    });

  }

  validateFields() {
    if (isNullOrUndefined(this.objectStore) && isNullOrUndefined(this.codSerie)) {
      this.objectStore = true;
      this.codSerie = true;
    } else if (isNullOrUndefined(this.objectStore)) {
      this.objectStore = true;
    } else if (isNullOrUndefined(this.codSerie)) {
      this.codSerie = true;
    }
  }

}
