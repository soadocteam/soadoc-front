import {ChangeDetectorRef, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {isNullOrUndefined} from "util";
import {InstrumentoApi} from "../../../infrastructure/api/instrumento.api";
import {Store} from "@ngrx/store";
import {State} from 'app/infrastructure/redux-store/redux-reducers';
import {Observable} from "rxjs/Observable";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Sandbox as DependenciaSandbox} from "../../../infrastructure/state-management/dependenciaGrupoDTO-state/dependenciaGrupoDTO-sandbox";
import {DependenciaDTO} from "../../../domain/dependenciaDTO";
import {getArrayData as sedeAdministrativaArrayData} from "../../../infrastructure/state-management/sedeAdministrativaDTO-state/sedeAdministrativaDTO-selectors";
import {Subscription} from "rxjs/Subscription";
import {map} from "rxjs/operators";
import {VALIDATION_MESSAGES} from "../../../shared/validation-messages";
import {LoadAction as SedeAdministrativaLoadAction} from "../../../infrastructure/state-management/sedeAdministrativaDTO-state/sedeAdministrativaDTO-actions";
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";
import {getSelectedDependencyGroupFuncionario} from "../../../infrastructure/state-management/funcionarioDTO-state/funcionarioDTO-selectors";
import {ConstanteDTO} from "../../../domain/constanteDTO";
import {OrganigramaDTO} from "../../../domain/organigramaDTO";
import {ConfirmationService} from "primeng/components/common/api";


@Component({
  selector: 'app-rediccion-multiple-asignar',
  templateUrl: './rediccion-multiple-asignar.component.html',
  styleUrls: ['./rediccion-multiple-asignar.component.scss']
})
export class RediccionMultipleAsignarComponent implements OnInit {

  form: FormGroup;
  validations: any = {};
  tiposComunicaciones: any[] = [];
  tipoComunicacion: any;
  sedeAdministrativaSuggestions$: Observable<ConstanteDTO[]>;
  tabla: any[] = [];
  AgenteIdEliminado: number;
  dependencias: DependenciaDTO[] = [];
  dependenciaSelected$: Observable<any>;
  dependenciaGrupoSuggestions$: Observable<ConstanteDTO[]>;
  dependenciaGrupo: ConstanteDTO[];
  dependenciaSelected: DependenciaDTO;
  subscriptions: Subscription[] = [];

  @Output()
  onChangeSedeAdministrativa: EventEmitter<any> = new EventEmitter();

  @Output()
  onRedirectComunication: EventEmitter<any > = new EventEmitter();

  @Output()
  hideRedireccion: EventEmitter<any > = new EventEmitter();

  constructor(private _store: Store<State>, private changeDetector: ChangeDetectorRef,
              private _instrumentoApi: InstrumentoApi,
              private confirmationService: ConfirmationService,
              protected _dependenciaSandbox: DependenciaSandbox,
              private formBuilder: FormBuilder) {
    this.initForm();
    this.listenForChanges();
    this.listenForErrors();
  }


  ngOnInit(): void {
    this.dependenciaSelected$ = this._store.select(getSelectedDependencyGroupFuncionario);
    this.dependenciaSelected$.subscribe((result) => {
      this.dependenciaSelected = result;
    });

    this.sedeAdministrativaSuggestions$ = this._store.select(sedeAdministrativaArrayData);
    this.dependenciaGrupoSuggestions$ = this._instrumentoApi.getDependenciesList();
    this.dependenciaGrupoSuggestions$.subscribe(dependencia => {
      this.dependenciaGrupo = dependencia;
    });
    this._store.dispatch(new SedeAdministrativaLoadAction());
  }

  initForm() {
    this.form = this.formBuilder.group({
      'justificacion': [{value: null, disabled: false}, Validators.required],
      'tipoComunicacion': [{value: null, disabled: false}],
      'sedeAdministrativa': [{value: null, disabled: false}, Validators.required],
      'dependenciaGrupo': [{value: null, disabled: false}, Validators.required],
    });
  }

  fillDependenciaOptions(filter?: (dependencia: ConstanteDTO) => boolean) {

    this.dependenciaGrupoSuggestions$ = this._instrumentoApi.getDependenciesList()
      .pipe(map(dependencias => dependencias.filter(dependncy => isNullOrUndefined(filter) || filter(dependncy))));
  }

  listenForChanges() {
    this.form.get('sedeAdministrativa').valueChanges.subscribe((value) => {
      const control = this.form.get('sedeAdministrativa');
      if (value && control.enabled) {
        this.onChangeSedeAdministrativa.emit(value);
        this.form.get('dependenciaGrupo').setValue(null);
        this.dependenciaGrupoSuggestions$ = this._instrumentoApi.ListDependences({value});
      }
    })
  }

  listenForErrors() {
    this.bindToValidationErrorsOf('sedeAdministrativa');
    this.bindToValidationErrorsOf('dependenciaGrupo');
    this.bindToValidationErrorsOf('justificacion');
    this.bindToValidationErrorsOf('tipoComunicacion');
  }

  bindToValidationErrorsOf(control: string) {
    const ac = this.form.get(control);
    ac.valueChanges.subscribe(value => {
      if ((ac.touched || ac.dirty) && ac.errors) {
        const error_keys = Object.keys(ac.errors);
        const last_error_key = error_keys[error_keys.length - 1];
        this.validations[control] = VALIDATION_MESSAGES[last_error_key];
      } else {
        delete this.validations[control];
      }
    });
  }

  listenForBlurEvents(control: string) {
    const ac = this.form.get(control);
    if (control === 'justificacion')
      if (!isNullOrUndefined(ac) && !isNullOrUndefined(ac.value))
        ac.setValue(ac.value.toString().trim());
    this.validations[control] = null;
    if (ac.touched && ac.invalid) {
      const error_keys = Object.keys(ac.errors);
      let last_error_key = error_keys[error_keys.length - 1];

      if (last_error_key == 'pattern')
        last_error_key = 'required';
      this.validations[control] = [...VALIDATION_MESSAGES[last_error_key]];
    }
  }

  redirectComunications() {
    let tipo;
    if(isNullOrUndefined(this.form.get('tipoComunicacion').value.value)){
      tipo = this.form.get('tipoComunicacion').value;
    } else {
      tipo = this.form.get('tipoComunicacion').value.value;
    }
    let array = this.tabla.filter( val => isNullOrUndefined(val.agentId));
    if(array.length > 0){
      this.onRedirectComunication.emit({
        justificacion: this.form.get('justificacion').value,
        tipoComunicacion: tipo,
        sedeAdministrativa: this.form.get('sedeAdministrativa').value,
        dependenciaGrupo: this.form.get('dependenciaGrupo').value,
        agente: this.AgenteIdEliminado
      });
    } else {
      this.confirmationService.confirm({
        message: '<p style="text-align: center">Debe agregar dependencias nuevas para ejecutar el redireccionamiento, el documento permanecerá en la bandeja asignar comunicaciones\n' +
        '\n</p>',
        accept: () => {
          this.hideRedireccion.emit();
        },
        reject: () => {

        }
      });

    }

  }

  refreshView() {
    this.changeDetector.detectChanges();
  }

  llenadoTabla( data: any[] ){
    this.tabla = data[0].agenteList.filter( value => value.agentTypeCode == "TP-AGED");
    const val = this.tabla.find( value => value.originalInd == "TP-DESC" && value.idNumber == this.dependenciaSelected.codigo );
    if(!isNullOrUndefined(val)){
      this.tiposComunicaciones = [
        {label: 'Con Copia', value: 'TP-DESC'}
      ];
      this.tipoComunicacion = {label: 'Con Copia', value: 'TP-DESC'};
      this.refreshView();
    } else {
      this.tiposComunicaciones = [
        {label: 'Principal', value: 'TP-DESP'},
        {label: 'Con Copia', value: 'TP-DESC'}
      ];
      this.tipoComunicacion = {label: 'Principal', value: 'TP-DESP'};
      this.refreshView();
    }
  }

  agregar(){
    let form = this.form.value;
    let tipo;
    if(isNullOrUndefined(this.form.get('tipoComunicacion').value.value)){
      tipo = this.form.get('tipoComunicacion').value;
    } else {
      tipo = this.form.get('tipoComunicacion').value.value;
    }

   let payload = {
      agentContactDTO: null,
      agentContactId: null,
      agentId: null,
      agentTypeCode: "TP-AGED",
      businessName: null,
      courtesyCode: null,
      creationDate: null,
      distributionState: "SD",
      idDocTypeCode: null,
      idNumber: form.dependenciaGrupo,
      instanceId: null,
      name: this.findNombre(form.dependenciaGrupo),
      nit: null,
      numRedirects: null,
      numReturns: null,
      originalInd: tipo,
      personDTO: null,
      personId: null,
      personTypeCode: null,
      qualityCode: null,
      remiTypeCode: null,
      sectionId: null,
      stateCode: null,
      taskId: null,
      justificacion: form.justificacion
    };

    if(isNullOrUndefined(this.tabla.find( value => value.idNumber == payload.idNumber ))){
      if( payload.originalInd == "TP-DESC" ){
        this.tabla.push(payload);
        this.refreshView();
      } else  if(isNullOrUndefined(this.tabla.find( value => value.originalInd == payload.originalInd )) ) {
        this.tabla.push(payload);
        this.refreshView();
      } else {
        this.confirmationService.confirm({
          message: '<p style="text-align: center"> Está seguro desea substituir el destinatario principal?</p>',
          accept: () => {
            const val =  this.tabla.find((val) => val.originalInd == "TP-DESP");
            if(isNullOrUndefined(this.AgenteIdEliminado)){
              this.AgenteIdEliminado = val.agentId;
            }
            this.tabla = this.tabla.filter((val) => val.originalInd != "TP-DESP");
            this.tabla.push(payload);
            this.refreshView();
          },
          reject: () => {

          }
        });
      }
    } else {
      this._store.dispatch(new PushNotificationAction({
        severity: 'warn',
        summary: `El destinatario ya existe en la lista `
      }));
    }
  }

  tipo(value: string): string{
    if(value == "TP-DESP"){
      return 'Principal'
    } else {
      return 'Con Copia'
    }
  }

  findSede(code): string {
    const result = this.dependenciaGrupo.find((element) => element.codigo == code);
    return result ? result.nomSede : '';
  }

  findNombre(code): string {
    const result = this.dependenciaGrupo.find((element) => element.codigo == code);
    return result ? result.nombre : '';
  }

  eliminar(eliminado){
    if(isNullOrUndefined(this.AgenteIdEliminado)){
      this.AgenteIdEliminado = eliminado.agentId;
    }
    let index = this.tabla.indexOf(eliminado);
    this.tabla = this.tabla.filter((val, i) => i != index);
    this.refreshView();
  }

  validateIcon(dep){
    if(this.dependenciaSelected.codigo == dep.idNumber || isNullOrUndefined(dep.agentId)){
      return true;
    } else {
      return false;
    }
  }

  ngOnDestroy(): void {
    this.tiposComunicaciones = null;
    this.tabla = null;
    this.subscriptions.forEach(s => s.unsubscribe())
  }

}
