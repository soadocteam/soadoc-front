import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupParametrizarHorarioComponent } from './popup-parametrizar-horario.component';

describe('PopupParametrizarHorarioComponent', () => {
  let component: PopupParametrizarHorarioComponent;
  let fixture: ComponentFixture<PopupParametrizarHorarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupParametrizarHorarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupParametrizarHorarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
