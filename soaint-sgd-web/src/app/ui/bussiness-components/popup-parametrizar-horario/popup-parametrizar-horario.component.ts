import {ChangeDetectorRef, Component, OnInit, AfterViewInit} from '@angular/core';
import {IndiceElectronicoApi} from "../../../infrastructure/api/indice-electronico.api";
import {FormBuilder, FormGroup} from "@angular/forms";
import {isNullOrUndefined} from "util";
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../infrastructure/redux-store/redux-reducers";

@Component({
  selector: 'app-popup-parametrizar-horario',
  templateUrl: './popup-parametrizar-horario.component.html',
  styleUrls: ['./popup-parametrizar-horario.component.scss']
})
export class ParametrizarHorarioComponent implements OnInit {

  executeData: any[];
  start: any[] = [];
  end: any[] = [];
  formHorario: FormGroup;
  idTable: any;
  strartExecute: any[];
  endExecute: any[];
  invalidDate: boolean = false;


  constructor( private _indiceElectronicoApi: IndiceElectronicoApi,
               private fe: FormBuilder,
               private _store: Store<RootState>,
               protected _detectChanges: ChangeDetectorRef,) { }

  ngOnInit() {
    this.initFormHorario();
  }


  initFormHorario() {
    this.formHorario = this.fe.group({
      strartExecute: [this.strartExecute],
      endExecute: [this.endExecute],
    });

  }

  getExecuteTime(){
    this.cargarHorario();
    var type = 0;
    this._indiceElectronicoApi.getExecutionTime(type).subscribe(data => {
      this.idTable = data.idTable;
      this.strartExecute = data.strartExecute;
      this.endExecute = data.endExecute;
      this.refreshView();
    });
  }

  refreshView() {
    this._detectChanges.detectChanges();
  }

  updateExecuteData() {
    const payload: any = {
      idTable: this.idTable,
      strartExecute: !isNullOrUndefined(this.formHorario.get('strartExecute').value) ? this.formHorario.get('strartExecute').value : "",
      endExecute: !isNullOrUndefined(this.formHorario.get('endExecute').value) ? this.formHorario.get('endExecute').value : "",
    };
    this._indiceElectronicoApi.updateTimeExecute(payload).subscribe(() => {
      this._store.dispatch(new PushNotificationAction({
        summary: 'Se ha registrado el nuevo horario de generación del índice electrónico',
        severity: 'success'
      }))
    });
  }


  cargarHorario() {
    let getHorarios = this.getHorario('00:00-23:30');
    this.start = getHorarios;
    this.end = getHorarios;
  }


  getHorario(horario): any[] {
    let payload: { value: any, label: any }[] = [];
    let jornada = horario.split('-');
    if (jornada[1] === '00:00') {
      return [];
    }
    let manada = jornada[0].split(':');
    let tarde = jornada[1].split(':');
    let pref = 'am';
    let hora1;
    let hora2;
    let maxManada = manada[1];
    let maxTarde = tarde[1];
    let index1;
    let data: { value: any, label: any }[] = [];
    if (manada[0] === tarde[0] && manada[1] < tarde[1]) {
      if (+manada[0] >= 13) {
        pref = 'pm';
      }
      payload.push({
        value: `${jornada[0]} ${pref}`,
        label: jornada[0]
      }, {
        value: `${jornada[1]} ${pref}`,
        label: jornada[1]
      });
      return payload;
    }
    for (let i = +manada[0]; i <= +tarde[0]; i++) {
      index1 = i;
      if (i === +tarde[0]) {
        hora1 = ((index1 <= 9) ? '0' + index1 : index1) + ':00';
        data = this.setPayloadHorario(payload, hora1);
        if (data.length > 0) {
          payload = data;
        }
        if (maxTarde === '30') {
          hora1 = ((index1 <= 9) ? '0' + index1 : index1) + ':30';
        }
        payload = this.setPayloadHorario(payload, hora1);
      } else {
        if (maxManada === '30') {
          hora1 = ((index1 <= 9) ? '0' + index1 : index1) + ':30';
          index1++;
          if (index1 <= +tarde[0] && hora1 !== jornada[1]) {
            hora2 = ((index1 <= 9) ? '0' + index1 : index1) + ':00';

            maxManada = '30'
          } else {
            hora2 = null;
          }
        } else {
          hora1 = ((index1 <= 9) ? '0' + index1 : index1) + ':00';
          hora2 = ((index1 <= 9) ? '0' + index1 : index1) + ':30';
          maxManada = '00';
        }
        payload = this.setPayloadHorario(payload, hora1);
        if (!isNullOrUndefined(hora2)) {
          payload = this.setPayloadHorario(payload, hora2);
        }
      }
    }
    return payload;
  }

  setPayloadHorario(payload: { value: any, label: any }[], hora: string): { value: any, label: any }[] {
    let dataP: any = {
      value: hora,
      label: hora
    };
    if (payload.length > 0) {
      if (!payload.find(date => date.value === dataP.value)) {
        payload.push(dataP);
      }
    } else {
      payload.push(dataP);
    }
    return payload;
  }

  changeStart(date) {
    this.endExecute = date.value;
    if (this.strartExecute === this.endExecute) {
      this.invalidDate = true;
    } else {
      this.invalidDate = false;
    }
  }

  changeEnd(date) {
    this.strartExecute = date.value;
    if (this.endExecute === this.strartExecute) {
      this.invalidDate = true;
    } else {
      this.invalidDate = false;
    }
  }



}
