import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AnnulmentDTO} from "../../../domain/AnnulmentDTO";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProduccionDocumentalApiService} from "../../../infrastructure/api/produccion-documental.api";
import {Observable} from "rxjs/Observable";
import {ConstanteDTO} from "../../../domain/constanteDTO";
import {VALIDATION_MESSAGES} from "../../../shared/validation-messages";
import {Subscription} from "rxjs/Subscription";
import {FuncionarioDTO} from "../../../domain/funcionarioDTO";
import {Sandbox as DependenciaGrupoSandbox} from "../../../infrastructure/state-management/dependenciaGrupoDTO-state/dependenciaGrupoDTO-sandbox";
import {FuncionariosService} from "../../../infrastructure/api/funcionarios.service";
import {DependenciaDTO} from "../../../domain/dependenciaDTO";
import {isNullOrUndefined} from "util";
import {RADICADOR} from "../../../app.roles";
import {RadicadosApi} from "../../../infrastructure/api/radicados.api";
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../infrastructure/redux-store/redux-reducers";
import {ConfirmationService} from "primeng/primeng";
import {
  getAuthenticatedFuncionario,
  getSelectedDependencyGroupFuncionario
} from "../../../infrastructure/state-management/funcionarioDTO-state/funcionarioDTO-selectors";
import {combineLatest} from "rxjs";
import {
  ClearActiveTaskAction
} from "../../../infrastructure/state-management/tareasDTO-state/tareasDTO-actions";

@Component({
  selector: 'app-informacion-anulacion',
  templateUrl: './informacion-anulacion.component.html',
  styleUrls: ['./informacion-anulacion.component.scss']
})
export class InformacionAnulacionComponent implements OnInit, OnDestroy {

  _selectedTrack: AnnulmentDTO = null;
  sections$: Observable<ConstanteDTO[]>;
  subsectionSelected: ConstanteDTO;
  form: FormGroup;
  validations: any = {};
  subsections: Array<any> = [];
  subscribers: Array<Subscription> = [];
  users$: Observable<FuncionarioDTO[]>;
  @Output() completar: EventEmitter<any> = new EventEmitter();
  @Output() reload: EventEmitter<any> = new EventEmitter();
  subscriptions: Subscription[] = [];

  constructor(private formBuilder: FormBuilder,
              private _produccionDocumentalApi: ProduccionDocumentalApiService,
              private _dependenciaGrupoSandbox: DependenciaGrupoSandbox,
              private userService: FuncionariosService,
              private radicadosService: RadicadosApi,
              private _store: Store<RootState>,
              private _confirmationService: ConfirmationService) {
    this.initForm();
  }

  initForm() {
    this.form = this.formBuilder.group({
      'section': [null, Validators.required],
      'subsection': [null, Validators.required],
      'user': [null, Validators.required],
      'observations': [null, Validators.required]
    });
  }

  clear() {
    this.form.reset();
  }

  ngOnInit(): void {
    this.sections$ = this._produccionDocumentalApi.getSedes({});
    this.listenForErrors();
    this.listenForChanges();
  }

  @Input() set selectedTrack(value: AnnulmentDTO) {
    this._selectedTrack = value;
  }

  listenForChanges() {
    this.subscribers.push(this.form.get('section').valueChanges.distinctUntilChanged().subscribe((section) => {
      this.form.get('subsection').reset();
      this.form.get('user').reset();
      if (section) {
        const subsectionsSubscription: Subscription = this._dependenciaGrupoSandbox.loadData({codigo: section.codigo}).subscribe(sections => {
          this.subsections = sections.organigrama;
          subsectionsSubscription.unsubscribe();
        });
      }
    }));
  }

  listenForErrors() {
    this.bindToValidationErrorsOf('section');
    this.bindToValidationErrorsOf('subsection');
    this.bindToValidationErrorsOf('user');
  }

  bindToValidationErrorsOf(control: string) {
    const ac = this.form.get(control);
    ac.valueChanges.subscribe(value => {
      if ((ac.touched || ac.dirty) && ac.errors) {
        const error_keys = Object.keys(ac.errors);
        const last_error_key = error_keys[error_keys.length - 1];
        this.validations[control] = VALIDATION_MESSAGES[last_error_key];
      } else {
        delete this.validations[control];
      }
    });
  }

  listenForBlurEvents(control: string) {
    const ac = this.form.get(control);
    if (ac.touched && ac.invalid) {
      const error_keys = Object.keys(ac.errors);
      const last_error_key = error_keys[error_keys.length - 1];
      this.validations[control] = VALIDATION_MESSAGES[last_error_key];
    }
  }

  sectionChange(event) {
    this.subsectionSelected = event.value;
    this.users$ = this.userService.getFuncionarioBySpecification(user => this.satisfyBy(event.value, user));
  }

  satisfyBy(subsection: DependenciaDTO, user: FuncionarioDTO) {

    if (isNullOrUndefined(subsection) || isNullOrUndefined(user))
      return false;

    return user.dependencias.some(d => d.codigo == subsection.codigo)
      && user.roles.some(rol => !isNullOrUndefined(rol) && rol.rol == RADICADOR);
  }

  deleteTracks() {

    this._confirmationService.confirm({
      message: `¿Está seguro que desea anular el radicado?`,
      header: 'Confirmación',
      icon: 'fa fa-question-circle',
      accept: async () => {
        this.subscriptions.push(combineLatest(this._store.select(getAuthenticatedFuncionario), this._store.select(getSelectedDependencyGroupFuncionario))
          .flatMap(([funcionario, selectedDependency]) => {
            const payloadAnnulment = this.getPayloadAnnulment();
            payloadAnnulment.annulmentUser = funcionario.loginName;
            payloadAnnulment.annulmentSubsectionCode = selectedDependency.codigo;
            return this.radicadosService.anularRadicados(this._selectedTrack.trackId, payloadAnnulment)
          }).subscribe(() => {
          this._store.dispatch(new PushNotificationAction({
            severity: 'success',
            summary: `El radicado ha sido anulado correctamente`
          }));
          this.completar.emit();
          this.reload.emit();
          this.complete();
        }));
      },
      reject: () => {
      }
    });
  }

  getPayloadAnnulment(): AnnulmentDTO {
    let sectionSelected: ConstanteDTO = this.form.get('section').value;
    let subsectionSelected: ConstanteDTO = this.form.get('subsection').value;
    let userSelected: FuncionarioDTO = this.form.get('user').value;
    let observations = "";
    if (!isNullOrUndefined(this.form.get('observations').value)) {
      observations = this.form.get('observations').value;
    }
    return {
      sectionId: sectionSelected.codigo,
      subsectionId: subsectionSelected.codigo,
      reqUser: userSelected.loginName,
      observations: observations
    };
  }

  complete() {
    this.clear();
    this.completar.emit();
    this.ngOnDestroy();
  }

  ngOnDestroy() {
    this._selectedTrack = null;
    this.subscriptions.forEach(s => s.unsubscribe());
    setTimeout(() => {
      this._store.dispatch(new ClearActiveTaskAction());
    }, 500);
  }
}
