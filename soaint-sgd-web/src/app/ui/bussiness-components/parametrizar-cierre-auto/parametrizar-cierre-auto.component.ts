import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs/Observable";
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";
import {ConfirmationService} from "primeng/api";
import {SerieSubserieApiService} from "../../../infrastructure/api/serie-subserie.api";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../infrastructure/redux-store/redux-reducers";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-parametrizar-cierre-auto',
  templateUrl: './parametrizar-cierre-auto.component.html',
  styleUrls: ['./parametrizar-cierre-auto.component.scss']
})
export class ParametrizarCierreAutoComponent implements OnInit {

  form: FormGroup;
  typesSerie: Array<{ label: string, value: string }>;
  yearOnHold: number;
  listConfigurations: Observable<any[]>;
  visibleConfigureSerie = false;
  @ViewChild('configure') configure;
  objectStore: Array<{ label: string, value: string }>;

  constructor(private formBuilder: FormBuilder, private _confirmDialog: ConfirmationService,
              private serieConfigurationServices: SerieSubserieApiService, private changeDetectorRef: ChangeDetectorRef,
              private _store: Store<RootState>) {
  }

  ngOnInit() {
    this.typesSerie = [
      {label: "Simple", value: "Simple"},
      {label: "Compuesta", value: "Compuesta"},
    ];

    this.objectStore = [
      {label: "COLPE", value: "COLPE"},
    ];

    this.yearOnHold = 1;

    this.form = this.formBuilder.group({
      objectStore: [this.objectStore[0].value, Validators.required],
      typeSerie: [this.typesSerie[0].value, Validators.required],
      codDependence: [null, Validators.required],
      codSerie: [null, Validators.required],
      codSubSerie: [null],
      yearOnHold: [this.yearOnHold, Validators.required],
    });
  }

  getAllConfigurations() {
    this.listConfigurations = this.serieConfigurationServices.getAllConfiguration();
    this.changeDetectorRef.detectChanges();
  }

  validateType() {
    // se valida si es compuesta
    if (this.form.get('typeSerie').value === this.typesSerie[1].value) {
      this.configure.visibleTypeRecord = true;
      let config: any = {
        objectStore: this.form.get('objectStore').value,
        codDependence: this.form.get('codDependence').value,
        codSerieRecord: this.form.get('codSerie').value,
        codSubSerieRecord: this.form.get('codSubSerie').value,
        typeSerie: this.form.get('typeSerie').value,
        yearOnHold: this.form.get('yearOnHold').value,
        codTypeRecord: [],
      }
      this.editConfig(config);
    } else {
      // tipo Simple
      this.configure.visibleTypeRecord = false;
      this.addConfig(null);
    }
  }

  addConfig(configEdit?: any) {
    let config: any = [];
    if (isNullOrUndefined(configEdit)) {
      config = {
        idConfig: null,
        codDependence: this.form.get('codDependence').value,
        codSerieRecord: this.form.get('codSerie').value,
        codSubSerieRecord: this.form.get('codSubSerie').value,
        typeSerie: this.form.get('typeSerie').value,
        codTypeRecord: null,
        yearOnHold: this.form.get('yearOnHold').value,
        objectStore: this.form.get('objectStore').value,
      }
    } else {
      config = configEdit;
    }
    this.serieConfigurationServices.addConfiguration(config).subscribe(response => {
      this._store.dispatch(new PushNotificationAction({
        summary: 'Se ha agregado la configuración exitosamente',
        severity: 'success'
      }));
      this.hideDialogConfigure();
      this.getAllConfigurations();
    });
  }

  deleteConfig(idConfig: any) {
    let config: any = {
      idConfig: idConfig,
    }
    this._confirmDialog.confirm({
      message: `<p style="text-align: center">¿Está seguro que desea eliminar la configuración?</p>`,
      header: 'Atención',
      accept: () => {
        this.serieConfigurationServices.deleteConfiguration(config).subscribe(() => {
          this._store.dispatch(new PushNotificationAction({
            summary: 'Se ha eliminado la configuración exitosamente',
            severity: 'success'
          }))
          this.changeDetectorRef.detectChanges();
          this.getAllConfigurations();
        })

      },
      reject: () => {
      }
    });
  }

  editConfig(config) {
    this.visibleConfigureSerie = true;
    this.configure.setConfig(config);
  }

  hideDialogConfigure() {
    this.visibleConfigureSerie = false;
    this.configure.clearForm();
    this.getAllConfigurations();
  }

  getConfiguration() {
    let config: any = {
      objectStore: this.form.get('objectStore').value,
      codDependence: this.form.get('codDependence').value,
      codSerieRecord: this.form.get('codSerie').value,
      codSubSerieRecord: this.form.get('codSubSerie').value,
      typeSerie: this.form.get('typeSerie').value,
      //codTypeRecord: this.form.get('codTypesRecord').value,
    }
    this.listConfigurations = this.serieConfigurationServices.getConfiguration(config);
    this.changeDetectorRef.detectChanges();
  }

  clearForm() {
    //this.form.reset();
    this.form.get('codDependence').reset("");
    this.form.get('codSerie').reset("");
    this.form.get('codSubSerie').reset("");
    this.form.get('yearOnHold').reset(1);
    this.getAllConfigurations();
  }
}
