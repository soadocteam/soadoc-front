import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurarCierreComponent } from './configurar-cierre.component';

describe('ConfigurarCierreComponent', () => {
  let component: ConfigurarCierreComponent;
  let fixture: ComponentFixture<ConfigurarCierreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurarCierreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurarCierreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
