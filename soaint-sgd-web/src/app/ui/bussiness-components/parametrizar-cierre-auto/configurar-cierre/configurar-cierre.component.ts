import {ChangeDetectorRef, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ConfirmationService} from "primeng/api";
import {SerieSubserieApiService} from "../../../../infrastructure/api/serie-subserie.api";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../../infrastructure/redux-store/redux-reducers";
import {isNullOrUndefined} from "util";
import {PushNotificationAction} from "../../../../infrastructure/state-management/notifications-state/notifications-actions";
import {InputElementContainer} from "html2canvas/dist/types/dom/replaced-elements/input-element-container";

@Component({
  selector: 'app-configurar-cierre',
  templateUrl: './configurar-cierre.component.html',
  styleUrls: ['./configurar-cierre.component.scss']
})
export class ConfigurarCierreComponent implements OnInit {

  form: FormGroup;
  typesSerie: Array<{ label: string, value: string }>;
  configUpdate: any;
  visibleTypeRecord = false;
  values1: string[];
  @Output()
  emitSaveOrEditConfig: EventEmitter<any> = new EventEmitter();
  isCompuesta: boolean = false;
  idConfig: any;
  @Output() hidePopup: EventEmitter<any> = new EventEmitter<any>();
  objectStore: Array<{ label: string, value: string }>;
  tempValueChips: string;

  constructor(private formBuilder: FormBuilder, private _confirmDialog: ConfirmationService,
              private serieConfigurationServices: SerieSubserieApiService, private changeDetectorRef: ChangeDetectorRef,
              private _store: Store<RootState>) {
  }

  ngOnInit() {
    this.typesSerie = [
      {label: "Simple", value: "Simple"},
      {label: "Compuesta", value: "Compuesta"},
    ];

    this.objectStore = [
      {label: "COLPE", value: "COLPE"},
    ];

    this.form = this.formBuilder.group({
      id: [null],
      objectStore: [this.objectStore[0].value],
      typeSerie: [this.typesSerie[0].value, Validators.required],
      codDependence: [null, Validators.required],
      codSerie: [null, Validators.required],
      codSubSerie: [null],
      yearOnHold: [null, Validators.required],
      codTypeRecord: [null]
    });
    this.form.get('codTypeRecord').valueChanges.subscribe(value => {
      if (value) {

      }
    });
  }

  changeChips() {
    let input = document.getElementsByClassName('undefined') as any;
    if (input[0] && input[0].value && input[0].value.length >= 200) {
      input[0].value = input[0].value.substring(0, 200);
    }
  }

  setConfig(config) {
    this.idConfig = config.idConfig;
    this.form.get('objectStore').setValue(config.objectStore);
    this.form.get('codDependence').setValue(config.codDependence);
    this.form.get('codSerie').setValue(config.codSerieRecord);
    this.form.get('codSubSerie').setValue(config.codSubSerieRecord);
    this.form.get('typeSerie').setValue(config.typeSerie);
    this.form.get('yearOnHold').setValue(config.yearOnHold);
    this.form.get('codTypeRecord').setValue(config.codTypeRecord);
    if (config.typeSerie === 'Compuesta') {
      this.isCompuesta = true;
    } else {
      this.isCompuesta = false;
    }
    this.changeDetectorRef.detectChanges();
  }

  saveConfig() {
    let codTypeRecord = this.form.get('codTypeRecord').value;
    let input = document.getElementsByClassName('undefined') as any;
    if (input[0] && input[0].value) {
      codTypeRecord.push(input[0].value);
    }
    let config: any = {
      objectStore: this.form.get('objectStore').value,
      codDependence: this.form.get('codDependence').value,
      codSerieRecord: this.form.get('codSerie').value,
      codSubSerieRecord: this.form.get('codSubSerie').value,
      typeSerie: this.form.get('typeSerie').value,
      yearOnHold: this.form.get('yearOnHold').value,
      codTypeRecord: codTypeRecord,
      idConfig: this.idConfig,
    }
    if (isNullOrUndefined(this.idConfig)) {
      this.emitSaveOrEditConfig.emit(config);
    } else {
      this.updateConfig(config);
    }

    this.clearForm();
  }

  clearForm() {
    this.form.reset();
  }

  changeType(event) {
    if (event.value === 'Compuesta') {
      this.isCompuesta = true;
      this.changeDetectorRef.detectChanges();
    } else {
      this.isCompuesta = false;
      this.form.get('codTypesRecord').setValue(null);
      this.changeDetectorRef.detectChanges();
    }
  }

  updateConfig(config) {
    this.serieConfigurationServices.updateConfiguration(config).subscribe(result => {
      this._store.dispatch(new PushNotificationAction({
        summary: 'Se ha actualizado la configuración exitosamente',
        severity: 'success'
      }));
      this.hidePopup.emit();
    });
  }

  get getValidateButtonSave() {
    return this.isCompuesta ? this.form.invalid || this.form.get('codTypeRecord').value.length == 0 : this.form.invalid;
  }

}
