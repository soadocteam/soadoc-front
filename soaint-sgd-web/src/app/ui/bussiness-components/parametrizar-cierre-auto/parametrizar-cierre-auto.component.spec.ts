import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametrizarCierreAutoComponent } from './parametrizar-cierre-auto.component';

describe('ParametrizarCierreAutoComponent', () => {
  let component: ParametrizarCierreAutoComponent;
  let fixture: ComponentFixture<ParametrizarCierreAutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParametrizarCierreAutoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametrizarCierreAutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
