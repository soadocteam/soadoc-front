import {Component} from '@angular/core';
import {Observable, of} from "rxjs";
import {EmptyObservable} from "rxjs-compat/observable/EmptyObservable";


@Component({
  selector: 'app-planilla-generada',
  templateUrl: './planilla-generada.component.html',
  styleUrls: ['./planilla-generada.component.css']
})
export class PlanillaGeneradaComponent {

  planillas$: Observable<any[]>;
  planillas = [];
  planillasSeleccionadas: any[] = [];

  constructor() {
  }

  setPlanillas(planillas: any[]) {
    this.planillas = planillas;
    this.planillas$ = of(planillas);
  }

  reset() {
    this.planillas = [];
    this.planillas$ = new EmptyObservable();
  }

}
