import {Component, Input, OnInit} from '@angular/core';
import {isNullOrUndefined} from "util";

export interface TicketRadicado {

  anexos?: string;

  tipoComunicacion?: string;

  asunto?: string;

  folios?: string;

  noRadicado?: string;

  fecha?: string;

  remitente?: string;

  remitenteSede?: string;

  remitenteGrupo?: string;

  destinatario?: string;

  destinatarioSede?: string;

  destinatarioGrupo?: string;

  abreviatura?:string;
}

@Component({
  selector: 'app-ticket-radicado',
  templateUrl: './ticket-radicado.component.html',
  styleUrls: ['./ticket-radicado.component.scss']
})
export class TicketRadicadoComponent implements OnInit {


  @Input() anexos: string = null;

  @Input() asunto: string = null;

  @Input() tipoComunicacion: string = null;

  @Input() folios: string = null;

  @Input() noRadicado: string = null;

  @Input() fecha: string = null;

  @Input() remitente: string = null;

  @Input() remitenteSede: string = null;

  @Input() remitenteGrupo: string = null;

  @Input() destinatario: string = null;

  @Input() destinatarioSede: string = null;

  @Input() destinatarioGrupo: string = null;

  abreviatura: string = null;
  constructor() {
  }

  setDataTicketRadicado(ticket: TicketRadicado) {
    this.anexos = ticket.anexos;
    this.tipoComunicacion = (ticket.tipoComunicacion.length <= 39) ? ticket.tipoComunicacion : ticket.tipoComunicacion.substring(0, 39);
    this.asunto = (ticket.asunto.length <= 39) ? ticket.asunto : ticket.asunto.substring(0, 39);
    this.folios = ticket.folios;
    this.noRadicado = ticket.noRadicado;
    this.fecha = ticket.fecha;
    this.remitente = !isNullOrUndefined(ticket.remitente) ? (ticket.remitente.length <= 39) ? ticket.remitente : ticket.remitente.substring(0, 39) : 'anónimo';
    this.remitenteSede = !isNullOrUndefined(ticket.remitenteSede) ? (ticket.remitenteSede.length <= 39) ? ticket.remitenteSede : ticket.remitenteSede.substring(0, 39) : '';
    this.remitenteGrupo = !isNullOrUndefined(ticket.remitenteGrupo) ? (ticket.remitenteGrupo.length <= 9) ? ticket.remitenteGrupo : ticket.remitenteGrupo.substring(0, 9) : '';
    this.destinatario = !isNullOrUndefined(ticket.destinatario) ? (ticket.destinatario.length <= 39) ? ticket.destinatario : ticket.destinatario.substring(0, 39) : '';
    this.destinatarioSede = !isNullOrUndefined(ticket.destinatarioSede) ? (ticket.destinatarioSede.length <= 39) ? ticket.destinatarioSede : ticket.destinatarioSede.substring(0, 39) : '';
    this.destinatarioGrupo = !isNullOrUndefined(ticket.destinatarioGrupo) ? (ticket.destinatarioGrupo.length <= 39) ? ticket.destinatarioGrupo : ticket.destinatarioGrupo.substring(0, 39) : '';
    this.abreviatura = !isNullOrUndefined(ticket.abreviatura) ? (ticket.abreviatura.length <= 30) ? ticket.abreviatura : ticket.abreviatura.substring(0, 30) : '';
  }

  ngOnInit() {
  }

}
