import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {TrazabilidadApi} from "../../../infrastructure/api/trazabilidad.api";
import {Observable} from "rxjs/Observable";
import {Utils} from "../../../shared/helpers";


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() valor: string;
  historico: Observable<any[]>;
  progreso: Observable<any[]>;
  mostrar: boolean = false;

  constructor(private trazabilidad: TrazabilidadApi,
              private _changeDetector: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  mostrarData(radicado, referido?: boolean, status?: boolean) {
    let payload: any = {
      objectId: Utils.niceRadicado(radicado)
    };
    if (referido) {
      payload.referred = referido;
    }
    if (status) {
      payload.status = status;
    }

    this.trazabilidad.listTrazabilidad(payload).subscribe( (value) => {
      this.mostrar = true;
      this.historico = value.completed;
      this.progreso = value.inProgress;
      this._changeDetector.detectChanges();
    });
  }

  getTrack(data: any) {
    return data.trackState.activity === 'Radicar documento producido' ? Utils.niceRadicado(data.objectId) : '';
  }

  getState(state) {
    let name;
    switch (state) {
      case 'Reserved':
        name = "Reservado";
        break;
      case 'Completed':
        name ="Completada";
        break;
      case 'Suspended ':
        name ="Suspedida";
        break;
      case 'Exited ':
        name ="Terminada";
        break;
      case 'Ready':
        name ="En Progreso";
        break;
      case 'InProgress':
        name ="En Progreso";
        break;
      case 'archivado':
        name ="Completada";
        break;
      default:
        name = null;
    }
    return name;
  }
}
