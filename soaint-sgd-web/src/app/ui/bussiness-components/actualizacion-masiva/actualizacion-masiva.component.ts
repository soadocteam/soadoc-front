import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FileUpload} from "primeng/fileupload";
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";
import {GestionEntregaApi} from "../../../infrastructure/api/gestionEntrega.api";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../infrastructure/redux-store/redux-reducers";
import {GestionEntregaTercerosDTO} from "../../../domain/gestionEntregaTercerosDTO";
import {DeliveriesMassiveDTO} from "../../../domain/deliveriesMassiveDTO";
import {DeliveryMassiveDTO} from "../../../domain/deliveryMassiveDTO";
import {SelectItem} from "primeng/api";
import {COURIER, MOTIVO_DEVOLUCION_SALIDA} from "../../../shared/constants";
import {isNullOrUndefined} from "util";
import {ERROR_DEFAULT, ERROR_MASSIVE} from "../../../shared/lang/es";

@Component({
  selector: 'app-actualizacion-masiva',
  templateUrl: './actualizacion-masiva.component.html'
})
export class ActualizacionMasivaComponent implements OnInit {

  first = 0;
  listaResultadoCarga: DeliveryMassiveDTO[] = [];
  listInfoUploaded: DeliveryMassiveDTO[] = [];
  itemsUploaded = 0;
  infoUploadedVisible = false;
  estados: SelectItem[];
  motivoDev: SelectItem[] = [];

  constructor(protected _services: GestionEntregaApi, protected _detectChanges: ChangeDetectorRef,
              protected _store: Store<RootState>) {
    this.estados = [
      {label: 'Entregado', value: 'EN'},
      {label: 'Devuelto', value: 'DV'},
      {label: 'Planillado', value: 'DT'}];
  }

  ngOnInit() {
    this._services.getListCourier().subscribe(list => {
      list.map(res => {
        if (res.codPadre === MOTIVO_DEVOLUCION_SALIDA) {
          this.motivoDev.push({label: res.nombre, value: res.codigo});
        }
      })
    });
  }

  uploadDocuments(event: any, uploader: FileUpload) {
    this.itemsUploaded = 0;
    const reader = new FileReader();
    reader.readAsDataURL(event.files[0]);
    reader.onload = () => {
      var payload = {
        file: reader.result.toString().replace(/^data:(.*,)?/, '')
      };
      this._services.uploadMessive(payload).subscribe(
        (response: DeliveryMassiveDTO[]) => {
          this.listaResultadoCarga = [...response];
          this.listaResultadoCarga.forEach(item => {
            if (item.status === 'Exitoso') {
              this.itemsUploaded++;
            }
          });
          this._detectChanges.detectChanges();
          this._store.dispatch(new PushNotificationAction({
            severity: 'success', summary: 'Documento adjuntado exitosamente'
          }));
        }, error => {
          this._store.dispatch(new PushNotificationAction({
            severity: 'error', summary: 'Ha ocurrido un error adjuntando el documento'
          }));
        }
      );
    };
    reader.onerror = (error) => {
      this._store.dispatch(new PushNotificationAction({
        severity: 'error', summary: 'Ha ocurrido un error al adjuntar el documento'
      }));
    };
    uploader.clear();
  }

  findDeliveryStatus(type): string {
    if (this.estados.some(res => res.value === type)) {
      return this.estados.find(res => res.value === type).label;
    }
  }

  findDevolution(name): any {
    if (this.motivoDev.some(res => res.value === name)) {
      return this.motivoDev.find(res => res.value === name).label;
    }
  }

  downloadReport() {
    if (isNullOrUndefined(this.listaResultadoCarga[0].massiveId) || this.listaResultadoCarga[0].massiveId === '') {
      this._store.dispatch(new PushNotificationAction({
        severity: 'info',
        summary: ERROR_MASSIVE
      }));
      return true;
    }

    this._services.downloadMessive(this.listaResultadoCarga[0].massiveId, {file: 'xls'}).subscribe(response => {
      if (response.file) {
        const xls = 'data:application/octet-stream;base64,' + response.file;
        const dlnk: any = document.getElementById('dwnldLnk');
        dlnk.href = xls;
        dlnk.download = 'Planilla_cargue_masivo_datos_envio_entrega.xlsx';
        dlnk.click();
      }
    });
  }

  viewInfo(data) {
    this.infoUploadedVisible = true;
    this.listInfoUploaded = [data];
    this._detectChanges.detectChanges();
  }

  hideInfoUploaded() {
    this.infoUploadedVisible = false;
  }
}
