import {
  ChangeDetectorRef,
  Component, EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ConstanteDTO} from 'app/domain/constanteDTO';
import {Store} from '@ngrx/store';
import {State} from 'app/infrastructure/redux-store/redux-reducers';

import {
  getTipoDocumentoArrayData,
  getTipoPersonaArrayData,
} from 'app/infrastructure/state-management/constanteDTO-state/constanteDTO-selectors';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {getArrayData as sedeAdministrativaArrayData} from 'app/infrastructure/state-management/sedeAdministrativaDTO-state/sedeAdministrativaDTO-selectors';
import {Sandbox as DependenciaGrupoSandbox} from 'app/infrastructure/state-management/dependenciaGrupoDTO-state/dependenciaGrupoDTO-sandbox';
import {VALIDATION_MESSAGES} from 'app/shared/validation-messages';
import {
  COMUNICACION_EXTERNA,
  COMUNICACION_INTERNA,
  PERSONA_ANONIMA,
  PERSONA_JURIDICA,
  PERSONA_NATURAL,
  TPDOC_CEDULA_CIUDADANIA,
  TPDOC_NRO_IDENTIFICACION_TRIBUTARIO
} from 'app/shared/bussiness-properties/radicacion-properties';
import {getActuaCalidadArrayData} from '../../../infrastructure/state-management/constanteDTO-state/selectors/actua-calidad-selectors';
import {Subscription} from 'rxjs/Subscription';
import {isNullOrUndefined} from "util";
import {DatosContactoApi} from "../../../infrastructure/api/datos-contacto.api";
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";
import {AgenteApi, PersonFilters} from "../../../infrastructure/api/agente.api";
import {map} from "rxjs/operators";
import {PersonDTO} from "../../../domain/personDTO";
import {of} from "rxjs/observable/of";
import {Utils} from "../../../shared/helpers";


@Component({
  selector: 'app-datos-remitente',
  templateUrl: './datos-remitente.component.html',
  encapsulation: ViewEncapsulation.None
})
export class DatosRemitenteComponent implements OnInit, OnDestroy {

  form: FormGroup;
  validations: any = {};
  visibility: any = {};
  display = false;

  // Observables
  tipoPersonaSuggestions$: Observable<ConstanteDTO[]>;
  tipoDocumentoSuggestions$: Observable<ConstanteDTO[]>;
  actuaCalidadSuggestions$: Observable<ConstanteDTO[]>;
  sedeAdministrativaSuggestions$: Observable<ConstanteDTO[]>;

  // Listas de subscripcion
  contacts: Array<any> = [];
  dependenciasGrupoList: Array<any> = [];
  ideAgente: any = null;

  subscriptionTipoDocumentoPersona: Array<ConstanteDTO> = [];

  subscribers: Array<Subscription> = [];

  @ViewChild('datosContactos') datosContactos;

  @Input() editable = true;
  @Input() tipoComunicacion: any;

  personsSuggestions$: Observable<PersonDTO[]>;
  personas: any;
  service: string[] = [];
  people: any[];

  tipoPersona: boolean = false;
  requeridoPerN: boolean = false;
  requeridoPerJ: boolean = false;
  requeridoSelected: boolean = false;
  contactosSelected: any;

  constructor(private _store: Store<State>,
              private formBuilder: FormBuilder,
              private _datosContactoApi: DatosContactoApi,
              private _dependenciaGrupoSandbox: DependenciaGrupoSandbox,
              private _agenteApi: AgenteApi,
              private _changeDetector: ChangeDetectorRef
  ) {
  }

  validRadicacionAgente() {
    if (this.form.get('tipoPersona').value === PERSONA_ANONIMA && this.datosContactos.contacts.length === 0) {
      return true;
    } else {
      return !isNullOrUndefined(this.datosContactos.direccionSeleccionada) ? true : false;
    }
  }

  validRadicacionAgenteCorregirdevolucion() {
    if (this.datosContactos.contacts.length === 0) {
      if(this.form.get('tipoPersona').value === PERSONA_ANONIMA){
        return true;
      } else {
        return false;
      }
    } else {
      return !isNullOrUndefined(this.datosContactos.direccionSeleccionada) ? true : false;
    }
  }

  ngOnInit(): void {
    this.initForm();
    //this.form.disable();
    this.listenForChanges();
    this.listenForErrors();
    this.initLoadTipoComunicacionExterna();
  }

  initLoadTipoComunicacionExterna() {
    this.tipoPersonaSuggestions$ = Utils.sortObservable(this._store.select(getTipoPersonaArrayData), 'nombre');
    this.tipoDocumentoSuggestions$ = Utils.sortObservable(this._store.select(getTipoDocumentoArrayData), 'nombre');
    this.actuaCalidadSuggestions$ = Utils.sortObservable(this._store.select(getActuaCalidadArrayData), 'nombre');
  }

  initLoadTipoComunicacionInterna() {
    this.sedeAdministrativaSuggestions$ = Utils.sortObservable(this._store.select(sedeAdministrativaArrayData), 'nombre');
  }

  initForm() {
    this.form = this.formBuilder.group({
      'tipoPersona': [{value: null, disabled: !this.editable}, Validators.required],
      'nit': [{value: null, disabled: !this.editable}, Validators.pattern(/^[0-9]+$/)],
      'actuaCalidad': [{value: null, disabled: !this.editable}],
      'tipoDocumento': [{value: null, disabled: !this.editable}],
      'razonSocial': [{value: null, disabled: !this.editable}],
      'nombreApellidos': [{value: null, disabled: !this.editable}],
      'nroDocumentoIdentidad': [{value: null, disabled: !this.editable}],
      'sedeAdministrativa': [{value: null, disabled: !this.editable}],
      'dependenciaGrupo': [{value: null, disabled: !this.editable}]
    });
  }

  setDatosRemitente(data) {
    if (data.nombreApellidos) {
      data.nombreApellidos = {name: data.nombreApellidos.toUpperCase()}
    }
    if (data.razonSocial) {
      data.razonSocial = {businessName: data.razonSocial.toUpperCase()}
    }
    this.form.patchValue(data);
  }

  listenForChanges() {
    this.subscribers.push(this.form.get('sedeAdministrativa').valueChanges.distinctUntilChanged().subscribe((sede) => {
      if (this.editable && sede) {
        this.form.get('dependenciaGrupo').reset();
        const depedenciaSubscription: Subscription = this._dependenciaGrupoSandbox.loadData({codigo: sede.id}).subscribe(dependencias => {
          this.dependenciasGrupoList = Utils.sort(dependencias.organigrama, 'nombre');
          depedenciaSubscription.unsubscribe();
        });
      }
    }));

    this.subscribers.push(this.form.get('tipoPersona').valueChanges.distinctUntilChanged().subscribe(value => {
      if (value !== null) {
        this.form.get("nit").setValue(null);
        this.form.get("nroDocumentoIdentidad").setValue(null);
        this.ideAgente = null;

        /*this.form.get("nombreApellidos").clearValidators();
        this.form.get("razonSocial").clearValidators();*/

        if (value == PERSONA_NATURAL) {
          this.form.get("nombreApellidos").setValidators(Validators.required);
        } else if (value == PERSONA_JURIDICA) {
          this.form.get("razonSocial").setValidators(Validators.required);
        }

        this.form.updateValueAndValidity();

        this.onSelectTipoPersona(value);
      }
    }));
  }

  getFormValue() {
    const formValue = this.form.value;
    if (formValue.nombreApellidos && typeof formValue.nombreApellidos == 'object') {
      formValue.nombreApellidos = formValue.nombreApellidos.name.toUpperCase();
    } else if(formValue.nombreApellidos){
      formValue.nombreApellidos = formValue.nombreApellidos.toUpperCase();
    }
    if (formValue.razonSocial && typeof  formValue.razonSocial == 'object') {
      formValue.razonSocial = formValue.razonSocial.businessName.toUpperCase();
    } else if(formValue.razonSocial){
      formValue.razonSocial = formValue.razonSocial.toUpperCase();
    }
    return formValue;
  }

  getFormRawValue() {
    const formValue = this.form.getRawValue();
    if (formValue.nombreApellidos && typeof formValue.nombreApellidos == 'object') {
      formValue.nombreApellidos = formValue.nombreApellidos.name.toUpperCase();
    }  else if(formValue.nombreApellidos){
      formValue.nombreApellidos = formValue.nombreApellidos.toUpperCase();
    }
    if (formValue.razonSocial && typeof  formValue.razonSocial == 'object') {
      formValue.razonSocial = formValue.razonSocial.businessName.toUpperCase();
    } else if(formValue.razonSocial){
      formValue.razonSocial = formValue.razonSocial.toUpperCase();
    }
    return formValue;
  }

  listenForErrors() {
    this.bindToValidationErrorsOf('sedeAdministrativa');
    this.bindToValidationErrorsOf('dependenciaGrupo');
    this.bindToValidationErrorsOf('tipoPersona');
  }

  onSelectTipoPersona(value) {
    // const value = event.value;


    if (value === PERSONA_JURIDICA && this.tipoComunicacion === COMUNICACION_EXTERNA) {
      this.visibility = {};
      this.visibility['nit'] = true;
      this.visibility['actuaCalidad'] = true;
      this.visibility['razonSocial'] = true;
      this.visibility['nombreApellidos'] = true;
      this.visibility['datosContacto'] = true;
      this.visibility['inactivo'] = true;
      this.visibility['tipoDocumento'] = true;
      this.tipoDocumentoSuggestions$.subscribe(docs => {
        this.subscriptionTipoDocumentoPersona = docs.filter(doc => doc.codigo === TPDOC_NRO_IDENTIFICACION_TRIBUTARIO);
        this.form.get('tipoDocumento').setValue(this.subscriptionTipoDocumentoPersona[0].codigo);
      }).unsubscribe();
      this.visibility['personaJuridica'] = true;
    } else if (value === PERSONA_NATURAL && this.tipoComunicacion === COMUNICACION_EXTERNA) {
      this.visibility = {};
      this.visibility['nombreApellidos'] = true;
      this.visibility['departamento'] = true;
      this.visibility['nroDocumentoIdentidad'] = true;
      this.visibility['datosContacto'] = true;
      this.visibility['tipoDocumento'] = true;
      this.visibility['nit'] = false;
      this.visibility['actuaCalidad'] = false;
      this.visibility['razonSocial'] = false;
      // this.visibility['inactivo'] = false;

      this.tipoDocumentoSuggestions$.subscribe(docs => {
        this.subscriptionTipoDocumentoPersona = docs.filter(doc => doc.codigo !== TPDOC_NRO_IDENTIFICACION_TRIBUTARIO);
        this.form.get('tipoDocumento').setValue(this.subscriptionTipoDocumentoPersona.filter(doc => doc.codigo === TPDOC_CEDULA_CIUDADANIA)[0].codigo);
      }).unsubscribe();

    } else if (value == PERSONA_ANONIMA) {
      this.visibility = {};
    }
    this.form.updateValueAndValidity();
  }

  setTipoComunicacion(value) {

    if (value) {
      this.visibility = {};
      this.tipoComunicacion = value.codigo;
      if (this.tipoComunicacion === COMUNICACION_INTERNA) {
        this.visibility['sedeAdministrativa'] = true;
        this.visibility['dependenciaGrupo'] = true;

        this.form.get('sedeAdministrativa').setValidators(Validators.required);
        this.form.get('dependenciaGrupo').setValidators(Validators.required);

        this.initLoadTipoComunicacionInterna();
      } else {
        this.visibility['tipoPersona'] = true;

        this.form.get('sedeAdministrativa').clearValidators();
        this.form.get('dependenciaGrupo').clearValidators();

        this.initLoadTipoComunicacionExterna();
      }
    }
  }

  listenForBlurEvents(control: string, eventType?: string) {
    const ac = this.form.get(control);

    delete this.validations[control];

    if (eventType == 'blur') {

      const controlTrims = ['nit', 'nroDocumentoIdentidad', 'nombreApellidos', 'razonSocial'];

      if (controlTrims.some(c => c == control)) {

        if (!isNullOrUndefined(ac.value))
          ac.setValue(ac.value.toString().trim())
      }
    }

    if (ac.touched && ac.invalid) {
      const error_keys = Object.keys(ac.errors);
      const last_error_key = error_keys[error_keys.length - 1];

      if (last_error_key == 'pattern') {
        switch (control) {

          case 'nit':
            this.validations[control] = 'Este campo solo permite números';
            break;
        }

      } else
        this.validations[control] = VALIDATION_MESSAGES[last_error_key];
    }


    if (!isNullOrUndefined(this[`blur_${control}`]) && eventType == 'blur')
      this[`blur_${control}`].call(this);
  }

  bindToValidationErrorsOf(control: string) {
    const ac = this.form.get(control);
    ac.valueChanges.subscribe(value => {
      if ((ac.touched || ac.dirty) && ac.errors) {
        const error_keys = Object.keys(ac.errors);
        const last_error_key = error_keys[error_keys.length - 1];
        this.validations[control] = VALIDATION_MESSAGES[last_error_key];
      } else {
        delete this.validations[control];
      }
    });
  }

  ngOnDestroy() {
    this.subscribers.forEach(subscriber => {
      subscriber.unsubscribe();
    });
  }

  buscarContactos() {

    this.datosContactos.contacts = [];
    this._changeDetector.detectChanges();

    const tipoPersona = this.form.get('tipoPersona').value;

    const nroIdentificacion = tipoPersona == PERSONA_NATURAL ? this.form.get('nroDocumentoIdentidad').value : this.form.get('nit').value;

    const tipoDocumentoIdentificacion = this.form.get("tipoDocumento").value;

    if (isNullOrUndefined(nroIdentificacion)) {

      const field = tipoPersona == PERSONA_NATURAL ? 'No. Documento identidad' : 'No Identificacion Tributario';

      this._store.dispatch(new PushNotificationAction({severity: 'error', summary: `Debe de seleccionar el ${field}`}));

      return;
    }

    if (isNullOrUndefined(tipoDocumentoIdentificacion) && tipoPersona == PERSONA_NATURAL) {
      this._store.dispatch(new PushNotificationAction({
        severity: 'info',
        summary: `Seleccione el tipo de documento`
      }));

      return;
    }
    const personFilter: PersonFilters = {
      personTypeCode: tipoPersona,
      idDocTypeCode: tipoDocumentoIdentificacion,
      idNumber: nroIdentificacion
    }
    this.subscribers.push(this._agenteApi.getPersonasByFilters(personFilter).map( value => {
      return value[0]
    }).subscribe(this.displayDataFromPersonDto));
  }

  buttonSearchContacts() {

    const tipoPersona = this.form.get('tipoPersona').value;

    if (isNullOrUndefined(tipoPersona))
      return false;

    return tipoPersona == PERSONA_NATURAL || tipoPersona == PERSONA_JURIDICA
  }

  buttonSearchContactsEnable(): boolean {
    this._datosContactoApi;

    if (isNullOrUndefined(this.form))
      return false;

    const tipoPersona = this.form.get('tipoPersona').value;

    if (isNullOrUndefined(tipoPersona))
      return false;

    let noIdentificacion;

    if (tipoPersona == PERSONA_NATURAL)
      noIdentificacion = this.form.get('nroDocumentoIdentidad').value

    if (tipoPersona == PERSONA_JURIDICA)
      noIdentificacion = this.form.get('nit').value;


    return !isNullOrUndefined(noIdentificacion) && noIdentificacion !== '';

  }

  resetForm() {
    this.form.reset();
    this.datosContactos.contacts = [];
  }

  filterCountrySingle(event) {
    let query = event.query;
    this.personas = this.service.filter(data => {
      return data.toLowerCase().includes(query.toLowerCase());

    });
  }

  onSelectPerson(personDTO: PersonDTO) {
    this.displayDataFromPersonDto(personDTO);
  }

  onFilterNombre(event) {
    const personFilters: PersonFilters = {
      name: event.query.trim()
    };
    this.filterPersons(personFilters);
  }
  onFilterRazonSocial(event) {
    const personFilters: PersonFilters = {
      businessName: event.query.trim()
    };
    this.filterPersons(personFilters);
  }
  filterPersons(initialFilters: PersonFilters){
    const personType = this.form.get('tipoPersona').value;
    if (personType) {
      initialFilters.personTypeCode = personType;
    }
    this.personsSuggestions$ = Utils.sortObservable(this._agenteApi.getPersonasByFilters(initialFilters), 'nombre');
  }

  displayDataFromPersonDto = (person: PersonDTO) => {
      if (!person) {
      this._store.dispatch(new PushNotificationAction({
        severity: 'info',
        summary: `No fueron  encontrados  los datos del contacto `
      }));
      this.datosContactos.contacts = [];
      this._changeDetector.detectChanges();
      return;
    }

    if (person.contactList.length > 0) {
      const contacts = person.contactList.map(c => {

        const contactAdpated: any = {};

        Object.keys(c).forEach(key => {
          switch (key) {
            case 'email':
              contactAdpated.correoEle = c[key];
              break;
            case 'municipalityCode':
              contactAdpated.municipio = {codigo: c[key]};
              break;
            case 'departmentCode':
              contactAdpated.departamento = {codigo: c[key]};
              break;
            case 'countryCode':
              contactAdpated.pais = {codigo: c[key]};
              break;
            case 'contactType':
              contactAdpated.tipoContacto = {codigo: c[key]};
              break;
            case 'landline' :
              contactAdpated.numeroTel = c[key];
              break;
            case 'cellphone' :
              contactAdpated.celular = c[key];
              break;
            default :
              contactAdpated[key] = c[key];
              break;
          }
        });

        return contactAdpated;

      });
      this.datosContactos.contacts = [...contacts];

      this.datosContactos.CompletarDatosContacto();

      this._changeDetector.detectChanges();
    }

    this.form.get('nombreApellidos').setValue(person);
    this.ideAgente = person.personId || null;

    if (Object.keys(person).length === 0 || isNullOrUndefined(person.personId)) {
      return;
    }
    const subsTipoPersona = this.tipoPersonaSuggestions$
      .pipe(map(types => types.find(type => type.codigo == person.personTypeCode)))
      .subscribe(type => this.form.get('tipoPersona').setValue(type.codigo));
    this.subscribers.push(subsTipoPersona);
    const subsTipoDocumento = this.tipoDocumentoSuggestions$
      .pipe(map(types => types.find(type => type.codigo == person.idDocTypeCode)))
      .subscribe(type => this.form.get('tipoDocumento').setValue(type.codigo));
    this.subscribers.push(subsTipoDocumento);
    if (person.personTypeCode == PERSONA_JURIDICA) {
      this.form.get('razonSocial').setValue(person);
      this.form.get('nit').setValue(person.nit);
      this.subscribers.push(this._store.select(getActuaCalidadArrayData)
        .pipe(map(ac => ac.find(a => a.codigo == person.qualityCode)))
        .subscribe(actCal => {

          if (!isNullOrUndefined(actCal)) {
            this.form.get('actuaCalidad').setValue(actCal.codigo);
          }
          this._changeDetector.detectChanges();
        })
      );
    } else {
      this.form.get('nroDocumentoIdentidad').setValue(person.idNumber);
      this.form.updateValueAndValidity();
      this._changeDetector.detectChanges();
    }
  }

  validatefieldsPersonaNatural( tipo: string, value: boolean){
    if(tipo == 'tipoPersona'){
      this.tipoPersona = value;
      this.requeridoPerN = false;
    } else if(tipo == 'nombreApellidos'){
      this.requeridoPerN = value;
      this.tipoPersona = false;
    }

  }

  validatefieldsPersonaJuridica( tipo: string, value: boolean){
    if(tipo == 'tipoPersona') {
      this.requeridoPerJ = false;
      this.tipoPersona = value;
    } else if(tipo == 'razonSocial'){
      this.requeridoPerJ = value;
      this.tipoPersona = false;
    }
  }

  obtenerSelected(){
    this.contactosSelected = this.datosContactos.direccionSeleccionada;
  }

  changeTipoPersona(ocultar: boolean){
    this.requeridoPerN = ocultar;
    this.requeridoPerJ = ocultar;
    this.datosContactos.contacts = [];
    this.datosContactos.direccionSeleccionada = null;
  }

  validateTable(value: boolean){
    this.requeridoSelected = value;
  }
}
