import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupGenerarIndiceIndividualComponent } from './popup-generar-indice-individual.component';

describe('PopupGenerarIndiceIndividualComponent', () => {
  let component: PopupGenerarIndiceIndividualComponent;
  let fixture: ComponentFixture<PopupGenerarIndiceIndividualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupGenerarIndiceIndividualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupGenerarIndiceIndividualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
