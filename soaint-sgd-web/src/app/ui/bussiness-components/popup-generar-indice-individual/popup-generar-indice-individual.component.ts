import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {faTachometerAlt} from "@fortawesome/free-solid-svg-icons";
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";
import {isNullOrUndefined} from "util";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../infrastructure/redux-store/redux-reducers";
import {IndiceElectronicoApi} from "../../../infrastructure/api/indice-electronico.api";


@Component({
  selector: 'app-popup-generar-indice-individual',
  templateUrl: './popup-generar-indice-individual.component.html',
  styleUrls: ['./popup-generar-indice-individual.component.scss']
})
export class PopupGenerarIndiceIndividualComponent implements OnInit {
  formGenerate: FormGroup;
  codSerie: boolean = false;
  faTachometerAlt = faTachometerAlt;
  ipm: any;


  constructor(private fe: FormBuilder,
              private _store: Store<RootState>,
              private _indiceElectronicoApi: IndiceElectronicoApi,
              private _changeDetectorRef: ChangeDetectorRef,) { }

  ngOnInit() {

    this.initFormGenerate();
  }

  getIPMValueIndividual(){
    this._indiceElectronicoApi.getIPM(1).subscribe(data => {
      this.ipm = data.value;
      this.refreshView()
    });
  }

  refreshView() {
    this._changeDetectorRef.detectChanges();
  }

  initFormGenerate(){
    this.formGenerate = this.fe.group({
      guid: [null, Validators.required],
    });
  }

  cleanForm() {
    this.formGenerate.reset();
  }

  generateIndex(){
    if (this.formGenerate.invalid) {
      this._store.dispatch(new PushNotificationAction({
        summary: 'Faltan datos obligatorios',
        severity: 'info'
      }))
    } else {
      const payload: any = {
        guidRecord: !isNullOrUndefined(this.formGenerate.get('guid').value) ? this.formGenerate.get('guid').value : "",
      };
      this._indiceElectronicoApi.generarIndiceDemanda(payload).subscribe(data => {
        this._store.dispatch(new PushNotificationAction({
          summary: 'Se ha enviado satisfactoriamente el proceso para su ejecución. Se procesarán ' + data + ' expedientes en total',
          severity: 'success'
        }))
      })
    }
  }


}
