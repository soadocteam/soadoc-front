import {Component, Input, ViewEncapsulation} from '@angular/core';
import {ConstanteDTO} from '../../../../domain/constanteDTO';
import {AgentDTO} from '../../../../domain/agentDTO';
import {ContactoDTO} from '../../../../domain/contactoDTO';
import {isNullOrUndefined} from "util";
import {ComunicacionOficialDTO} from "../../../../domain/comunicacionOficialDTO";
import {
  COMUNICACION_EXTERNA,
  COMUNICACION_INTERNA_ENVIADA
} from "../../../../shared/bussiness-properties/radicacion-properties";
import {AgentContactDTO} from "../../../../domain/AgentContactDTO";
import {AgentPersonDTO} from "../../../../domain/agentPersonDTO";
import {DependenciaDTO} from "../../../../domain/dependenciaDTO";
import {Utils} from "../../../../shared/helpers";
import {Sandbox as DependenciaSandbox} from "../../../../infrastructure/state-management/dependenciaGrupoDTO-state/dependenciaGrupoDTO-sandbox";
import {DependenciaApiService} from "../../../../infrastructure/api/dependencia.api";

@Component({
  selector: 'app-detalles-datos-remitente',
  templateUrl: './detalles-datos-remitente.component.html',
  styleUrls: ['./detalles-datos-remitente.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DetallesDatosRemitenteComponent {

  @Input()
  constantesList: ConstanteDTO[];

  @Input()
  municipiosList: any[] = [];

  @Input()
  remitente: AgentPersonDTO;

  @Input()
  contactos: AgentContactDTO[];

  @Input() paises: any[];

  @Input() comunicacion:ComunicacionOficialDTO;
  allDependencies: DependenciaDTO[] = [];
  dependencias: DependenciaDTO[] = [];

  constructor(protected _dependenciaSandbox: DependenciaSandbox,
              protected _dependenciaApi: DependenciaApiService) {
  }
  ngOnInit() {
    this._dependenciaSandbox.loadDependencies({}).subscribe((results) => {
      this.dependencias = results.dependencias;
    });
  }

  getPais(codigoPais): string {
    if(this.paises !== undefined){
      const pais = this.paises.find((p) => p.codigo === codigoPais);
      if (pais) {
        return pais.nombre;
      }
    }
    return '';
  }

  getDepartamento(codigoDepartamento): string {
    return !isNullOrUndefined(this.municipiosList) ? this.municipiosList.length > 0 ? (this.municipiosList.find((municipio) => municipio.departamento.codigo === codigoDepartamento).departamento.nombre) : '' : '';
  }

  getMunicipio(codigoMunicipio): string {

    if(isNullOrUndefined(this.municipiosList))
       return '';
    const municipio =  this.municipiosList.find((mun) => mun.codigo === codigoMunicipio);
    return  !isNullOrUndefined(municipio) ? municipio.nombre: '';
  }

  get isComunicacionEntrada(){

    return !isNullOrUndefined(this.comunicacion) && !isNullOrUndefined(this.comunicacion.correspondencia) && this.comunicacion.correspondencia.codTipoCmc == COMUNICACION_EXTERNA;
  }

  findDependency(code): string {
    const result = this.dependencias.find((element) => element.codigo == code);
    return result ? result.nombre : '';
  }


  findSede(code): string {
    const result = this.dependencias.find((element) => element.codSede == code);
    return result ? result.nomSede : '';
  }

  get isComunicacionSalidaInterna(){
    return !isNullOrUndefined(this.comunicacion) && !isNullOrUndefined(this.comunicacion.correspondencia) && this.comunicacion.correspondencia.codTipoCmc == COMUNICACION_INTERNA_ENVIADA;
  }

}
