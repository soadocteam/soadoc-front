import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { faDownload, faSyncAlt } from '@fortawesome/free-solid-svg-icons';
import {IndiceElectronicoApi} from "../../../infrastructure/api/indice-electronico.api";
import {Observable} from "rxjs/Observable";
import {isNullOrUndefined} from "util";
import {AngularCsv} from "angular-csv-ext";
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";
import {Subscription} from "rxjs";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../infrastructure/redux-store/redux-reducers";

@Component({
  selector: 'app-popup-detalle-ejecucion',
  templateUrl: './popup-detalle-ejecucion.component.html',
  styleUrls: ['./popup-detalle-ejecucion.component.scss']
})
export class PopupDetalleEjecucionComponent implements OnInit {

  faDownload = faDownload;
  faSyncAlt = faSyncAlt;
  details: any;
  listaDocuments$: Observable<any>;
  subscriptions: Subscription[] = [];
  @Output() opt: EventEmitter<any> = new EventEmitter<any>();
  documents: any;

  constructor(private _indiceElectronicoApi: IndiceElectronicoApi,
              private _store: Store<RootState>,) { }

  ngOnInit() {
  }

  dataDetails(exception) {
    this.details = exception;
    this.findIndiceData();
  }

  findIndiceData(){
    this.listaDocuments$ = this._indiceElectronicoApi.findIndiceData(this.details.idExpediente.slice(1,-1));
    this.subscriptions.push(this.listaDocuments$.subscribe((data) => {
      this.documents = data;
    }))
  }

  downloadCSV(){
    if (!isNullOrUndefined(this.documents) && this.documents.length>0) {
      var options = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true,
        showTitle: true,
        title: 'Consola de Seguimiento ejecucción índice electrónico',
        useBom: true,
        noDownload: false,
        headers: ["Documento", "Código tipo documental", "Nombre del documento", "Evento", "Fecha de Evento", "Usuario", "Estado", "Descripción error generado"],
        useHeader: false,
        nullToEmptyString: true,
      };
        // This data will be generated in the CSV file
        new AngularCsv(this.documents, "Detalle-Ejecucion", options);
    } else {
      this._store.dispatch(new PushNotificationAction({
        summary: 'No hay datos para generar el CSV',
        severity: 'info'
      }))
    }
  }

  hideExecutionDetailsPopup(){
    this.opt.emit();
  }


}
