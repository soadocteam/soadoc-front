import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupDetalleEjecucionComponent } from './popup-detalle-ejecucion.component';

describe('PopupDetalleEjecucionComponent', () => {
  let component: PopupDetalleEjecucionComponent;
  let fixture: ComponentFixture<PopupDetalleEjecucionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupDetalleEjecucionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupDetalleEjecucionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
