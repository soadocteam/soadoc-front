import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupGenerarIndiceMasivoComponent } from './popup-generar-indice-masivo.component';

describe('PopupGenerarIndiceMasivoComponent', () => {
  let component: PopupGenerarIndiceMasivoComponent;
  let fixture: ComponentFixture<PopupGenerarIndiceMasivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupGenerarIndiceMasivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupGenerarIndiceMasivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
