import {AfterViewInit, ChangeDetectorRef, Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {isNullOrUndefined} from "util";
import {LocaleSettings} from "primeng/calendar";
import {localeCalendarEs} from "../../../shared/localeCalendarEs";
import {ICON_CALENDAR} from "../../../shared/constants";
import {faTachometerAlt} from "@fortawesome/free-solid-svg-icons";
import {PushNotificationAction} from "../../../infrastructure/state-management/notifications-state/notifications-actions";
import {Store} from "@ngrx/store";
import {State as RootState} from "../../../infrastructure/redux-store/redux-reducers";
import {IndiceElectronicoApi} from "../../../infrastructure/api/indice-electronico.api";
import {DateFormatPipe} from "../../../shared/pipes/date.pipe";

@Component({
  selector: 'app-popup-generar-indice-masivo',
  templateUrl: './popup-generar-indice-masivo.component.html',
  styleUrls: ['./popup-generar-indice-masivo.component.scss'],
  providers: [DateFormatPipe]
})
export class PopupGenerarIndiceMasivoComponent implements OnInit {

  formGenerate: FormGroup;
  codSerie: boolean = false;
  es: LocaleSettings;
  iconCalendar = ICON_CALENDAR;
  faTachometerAlt = faTachometerAlt;
  end_date: Date = new Date();
  start_date: Date = new Date();
  date = new Date();
  isStartDate: boolean = false;
  isEndDate: boolean = false;
  ipm: any;
  ipmValue: any;

  constructor(private fe: FormBuilder,
              private _store: Store<RootState>,
              private _indiceElectronicoApi: IndiceElectronicoApi,
              private _dateFormat: DateFormatPipe,
              private _changeDetectorRef: ChangeDetectorRef,) { }

  ngOnInit() {

    this.initFormGenerate();
    this.es = localeCalendarEs;
    this.start_date = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
  }

  getIPMValueMassive(){
    this._indiceElectronicoApi.getIPM(1).subscribe(data => {
      this.ipm = data.value;
      this.refreshView();
    });
  }

  refreshView() {
    this._changeDetectorRef.detectChanges();
  }

  initFormGenerate(){
    this.formGenerate = this.fe.group({
      codDependencia: [null],
      codSerie: [null, Validators.required],
      codSubSerie: [null],
      startDate: [null, Validators.required],
      endDate: [null, Validators.required],
    });
  }

  validateFields() {
    if (isNullOrUndefined(this.codSerie)) {
      this.codSerie = true;
    }
  }

  cleanForm() {
    this.formGenerate.reset();
    this.end_date = new Date();
    this.start_date = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    this.isStartDate = false;
    this.isEndDate = false;
  }

  generateIndex(){
    if (this.formGenerate.invalid) {
      this.validateFields();
      this._store.dispatch(new PushNotificationAction({
        summary: 'Faltan datos obligatorios',
        severity: 'info'
      }))
    } else {
      const payload: any = {
        codSerie: !isNullOrUndefined(this.formGenerate.get('codSerie').value) ? this.formGenerate.get('codSerie').value : "",
        codSubSerie: !isNullOrUndefined(this.formGenerate.get('codSubSerie').value) ? this.formGenerate.get('codSubSerie').value : "",
        codDependencia: !isNullOrUndefined(this.formGenerate.get('codDependencia').value) ? this.formGenerate.get('codDependencia').value : "",
        startDate: this.convertDate(this.start_date),
        endDate: this.convertDate(this.end_date),
      };
      this._indiceElectronicoApi.generarIndiceDemanda(payload).subscribe(data => {
        this._store.dispatch(new PushNotificationAction({
          summary: 'Se ha enviado satisfactoriamente el proceso para su ejecución. Se procesarán ' + data + ' expedientes en total',
          severity: 'success'
        }))
      })
    }
  }

  convertDate(inputFormat, dateFin?) {
    function pad(s) {
      return (s < 10) ? '0' + s : s;
    }

    const d = new Date(inputFormat);
    if (dateFin) {
      d.setDate(d.getDate() + 1);
    }
    return [pad(d.getFullYear()), pad(d.getMonth() + 1), d.getDate()].join('/');
  }

}
