import {
  ChangeDetectorRef,
  Component, EventEmitter, Input,
  OnDestroy,
  OnInit, Output,
  ViewEncapsulation
} from '@angular/core';
import {State as RootState} from '../../../infrastructure/redux-store/redux-reducers';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs/Subscription';
import {getActiveTask, getNextTask} from '../../../infrastructure/state-management/tareasDTO-state/tareasDTO-selectors';
import {TareaDTO} from '../../../domain/tareaDTO';
import {
  ContinueWithNextTaskAction,
  ResetTaskAction, SetNameNextTaskAction
} from '../../../infrastructure/state-management/tareasDTO-state/tareasDTO-actions';
import {ROUTES_PATH} from '../../../app.route-names';
import {isNullOrUndefined} from "util";
import {Sandbox as TaskSandBox} from '../../../infrastructure/state-management/tareasDTO-state/tareasDTO-sandbox';
import {Router} from '@angular/router';
import {combineLatest} from "rxjs/index";
import {distinctUntilChanged} from 'rxjs/operators';
import {MenuItem} from "primeng/api";
import {LoadingService} from "../../../infrastructure/utils/loading.service";


@Component({
  selector: 'app-task-container',
  templateUrl: './task-container.component.html',
  styleUrls: ['./task-container.component.css'],
  encapsulation: ViewEncapsulation.None,
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskContainerComponent implements OnInit, OnDestroy {

  task: TareaDTO = null;
  @Input() processName = '';
  @Input() taskName = "";
  @Input() isActive = true;
  @Input() forceHiddenButtons = false;
  @Input() hideDefaultButton = false;
  @Input() nameNextTask: string;
  @Input() hidden: boolean;
  @Input() tituloName: string;
  ocultar: boolean;
  hasToContinue: boolean = false;

  activeTaskUnsubscriber: Subscription;
  infoUnsubscriber: Subscription;

  @Output() onFinalizar: EventEmitter<any> = new EventEmitter;
  @Output() onContinueTask: EventEmitter<any> = new EventEmitter;

  items: MenuItem[];
  activeIndex = 0;

  constructor(private _store: Store<RootState>, private loadingService: LoadingService,
              private _changeDetector: ChangeDetectorRef,
              private _taskSandBox: TaskSandBox,
              private _router: Router) {

  }


  ngOnInit() {

    this.items = [
      {
        label: 'Básico',
        command: () => {

        }
      },
      {
        label: 'Remitente',
        command: () => {

        }
      },
      {label: 'Destinatario'},
      {label: 'Anexos'},
    ];

    this.ocultar = this.hidden;

    this.infoUnsubscriber = this._store.select(getActiveTask).subscribe(tarea => {
      this.task = tarea;
      if (!isNullOrUndefined(this.task)) {
        this.taskName = this.task.descripcion;
        this.processName = this.task.nombreProceso;
      }
    });
    /* if(!this.processName && !this.taskName)
     this.infoUnsubscriber = Observable.combineLatest(
       this._store.select(getActiveTask),
       this._store.select(getProcessEntities)
     ).take(1).subscribe(([activeTask, procesos]) => {
       if (activeTask) {
         this.task = activeTask;
         this.taskName = this.task.nombre;
         this.processName = (process_info[procesos[activeTask.idProceso].codigoProceso]) ? process_info[procesos[activeTask.idProceso].codigoProceso].displayValue : '';
         this._changeDetector.detectChanges();
       }
     });*/

    this.activeTaskUnsubscriber = combineLatest(
      this._store.select(getActiveTask),
      this._store.select(getNextTask)
    ).pipe(distinctUntilChanged())
      .subscribe(([activeTask, hasNextTask]) => {

        this.isActive = activeTask !== null;
        this.hasToContinue = hasNextTask !== null;
        this._changeDetector.detectChanges();

      });
  }


  navigateBack() {

    this.onFinalizar.emit();

    this._router.navigate(['bussiness/' + ROUTES_PATH.workspace]);
  }


  navigateToNextTask() {
    this.loadingService.presentLoading();
    setTimeout(() => {
      this.onContinueTask.emit();
      this._store.dispatch(new SetNameNextTaskAction(this.nameNextTask));
      this._store.dispatch(new ContinueWithNextTaskAction());
    }, 1000);
  }

  ngOnDestroy() {
    if (!isNullOrUndefined(this.infoUnsubscriber))
      this.infoUnsubscriber.unsubscribe();
    this.activeTaskUnsubscriber.unsubscribe();
    this._store.dispatch(new ResetTaskAction());

  }
}
