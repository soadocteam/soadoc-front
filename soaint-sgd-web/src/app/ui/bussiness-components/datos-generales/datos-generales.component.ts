import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import {ConstanteDTO} from '../../../domain/constanteDTO';
import {Store} from '@ngrx/store';
import {State} from '../../../infrastructure/redux-store/redux-reducers';

import {
  getMediosRecepcionArrayData,
  getSoporteAnexoArrayData,
  getTipoAnexosArrayData,
  getTipoComunicacionArrayData,
  getTipologiaDocumentalArrayData,
  getUnidadTiempoArrayData
} from '../../../infrastructure/state-management/constanteDTO-state/constanteDTO-selectors';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import 'rxjs/add/operator/single';
import {VALIDATION_MESSAGES} from '../../../shared/validation-messages';
import {DatosGeneralesApiService} from '../../../infrastructure/api/datos-generales.api';
import {LoadAction as SedeAdministrativaLoadAction} from '../../../infrastructure/state-management/sedeAdministrativaDTO-state/sedeAdministrativaDTO-actions';
import {
  COMUNICACION_EXTERNA,
  COMUNICACION_EXTERNA_ENVIADA,
  CORREGIR_RADICACION,
  MEDIO_RECEPCION_CORREO_CERTIFICADO,
  RADICACION_DOC_PRODUCIDO,
  RADICACION_ENTRADA,
  RADICACION_INTERNA,
  RADICACION_SALIDA,
  SOPORTE_HIBRIDO,
  TIPOLOGIA_DERECHO_PETICION
} from '../../../shared/bussiness-properties/radicacion-properties';
import {ViewFilterHook} from '../../../shared/ViewHooksHelper';
import {isNullOrUndefined} from 'util';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {SelectItem} from "primeng/api";
import {COURIER} from "../../../shared/constants";
import {ConstanteApiService} from "../../../infrastructure/api/constantes.api";
import {ValidationErrors} from "@angular/forms/src/directives/validators";
import {Utils} from "../../../shared/helpers";
import {MasterConfigurationApi} from "../../../infrastructure/api/master-configuration.api";
import {getListRolesFun} from "../../../infrastructure/state-management/funcionarioDTO-state/funcionarioDTO-selectors";
import {DIGITALIZADOR} from "../../../app.roles";

@Component({
  selector: 'app-datos-generales',
  templateUrl: './datos-generales.component.html',
  styles: [`
    .ui-datalist-header, .ui-datatable-header {
      text-align: left !important;
    }

    body .ui-buttonset .ui-button:not(.ui-state-disabled):not(.ui-state-active):hover {
      background-color: #005C90 !important;
    }

    .ui-fluid .ui-buttonset.ui-buttonset-2 .ui-button {
      border-radius: 3px;
      background: #81868b;
      padding-left: 4px;
      padding-right: 4px;
    }

    .ui-fluid .ui-buttonset, .ui-fluid .ui-buttonset.ui-buttonset-1 .ui-button {
      width: 113%;
    }
  `],
  encapsulation: ViewEncapsulation.None
})
export class DatosGeneralesComponent implements OnInit, OnDestroy {

  @Input() isVisibleCheckAttachDocument = false;
  optionsCheckAttachDocument: { label: any, value: any }[];
  isDigitizer = false;
  disabledAttachDocument = true;
  originalOptionCheckAttachDocument: string;
  form: FormGroup;
  visibility: any = {};
  derechoPeticiobtn: any;
  visibleUnidadTiempoRespuesta = false;
  radicadosReferidos: Array<{ nombre: string, blocked?: boolean }> = [];
  descripcionAnexos: Array<{ tipoAnexo: ConstanteDTO, descripcion: string, soporteAnexo: ConstanteDTO }> = [];
  tipoComunicacionSuggestions$: Observable<ConstanteDTO[]>;
  unidadTiempoSuggestions$: Observable<ConstanteDTO[]>;
  tipoAnexosSuggestions$: Observable<ConstanteDTO[]>;
  soporteAnexosSuggestions$: Observable<ConstanteDTO[]>;
  medioRecepcionSuggestions$: Observable<ConstanteDTO[]>;
  tipologiaDocumentalSuggestions$: Observable<ConstanteDTO[]>;
  detallarDescripcion = false;
  @Input() dataDefault: Observable<any> = Observable.empty();
  @Input() documentoProducido = false;
  @Input() fieldsDisabled: any = {};
  @Input() medioDocumentoProducido = true;
  @Input() editable: any = true;
  @Input() tipoRadicacion = RADICACION_ENTRADA;
  @Input() editmode = false;
  @Input() mediosRecepcionInput: ConstanteDTO = null;
  @Input() tipoComunicacion: (tipo: ConstanteDTO) => boolean = null;
  @Input() tipologiaDocFilter: (tipo: ConstanteDTO) => boolean = null;
  @Output() onChangeTipoComunicacion: EventEmitter<any> = new EventEmitter();
  @Output() onChangeTipoDistribucion: EventEmitter<boolean> = new EventEmitter;
  subcriptions: Subscription[] = [];
  validations: any = {};
  nFolios: number;
  tipoDocumentoSelected: any;
  opcionesCurrier: SelectItem[] = [];
  requeridocheckSalida: boolean = false;
  requeridocheckSalidaDistribuciones: boolean = false;
  requeridocheck: boolean = false;
  requeridotipologiaDocumental: boolean = false;
  requeridonumeroFolio: boolean = false;
  requeridoasunto: boolean = false;

  constructor(private _store: Store<State>, private _apiDatosGenerales: DatosGeneralesApiService, private formBuilder: FormBuilder, private changeDetector: ChangeDetectorRef
    , private _listaApi: ConstanteApiService, private _masterCofiguration: MasterConfigurationApi) {
    this.optionsCheckAttachDocument = [
      {label: 'Ahora', value: 'ahora'},
      {label: 'Despues', value: 'despues'}
    ]
  }

  get isDerechoPeticio() {
    this.form.get('tipologiaDocumental').valueChanges.subscribe(value => {
      this.derechoPeticiobtn = value;
    })
    return this.derechoPeticiobtn == 'TL-DOCDP';
  }

  get reqDistFisica() {
    return this.form.get('reqDistFisica').value;
  }

  initForm() {

    const asuntoValidators = [Validators.maxLength(500)];

    if (this.isRadicacionSalida()) {
      asuntoValidators.push(Validators.required);
    }

    let tipoComunicacionDefault = null;

    if (this.isRadicacionEntrada()) {
      tipoComunicacionDefault = COMUNICACION_EXTERNA;
    }

    if (this.tipoRadicacion == RADICACION_SALIDA) {
      tipoComunicacionDefault = COMUNICACION_EXTERNA_ENVIADA;
    }

    const fields: any = {
      fechaRadicacion: [null],
      nroRadicado: [null],
      tipoComunicacion: [{value: null, disabled: !this.isEditable('tipoComunicacion')}, Validators.required],
      medioRecepcion: [{
        value: null,
        disabled: !this.isEditable('medioRecepcion')
      }, this.isRadicacionEntrada() ? Validators.required : null],
      empresaMensajeria: [{value: null, disabled: true}],
      numeroGuia: [{
        value: null,
        disabled: true
      }, Validators.compose([Validators.required, Validators.maxLength(20)])],
      tipologiaDocumental: [{value: null}, Validators.required],
      unidadTiempo: [null],
      numeroFolio: [{
        value: null,
        disabled: !this.isEditable('numeroFolio')
      }, Validators.compose([Validators.pattern(/^[0-9]+$/), this.tipoRadicacion != RADICACION_DOC_PRODUCIDO ? Validators.required : null])],
      inicioConteo: [null],
      reqDistFisica: [{
        value: true,
        disabled: !this.isEditable('reqDistFisica')
      }, this.customValidatorCheckbox('reqDistElect', true)],
      reqDistElect: [{
        value: null,
        disabled: !this.isEditable('reqDistElect')
      }, this.customValidatorCheckbox('reqDistFisica', true)],
      reqDigit: [{
        value: null,
        disabled: !this.isEditable('reqDigit')
      }, this.customValidatorCheckbox('adjuntarDocumento')],
      adjuntarDocumento: [{
        value: null,
        disabled: !this.isEditable('adjuntarDocumento')
      }, this.customValidatorCheckbox('reqDigit')],
      asunto: [{value: null, disabled: !this.isEditable('asunto')}, Validators.compose(asuntoValidators)],
      radicadoReferido: [{value: null, disabled: !this.isEditable('radicadoReferido')}],
      detallarDescripcion: [{value: null, disabled: !this.isEditable('detallarDescripcion')}],
      tipoAnexos: [{value: null, disabled: !this.isEditable('tipoAnexos')}],
      soporteAnexos: [{value: null, disabled: !this.isEditable('soporteAnexos')}],
      tipoAnexosDescripcion: [{
        value: null,
        disabled: !this.isEditable('tipoAnexosDescripcion')
      }, Validators.maxLength(300)],
      hasAnexos: [null],
      checkAttachDocument: [null]
    };


    if (this.isRadicacionEntrada()) {
      fields.tiempoRespuesta = [{value: null, disabled: !this.editable}];
    }

    this.form = this.formBuilder.group(fields);

    if (this.isRadicacionSalida()) {
      this.form.setValidators((fr: FormGroup) => {

        if (isNullOrUndefined(fr.get('reqDistFisica').value) && isNullOrUndefined(fr.get('reqDistElect').value)) {
          return {
            reqDistFisicaOrReqDistElect: {
              valid: false
            }
          };
        }
        return null;
      });
    }

    if (this.isRadicacionEntrada()) {

      this.form.get('tipologiaDocumental').valueChanges.subscribe(value => {

        this.form.get('asunto').clearValidators();

        if (value == TIPOLOGIA_DERECHO_PETICION) {
          this.form.get('asunto').setValidators([Validators.required, Validators.maxLength(500)]);
        } else {
          this.form.get('asunto').setValidators(Validators.maxLength(500));
        }

        this.listenForBlurEvents('asunto');
        this.form.get('asunto').updateValueAndValidity();
        this.onSelectTipologiaDocumental(value);
      });
    }

    if (!isNullOrUndefined(tipoComunicacionDefault)) {
      this.form.get('tipoComunicacion').setValue(tipoComunicacionDefault);
    }

    this.form.get('tipoComunicacion').valueChanges.subscribe((value) => {
      this.onSelectTipoComunicacion(value);
    });

    this.form.get('medioRecepcion').valueChanges.subscribe((value) => {
      if (value === MEDIO_RECEPCION_CORREO_CERTIFICADO) {
        this.visibility.empresaMensajeria = true;
        this.visibility.numeroGuia = true;
      } else if (this.visibility.empresaMensajeria && this.visibility.numeroGuia) {
        delete this.visibility.empresaMensajeria;
        delete this.visibility.numeroGuia;
      }
    });

    if (this.isRadicarDocProducido()) {

      this.form.get('reqDistFisica').valueChanges.subscribe(value => {

        const control = this.form.get('numeroFolio');

        control.clearValidators();

        if (value) {
          control.setValidators(Validators.required);
        }

      });
    }
    const digitalizacionControl = this.form.get('reqDigit');
    const adjuntarDocumentoControl = this.form.get('adjuntarDocumento');
  };

  changeTipoDistribucion(evt: boolean) {

    this.onChangeTipoDistribucion.emit(evt);
  }

  customValidatorCheckbox(otherFormControl: string, doingFormControl?: boolean): ValidatorFn {
    if (doingFormControl && (this.tipoRadicacion == RADICACION_ENTRADA || this.tipoRadicacion == CORREGIR_RADICACION)) {
      return null;
    }
    return (control: AbstractControl): ValidationErrors | null => {
      if (!control.parent) return null;
      const parentValue = control.parent.get(otherFormControl).value;
      if (parentValue || control.value) {
        control.parent.get(otherFormControl).setErrors(null);
        return null;
      }
      return {'error': {value: control.value}}
    };
  }

  listenForErrors() {
    this.bindToValidationErrorsOf('tipoComunicacion');
    this.bindToValidationErrorsOf('tipologiaDocumental');
    this.bindToValidationErrorsOf('numeroFolio');
    this.bindToValidationErrorsOf('asunto');
    this.bindToValidationErrorsOf('tipoAnexosDescripcion');
    this.bindToValidationErrorsOf('empresaMensajeria');
    this.bindToValidationErrorsOf('numeroGuia');
    this.bindToValidationErrorsOf('medioRecepcion');
  }

  ngOnInit(): void {

    this._listaApi.getListaConstantes().subscribe(list => {
      list = Utils.sort(list, 'label');
      list.map(res => {
        if (res.codPadre === COURIER) {
          this.opcionesCurrier.push({label: res.nombre, value: res.codigo});
        }
      })
    });

    this.tipoComunicacionSuggestions$ = Utils.sortObservable(this._store.select(getTipoComunicacionArrayData)
      .pipe(map(tipo_comunicaciones => isNullOrUndefined(this.tipoComunicacion) ? tipo_comunicaciones : tipo_comunicaciones.filter(this.tipoComunicacion))), 'nombre');

    this.unidadTiempoSuggestions$ = this._store.select(getUnidadTiempoArrayData);
    this.tipoAnexosSuggestions$ = Utils.sortObservable(this._store.select(getTipoAnexosArrayData), 'nombre');
    this.medioRecepcionSuggestions$ = Utils.sortObservable(this._store.select(getMediosRecepcionArrayData), 'nombre');
    this.tipologiaDocumentalSuggestions$ = Utils.sortObservable(this._store.select(getTipologiaDocumentalArrayData)
      .pipe(map(tipologias => isNullOrUndefined(this.tipologiaDocFilter) ? tipologias : tipologias.filter(this.tipologiaDocFilter))), 'nombre');


    this._store.dispatch(new SedeAdministrativaLoadAction());

    this.initForm();
    if (this.isRadicacionEntrada()) {
      this.soporteAnexosSuggestions$ = Utils.sortObservable(this._store.select(getSoporteAnexoArrayData)
        .pipe(map(soporte => soporte.filter(res => res.codigo !== SOPORTE_HIBRIDO))), 'nombre');
    } else {
      this.soporteAnexosSuggestions$ = Utils.sortObservable(this._store.select(getSoporteAnexoArrayData), 'nombre');
    }

    this.dataDefault.subscribe(datosGenerales => {  // console.log('datosGenerales',datosGenerales);

      this.form.get('tipoComunicacion').setValue(datosGenerales.tipoComunicacion);
      if (datosGenerales.tipologiaDocumental) {
        this.form.get('tipologiaDocumental').setValue(datosGenerales.tipologiaDocumental);
      }

      if (this.isRadicacionEntrada()) {
        this.form.get('unidadTiempo').setValue(datosGenerales.unidadTiempo);
      }
      this.form.get('tiempoRespuesta').setValue(datosGenerales.tiempoRespuesta);
      this.form.get('medioRecepcion').setValue(datosGenerales.medioRecepcion);
      this.form.get('empresaMensajeria').setValue(datosGenerales.empresaMensajeria);
      this.form.get('numeroGuia').setValue(datosGenerales.numeroGuia);
      this.form.get('inicioConteo').setValue(datosGenerales.inicioConteo);
      this.form.get('asunto').setValue(datosGenerales.asunto);
      this.form.get('reqDigit').setValue(datosGenerales.reqDigit);
      this.form.get('reqDistFisica').setValue(datosGenerales.reqDistFisica);
      this.form.get('reqDistElect').setValue(datosGenerales.reqDistElect);

      this._store.select(getSoporteAnexoArrayData).subscribe(soportes => {

        this.descripcionAnexos = datosGenerales.listaAnexos.map((anexo: any) => {

          return {
            tipoAnexo: anexo.tipo,
            soporteAnexo: soportes.find(soporte => soporte.codigo == anexo.soporte),
            descripcion: anexo.descripcion
          };
        });
      });


      /*this.descripcionAnexos = datosGenerales.listaAnexos.map( anexo => {
        return { tipoAnexo : anexo.tipo, soporteAnexo : { nombre : anexo.soporte},descripcion: anexo.descripcion}
      });*/
      if (isNullOrUndefined(datosGenerales.radicadosReferidos)) {
        return
      } else {
        this.radicadosReferidos = datosGenerales.radicadosReferidos.map((data: any) => {
          return Object.assign(data, {blocked: true});
        });
      }

    });

    this.listenForErrors();
    // this.validIsDigitizer();
    this._masterCofiguration.findOptinalId(13).subscribe(response => {
      this.originalOptionCheckAttachDocument = response.value;
      // this.setValueCheckAttachDocument(response.value)
    });

    this.form.get('reqDigit').valueChanges.subscribe(value => {
      let option;
      if (value && this.form.get('adjuntarDocumento').value) {
        option = 'despues';
        this.disabledAttachDocument = true;
      } else if (this.form.get('adjuntarDocumento').value) {
        option = this.originalOptionCheckAttachDocument;
        this.validIsDigitizer();
      } else {
        option = null;
        this.disabledAttachDocument = true;
      }
      this.setValueCheckAttachDocument(option);
    });

    this.form.get('adjuntarDocumento').valueChanges.subscribe(value => {
      let option;
      if (value && this.form.get('reqDigit').value) {
        option = 'despues';
        this.disabledAttachDocument = true;
      } else if (value) {
        option = this.originalOptionCheckAttachDocument;
        this.validIsDigitizer();
      } else {
        option = null;
        this.disabledAttachDocument = true;
      }
      this.setValueCheckAttachDocument(option);
    });
  }

  setValueCheckAttachDocument(value) {
    this.form.get('checkAttachDocument').setValue(this.optionsCheckAttachDocument.find(opt => opt.value == value));
    this.changeDetector.detectChanges();
  }

  validIsDigitizer() {
    this._store.select(getListRolesFun).subscribe(list => {
      this.isDigitizer = (list.find(rol => rol.rol == DIGITALIZADOR && rol.applicationId == "1")) ? true : false;
    });

    this.disabledAttachDocument = (this.isDigitizer) ? false : true;
  }

  deleteSoporteHibrido(value) {
    if (value) {
      this.soporteAnexosSuggestions$ = Utils.sortObservable(this._store.select(getSoporteAnexoArrayData)
        .pipe(map(soporte => soporte.filter(res => res.codigo !== SOPORTE_HIBRIDO))), 'nombre');
    } else {
      this.soporteAnexosSuggestions$ = Utils.sortObservable(this._store.select(getSoporteAnexoArrayData), 'nombre');
    }
  }

  addRadicadosReferidos() {
    const insertVal = [{nombre: this.form.get('radicadoReferido').value}];
    this.radicadosReferidos = [...this.radicadosReferidos.filter(value => value.nombre !== this.form.get('radicadoReferido').value), ...insertVal];
    this.form.get('radicadoReferido').reset();
  }

  addTipoAnexosDescripcion() {
    const tipoAnexo = this.form.get('tipoAnexos').value;
    const soporteAnexo = this.form.get('soporteAnexos').value;
    const descripcion = this.form.get('tipoAnexosDescripcion').value;

    if (!tipoAnexo) {
      return;
    }
    const newAnexo = [{tipoAnexo, descripcion, soporteAnexo}];
    this.descripcionAnexos = [
      ...newAnexo,
      ...this.descripcionAnexos
    ];
    this.form.get('hasAnexos').setValue(this.descripcionAnexos.length);
    this.form.get('tipoAnexos').reset();
    this.form.get('soporteAnexos').reset();
    this.form.get('tipoAnexosDescripcion').reset();
  }

  deleteRadicadoReferido(index) {
    const removeVal = [...this.radicadosReferidos];
    removeVal.splice(index, 1);
    this.radicadosReferidos = removeVal;
  }

  deleteTipoAnexoDescripcion(index) {
    const removeVal = [...this.descripcionAnexos];
    removeVal.splice(index, 1);
    if (removeVal.length === 0) {
      this.form.get('hasAnexos').reset();
    }
    this.descripcionAnexos = removeVal;
  }

  onSelectTipologiaDocumental(codigoTipologia) {

    if (this.isRadicacionEntrada()) {
      // this.metricasTiempoTipologia$ = this._apiDatosGenerales.loadMetricasTiempo(codigoTipologia);
      // this.metricasTiempoTipologia$.subscribe(metricas => {
      //
      //   if (isNullOrUndefined(metricas)) {
      //     return;
      //   }
      //
      //   this.form.get('inicioConteo').setValue(metricas.inicioConteo);
      //   this.form.get('tiempoRespuesta').setValue(metricas.tiempoRespuesta);
      //   this.form.get('unidadTiempo').setValue(metricas.codUnidaTiempo);
      //   this.changeDetector.detectChanges();
      //
      // });
      this.changeDetector.detectChanges();
    }
  }

  onSelectTipoComunicacion(value) {
    this.onChangeTipoComunicacion.emit(value);
  }

  // resetCarFilter() {
  //  console.log(this.dropDownThing);
  //  this.dropDownThing.selectedOption = null;
  // }

  listenForBlurEvents(control: string) {
    const ac = this.form.get(control);

    if ((control == 'asunto' || control == 'tipoAnexosDescripcion' || 'radicadoReferido') && !isNullOrUndefined(ac) && !isNullOrUndefined(ac.value)) {
      ac.setValue(ac.value.toString().trim());
    }

    if (ac.touched && ac.invalid) {
      const error_keys = Object.keys(ac.errors);

      if (control != 'radicadoReferido') {
        const last_error_key = error_keys[error_keys.length - 1];
        this.validations[control] = VALIDATION_MESSAGES[last_error_key];
      }

    }
  }

  bindToValidationErrorsOf(control: string) {

    const ac = this.form.get(control);
    ac.valueChanges.subscribe(value => {
      if ((ac.touched || ac.dirty) && ac.errors) {
        const error_keys = Object.keys(ac.errors);
        const last_error_key = error_keys[error_keys.length - 1];
        this.validations[control] = VALIDATION_MESSAGES[last_error_key];
      } else {
        delete this.validations[control];
      }
    });
  }

  showBlockDistribucionDig() {

    return ViewFilterHook.applyFilter('app-datos-direccion-show-block-dist-dig', true);
  }

  isRadicacionEntrada() {
    return this.tipoRadicacion == RADICACION_ENTRADA || this.tipoRadicacion == CORREGIR_RADICACION;
  }

  isRadicacionSalida() {

    return this.tipoRadicacion == RADICACION_SALIDA || this.tipoRadicacion == RADICACION_DOC_PRODUCIDO;
  }

  isRadicacionInterna() {
    return this.tipoRadicacion == RADICACION_INTERNA;
  }

  isRadicarDocProducido() {
    return this.tipoRadicacion == RADICACION_DOC_PRODUCIDO;
  }

  ngOnDestroy(): void {

    this.subcriptions.forEach(s => s.unsubscribe());
  }

  onChangeTipologiaDocumental(data: any) {
    if (this.isRadicacionEntrada()) {
      var payload: any;
      this.tipologiaDocumentalSuggestions$.subscribe(res => res.map(type => {
        if (type.codigo === data.value) {
          return payload = {
            documentType: type.nombre
          };
        }
      }));

      this._apiDatosGenerales.getUnidadTipologiaDocumental(payload).subscribe((response: any) => {
        if (!isNullOrUndefined(response)) {
          if (response.enable == "false") {
            this.visibleUnidadTiempoRespuesta = false;
          } else {
            this.visibleUnidadTiempoRespuesta = true;
          }
          this.form.get('tiempoRespuesta').setValue(response.quantity);
          this.unidadTiempoSuggestions$.subscribe(unidad => {
            unidad.map(res => {
              if (res.nombre === response.unitTime) {
                this.form.get('unidadTiempo').setValue(res.codigo);
              }
            })
          });
        } else {
          this.visibleUnidadTiempoRespuesta = false;
          this.form.get('tiempoRespuesta').setValue(null);
          this.form.get('unidadTiempo').setValue(null);
        }
      });
    }
  }

  private isEditable(field) {
    return !this.fieldsDisabled[field] && this.editable;
  }

  validateCheckEntrdada(value: boolean) {
    this.requeridocheck = value;
  }

  validarChecckSalida(tipo: string, value: boolean) {
    if (tipo == 'distribucion') {
      this.requeridocheckSalidaDistribuciones = value;
      this.requeridocheckSalida = false;
    } else {
      this.requeridocheckSalidaDistribuciones = false;
      this.requeridocheckSalida = value;
    }

  }

  validatefields(tipo: string, value: boolean) {
    if (tipo == 'tipologiaDocumental') {
      this.requeridotipologiaDocumental = value;
      this.requeridonumeroFolio = false;
      this.requeridoasunto = false;
    } else if (tipo == 'numeroFolio') {
      this.requeridotipologiaDocumental = false;
      this.requeridonumeroFolio = value;
      this.requeridoasunto = false;
    } else if (tipo == 'asunto') {
      this.requeridotipologiaDocumental = false;
      this.requeridonumeroFolio = false;
      this.requeridoasunto = value;
    } else {
      this.requeridotipologiaDocumental = false;
      this.requeridonumeroFolio = false;
      this.requeridoasunto = false;
    }
  }

}
