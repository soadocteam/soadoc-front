import { Component, OnInit, ViewEncapsulation} from '@angular/core';
import {isNullOrUndefined} from "util";
import {environment} from "../../../../environments/environment";

declare var ActiveXObject: (type: string) => void;

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-xml-viewer',
  templateUrl: './xml-viewer.component.html',
  styleUrls: ['./xml-viewer.component.scss']
})


export class XmlViewerComponent implements OnInit {

  xhttp: any;
  xmlData: any;

  public readonly baseUrl = environment.base_url;

  constructor() {
  }

  ngOnInit() {
  }

  getParameters(data) {
    this.xmlData = data;
    var newDiv = document.createElement("div");
    newDiv.setAttribute("id", "xml");
    this.displayResult();
  }


  loadXMLDoc(filename) {

    if ((<any>window).ActiveXObject) {
      this.xhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } else {
      this.xhttp = new XMLHttpRequest();
    }
    this.xhttp.open("GET", filename, false);
    try {
      this.xhttp.responseType = "msxml-document"
    } catch (err) {
    } // Helping IE11
    this.xhttp.send("");
    return this.xhttp.responseXML;
  }

  displayResult() {
    var xml = this.loadXMLDoc(this.xmlData);
    const agent = window.navigator.userAgent.toLowerCase()
    switch (true) {
      case agent.indexOf('firefox') > -1:
        //PARA PROBAR LOCALMENTE REEMPLAZAR POR: var xsl = this.loadXMLDoc("../../../../assets/'nombreArchivoTransformacion'.xsl");
        var xsl = this.loadXMLDoc(this.baseUrl.concat("/assets/transformacionFirefox.xsl"));
        break;
      default:
        var xsl = this.loadXMLDoc(this.baseUrl.concat("/assets/transformacionColpen.xsl"));
        // var xsl = this.loadXMLDoc("../../../../assets/transformacionColpen.xsl");
        break;
    }

// code for IE
    if ((<any>window).ActiveXObject || this.xhttp.responseType === "msxml-document") {
      var ex = xml.transformNode(xsl);
      document.getElementById("xml").innerHTML = ex;
    }
// code for Chrome, Firefox, Opera, etc.
    else if (document.implementation && document.implementation.createDocument) {
      var xsltProcessor = new XSLTProcessor();
      xsltProcessor.importStylesheet(xsl);
      var resultDocument = xsltProcessor.transformToFragment(xml, document);
      var newDiv = document.createElement("div");
      newDiv.setAttribute("id", "xml");
      newDiv.appendChild(resultDocument);
      var div = document.getElementById("xml");
      if (isNullOrUndefined(div)){
        return false;
      } else {
        var parentDiv = div.parentNode;
        parentDiv.replaceChild(newDiv, div)
      }
    }
  }

  hideXML(){
    var d = document.getElementById("estructura");
    var d_nested = document.getElementById("xml");
    var throwawayNode = d.removeChild(d_nested);
    var newDiv = document.createElement("div");
    newDiv.setAttribute("id", "xml");
    newDiv.setAttribute("value", "");
    var div = document.getElementById("estructura");
    div.appendChild(newDiv);
  }

}




