import {Routes} from '@angular/router';
import {ROUTES_PATH} from './app.route-names';
import {LoginComponent} from './ui/page-components/login/login.component';
//Array of routes
export const HOME_ROUTES: Routes = [
  {
    path: 'bussiness',
    loadChildren: './ui/page-components/home/home.module#HomeModule'
  },
  {path: ROUTES_PATH.login, component: LoginComponent},
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: '**', redirectTo: '/login'}
];
