export interface SerieDTO {
  idSerie?: number
  codigoSerie: string;
  nombreSerie: string;
}
