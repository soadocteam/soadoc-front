import {ContactRequestDTO} from "./ContactRequestDTO";

export interface PersonRequestDTO {
  personId?: number
  name?: string
  identificationType?: string
  personType?: string
  identificationNumber?: string
  qualityCode: string
  contactList?: ContactRequestDTO[]
}
