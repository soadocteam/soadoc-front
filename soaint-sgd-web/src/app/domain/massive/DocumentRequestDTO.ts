export interface DocumentRequestDTO {
  documentType?: string
  documentDate?: string
  subject?: string
  foliosNumber?: number
  attachmentNumber?: number
  ecmId?: string
}
