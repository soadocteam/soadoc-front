export interface ContactRequestDTO {
  contactId?: number;
  city?: string;
  country?: string
  department?: string
  municipality?: string
  email?: string
  cellphone?: string
  phone?: string
  zipcode?: string
  address?: string
  contactType?: string
  creationDate?: Date
  IsPrincipal?: boolean
}
