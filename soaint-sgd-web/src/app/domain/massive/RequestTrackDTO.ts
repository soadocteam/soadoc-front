import {CorrespondeceRequestDTO} from "./CorrespondeceRequestDTO";
import {AgentRequestDTO} from "./AgentRequestDTO";
import {DocumentRequestDTO} from "./DocumentRequestDTO";
import {AttachmentsRequestDTO} from "./AttachmentsRequestDTO";
import {LocationType} from "../../shared/enums/enums";

export interface RequestTrackDTO {
  isSenderReferredRecipient?: boolean
  correspondence?: CorrespondeceRequestDTO
  listAgents?: AgentRequestDTO[]
  documentList?: DocumentRequestDTO[]
  attachmentList?: AttachmentsRequestDTO[]
  referencedList?: []

  templateId?: number
  signaturesJson?: string
  userSign?: string
  passSign?: string

  principalFileId?: any;
  nroRadicado?: any;
  codFuncRadica?: any;
  idTarea?: any;
  idInstanceProcess?: any;
  commonTagsAttributes?: { [key: string]: any };
  locationStamp?: LocationType;
}
