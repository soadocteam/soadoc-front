export  interface ResponseMassiveDetailDTO {
  successDetail: boolean;
  nroRad: string;
  dateRad: string;
  addresseeType: string;
  addresseeName: string;
  idEcmDocument: string;
  documentName: string;
}
