export interface MassiveTrackDTO {
  success: any;
  dateTrack: any;
  person: any;
  ecmId: any;
  idTarea: any;
  codFuncRadica: any;
  longTackId: string;
}
