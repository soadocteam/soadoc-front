import {ResponseMassiveDetailDTO} from "./ResponseMassiveDetailDTO";

export  interface ResponseStatusSummaryDTO {
  total: number;
  success: number;
  failed: number;
  massiveDetail: ResponseMassiveDetailDTO[];
}
