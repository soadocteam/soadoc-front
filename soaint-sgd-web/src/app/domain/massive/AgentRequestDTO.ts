import {PersonRequestDTO} from "./PersonRequestDTO";

export interface AgentRequestDTO {
  agentId?: number
  agentType?: string
  name?: string
  identificationNumber?: string
  personType?: string
  person?: PersonRequestDTO
}
