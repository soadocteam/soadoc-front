export interface CorrespondeceRequestDTO {
  description?: string
  responseTime?: number
  unitTime?: string
  receptionChannel?: string;
  dateRequest?: string;
  dateDocument?: string;
  documentType?: string;
  comunicationType?: string
  requestSubsection?: string
  requestOffice?: string
  physicDistribution?: boolean
  electronicDistribution?: boolean
  digitazionRequired?: boolean
  attachDocument?: boolean
  shippmentNumber?: string
  transporter?: string
  shippingClass?: string
  mailType?: string
}
