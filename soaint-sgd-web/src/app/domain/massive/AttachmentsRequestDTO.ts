export interface AttachmentsRequestDTO {
  attachmentCode: string
  description: string
  supportTypeCode: string
}
