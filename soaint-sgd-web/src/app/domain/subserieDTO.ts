export interface SubserieDTO {
  idSubSerie?: number;
  codigoSubSerie: string;
  nombreSubSerie: string;
}
