export interface ContactDTO {
  contactId?: string;
  cellphone?: string;
  city?: string;
  departmentCode?: string;
  municipalityCode?: string;
  countryCode?: string;
  zipCode?: string;
  quadrantPrefixCode?: string;
  roadTypeCode?: string;
  email?: string;
  address?: string;
  extension?: string;
  plateNumber?: string;
  generatingRoadNum?: string;
  principal?: any;
  provState?: string;
  landline?: string;
  contactType?: string;
  creationDate?: Date;
  state?: string;
  personId?: any;
}
