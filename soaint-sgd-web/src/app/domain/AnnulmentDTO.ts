export interface AnnulmentDTO {
  id?: any,
  trackId?: any,
  trackDate?: any,
  subject?: any,
  nroFolios?: any,
  nroAttachment?: any,
  sectionId?: any,
  subsectionId?: any,
  reqUser?: any,
  observations?: any
  annulmentUser?: any
  annulmentSubsectionCode?: any
}
