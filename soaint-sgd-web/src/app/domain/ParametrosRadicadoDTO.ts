export interface ParametrosRadicadoDTO {
  id?: number;
  field?: string;
  activo?: boolean;
  codigo?: string;
  nombre?: string;
  value?: any;
  options?: string[];
}
