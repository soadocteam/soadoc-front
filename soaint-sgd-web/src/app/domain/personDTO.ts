import {ContactDTO} from "./ContactDTO";

export interface PersonDTO {
  personId: any;
  name: any;
  idNumber: any;
  idDocTypeCode: any;
  personTypeCode: any;
  businessName: any;
  nit: any;
  qualityCode: any;
  contactList: Array<ContactDTO>;
}
