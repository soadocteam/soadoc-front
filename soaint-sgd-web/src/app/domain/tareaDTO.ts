type tareaStatus = 'RESERVADO' | 'ENPROGRESO' | 'LISTO' | 'CANCELADO' | 'COMPLETADO';

export interface TareaDTO {
  codigoDependencia?:any;
  idTarea?: string;
  nombre?: string;
  estado?: tareaStatus;
  prioridad?: number;
  idResponsable?: string;
  idCreador?: string;
  fechaCreada?: Date;
  tiempoActivacion?: Date;
  tiempoExpiracion?: Date;
  idProceso?: string;
  idInstanciaProceso?: string;
  idDespliegue?: string;
  idParent?: number;
  variables?: any;
  rol?:string;
  descripcion?:string;
  nombreProceso?:string;
}
