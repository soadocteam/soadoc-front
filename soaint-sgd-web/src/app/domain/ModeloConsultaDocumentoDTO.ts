import {DocumentoDTO} from "./documentoDTO";

export interface ModeloConsultaDocumentoDTO extends DocumentoDTO{
  idDocDb: number;
  destinatario: string;
  estado: string;
  funcionario: string;
  fechaVencimiento?: string;
  nroGuia: string;
  estadoEntrega: string;
  tipoCMC: string;
  tipologiaDocumental?: string;
  nroRadicado?: string;
  fechaRadicacion?: string;
  nombreRemitente?: string;
  fechaDVencimiento?: string;
  anulado?: string;
  origen?: string
  codFuncionario?: string;
  idUnidadDocumental?: string;
  nombreDocumento?: string;
}
