export interface VersionDTO {
  ideVersion: number;
  valVersion: string;
  fecVersion: Date;
  nombreComite: string;
  numActa: string;
  fechaActa: Date;
}
