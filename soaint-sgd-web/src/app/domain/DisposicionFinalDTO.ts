import {UnidadDocumentalDTO} from './unidadDocumentalDTO'

export interface DisposicionFinalDTO {
  tipificador?: number
  unidadDocumentalDTO?: UnidadDocumentalDTO;
  disposicionFinalList?: string[];
  noShowNoFoundNotification?: boolean;

}
