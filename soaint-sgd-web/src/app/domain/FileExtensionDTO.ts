export interface FileExtensionDTO {
  extension: string,
  active:boolean,
  mimetype:string,
  transformation:boolean
}
