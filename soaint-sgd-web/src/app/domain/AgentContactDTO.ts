export interface AgentContactDTO {
  agentContactId: any;
  cellphone: any;
  city: any;
  departmentCode: any;
  municipalityCode: any;
  countryCode: any;
  zipCode: any;
  quadrantPrefixCode: any;
  roadTypeCode: any;
  email: any;
  address: any;
  extension: any;
  plateNumber: any;
  generatingRoadNum: any;
  principal: any;
  provState: any;
  landline: any;
  contactType: any;

}
