import {DeliveryMassiveDTO} from "./deliveryMassiveDTO";

export interface DeliveriesMassiveDTO {
  file: any
  deliveryMassiveDTO?: DeliveryMassiveDTO[]
}
