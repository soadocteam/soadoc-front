import {PersonDTO} from "./personDTO";
import {ContactoDTO} from "./contactoDTO";
import {AgentDTO} from "./agentDTO";
import {AgentContactDTO} from "./AgentContactDTO";

export interface AgentPersonDTO extends AgentDTO{
  personDTO?: PersonDTO;
  agentContactDTO?: AgentContactDTO;
}
