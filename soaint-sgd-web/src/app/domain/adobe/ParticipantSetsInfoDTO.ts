import {MemberInfosDTO} from "./MemberInfosDTO";

export class ParticipantSetsInfoDTO {
  memberInfos: MemberInfosDTO[] = [];
  order: number;
  role: string;
}
