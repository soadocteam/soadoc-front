import {ParticipantSetsInfoDTO} from "./ParticipantSetsInfoDTO";

export class AdobeDocumentDTO {
  participantSetsInfo: ParticipantSetsInfoDTO[] = [];
  nameDocument: string;
  document: any;
}

export interface AdobeDocumentMassiveDTO {
  documents: AdobeDocumentIdDTO[];
}

export interface AdobeDocumentIdDTO  {
  id: string;
  name: string;
  mimeType: string;
  numPages: string;
  createdDate: string;
  idEcm: string;
  nomHeadquarters: string;
  codDependence: string;
  dependence: string;
  completeTaskDTO: string;
}

