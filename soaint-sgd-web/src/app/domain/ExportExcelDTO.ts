
export class ExportExcelDTO {
   tipoDocumental?: string;
   numeroRadicado?: string;
   fechaRadicado?: string;
   remitente?: string;
   destinatario?: string;
   anulado?: string;
   origen?: string;
   usuarioRadicador?: string;
   numeroGuia?: string;
   asunto?: string;
   funcionario?: string;
   fechaVencimiento?: string;
   nombreDocumento?: string;
   autor?: string;
   idExpediente?: string;

}
