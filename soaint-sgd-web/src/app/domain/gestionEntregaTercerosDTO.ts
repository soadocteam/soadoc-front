export interface GestionEntregaTercerosDTO {
  nombre?: string;
  fechaEntrega?: string;
  estadoEntrega?: string;
  observacion?: string;
}
