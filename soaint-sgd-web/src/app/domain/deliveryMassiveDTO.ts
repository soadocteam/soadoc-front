import {DeliveryFormDTO} from "./deliveryFormDTO";

export interface DeliveryMassiveDTO {
  deliveryMassiveId?: any
  massiveId?: any
  rowId?: any
  massiveDate?: any
  status?: any
  statusDescription?: any
  agentFormId?: any
  deliveryFormDTO?: DeliveryFormDTO
}
