import {ContactoDTO} from './contactoDTO';
import {ReferidoDTO} from './referidoDTO';
import {AnexoDTO} from './anexoDTO';
import {DocumentoDTO} from './documentoDTO';
import {CorrespondenciaDTO} from './correspondenciaDTO';
import { FuncionarioDTO } from './funcionarioDTO';
import {AgentPersonDTO} from "./agentPersonDTO";


export interface  ComunicacionOficialDTO {
  esRemitenteReferidoDestinatario?: boolean,
  correspondencia?: CorrespondenciaDTO,
  agenteList?: Array<AgentPersonDTO>,
  ppdDocumentoList?: Array<DocumentoDTO>,
  anexoList?: Array<AnexoDTO>,
  referidoList?: Array<ReferidoDTO>,
  datosContactoList?: Array<ContactoDTO>,
  //
  funcionario?: FuncionarioDTO
}
