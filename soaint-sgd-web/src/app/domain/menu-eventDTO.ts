import {MenuItem} from "primeng/api";
import {AppSubMenuComponent} from "../ui/layout-components/presentation/menu/menu.component";

export interface MenuEventDTO {
  originalEvent: Event,
  item: MenuItem,
  from: AppSubMenuComponent
}
