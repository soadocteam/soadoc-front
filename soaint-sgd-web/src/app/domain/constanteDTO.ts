export interface ConstanteDTO {
  id?: number;
  codigo?: string;
  codPadre?: string;
  nombre: string;
  estado?:string;
  ideSede?: any;
  codSede?: string;
  nomSede?: string;
}
