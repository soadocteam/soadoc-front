export interface TemplatesDTO {
  footer?: any,
  templateType?: string,
  template?: any,
  image?: string,
  templateId?: number,
  state?: boolean,
  comunicationType?: string,
  map?: any
}
