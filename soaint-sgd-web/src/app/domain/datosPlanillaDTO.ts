export interface DatosPlanillaDTO {
  formNumber?: number;
  creationDate?: any;
  type?: string;
  subsection?: string;
  formProperties?: any[];
  file?: any;
}
