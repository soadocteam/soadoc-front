export interface DeliveryFormDTO {
  trackId?: any;
  agentId?: any;
  courier?: any;
  shipmentNumber?: any;
  weight?: any;
  shippingValue?: any;
  deliveryStatus?: any;
  devolutionCode?: any;
  deliveryDate?: any;
  observations?: any;
  accepted?: any;
  documentId?: any;
  receiverName?: any;
}
