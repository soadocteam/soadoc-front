export interface IndiceElectronicoDTO {
  fechaInicial: Date;
  fechaFinal: Date;
  codigoSerie: string;
  codSubSerie: string;
  codigoDependencia: string;
  estado: string;
  objectStore: string;
  idExpediente: string;

}
