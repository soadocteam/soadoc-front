export interface FileValidationDto {
  [fileName: string]: {
    message: string,
  }
}
