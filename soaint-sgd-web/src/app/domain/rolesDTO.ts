export interface RolDTO {
    id?: number,
    rol: string,
    nombre?: string,
    applicationId?:string
}
